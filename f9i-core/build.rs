#![feature(int_roundings)]
#![feature(extend_one)]

use std::convert::TryInto;
use std::env;
use std::error::Error;
use std::fmt::Debug;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

use proc_macro2::{Ident, Span, TokenStream};
use quote::{format_ident, quote, IdentFragment};
use rust_format::{Formatter, RustFmt};
use syn::Index;

// The bag of syntactic items for use in the generated code.
struct Ctx {
    tile_component_ids: [Ident; 4],
    get_tile_component_ids: [Ident; 4],
    coda_id: Ident,
    tiles_len: Ident,
    prev_tiles: Ident,
}

impl Ctx {
    pub fn new() -> Self {
        Self {
            tile_component_ids: [
                Ident::new("Initial", Span::call_site()),
                Ident::new("Glide", Span::call_site()),
                Ident::new("Vowel", Span::call_site()),
                Ident::new("SimpleCoda", Span::call_site()),
            ],
            get_tile_component_ids: [
                Ident::new("get_initial", Span::call_site()),
                Ident::new("get_glide", Span::call_site()),
                Ident::new("get_vowel", Span::call_site()),
                Ident::new("get_simple_coda", Span::call_site()),
            ],
            coda_id: Ident::new("Coda", Span::call_site()),
            tiles_len: Ident::new("tiles_len", Span::call_site()),
            prev_tiles: Ident::new("prev_tiles", Span::call_site()),
        }
    }

    // The components of each tile.
    fn component(&self, i: usize) -> &Ident {
        &self.tile_component_ids[i % 4]
    }
}

// A boundary location.
// For simplicity, this includes only the repeating types,
// and not the terminal end type.
// These locations are presented in a cyclic order.
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
#[repr(u8)]
enum Pos {
    Syllabic,
    Glide,
    Onset,
    Nuclear,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum EPos {
    Terminal,
    Nonterminal(Pos),
}

impl Pos {
    fn next(self) -> Self {
        match self {
            Syllabic => Glide,
            Glide => Onset,
            Onset => Nuclear,
            Nuclear => Syllabic,
        }
    }

    fn prev(self) -> Self {
        match self {
            Syllabic => Nuclear,
            Glide => Syllabic,
            Onset => Glide,
            Nuclear => Onset,
        }
    }
}

impl IdentFragment for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl IdentFragment for EPos {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Terminal => f.write_str("Terminal"),
            Nonterminal(p) => IdentFragment::fmt(p, f),
        }
    }
}

impl EPos {
    fn closest_pos(self) -> Pos {
        match self {
            Nonterminal(p) => p,
            Terminal => Nuclear, // followed by a complex coda
        }
    }
}

use EPos::*;
use Pos::*;

#[derive(Clone, Copy, Debug)]
struct JunctureRuleInput<'a> {
    mandatory: &'a [TokenStream],
    optional: &'a [TokenStream],
}

#[derive(Clone, Copy)]
struct MedialJunctureRule {
    start: Pos,
    len: usize,
    opt_capture: usize,
    subst: fn(JunctureRuleInput) -> TokenStream,
}

impl MedialJunctureRule {
    // b: start of each tile in LHS; 0 <= b < 4
    // j: start of each tile in RHS; b <= j < b + 4
    // i.e. j - b is the number of elements in LHS’s remainder.
    // z: end of the remainder in RHS; j <= z < j + 4
    fn apply(&self, b: usize, j: usize, z: usize, ctx: &Ctx, ts: &mut TokenStream) {
        let prev_tiles = &ctx.prev_tiles;
        let tiles_len = &ctx.tiles_len;
        // A lot easier to work with signed arithmetic here.
        let s = self.start as isize;
        let e = s + (self.len as isize);
        let opt_capture = self.opt_capture as isize;
        let b = b as isize;
        let j = j as isize;
        let z = (z % 4) as isize;
        let z = if z < b { z + 4 } else { z };
        /*
        We use 0 for the last syllabic boundary before the start of the remainder. This position might lie before the start of the final assemblage.

        The start of the first tile is then at position b.
        The juncture is positioned at j + 4 * prev_tiles.
        The end of the last tile is at b + 4 * total_tiles.
        The end of the remainder is at z + 4 * total_tiles.

        We shift s and e by multiples of 4 to find positions that capture the juncture while staying within the assemblage. That is, the following must hold:

        * s′ < j + 4 * prev_tiles < e′
        * s′ >= b
        * e′ <= z + 4 * total_tiles

        Letting s′ = s + 4 * k and e′ = e + 4 * k, we get:

        s + 4k < j IFF 4k < j + 4 * prev_tiles - s
        e + 4k > j IFF 4k > j + 4 * prev_tiles - e
        s + 4k >= b IFF 4k >= b - s
        e + 4k <= z + 4 * total_tiles IFF 4k <= (z + 4 * total_tiles - e)

        If we let k = k′ + prev_tiles, then these constraints can be rewritten as:

        4k′ < j - s
        4k′ > j - e
        4 * prev_tiles >= b - s - 4k′
        4 * prev_tiles <= z + 4 * (total_tiles - k′) - e = 4 * total_tiles + (z - 4 * k′ - e)
        */
        let kplower = (j - e).div_floor(4) + 1;
        let kpupper = (j - s - 1).div_floor(4);
        let mut v = Vec::with_capacity(self.len);
        for kp in kplower..=kpupper {
            let min_4_prev_tiles = b - s - 4 * kp;
            let min_4_prev_tiles = min_4_prev_tiles.max(0) as usize;
            let max_4_prev_tiles_x = z - 4 * kp - e;
            v.clear();
            for i in (s - opt_capture)..e {
                let is_opt_capture = i < s;
                let rw_tiles_4_lb = 4 * kp + i - b;
                let rw_tiles_4_lb_quot = rw_tiles_4_lb.div_euclid(4);
                let rw_tiles_4_lb_rem = rw_tiles_4_lb.rem_euclid(4);
                let tile_x = {
                    let rw_tiles_4_lb_rem = ti(rw_tiles_4_lb_rem.try_into().unwrap());
                    // Number of elements from the start of the first tile = i + 4 * k - b
                    // = (4 * k′ + i - b) + 4 * prev_tiles
                    if is_opt_capture {
                        quote! {
                            {
                                let tile = output.tiles.get_mut(((#prev_tiles as isize) + #rw_tiles_4_lb_quot) as usize);
                                match tile {
                                    None => ptr::null_mut(),
                                    Some(t) => std::ptr::addr_of_mut!(t.#rw_tiles_4_lb_rem),
                                }
                            }
                        }
                    } else {
                        quote! {
                            std::ptr::addr_of_mut!(output.tiles[((#prev_tiles as isize) + #rw_tiles_4_lb_quot) as usize].#rw_tiles_4_lb_rem)
                        }
                    }
                };
                let rem_x = if rw_tiles_4_lb_rem >= (z - b) % 4 {
                    if is_opt_capture {
                        quote!(ptr::null_mut())
                    } else {
                        quote!(unreachable!())
                    }
                } else {
                    // Lies in the remainder, at element i + 4 * k - b - 4 * total_tiles
                    // (4 * k′ + i - b) + 4 * (prev_tiles - total_tiles)
                    let rw_tiles_4_lb_rem = ti(rw_tiles_4_lb_rem.try_into().unwrap());
                    quote!(std::ptr::addr_of_mut!(output.remainder.#rw_tiles_4_lb_rem))
                };
                // TODO: can we avoid generating a branch by
                // temporarily pushing the remainder as an additional tile?
                // (Not compatible with complex codas, but we’re not generating code for those anyway)
                let elem = quote! {
                    // Does this element lie in a tile? That is, does the following hold?
                    // i + 4 * k < b + 4 * total_tiles
                    // IFF 4 * (k′ + prev_tiles) + i - b < 4 * total_tiles
                    // IFF 4 * k′ + i - b < 4 * (total_tiles - prev_tiles)
                    if 4 * (#tiles_len - #prev_tiles) as isize > #rw_tiles_4_lb {
                        #tile_x
                    } else {
                        #rem_x
                    }
                };
                v.push(elem);
            }
            let ids = (0..v.len())
                .map(|i| format_ident!("_i{}", i))
                .collect::<Vec<_>>();

            let opt_capture_ids = &ids[..self.opt_capture];
            let main_ids = &ids[self.opt_capture..];

            let call = (self.subst)(JunctureRuleInput {
                mandatory: &main_ids.iter().map(|id| quote!(*#id)).collect::<Vec<_>>(),
                optional: &opt_capture_ids
                    .iter()
                    .map(|id| quote!(#id))
                    .collect::<Vec<_>>(),
            });

            ts.extend(quote! {
                if 4 * prev_tiles >= #min_4_prev_tiles && (#max_4_prev_tiles_x + (4 * #tiles_len as isize)) >= 0 && 4 * prev_tiles <= (#max_4_prev_tiles_x + (4 * #tiles_len as isize)) as usize {
                    #(let #ids = #v;)*
                    // SAFETY: all _i<n>s are valid and point to different locations
                    #(let #opt_capture_ids = unsafe { #opt_capture_ids.as_mut() };)*
                    #(let #main_ids = unsafe { &mut *#main_ids };)*
                    #call
                }
            });
        }
    }
}

#[derive(Clone, Copy)]
struct TerminalJunctureRule {
    len: usize,
    opt_capture: usize,
    subst: fn(JunctureRuleInput) -> TokenStream,
}

impl TerminalJunctureRule {
    fn apply(&self, b: usize, j: usize, ctx: &Ctx, ts: &mut TokenStream) {
        let prev_tiles = &ctx.prev_tiles;
        let tiles_len = &ctx.tiles_len;

        let len = self.len;
        let opt_capture = self.opt_capture;
        let remainder_len = 4 - b;

        /*
        We use 0 for the last syllabic boundary before the start of the remainder. This position might lie before the start of the final assemblage.

        The start of the first tile is then at position b.
        The juncture is positioned at j + 4 * prev_tiles.
        The end of the last tile is at b + 4 * total_tiles.
        The end of the remainder is at z + 4 * total_tiles.

        In this case, we want the following to hold:

        * total number of components >= len
        * juncture occurs within (but not on the edge) of the last len components

        The second condition is equivalent to

        z + 4 * total_tiles - len < j + 4 * prev_tiles < z + 4 * total_tiles
        */

        let opts: Vec<_> = (0..opt_capture)
            .map(|i| {
                // Does this component lie in the remainder?
                if i + remainder_len >= len + opt_capture {
                    let idx = ti(i - opt_capture - (len - remainder_len));
                    quote!(ptr::addr_of_mut!(output.remainder.#idx))
                } else {
                    let comp_offset = (len - remainder_len) - i + opt_capture;
                    let offset = (comp_offset + 3) / 4;
                    let idx = ti(4 * offset - comp_offset);
                    quote! {
                        {
                            let tile = output.tiles.get_mut(#tiles_len.wrapping_sub(#offset));
                            match tile {
                                None => ptr::null_mut(),
                                Some(t) => ptr::addr_of_mut!(t.#idx),
                            }
                        }
                    }
                }
            })
            .collect();
        let v: Vec<_> = (0..len)
            .map(|i| {
                // Does this component lie in the remainder?
                if i + remainder_len >= len {
                    let idx = ti(i - (len - remainder_len));
                    quote!(ptr::addr_of_mut!(output.remainder.#idx))
                } else {
                    let comp_offset = (len - remainder_len) - i;
                    let offset = (comp_offset + 3) / 4;
                    let idx = ti(4 * offset - comp_offset);
                    quote!(ptr::addr_of_mut!(output.tiles[#tiles_len - #offset].#idx))
                }
            })
            .collect();
        let opt_ids = (0..opts.len())
            .map(|i| format_ident!("_o{}", i))
            .collect::<Vec<_>>();
        let ids = (0..v.len())
            .map(|i| format_ident!("_i{}", i))
            .collect::<Vec<_>>();
        let call = (self.subst)(JunctureRuleInput {
            mandatory: &ids.iter().map(|id| quote!(*#id)).collect::<Vec<_>>(),
            optional: &opt_ids.iter().map(|id| quote!(#id)).collect::<Vec<_>>(),
        });

        ts.extend(quote! {
            let total_comps = 4 * #tiles_len + #remainder_len;
            let jj = #j + 4 * #prev_tiles;
            if total_comps >= #len && jj > total_comps - #len && jj < total_comps {
                #(let #ids = #v;)*
                #(let #opt_ids = #opts;)*
                // SAFETY: all _i<n>s point to different locations
                #(let #ids = unsafe { &mut *#ids };)*
                #(let #opt_ids = unsafe { #opt_ids.as_mut() };)*
                #call
            }
        });
    }
}

#[derive(Clone, Copy)]
enum JunctureRule {
    Medial(MedialJunctureRule),
    Terminal(TerminalJunctureRule),
}

impl JunctureRule {
    fn apply(&self, b: usize, j: usize, z: usize, terminal: bool, ctx: &Ctx, ts: &mut TokenStream) {
        match self {
            JunctureRule::Medial(m) => m.apply(b, j, z, ctx, ts),
            JunctureRule::Terminal(t) => {
                if terminal {
                    t.apply(b, j, ctx, ts)
                }
            }
        }
    }
}

const REPAIR_J_IU: MedialJunctureRule = MedialJunctureRule {
    start: Glide,
    len: 2,
    opt_capture: 0,
    subst: |sl| {
        let glide = &sl.mandatory[0];
        let vowel = &sl.mandatory[1];
        quote! {
            if #glide == Glide::J && matches!(#vowel, Vowel::I | Vowel::IHat | Vowel::U) {
                #glide = Glide::None;
            }
        }
    },
};

const REPAIR_DEDUP: MedialJunctureRule = MedialJunctureRule {
    start: Syllabic,
    len: 5,
    opt_capture: 1,
    subst: |sl| {
        let prev_coda = &sl.optional[0];
        let onset = &sl.mandatory[0];
        let glide = &sl.mandatory[1];
        let vowel = &sl.mandatory[2];
        let coda = &sl.mandatory[3];
        let onset2 = &sl.mandatory[4];
        quote! {
            #onset = do_dedup(#prev_coda, #onset, #glide, #vowel, #coda, #onset2);
        }
    },
};

const REPAIR_BRIDGE: MedialJunctureRule = MedialJunctureRule {
    start: Glide,
    len: 4,
    opt_capture: 0,
    subst: |sl| {
        let glide = &sl.mandatory[0];
        let vowel = &sl.mandatory[1];
        let coda = &sl.mandatory[2];
        let onset = &sl.mandatory[3];
        quote! {
            (#glide, #vowel, #coda, #onset) = resolve_bridge_with_prev_vowel(#glide, #vowel, #coda, #onset)
        }
    },
};

const REPAIR_DEDUP_TERMINAL: TerminalJunctureRule = TerminalJunctureRule {
    len: 4,
    opt_capture: 1,
    subst: |sl| {
        let prev_coda = &sl.optional[0];
        let onset = &sl.mandatory[0];
        let glide = &sl.mandatory[1];
        let vowel = &sl.mandatory[2];
        let coda = &sl.mandatory[3];
        quote! {
            #onset = do_dedup_fin(#prev_coda, #onset, #glide, #vowel, #coda);
        }
    },
};

const REPAIRS: &[JunctureRule] = &[
    JunctureRule::Medial(REPAIR_J_IU),
    JunctureRule::Medial(REPAIR_DEDUP),
    JunctureRule::Terminal(REPAIR_DEDUP_TERMINAL),
    JunctureRule::Medial(REPAIR_BRIDGE),
];

fn ti(i: usize) -> Index {
    Index {
        index: i as u32,
        span: Span::call_site(),
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Se {
    start: Pos,
    end: EPos,
}

impl Se {
    fn buf_name(&self) -> Ident {
        format_ident!("{}To{}", self.start, self.end)
    }
    fn ref_name(&self) -> Ident {
        format_ident!("{}To{}Ref", self.start, self.end)
    }
    fn struct_def(&self, ctx: &Ctx) -> TokenStream {
        let buf_name = self.buf_name();
        let ref_name = self.ref_name();

        let s = self.start as usize;
        let e = self.end.closest_pos() as usize;
        let e = if e < s { e + 4 } else { e };

        let tilecmp = [
            ctx.component(s),
            ctx.component(s + 1),
            ctx.component(s + 2),
            ctx.component(s + 3),
        ];
        let tile_type = quote!((#(#tilecmp),*));
        // this will have no more than 4 entries
        let mut remcmp = (s..e).map(|i| ctx.component(i)).collect::<Vec<_>>();
        if self.end == Terminal {
            remcmp.push(&ctx.coda_id);
        }

        let mutation_impls = (self.start == Syllabic).then(|| {
            let rest1 = (1..4).map(ti);
            let lenite_remainder_0 = if s == e {
                quote! {
                    panic!("cannot lenite an empty assemblage")
                }
            } else {
                quote! {
                    self.remainder.0.lenite(lt)
                }
            };
            let eclipse_remainder_0 = if s == e {
                quote! {
                    panic!("cannot eclipse an empty assemblage")
                }
            } else if e == s + 1 {
                quote! {
                    self.remainder.0.eclipse(_next)
                }
            } else {
                let rest2 = (1..(e - s)).map(ti);
                quote! {
                    self.remainder.0.eclipse((#(self.remainder.#rest2,)*).peek().or(_next))
                }
            };
            quote! {
                impl Lenite for #buf_name {
                    fn lenite(&mut self, lt: LenitionType) {
                        match self.tiles.first_mut() {
                            Some(tile) => tile.0.lenite(lt),
                            None => #lenite_remainder_0,
                        }
                    }
                }
                impl Eclipse for #buf_name {
                    fn eclipse(&mut self, _next: Option<Mgc>) {
                        match self.tiles.first_mut() {
                            Some(tile) => tile.0.eclipse((#(tile.#rest1,)*).peek()), // ← guaranteed to be Some because of the vowel
                            None => #eclipse_remainder_0,
                        }
                    }
                }
            }
        });

        let defo = (s == e && !matches!(self.end, Terminal)).then(|| quote!(, Default));

        let add_impl = match self.end {
            Terminal => None,
            Nonterminal(end) => Some({
                let rhs_type = &ctx.tile_component_ids[end as usize];
                let rhs_type_x = (Self {
                    start: end,
                    end: Nonterminal(end.next()),
                })
                .ref_name();
                let output_type = (Self {
                    start: self.start,
                    end: Nonterminal(end.next()),
                })
                .buf_name();
                quote! {
                    impl Add<#rhs_type> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn add(self, rhs: #rhs_type) -> #output_type {
                            self + #rhs_type_x {
                                    tiles: &[],
                                    remainder: (rhs,)
                            }
                        }
                    }
                }
            }),
        };

        let mul_impl = match self.end {
            Terminal => None,
            Nonterminal(end) => Some({
                let rhs_type = &ctx.tile_component_ids[end as usize];
                // let rhs_type_x = (Self {
                //     start: end,
                //     end: Nonterminal(end.next()),
                // })
                // .ref_name();
                let output_type = (Self {
                    start: self.start,
                    end: Nonterminal(end.next()),
                })
                .buf_name();
                let action = if e == s + 3 {
                    quote! {
                        let mut last_tile = (remainder.0, remainder.1, remainder.2, rhs);
                        let remainder = ();
                        let ll = tiles.len();
                        tiles.push(last_tile);
                    }
                } else {
                    let s2j_part = (s..e).map(|i| {
                        let tuple_idx = ti(i - s);
                        quote! {
                            remainder.#tuple_idx
                        }
                    });
                    quote! {
                        let mut remainder = (#(#s2j_part,)* rhs,);
                        let ll = tiles.len();
                    }
                };
                quote! {
                    impl Mul<#rhs_type> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn mul(self, rhs: #rhs_type) -> #output_type {
                            // self * #rhs_type_x {
                            //         tiles: &[],
                            //         remainder: (rhs,)
                            // }
                            let Self { mut tiles, remainder } = self;
                            #action

                            #output_type {
                                tiles,
                                remainder
                            }
                        }
                    }
                }
            }),
        };

        let add_to_onset_impl = match self.end {
            Nonterminal(Syllabic) => Some({
                let rhs_type_x = (Self {
                    start: Syllabic,
                    end: Nonterminal(Onset),
                })
                .ref_name();
                let output_type = (Self {
                    start: self.start,
                    end: Nonterminal(Onset),
                })
                .buf_name();
                quote! {
                    impl Add<Onset> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn add(self, rhs: Onset) -> #output_type {
                            self + #rhs_type_x::from(rhs)
                        }
                    }
                    impl Mul<Onset> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn mul(self, rhs: Onset) -> #output_type {
                            self * #rhs_type_x::from(rhs)
                        }
                    }
                }
            }),
            _ => None,
        };

        let add_to_nucleus_impl = match self.end {
            Nonterminal(Glide | Onset) => Some({
                let rhs_type_x = (Self {
                    start: Glide,
                    end: Nonterminal(Nuclear),
                })
                .ref_name();
                let output_type = (Self {
                    start: self.start,
                    end: Nonterminal(Nuclear),
                })
                .buf_name();
                quote! {
                    impl Add<Nucleus> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn add(self, rhs: Nucleus) -> #output_type {
                            self + #rhs_type_x::from(rhs)
                        }
                    }
                }
            }),
            _ => None,
        };

        let add_to_ccoda_impl = match self.end {
            Nonterminal(Nuclear) => Some({
                let rhs_type_x = (Self {
                    start: Nuclear,
                    end: Terminal,
                })
                .ref_name();
                let output_type = (Self {
                    start: self.start,
                    end: Terminal,
                })
                .buf_name();
                quote! {
                    impl Add<Coda> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn add(self, rhs: Coda) -> #output_type {
                            self + #rhs_type_x::from(rhs)
                        }
                    }
                    impl Mul<Coda> for #buf_name {
                        type Output = #output_type;
                        #[allow(unused_mut, unused_variables, unused_comparisons)]
                        fn mul(self, rhs: Coda) -> #output_type {
                            self * #rhs_type_x::from(rhs)
                        }
                    }
                }
            }),
            _ => None,
        };

        let pop_method = {
            let popped_type = match self.end {
                Nonterminal(_) => &ctx.tile_component_ids[(e + 3) % 4],
                Terminal => &ctx.coda_id,
            };
            let rte = match self.end {
                Nonterminal(c) => c.prev(),
                Terminal => Nuclear,
            };
            let remaining_type = (Self {
                start: self.start,
                end: Nonterminal(rte),
            })
            .buf_name();
            let em1 = rte as usize;
            let em1 = if em1 < s { em1 + 4 } else { em1 };
            let return_type;
            let implementation;
            let peel_return_type;
            let peel_impl;
            let peelx;
            if self.end == Nonterminal(self.start) {
                return_type = quote!(Option<(#remaining_type, #popped_type)>);
                implementation = quote! {
                    if let Some(last_tile) = self.tiles.pop() {
                        Some((
                            #remaining_type {
                                tiles: self.tiles,
                                remainder: (last_tile.0, last_tile.1, last_tile.2)
                            },
                            last_tile.3,
                        ))
                    } else {
                        None
                    }
                };
                peel_return_type = quote!(Option<#remaining_type>);
                peel_impl = quote! {
                    self.pop().map(|a| a.0)
                };
                peelx = quote! {
                    #[inline]
                    pub fn peelx(self) -> #remaining_type {
                        self.peel().unwrap()
                    }
                }
            } else {
                return_type = quote!((#remaining_type, #popped_type));
                let nonfinal = (s..em1).map(|i| {
                    let tuple_idx = ti(i - s);
                    quote! {
                        self.remainder.#tuple_idx
                    }
                });
                let fin = ti(em1 - s);
                implementation = quote! {
                    (
                        #remaining_type {
                            tiles: self.tiles,
                            remainder: (#(#nonfinal,)*)
                        },
                        self.remainder.#fin
                    )
                };
                peel_return_type = quote!(#remaining_type);
                peel_impl = quote! {
                    self.pop().0
                };
                peelx = quote!();
            }
            quote! {
                impl #buf_name {
                    #[inline]
                    #[allow(unused_mut)]
                    pub fn pop(mut self) -> #return_type {
                        #implementation
                    }
                    #[inline]
                    pub fn peel(self) -> #peel_return_type {
                        #peel_impl
                    }
                    #peelx
                }
            }
        };

        let get_methods = {
            (0..4).map(|k| {
                let remainder_len = e - s;
                let idx_in_tile = (4 + k - s) % 4;
                let idx = ti(idx_in_tile);
                let else_clause = (idx_in_tile < remainder_len).then(|| {
                    quote! {
                        else if i == self.tiles.len() { Some(self.remainder.#idx) }
                    }
                });
                let else_clause_mut = (idx_in_tile < remainder_len).then(|| {
                    quote! {
                        else if i == self.tiles.len() { Some(&mut self.remainder.#idx) }
                    }
                });
                let method_id = &ctx.get_tile_component_ids[k];
                let method_id_mut = format_ident!("{}_mut", method_id);
                let component_id = &ctx.tile_component_ids[k];
                quote! {
                    impl #ref_name<'_> {
                        pub fn #method_id(&self, i: usize) -> Option<#component_id> {
                            if i < self.tiles.len() {
                                Some(self.tiles[i].#idx)
                            } #else_clause
                            else { None }
                        }
                    }
                    impl #buf_name {
                        pub fn #method_id_mut(&mut self, i: usize) -> Option<&mut #component_id> {
                            if i < self.tiles.len() {
                                Some(&mut self.tiles[i].#idx)
                            } #else_clause_mut
                            else { None }
                        }
                    }
                }
            })
        };

        quote! {

            #[derive(Clone, Eq, PartialEq, Debug #defo)]
            pub struct #buf_name {
                pub tiles: SmallVec<[(#(#tilecmp),*); 3]>,
                pub remainder: (#(#remcmp,)*),
            }
            #[derive(Clone, Copy, Eq, PartialEq, Debug, SelfRustTokenize #defo)]
            pub struct #ref_name<'a> {
                pub remainder: (#(#remcmp,)*),
                pub tiles: &'a [#tile_type],
            }

            // Slicing conversions
            impl<'a> From<&'a #buf_name> for #ref_name<'a> {
                fn from(buf: &'a #buf_name) -> Self {
                    #ref_name {
                        tiles: &buf.tiles,
                        remainder: buf.remainder,
                    }
                }
            }
            impl<'a> From<#ref_name<'a>> for #buf_name {
                fn from(rf: #ref_name<'a>) -> Self {
                    #buf_name {
                        tiles: From::from(rf.tiles),
                        remainder: rf.remainder,
                    }
                }
            }
            impl #buf_name {
                pub fn view<'a>(&'a self) -> #ref_name<'a> {
                    self.into()
                }
                pub fn last_tile(&self) -> &#tile_type {
                    self.tiles.last().unwrap()
                }
                pub fn last_tile_mut(&mut self) -> &mut #tile_type {
                    self.tiles.last_mut().unwrap()
                }
            }

            impl<'a> Tokenize for #ref_name<'a> {
                fn append_to_tokens(&self, s: &mut Fragment) {
                    for tile in self.tiles {
                        tile.append_to_tokens(s);
                    }
                    self.remainder.append_to_tokens(s);
                }
            }
            impl Tokenize for #buf_name {
                fn append_to_tokens(&self, s: &mut Fragment) {
                    #ref_name::from(self).append_to_tokens(s);
                }
            }

            // TODO: what to do with invalid assemblages?
            impl Parse for #buf_name {
                fn parse(letters: &[Mgc]) -> Result<Self, SyllabifyError> {
                    // haha type inference go brrr
                    let (mut letters, remainder) = TryExtract::try_extract(letters)?;
                    let mut tiles = SmallVec::new();
                    while !letters.is_empty() {
                        let tile;
                        (letters, tile) = TryExtract::try_extract(letters)?;
                        tiles.push(tile);
                    }
                    tiles.reverse();
                    Ok(Self { tiles, remainder })
                }
            }

            #mutation_impls
            #add_impl
            #mul_impl
            #add_to_onset_impl
            #add_to_ccoda_impl
            #add_to_nucleus_impl
            #pop_method
            #(#get_methods)*

        }
    }

    fn append_def(&self, other: &Self, ctx: &Ctx) -> TokenStream {
        assert!(self.end == Nonterminal(other.start));
        let self_buf_name = self.buf_name();
        let other_ref_name = other.ref_name();
        let other_buf_name = other.buf_name();
        let output = Self {
            start: self.start,
            end: other.end,
        };
        let output_buf_name = output.buf_name();

        let s = self.start as usize;
        let j = other.start as usize;
        let j = if j < s { j + 4 } else { j };
        let mut e = other.end.closest_pos() as usize;
        while e < j {
            e += 4
        }
        let e = e;
        let k = (other.end == Terminal) as usize;

        // merge s..j remainder with part of j..j tile
        let tile_merge_expr = {
            let s2j_part = (s..j).map(|i| {
                let tuple_idx = ti(i - s);
                quote! {
                    remainder.#tuple_idx
                }
            });
            let j2s_part = (j..(s + 4)).map(|i| {
                let tuple_idx = ti(i - j);
                quote! {
                    t.#tuple_idx
                }
            });
            quote! { (#(#s2j_part,)* #(#j2s_part,)*) }
        };
        // get the s..j part from the tile to use for the next one
        let remainder_update_expr = {
            let s2j_part = (s..j).map(|i| {
                let tuple_idx = ti(i + 4 - j);
                quote! {
                    t.#tuple_idx
                }
            });
            quote! { (#(#s2j_part,)*) }
        };
        let last_tile_merge_stmt = {
            if e >= s + 4 {
                // enough material for an extra tile (s..s)
                let s2j_part = (s..j).map(|i| {
                    let tuple_idx = ti(i - s);
                    quote! {
                        remainder.#tuple_idx
                    }
                });
                let j2s_part = (j..(s + 4)).map(|i| {
                    let tuple_idx = ti(i - j);
                    quote! {
                        rhs.remainder.#tuple_idx
                    }
                });
                let s2e_part = ((s + 4)..(e + k)).map(|i| {
                    let tuple_idx = ti(i - j);
                    quote! {
                        rhs.remainder.#tuple_idx
                    }
                });
                quote! {
                    let mut last_tile = (#(#s2j_part,)* #(#j2s_part,)*);
                    tiles.push(last_tile);
                    let mut remainder = (#(#s2e_part,)*);
                }
            } else {
                let s2j_part = (s..j).map(|i| {
                    let tuple_idx = ti(i - s);
                    quote! {
                        remainder.#tuple_idx
                    }
                });
                let j2e_part = (j..(e + k)).map(|i| {
                    let tuple_idx = ti(i - j);
                    quote! {
                        rhs.remainder.#tuple_idx
                    }
                });
                quote! {
                    let mut remainder = (#(#s2j_part,)* #(#j2e_part,)*);
                }
            }
        };
        let post_merge = {
            let mut ts = TokenStream::new();
            for repair in REPAIRS {
                repair.apply(s, j, e, other.end == EPos::Terminal, ctx, &mut ts);
            }
            ts
        };

        let glide_fix = {
            quote! {
                let next_vowel = output.view().get_vowel(prev_tiles);
                if !matches!(next_vowel, Some(Vowel::I | Vowel::IHat | Vowel::U)) {
                    if let Some(g) = output.get_glide_mut(prev_tiles) {
                        *g = Glide::J;
                    }
                }
            }
        };

        let append = format_ident!("append_{}", other_ref_name);

        quote! {
            impl<'a> Add<#other_ref_name<'a>> for #self_buf_name {
                type Output = #output_buf_name;
                fn add(self, rhs: #other_ref_name<'a>) -> Self::Output {
                    self.#append(rhs, true, Glide::None)
                }
            }
            impl<'a> Add<&'a #other_buf_name> for #self_buf_name {
                type Output = #output_buf_name;
                fn add(self, rhs: &'a #other_buf_name) -> Self::Output {
                    self + #other_ref_name::from(rhs)
                }
            }
            impl<'a> Mul<#other_ref_name<'a>> for #self_buf_name {
                type Output = #output_buf_name;
                fn mul(self, rhs: #other_ref_name<'a>) -> Self::Output {
                    self.#append(rhs, false, Glide::None)
                }
            }
            impl<'a> Mul<&'a #other_buf_name> for #self_buf_name {
                type Output = #output_buf_name;
                fn mul(self, rhs: &'a #other_buf_name) -> Self::Output {
                    self * #other_ref_name::from(rhs)
                }
            }
            impl #self_buf_name {
                #[allow(unused_mut, unused_comparisons, unused_variables, unused_assignments, non_snake_case)]
                fn #append(self, rhs: #other_ref_name, fix: bool, glide: Glide) -> #output_buf_name {
                    let prev_tiles = self.tiles.len();
                    let mut tiles = self.tiles;
                    let mut remainder = self.remainder;
                    for t in rhs.tiles {
                        let mut new_tile = #tile_merge_expr;
                        tiles.push(new_tile);
                        remainder = #remainder_update_expr;
                    }
                    #last_tile_merge_stmt
                    let tiles_len = tiles.len();
                    let mut output = #output_buf_name {
                        tiles,
                        remainder,
                    };
                    if glide == Glide::J {
                        #glide_fix
                    }
                    if fix { #post_merge }
                    output
                }
            }
        }
    }

    fn append_def_og(&self) -> TokenStream {
        let l = Self {
            start: self.start,
            end: Nonterminal(Onset),
        };
        let r = Self {
            start: Glide,
            end: self.end,
        };
        let l_trunc = Self {
            start: self.start,
            end: Nonterminal(Glide),
        };
        let self_buf_name = l.buf_name();
        let l_trunc_buf_name = l_trunc.buf_name();
        let other_ref_name = r.ref_name();
        let other_buf_name = r.buf_name();
        let output_buf_name = self.buf_name();

        let s = self.start as usize;
        let j = Onset as usize;
        let j = if j < s { j + 4 } else { j };
        let mut e = self.end.closest_pos() as usize;
        while e < j {
            e += 4
        }

        let truncated_expr = if self.start == Onset {
            quote! {
                {
                    let last_tile = self.tiles.pop().unwrap();
                    (#l_trunc_buf_name {
                        tiles: self.tiles,
                        remainder: (last_tile.0, last_tile.1, last_tile.2),
                    }, last_tile.3)
                }
            }
        } else {
            let s2j_part = (s..(j - 1)).map(|i| {
                let tuple_idx = ti(i - s);
                quote! {
                    self.remainder.#tuple_idx
                }
            });
            let last = {
                let tuple_idx = ti(j - 1 - s);
                quote! {
                    self.remainder.#tuple_idx
                }
            };
            quote! {
                (#l_trunc_buf_name {
                    tiles: self.tiles,
                    remainder: (#(#s2j_part,)*)
                }, #last)
            }
        };

        let append = format_ident!("append_{}", other_ref_name);

        quote! {
            impl<'a> Add<#other_ref_name<'a>> for #self_buf_name {
                type Output = #output_buf_name;
                #[allow(unused_mut)]
                fn add(mut self, rhs: #other_ref_name<'a>) -> Self::Output {
                    let (truncated, glide) = #truncated_expr;
                    truncated.#append(rhs, true, glide)
                }
            }
            impl<'a> Add<&'a #other_buf_name> for #self_buf_name {
                type Output = #output_buf_name;
                fn add(self, rhs: &'a #other_buf_name) -> Self::Output {
                    self + #other_ref_name::from(rhs)
                }
            }
        }
    }
}

const STARTS: [Pos; 4] = [Syllabic, Glide, Onset, Nuclear];

const ENDS: [EPos; 5] = [
    Nonterminal(Syllabic),
    Nonterminal(Glide),
    Nonterminal(Onset),
    Nonterminal(Nuclear),
    Terminal,
];

fn main() -> Result<(), Box<dyn Error>> {
    let ctx = Ctx::new();
    let mut ts = TokenStream::new();

    for start in STARTS {
        for end in ENDS {
            let combo = Se { start, end };
            ts.extend(combo.struct_def(&ctx));
        }
    }

    for start in STARTS {
        for middle in STARTS {
            for end in ENDS {
                let l = Se {
                    start,
                    end: Nonterminal(middle),
                };
                let r = Se { start: middle, end };
                ts.extend(l.append_def(&r, &ctx));
            }
        }
    }

    for start in STARTS {
        for end in ENDS {
            let l = Se { start, end };
            ts.extend(l.append_def_og());
        }
    }

    let build_profile = env::var_os("PROFILE").unwrap();
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("se.rs");
    let fh = File::create(dest_path)?;
    let mut fh = BufWriter::new(fh);
    // Format script if we’re in debug mode.
    // Since GitLab CI now throws a hissy fit in this step, skip it
    // when we’re on CI.
    if build_profile == "debug" && env::var_os("CI").is_none() {
        let ts = match RustFmt::default().format_tokens(ts) {
            Ok(ts) => ts,
            Err(e) => {
                eprintln!("syntax error in generated script: {e}");
                std::process::exit(-1);
            }
        };
        write!(&mut fh, "{ts}")?;
    } else {
        write!(&mut fh, "{ts}")?;
    }
    println!("cargo:rerun-if-changed=build.rs");
    Ok(())
}
