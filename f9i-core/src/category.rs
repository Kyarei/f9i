/*!

Grammatical categories relevant to Ŋarâþ Crîþ v9.

See the [sections on nouns] and [verbs] in the Ŋarâþ Crîþ v9 grammar for more information.

[sections on nouns]: https://ncv9.flirora.xyz/grammar/morphology/nouns.html
[verbs]: https://ncv9.flirora.xyz/grammar/morphology/verbs.html

*/

use crate::mgc::Mgc;
use gridlock::Gridlock;
use std::fmt::Display;

/// A *grammatical case* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Case {
    Nominative,
    Accusative,
    Dative,
    Genitive,
    Locative,
    Instrumental,
    Abessive,
    Semblative,
}

impl Case {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Case::Nominative => "nominative",
            Case::Accusative => "accusative",
            Case::Dative => "dative",
            Case::Genitive => "genitive",
            Case::Locative => "locative",
            Case::Instrumental => "instrumental",
            Case::Abessive => "abessive",
            Case::Semblative => "semblative",
        }
    }

    #[inline]
    pub fn abbreviation(self) -> &'static str {
        match self {
            Case::Nominative => "NOM",
            Case::Accusative => "ACC",
            Case::Dative => "DAT",
            Case::Genitive => "GEN",
            Case::Locative => "LOC",
            Case::Instrumental => "INS",
            Case::Abessive => "ABESS",
            Case::Semblative => "SEMBL",
        }
    }
}

/// A *clareþ* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Gridlock)]
pub enum Clareth {
    Singular,
    Collective,
    Mass,
    Universal,
}

impl Clareth {
    #[inline]
    pub const fn name(self) -> &'static str {
        match self {
            Clareth::Singular => "singular",
            Clareth::Collective => "collective",
            Clareth::Mass => "mass",
            Clareth::Universal => "universal",
        }
    }

    #[inline]
    pub const fn num_count(self) -> usize {
        match self {
            Clareth::Singular => 4,
            Clareth::Collective => 3,
            Clareth::Mass => 2,
            Clareth::Universal => 5,
        }
    }

    #[inline]
    pub const fn suffix(self) -> &'static str {
        match self {
            Clareth::Singular => "",
            Clareth::Collective => ".c",
            Clareth::Mass => ".m",
            Clareth::Universal => ".*",
        }
    }

    #[inline]
    pub const fn associated_axis_type(self) -> AxisType {
        match self {
            Clareth::Singular => AxisType::NumberS,
            Clareth::Collective => AxisType::NumberC,
            Clareth::Mass => AxisType::NumberM,
            Clareth::Universal => AxisType::NumberU,
        }
    }
}

impl Display for Clareth {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name())
    }
}

/// A *nominal number* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum NNumber {
    Direct,
    Dual,
    Plural,
    Singulative,
    Generic,
}

impl NNumber {
    #[inline]
    pub const fn name(self) -> &'static str {
        match self {
            NNumber::Direct => "direct",
            NNumber::Dual => "dual",
            NNumber::Plural => "plural",
            NNumber::Singulative => "singulative",
            NNumber::Generic => "generic",
        }
    }

    /// Get the column index for a number as it would be shown in a declension table for a noun with a given clareþ.
    ///
    /// Returns `None` if no such column exists.
    pub const fn get_column_index(self, clareth: Clareth) -> Option<usize> {
        match (self, clareth) {
            (NNumber::Direct, _) => Some(0),
            (NNumber::Dual, Clareth::Singular) => Some(1),
            (NNumber::Plural, Clareth::Singular) => Some(2),
            (NNumber::Generic, Clareth::Singular) => Some(3),
            (NNumber::Singulative, Clareth::Collective) => Some(1),
            (NNumber::Generic, Clareth::Collective) => Some(2),
            (NNumber::Generic, Clareth::Mass) => Some(1),
            (number, Clareth::Universal) => Some(number as usize),
            _ => None,
        }
    }

    pub const fn from_index_and_clareth(i: usize, clareth: Clareth) -> NNumber {
        match clareth {
            Clareth::Singular => [
                NNumber::Direct,
                NNumber::Dual,
                NNumber::Plural,
                NNumber::Generic,
            ][i],
            Clareth::Collective => [NNumber::Direct, NNumber::Singulative, NNumber::Generic][i],
            Clareth::Mass => [NNumber::Direct, NNumber::Generic][i],
            Clareth::Universal => NNumber::VALUES[i],
        }
    }
}

/// A *verbal number* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum VNumber {
    Singular,
    Dual,
    Plural,
    Generic,
}

impl VNumber {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            VNumber::Singular => "singular",
            VNumber::Dual => "dual",
            VNumber::Plural => "plural",
            VNumber::Generic => "generic",
        }
    }
}

/// A *grammatical gender* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Gender {
    Celestial,
    Terrestrial,
    Human,
}

impl Gender {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Gender::Celestial => "celestial",
            Gender::Terrestrial => "terrestrial",
            Gender::Human => "human",
        }
    }
    pub const fn abbreviation(self) -> char {
        match self {
            Gender::Celestial => 'c',
            Gender::Terrestrial => 't',
            Gender::Human => 'h',
        }
    }
}

/// A *person* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Person {
    FirstExcl,
    FirstIncl,
    Second,
    Third,
}

impl Person {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Person::FirstExcl => "exclusive first",
            Person::FirstIncl => "inclusive first",
            Person::Second => "second",
            Person::Third => "third",
        }
    }
}

/// A *person* in Ŋarâþ Crîþ v9, further specifying a *gender* for the third person.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum PersonObj {
    None,
    FirstExcl,
    FirstIncl,
    Second,
    ThirdC,
    ThirdT,
    ThirdH,
    ThirdE,
    Reflexive,
    Reciprocal,
}

/// A *tense* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Tense {
    Present,
    Past,
}

impl Tense {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Tense::Present => "present",
            Tense::Past => "past",
        }
    }
}

/// An *aspect* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Aspect {
    Imperfective,
    Perfective,
}

impl Aspect {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Aspect::Imperfective => "imperfective",
            Aspect::Perfective => "perfective",
        }
    }
}

/// An *rcase* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum RCase {
    Nominative,
    Accusative,
    Dative,
    GenitiveNom,
    GenitiveAcc,
    GenitiveDat,
}

impl RCase {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            RCase::Nominative => "nominative",
            RCase::Accusative => "accusative",
            RCase::Dative => "dative",
            RCase::GenitiveNom => "genitive of nominative",
            RCase::GenitiveAcc => "genitive of accusative",
            RCase::GenitiveDat => "genitive of dative",
        }
    }
}

/// The person and number of a subject, as expressed by a verbal subject suffix, in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum SubjectType {
    S1,
    D1X,
    P1X,
    G1,
    D1I,
    P1I,
    S2,
    D2,
    P2,
    G2,
    S3,
    D3,
    P3,
    G3,
}

impl SubjectType {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            SubjectType::S1 => "first-person singular",
            SubjectType::D1X => "first-person dual exclusive",
            SubjectType::P1X => "first-person plural exclusive",
            SubjectType::G1 => "first-person generic",
            SubjectType::D1I => "first-person dual inclusive",
            SubjectType::P1I => "first-person plural inclusive",
            SubjectType::S2 => "second-person singular",
            SubjectType::D2 => "second-person dual",
            SubjectType::P2 => "second-person plural",
            SubjectType::G2 => "second-person generic",
            SubjectType::S3 => "third-person singular",
            SubjectType::D3 => "third-person dual",
            SubjectType::P3 => "third-person plural",
            SubjectType::G3 => "third-person generic",
        }
    }

    pub const fn from_person_and_number(p: Person, n: VNumber) -> Option<SubjectType> {
        match (p, n) {
            (Person::FirstExcl, VNumber::Singular) => Some(SubjectType::S1),
            (Person::FirstExcl, VNumber::Dual) => Some(SubjectType::D1X),
            (Person::FirstExcl, VNumber::Plural) => Some(SubjectType::P1X),
            (Person::FirstExcl, VNumber::Generic) => Some(SubjectType::G1),
            (Person::FirstIncl, VNumber::Dual) => Some(SubjectType::D1I),
            (Person::FirstIncl, VNumber::Plural) => Some(SubjectType::P1I),
            (Person::Second, VNumber::Singular) => Some(SubjectType::S2),
            (Person::Second, VNumber::Dual) => Some(SubjectType::D2),
            (Person::Second, VNumber::Plural) => Some(SubjectType::P2),
            (Person::Second, VNumber::Generic) => Some(SubjectType::G2),
            (Person::Third, VNumber::Singular) => Some(SubjectType::S3),
            (Person::Third, VNumber::Dual) => Some(SubjectType::D3),
            (Person::Third, VNumber::Plural) => Some(SubjectType::P3),
            (Person::Third, VNumber::Generic) => Some(SubjectType::G3),
            _ => None,
        }
    }
    pub const fn to_person_and_number(self) -> (Person, VNumber) {
        match self {
            SubjectType::S1 => (Person::FirstExcl, VNumber::Singular),
            SubjectType::D1X => (Person::FirstExcl, VNumber::Dual),
            SubjectType::P1X => (Person::FirstExcl, VNumber::Plural),
            SubjectType::G1 => (Person::FirstExcl, VNumber::Generic),
            SubjectType::D1I => (Person::FirstIncl, VNumber::Dual),
            SubjectType::P1I => (Person::FirstIncl, VNumber::Plural),
            SubjectType::S2 => (Person::Second, VNumber::Singular),
            SubjectType::D2 => (Person::Second, VNumber::Dual),
            SubjectType::P2 => (Person::Second, VNumber::Plural),
            SubjectType::G2 => (Person::Second, VNumber::Generic),
            SubjectType::S3 => (Person::Third, VNumber::Singular),
            SubjectType::D3 => (Person::Third, VNumber::Dual),
            SubjectType::P3 => (Person::Third, VNumber::Plural),
            SubjectType::G3 => (Person::Third, VNumber::Generic),
        }
    }
}
/// The person, number, and gender of an object, as expressed by a verbal object suffix, in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum ObjectType {
    None,
    S1,
    D1X,
    P1X,
    G1,
    D1I,
    P1I,
    S2,
    D2,
    P2,
    G2,
    S3C,
    S3T,
    S3H,
    D3H,
    P3H,
    D3,
    P3,
    G3,
    Reflexive,
    Reciprocal,
}

const REALIZATIONS: [&[Mgc]; ObjectType::COUNT] = [
    &[],
    &[Mgc::P, Mgc::E],
    &[Mgc::P, Mgc::J, Mgc::O],
    &[Mgc::P, Mgc::O],
    &[Mgc::T, Mgc::E, Mgc::N],
    &[Mgc::P, Mgc::J, Mgc::OHat],
    &[Mgc::P, Mgc::OHat],
    &[Mgc::V, Mgc::E],
    &[Mgc::V, Mgc::I],
    &[Mgc::V, Mgc::O],
    &[Mgc::V, Mgc::E, Mgc::S],
    &[Mgc::L, Mgc::E],
    &[Mgc::L, Mgc::U],
    &[Mgc::L, Mgc::E, Mgc::S],
    &[Mgc::L, Mgc::I, Mgc::S],
    &[Mgc::L, Mgc::O, Mgc::S],
    &[Mgc::L, Mgc::I],
    &[Mgc::L, Mgc::O],
    &[Mgc::L, Mgc::A, Mgc::S],
    &[Mgc::C, Mgc::I, Mgc::N],
    &[Mgc::R, Mgc::I, Mgc::Th],
];
impl ObjectType {
    /// Returns the affix associated with this [ObjectType].
    pub const fn object_affix(self) -> &'static [Mgc] {
        REALIZATIONS[self as usize]
    }

    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            ObjectType::None => "none",
            ObjectType::S1 => "first-person singular",
            ObjectType::D1X => "first-person dual exclusive",
            ObjectType::P1X => "first-person plural exclusive",
            ObjectType::G1 => "first-person generic",
            ObjectType::D1I => "first-person dual inclusive",
            ObjectType::P1I => "first-person plural inclusive",
            ObjectType::S2 => "second-person singular",
            ObjectType::D2 => "second-person dual",
            ObjectType::P2 => "second-person plural",
            ObjectType::G2 => "second-person generic",
            ObjectType::S3C => "third-person celestial singular",
            ObjectType::S3T => "third-person terrestrial singular",
            ObjectType::S3H => "third-person human singular",
            ObjectType::D3H => "third-person human dual",
            ObjectType::P3H => "third-person human plural",
            ObjectType::D3 => "third-person epicene dual",
            ObjectType::P3 => "third-person epicene plural",
            ObjectType::G3 => "third-person generic",
            ObjectType::Reflexive => "reflexive",
            ObjectType::Reciprocal => "reciprocal",
        }
    }

    pub const fn from_person_and_number(p: PersonObj, n: VNumber) -> Option<ObjectType> {
        match (p, n) {
            (PersonObj::None, VNumber::Singular) => Some(ObjectType::None),
            (PersonObj::FirstExcl, VNumber::Singular) => Some(ObjectType::S1),
            (PersonObj::FirstExcl, VNumber::Dual) => Some(ObjectType::D1X),
            (PersonObj::FirstExcl, VNumber::Plural) => Some(ObjectType::P1X),
            (PersonObj::FirstExcl, VNumber::Generic) => Some(ObjectType::G1),
            (PersonObj::FirstIncl, VNumber::Dual) => Some(ObjectType::D1I),
            (PersonObj::FirstIncl, VNumber::Plural) => Some(ObjectType::P1I),
            (PersonObj::Second, VNumber::Singular) => Some(ObjectType::S2),
            (PersonObj::Second, VNumber::Dual) => Some(ObjectType::D2),
            (PersonObj::Second, VNumber::Plural) => Some(ObjectType::P2),
            (PersonObj::Second, VNumber::Generic) => Some(ObjectType::G2),
            (PersonObj::ThirdC, VNumber::Singular) => Some(ObjectType::S3C),
            (PersonObj::ThirdT, VNumber::Singular) => Some(ObjectType::S3T),
            (PersonObj::ThirdH, VNumber::Singular) => Some(ObjectType::S3H),
            (PersonObj::ThirdH, VNumber::Dual) => Some(ObjectType::D3H),
            (PersonObj::ThirdH, VNumber::Plural) => Some(ObjectType::P3H),
            (PersonObj::ThirdE, VNumber::Dual) => Some(ObjectType::D3),
            (PersonObj::ThirdE, VNumber::Plural) => Some(ObjectType::P3),
            (PersonObj::ThirdE, VNumber::Generic) => Some(ObjectType::G3),
            (PersonObj::Reflexive, VNumber::Singular) => Some(ObjectType::Reflexive),
            (PersonObj::Reciprocal, VNumber::Singular) => Some(ObjectType::Reciprocal),
            _ => None,
        }
    }
    pub const fn to_person_and_number(self) -> (PersonObj, VNumber) {
        match self {
            ObjectType::None => (PersonObj::None, VNumber::Singular),
            ObjectType::S1 => (PersonObj::FirstExcl, VNumber::Singular),
            ObjectType::D1X => (PersonObj::FirstExcl, VNumber::Dual),
            ObjectType::P1X => (PersonObj::FirstExcl, VNumber::Plural),
            ObjectType::G1 => (PersonObj::FirstExcl, VNumber::Generic),
            ObjectType::D1I => (PersonObj::FirstIncl, VNumber::Dual),
            ObjectType::P1I => (PersonObj::FirstIncl, VNumber::Plural),
            ObjectType::S2 => (PersonObj::Second, VNumber::Singular),
            ObjectType::D2 => (PersonObj::Second, VNumber::Dual),
            ObjectType::P2 => (PersonObj::Second, VNumber::Plural),
            ObjectType::G2 => (PersonObj::Second, VNumber::Generic),
            ObjectType::S3C => (PersonObj::ThirdC, VNumber::Singular),
            ObjectType::S3T => (PersonObj::ThirdT, VNumber::Singular),
            ObjectType::S3H => (PersonObj::ThirdH, VNumber::Singular),
            ObjectType::D3H => (PersonObj::ThirdH, VNumber::Dual),
            ObjectType::P3H => (PersonObj::ThirdH, VNumber::Plural),
            ObjectType::D3 => (PersonObj::ThirdE, VNumber::Dual),
            ObjectType::P3 => (PersonObj::ThirdE, VNumber::Plural),
            ObjectType::G3 => (PersonObj::ThirdE, VNumber::Generic),
            ObjectType::Reflexive => (PersonObj::Reflexive, VNumber::Singular),
            ObjectType::Reciprocal => (PersonObj::Reciprocal, VNumber::Singular),
        }
    }
    pub const fn to_table_index(self) -> usize {
        let (p, n) = self.to_person_and_number();
        VNumber::COUNT * (p as usize) + (n as usize)
    }
}

#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Gridlock)]
/// A transfinite form for a resinous verb.
pub enum Transfinite {
    /// The second-person singular imperative.
    Imp2S,
    /// The supine form.
    Supine,
    /// The finite active gerundive.
    ActiveGerundiveFin,
    /// The terrestrial adnominal active gerundive.
    ActiveGerundiveAttrT,
    /// The non-terrestrial adnominal active gerundive.
    ActiveGerundiveAttrC,
    /// The finite passive gerundive.
    PassiveGerundiveFin,
    /// The adnominal active gerundive.
    PassiveGerundiveAttr,
}

impl Transfinite {
    /// Returns a user-facing string describing this transfinite verb form.
    pub const fn name(self) -> &'static str {
        match self {
            Transfinite::Imp2S => "second-person singular imperative",
            Transfinite::Supine => "supine",
            Transfinite::ActiveGerundiveFin => "active gerund (finite)",
            Transfinite::ActiveGerundiveAttrT => "active gerund (adnominal, terrestrial)",
            Transfinite::ActiveGerundiveAttrC => "active gerund (adnominal, non-terrestrial)",
            Transfinite::PassiveGerundiveFin => "passive gerund (finite)",
            Transfinite::PassiveGerundiveAttr => "passive gerund (adnominal)",
        }
    }
}

/// A *mood* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Mood {
    Indicative,
    Subjunctive,
}

/// A nominalized verb form in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Nominalized {
    Nominative,
    Accusative,
    DativeInd,
    DativeSubj,
    Genitive,
    LocativeInd,
    LocativeSubj,
    InstrumentalInd,
    InstrumentalSubj,
    AbessiveInd,
    AbessiveSubj,
    SemblativeInd,
    SemblativeSubj,
}

impl Nominalized {
    pub fn name(self) -> &'static str {
        match self {
            Nominalized::Nominative => "nominative",
            Nominalized::Accusative => "accusative",
            Nominalized::DativeInd => "dative indicative",
            Nominalized::DativeSubj => "dative subjunctive",
            Nominalized::Genitive => "genitive",
            Nominalized::LocativeInd => "locative indicative",
            Nominalized::LocativeSubj => "locative subjunctive",
            Nominalized::InstrumentalInd => "instrumental indicative",
            Nominalized::InstrumentalSubj => "instrumental subjunctive",
            Nominalized::AbessiveInd => "abessive indicative",
            Nominalized::AbessiveSubj => "abessive subjunctive",
            Nominalized::SemblativeInd => "semblative indicative",
            Nominalized::SemblativeSubj => "semblative subjunctive",
        }
    }
}

impl Nominalized {
    pub fn to_case_and_mood(self) -> (Case, Mood) {
        match self {
            Nominalized::Nominative => (Case::Nominative, Mood::Indicative),
            Nominalized::Accusative => (Case::Accusative, Mood::Indicative),
            Nominalized::DativeInd => (Case::Dative, Mood::Indicative),
            Nominalized::DativeSubj => (Case::Dative, Mood::Subjunctive),
            Nominalized::Genitive => (Case::Genitive, Mood::Subjunctive),
            Nominalized::LocativeInd => (Case::Locative, Mood::Indicative),
            Nominalized::LocativeSubj => (Case::Locative, Mood::Subjunctive),
            Nominalized::InstrumentalInd => (Case::Instrumental, Mood::Indicative),
            Nominalized::InstrumentalSubj => (Case::Instrumental, Mood::Subjunctive),
            Nominalized::AbessiveInd => (Case::Abessive, Mood::Indicative),
            Nominalized::AbessiveSubj => (Case::Abessive, Mood::Subjunctive),
            Nominalized::SemblativeInd => (Case::Semblative, Mood::Indicative),
            Nominalized::SemblativeSubj => (Case::Semblative, Mood::Subjunctive),
        }
    }

    pub fn from_case_and_mood(case: Case, mood: Mood) -> Option<Self> {
        match (case, mood) {
            (Case::Nominative, Mood::Indicative) => Some(Nominalized::Nominative),
            (Case::Accusative, Mood::Indicative) => Some(Nominalized::Accusative),
            (Case::Dative, Mood::Indicative) => Some(Nominalized::DativeInd),
            (Case::Dative, Mood::Subjunctive) => Some(Nominalized::DativeSubj),
            (Case::Genitive, Mood::Subjunctive) => Some(Nominalized::Genitive),
            (Case::Locative, Mood::Indicative) => Some(Nominalized::LocativeInd),
            (Case::Locative, Mood::Subjunctive) => Some(Nominalized::LocativeSubj),
            (Case::Instrumental, Mood::Indicative) => Some(Nominalized::InstrumentalInd),
            (Case::Instrumental, Mood::Subjunctive) => Some(Nominalized::InstrumentalSubj),
            (Case::Abessive, Mood::Indicative) => Some(Nominalized::AbessiveInd),
            (Case::Abessive, Mood::Subjunctive) => Some(Nominalized::AbessiveSubj),
            (Case::Semblative, Mood::Indicative) => Some(Nominalized::SemblativeInd),
            (Case::Semblative, Mood::Subjunctive) => Some(Nominalized::SemblativeSubj),
            _ => None,
        }
    }
}

/// A *transitivity* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Transitivity {
    Intransitive,
    Semitransitive,
    Transitive,
    Ditransitive,
    Auxiliary,
}

impl Transitivity {
    /// Gets the letter used to abbreviate this transitivity.
    pub const fn to_letter(self) -> char {
        match self {
            Transitivity::Intransitive => 'i',
            Transitivity::Semitransitive => 's',
            Transitivity::Transitive => 't',
            Transitivity::Ditransitive => 'd',
            Transitivity::Auxiliary => 'a',
        }
    }
}

/// An *attachment type* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Attachment {
    Adnominal,
    Adverbial,
}

impl Attachment {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Attachment::Adnominal => "adnominal",
            Attachment::Adverbial => "adverbial",
        }
    }
    /// Gets the letter used to abbreviate this transitivity.
    pub const fn to_letter(self) -> char {
        match self {
            Attachment::Adnominal => 'n',
            Attachment::Adverbial => 'v',
        }
    }
}

/// A *motion type* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum MotionType {
    Static,
    Allative,
    Ablative,
}

impl MotionType {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            MotionType::Static => "static",
            MotionType::Allative => "allative",
            MotionType::Ablative => "ablative",
        }
    }
}

/// A *polarity* in Ŋarâþ Crîþ v9.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Gridlock)]
pub enum Polarity {
    Affirmative,
    Negative,
}

impl Polarity {
    #[inline]
    pub fn name(self) -> &'static str {
        match self {
            Polarity::Affirmative => "affirmative",
            Polarity::Negative => "negative",
        }
    }
}

/// A tag type representing a grammatical category in Ŋarâþ Crîþ v9.
///
/// See [AxisType] for more information.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Gridlock)]
pub enum CategoryTag {
    /// Case (see [Case]).
    Case,
    /// Number for a singular noun.
    NumberS,
    /// Number for a collective noun.
    NumberC,
    /// Number for a mass noun.
    NumberM,
    /// Number for a universal noun (see [NNumber]).
    NumberU,
    /// Number for other parts of speech (see [VNumber]).
    VNumber,
    /// Person (see [Person]).
    Person,
    /// The type of an object affix (see [ObjectType]).
    ObjectType,
    /// Gender (see [Gender]).
    Gender,
    /// Relative case (see [RCase]).
    RCase,
    /// Aspect (see [Aspect]).
    Aspect,
    /// Tense (see [Tense]).
    Tense,
    /// Attachment (see [Attachment]).
    Attachment,
    /// Motion type (see [MotionType]).
    MotionType,
    /// Polarity (see [Polarity]).
    Polarity,
    /// The ‘person’ axis on a table depicting object type.
    PersonObj,
    /// A transfinite form (see [Transfinite]).
    Transfinite,
    /// Both the gender and number, as distinguished by genus III participles.
    H3,
    /// Mood (see [Mood]).
    Mood,
}

impl CategoryTag {
    /// Get the number of entries for this category.
    pub const fn get_count(self) -> usize {
        match self {
            CategoryTag::Case => Case::COUNT,
            CategoryTag::NumberS => 4,
            CategoryTag::NumberC => 3,
            CategoryTag::NumberM => 2,
            CategoryTag::NumberU => 5,
            CategoryTag::VNumber => VNumber::COUNT,
            CategoryTag::Person => Person::COUNT,
            CategoryTag::ObjectType => ObjectType::COUNT,
            CategoryTag::Gender => Gender::COUNT,
            CategoryTag::RCase => RCase::COUNT,
            CategoryTag::Aspect => Aspect::COUNT,
            CategoryTag::Tense => Tense::COUNT,
            CategoryTag::Attachment => Attachment::COUNT,
            CategoryTag::MotionType => MotionType::COUNT,
            CategoryTag::Polarity => Polarity::COUNT,
            CategoryTag::PersonObj => PersonObj::COUNT,
            CategoryTag::Transfinite => Transfinite::COUNT,
            CategoryTag::H3 => 4,
            CategoryTag::Mood => Mood::COUNT,
        }
    }

    /// Get the display name for this category.
    pub const fn get_display_name(self) -> &'static str {
        match self {
            CategoryTag::Case => "Case",
            CategoryTag::NumberS => "Number",
            CategoryTag::NumberC => "Number",
            CategoryTag::NumberM => "Number",
            CategoryTag::NumberU => "Number",
            CategoryTag::VNumber => "Number",
            CategoryTag::Person => "Person",
            CategoryTag::ObjectType => "Object type",
            CategoryTag::Gender => "Gender",
            CategoryTag::RCase => "Rcase",
            CategoryTag::Aspect => "Aspect",
            CategoryTag::Tense => "Tense",
            CategoryTag::Attachment => "Attachment",
            CategoryTag::MotionType => "Motion",
            CategoryTag::Polarity => "Polarity",
            CategoryTag::PersonObj => "Person & gender",
            CategoryTag::Transfinite => "Form",
            CategoryTag::H3 => "Gender & number",
            CategoryTag::Mood => "Mood",
        }
    }

    /// Get the human-readable names of each entry in this category.
    pub const fn get_axis_labels(self) -> &'static [&'static str] {
        match self {
            CategoryTag::Case => &[
                "Nominative",
                "Accusative",
                "Dative",
                "Genitive",
                "Locative",
                "Instrumental",
                "Abessive",
                "Semblative",
            ],
            CategoryTag::NumberS => &["Singular", "Dual", "Plural", "Generic"],
            CategoryTag::NumberC => &["Collective", "Singulative", "Generic"],
            CategoryTag::NumberM => &["Direct", "Generic"],
            CategoryTag::NumberU => &["Direct", "Dual", "Plural", "Singulative", "Generic"],
            CategoryTag::VNumber => &["Singular", "Dual", "Plural", "Generic"],
            CategoryTag::Person => &["1st excl.", "1st incl.", "2nd", "3rd"],
            CategoryTag::ObjectType => &[
                "None",
                "1sg",
                "1du excl.",
                "1pl excl.",
                "1gc",
                "1du incl.",
                "1pl incl.",
                "2sg",
                "2du",
                "2pl",
                "2gc",
                "3sg c.",
                "3sg t.",
                "3sg h.",
                "3du h.",
                "3pl h.",
                "3du",
                "3pl",
                "3gc",
                "Refl.",
                "Recip.",
            ],
            CategoryTag::Gender => &["Celestial", "Terrestrial", "Human"],
            CategoryTag::RCase => &[
                "Nominative",
                "Accusative",
                "Dative",
                "Genitive of nom.",
                "Genitive of acc.",
                "Genitive of dat.",
            ],
            CategoryTag::Aspect => &["Imperfective", "Perfective"],
            CategoryTag::Tense => &["Present", "Past"],
            CategoryTag::Attachment => &["Adnominal", "Adverbial"],
            CategoryTag::MotionType => &["Static", "Toward", "Away"],
            CategoryTag::Polarity => &["Affirmative", "Negative"],
            CategoryTag::PersonObj => &[
                "None",
                "1st exclusive",
                "1st inclusive",
                "2nd",
                "3rd celestial",
                "3rd terrestrial",
                "3rd human",
                "3rd epicene",
                "Reflexive",
                "Reciprocal",
            ],
            CategoryTag::Transfinite => &[
                "2sg imperative",
                "Supine",
                "Act. gerundive (finite)",
                "Act. gerundive (adnominal, terrestrial)",
                "Act. gerundive (adnominal, non-terrestrial)",
                "Pass. gerundive (finite)",
                "Pass. gerundive (adnominal)",
            ],
            CategoryTag::H3 => &["Celestial", "Terrestrial", "Human", "Non-singular"],
            CategoryTag::Mood => &["Indicative", "Subjunctive"],
        }
    }
}

/// A possible axis type in an inflection table.
///
/// This differs from [CategoryTag] in that multiple axis
/// types may correspond to the same category tag. For
/// instance, the ‘subject number’ axis in a finite
/// conjugation table and the ‘hnumber’ axis in a type II
/// participle table correspond to the ‘verbal number’
/// category.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Gridlock)]
pub enum AxisType {
    /// The case of a noun (see [Case]).
    Case,
    /// Number for a singular noun.
    NumberS,
    /// Number for a collective noun.
    NumberC,
    /// Number for a mass noun.
    NumberM,
    /// Number for a universal noun (see [NNumber]).
    NumberU,
    /// Number for other parts of speech (see [VNumber]).
    VNumber,
    /// The person of the subject of a verb (see [Person]).
    SubjPerson,
    /// The number of the subject of a verb.
    SubjNumber,
    /// The type of an object affix (see [ObjectType]).
    ObjectType,
    /// The hgender of a participle (see [Gender]).
    HGender,
    /// The hcase of a participle.
    HCase,
    /// The hnumber of a participle.
    HNumber,
    /// Both the hgender and hnumber, as distinguished by genus III participles.
    H3,
    /// The rcase of a participle (see [RCase]).
    RCase,
    /// The aspect of a verb (see [Aspect]).
    Aspect,
    /// The tense of a verb (see [Tense]).
    Tense,
    /// The attachment of a relational (see [Attachment]).
    Attachment,
    /// The motion type of a relational (see [MotionType]).
    MotionType,
    /// The polarity of a finite relational form (see [Polarity]).
    Polarity,
    /// The ‘person’ axis on a table depicting object type.
    PersonObj,
    /// The transfinite form of a resinous verb (see [Transfinite]).
    Transfinite,
    /// The mood of a nominalized verb form (see [Mood]).
    Mood,
}

impl AxisType {
    /// Get the number of elements in this axis type.
    pub const fn get_count(self) -> usize {
        self.get_delegate().get_count()
    }

    /// Get the human-readable names of each element in this axis type.
    pub const fn get_axis_labels(self) -> &'static [&'static str] {
        self.get_delegate().get_axis_labels()
    }

    /// Get the [CategoryTag] corresponding to this axis type.
    pub const fn get_delegate(self) -> CategoryTag {
        match self {
            AxisType::Case => CategoryTag::Case,
            AxisType::NumberS => CategoryTag::NumberS,
            AxisType::NumberC => CategoryTag::NumberC,
            AxisType::NumberM => CategoryTag::NumberM,
            AxisType::NumberU => CategoryTag::NumberU,
            AxisType::VNumber => CategoryTag::VNumber,
            AxisType::SubjPerson => CategoryTag::Person,
            AxisType::SubjNumber => CategoryTag::VNumber,
            AxisType::ObjectType => CategoryTag::ObjectType,
            AxisType::HGender => CategoryTag::Gender,
            AxisType::HCase => CategoryTag::Case,
            AxisType::HNumber => CategoryTag::VNumber,
            AxisType::H3 => CategoryTag::H3,
            AxisType::RCase => CategoryTag::RCase,
            AxisType::Aspect => CategoryTag::Aspect,
            AxisType::Tense => CategoryTag::Tense,
            AxisType::Attachment => CategoryTag::Attachment,
            AxisType::MotionType => CategoryTag::MotionType,
            AxisType::Polarity => CategoryTag::Polarity,
            AxisType::PersonObj => CategoryTag::PersonObj,
            AxisType::Transfinite => CategoryTag::Transfinite,
            AxisType::Mood => CategoryTag::Mood,
        }
    }

    /// Get the human-readable name of this axis type.
    pub const fn get_display_name(self) -> &'static str {
        match self {
            AxisType::Case => "Case",
            AxisType::NumberS => "Number",
            AxisType::NumberC => "Number",
            AxisType::NumberM => "Number",
            AxisType::NumberU => "Number",
            AxisType::VNumber => "Number",
            AxisType::SubjPerson => "Subject person",
            AxisType::SubjNumber => "Subject number",
            AxisType::ObjectType => "Object type",
            AxisType::HGender => "Hgender",
            AxisType::HCase => "Hcase",
            AxisType::HNumber => "Hnumber",
            AxisType::H3 => "Hcase & hnumber",
            AxisType::RCase => "Rcase",
            AxisType::Aspect => "Aspect",
            AxisType::Tense => "Tense",
            AxisType::Attachment => "Attachment",
            AxisType::MotionType => "Motion",
            AxisType::Polarity => "Polarity",
            AxisType::PersonObj => "Person",
            AxisType::Transfinite => "Form",
            AxisType::Mood => "Mood",
        }
    }

    /// Return true if the name of this axis should be
    /// displayed in the table heading along with its value
    /// because the name of the value alone does not tell
    /// what it pertains to.
    pub const fn wants_label(self) -> bool {
        matches!(self, AxisType::RCase | AxisType::MotionType)
    }
}

/// The maximum number of dimensions that are supported in a single table.
pub const MAX_CATEGORY_TAGS: usize = 8;

const fn unpackh<T: Copy, const N: usize>(
    mut arr: [Option<T>; N],
    slice: &[T],
    idx: usize,
) -> [Option<T>; N] {
    if idx == N || idx == slice.len() {
        return arr;
    }
    arr[idx] = Some(slice[idx]);
    unpackh(arr, slice, idx + 1)
}

/// Turns a slice into a fixed-size array of its elements, padded with [None]s.
///
/// That is, if `slice.len()` is less than `N`, then the array returned will have instances of [Some] in the range `[0, slice.len())` and instances of [None] in the range `[slice.len(), N)`.
///
/// If `slice.len()` is greater than `N`, then the resulting array will be truncated.
pub const fn unpack<T: Copy, const N: usize>(slice: &[T]) -> [Option<T>; N] {
    let arr = [None; N];
    unpackh(arr, slice, 0)
}
