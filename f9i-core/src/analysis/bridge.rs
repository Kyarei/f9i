use std::convert::TryInto;

use paste::paste;

use crate::mgc::{Consonant, EffectiveClass, Epf, Glide, Initial, Mgc, SimpleCoda, Vowel};

// Assert that the bridge is canonical when running in debug mode.
macro_rules! debug_assert_canon {
    ($coda:expr, $initial:expr) => {
        match ($coda, $initial) {
            (ResolvedCoda::Ng, i) => debug_assert_ne!(i, Initial::Empty, "resolution left a non-canonical bridge: quasi-⟦ŋ⟧ not allowed before a null onset"),
            (ResolvedCoda::Coda(c), i) => debug_assert_eq!(
                (c, i),
                canonicalize_bridge(c, i),
                "resolution left a non-canonical bridge"
            ),
        }
    };
}

/// The results of a coda after [resolving a bridge][resolve_bridge].
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ResolvedCoda {
    /// A real coda.
    Coda(SimpleCoda),
    /// A quasi-⟦ŋ⟧, which affects the previous vowel and turns into ⟦-r⟧.
    Ng,
}

impl ResolvedCoda {
    pub fn final_consonant(self) -> Option<Consonant> {
        match self {
            ResolvedCoda::Coda(c) => c.final_consonant(),
            ResolvedCoda::Ng => Some(Consonant::Ng),
        }
    }
}

macro_rules! canon_match_arms {
    (
        $coda:ident, $initial:ident ;
        $($c_erl:ident),* ;
        $($c_e:ident),* ;
        $($c_th:ident),* ;
        $(($c_cc1:ident, $c_cc2:ident, $c_cc3:ident)),*
    ) => {
        paste! {
            match ($coda, $initial) {
                $((SimpleCoda::$c_erl, Initial::Empty) => (SimpleCoda::Empty, Initial::Single(Consonant::$c_erl)),)*
                $((SimpleCoda::$c_erl, Initial::Single(Consonant::R)) => (SimpleCoda::Empty, Initial::R(Epf::$c_erl)),)*
                $((SimpleCoda::$c_erl, Initial::Single(Consonant::L)) => (SimpleCoda::Empty, Initial::L(Epf::$c_erl)),)*

                $((SimpleCoda::$c_e, Initial::Empty) => (SimpleCoda::Empty, Initial::Single(Consonant::$c_e)),)*

                (SimpleCoda::RTh, Initial::Empty) => (SimpleCoda::R, Initial::Single(Consonant::Th)),
                (SimpleCoda::CTh, Initial::Empty) => (SimpleCoda::Empty, Initial::Cth),

                $((SimpleCoda::[<$c_th Th>], Initial::Single(Consonant::R)) => (SimpleCoda::$c_th, Initial::R(Epf::Th)),)*
                $((SimpleCoda::[<$c_th Th>], Initial::Single(Consonant::L)) => (SimpleCoda::$c_th, Initial::L(Epf::Th)),)*

                $((SimpleCoda::$c_cc1 , Initial::Single(Consonant::$c_cc2)) => (SimpleCoda::Empty, Initial::$c_cc3),)*
                _ => ($coda, $initial)
            }
        }
    }
}

/// Canonicalizes a bridge according to the maximal-onset principle
/// without performing any [further resolutions][resolve_bridge].
pub fn canonicalize_bridge(coda: SimpleCoda, initial: Initial) -> (SimpleCoda, Initial) {
    canon_match_arms!(
        coda, initial;
        S, Th, T, C, F;
        N, L, R, Lh;
        R, C;
        (T, F, Tf), (C, F, Cf), (C, Th, Cth), (C, S, Cs), (C, Sh, Csh)
    )
}

/// Resolve the given bridge according to the rules proposed by [Project Elaine].
///
/// The bridge is [canonicalized][canonicalize_bridge] before any further steps.
///
/// Note that bridge repair is **not** idempotent; for instance, ⟦-sð·-⟧ resolves to ⟦-ss-⟧, but ⟦-ss-⟧ resolves to ⟦-þ-⟧.
///
/// [Project Elaine]: https://ncv9.flirora.xyz/diary/projects/elaine.html
pub fn resolve_bridge(coda: SimpleCoda, initial: Initial) -> (ResolvedCoda, Initial) {
    let (coda, mut initial) = canonicalize_bridge(coda, initial);
    let mut coda = ResolvedCoda::Coda(coda);
    // 1. Coalescence of ⟦-tš-⟧
    if (coda, initial)
        == (
            ResolvedCoda::Coda(SimpleCoda::T),
            Initial::Single(Consonant::Sh),
        )
    {
        (coda, initial) = (
            ResolvedCoda::Coda(SimpleCoda::Empty),
            Initial::Single(Consonant::Ch),
        )
    }
    debug_assert_canon!(coda, initial);
    // 2. Fortition of ⟦h-⟧ and ⟦ħ-⟧
    initial = match initial {
        Initial::Single(Consonant::H) if fortites_h(coda) => Initial::Single(Consonant::C),
        Initial::R(Epf::H) if fortites_h(coda) => Initial::R(Epf::C),
        Initial::L(Epf::H) if fortites_h(coda) => Initial::L(Epf::C),
        Initial::Single(Consonant::Hst) if fortites_hst(coda) => Initial::Single(Consonant::G),
        Initial::R(Epf::Hst) if fortites_hst(coda) => Initial::R(Epf::G),
        Initial::L(Epf::Hst) if fortites_hst(coda) => Initial::L(Epf::G),
        _ => initial,
    };
    debug_assert_canon!(coda, initial);
    // 3. Metathesis
    macro_rules! metathesis_3 {
        ($(($I:ident, $J:ident)),*) => {
            (coda, initial) = match (coda, initial) {
                $(
                    // For (C, T) in {(c, t), (g, d), (c·, t·), (g·, d·)}:

                    // -tC- → -cT-
                    (ResolvedCoda::Coda(SimpleCoda::T), Initial::Single(Consonant::$I)) => (
                        ResolvedCoda::Coda(SimpleCoda::C),
                        Initial::Single(Consonant::$J),
                    ),
                    // -tCr- → -cTr-
                    (ResolvedCoda::Coda(SimpleCoda::T), Initial::R(Epf::$I)) => {
                        (ResolvedCoda::Coda(SimpleCoda::C), Initial::R(Epf::$J))
                    }
                    // -tCl- → -cTl-
                    (ResolvedCoda::Coda(SimpleCoda::T), Initial::L(Epf::$I)) => {
                        (ResolvedCoda::Coda(SimpleCoda::C), Initial::L(Epf::$J))
                    }
                )*
                _ => (coda, initial),
            }
        }
    }
    metathesis_3! { (C, T), (G, D), (CDot, TDot), (GDot, DDot) };
    debug_assert_canon!(coda, initial);
    if coda == ResolvedCoda::Coda(SimpleCoda::T) {
        match initial {
            Initial::Cf | Initial::Cth | Initial::Cs | Initial::Csh => {
                coda = ResolvedCoda::Coda(SimpleCoda::Empty)
            }
            Initial::Gv => {
                coda = ResolvedCoda::Coda(SimpleCoda::Empty);
                initial = Initial::Cf;
            }
            Initial::Gdh => {
                coda = ResolvedCoda::Coda(SimpleCoda::Empty);
                initial = Initial::Cth;
            }
            _ => (),
        }
    }
    debug_assert_canon!(coda, initial);
    // 4. Nasal assimilation
    match (coda, initial) {
        (
            ResolvedCoda::Coda(SimpleCoda::T),
            Initial::Single(Consonant::M | Consonant::MDot | Consonant::N | Consonant::Ng),
        ) => coda = ResolvedCoda::Coda(SimpleCoda::N),
        (ResolvedCoda::Coda(SimpleCoda::C), Initial::Single(Consonant::Ng)) => {
            coda = ResolvedCoda::Coda(SimpleCoda::Empty)
        }
        (
            ResolvedCoda::Coda(SimpleCoda::C),
            Initial::Single(Consonant::M | Consonant::MDot | Consonant::N),
        ) => coda = ResolvedCoda::Ng,
        _ => (),
    }
    debug_assert_canon!(coda, initial);
    // 5. Denasalization of ⟦ŋ-⟧
    if denasalizes_ng(coda) && initial == Initial::Single(Consonant::Ng) {
        initial = Initial::Single(Consonant::G);
    }
    debug_assert_canon!(coda, initial);
    // 5a. Things with ⟦-ł⟧ codas
    if coda == ResolvedCoda::Coda(SimpleCoda::Lh) {
        match initial {
            // Fortition after ⟦-ł⟧
            Initial::Single(Consonant::Th | Consonant::TDot | Consonant::S) => {
                initial = Initial::Single(Consonant::T)
            }
            Initial::Single(Consonant::Dh | Consonant::DDot) => {
                initial = Initial::Single(Consonant::D)
            }
            Initial::R(Epf::Th | Epf::TDot | Epf::S) => initial = Initial::R(Epf::T),
            Initial::R(Epf::Dh | Epf::DDot) => initial = Initial::R(Epf::D),
            Initial::L(Epf::Th | Epf::TDot | Epf::S) => initial = Initial::L(Epf::T),
            Initial::L(Epf::Dh | Epf::DDot) => initial = Initial::L(Epf::D),
            _ => (),
        }
    }
    debug_assert_canon!(coda, initial);
    let mut step6_devoiced_dhdot = false;
    // 6. Devoicing of ⟦v-⟧ and ⟦ð-⟧
    match (coda, initial) {
        // (a) After ⟦-þ⟧, ⟦-rþ⟧, ⟦-t⟧, ⟦-c⟧, and ⟦-f⟧, ⟦v-⟧ devoices to ⟦f-⟧ and ⟦ð-⟧ devoices to ⟦þ-⟧. Additionally, ⟦ð-⟧ is devoiced after ⟦-s⟧.

        // This needs special attention with ⟦-c⟧ and ⟦-t⟧ to maintain canonicality.
        (ResolvedCoda::Coda(SimpleCoda::T), Initial::Single(Consonant::V)) => {
            initial = Initial::Tf;
            coda = ResolvedCoda::Coda(SimpleCoda::Empty);
        }
        (ResolvedCoda::Coda(SimpleCoda::C), Initial::Single(Consonant::V)) => {
            initial = Initial::Cf;
            coda = ResolvedCoda::Coda(SimpleCoda::Empty);
        }
        (ResolvedCoda::Coda(SimpleCoda::C), Initial::Single(Consonant::Dh)) => {
            initial = Initial::Cth;
            coda = ResolvedCoda::Coda(SimpleCoda::Empty);
        }
        // End special cases
        (coda, Initial::Single(Consonant::V)) if devoices_v(coda) => {
            initial = Initial::Single(Consonant::F)
        }
        (coda, Initial::Single(Consonant::Dh)) if fortites_h(coda) => {
            initial = Initial::Single(Consonant::Th)
        }
        // (for v· and ð·)
        (coda, Initial::Single(Consonant::VDot)) if devoices_v(coda) => {
            initial = Initial::Single(Consonant::FDot)
        }
        (coda, Initial::Single(Consonant::DhDot)) if fortites_h(coda) => {
            step6_devoiced_dhdot = true;
            initial = Initial::Single(coda.final_consonant().unwrap())
        }
        // (c) ⟦-þ⟧ is deleted before ⟦vr-⟧ and ⟦vl-⟧ instead.
        (
            ResolvedCoda::Coda(SimpleCoda::Th),
            Initial::R(Epf::V | Epf::VDot) | Initial::L(Epf::V | Epf::VDot),
        ) => coda = ResolvedCoda::Coda(SimpleCoda::Empty),
        // (d) Additionally, ⟦-rþCR-⟧ onsets (with R = ⟦r⟧ or ⟦l⟧ and C = ⟦v⟧ or ⟦ð⟧) are corrected to ⟦-RC-⟧.
        (
            ResolvedCoda::Coda(SimpleCoda::RTh),
            Initial::R(c @ (Epf::V | Epf::VDot | Epf::Dh | Epf::DhDot)),
        ) => {
            coda = ResolvedCoda::Coda(SimpleCoda::R);
            initial = Initial::Single(c.into());
        }
        (
            ResolvedCoda::Coda(SimpleCoda::RTh),
            Initial::L(c @ (Epf::V | Epf::VDot | Epf::Dh | Epf::DhDot)),
        ) => {
            coda = ResolvedCoda::Coda(SimpleCoda::L);
            initial = Initial::Single(c.into());
        }
        // (b) This process occurs analogously for the onsets ⟦vr-⟧, ⟦vl-⟧, ⟦ðr-⟧, and ⟦ðl-⟧, …
        (coda, Initial::R(Epf::V)) if devoices_v(coda) => initial = Initial::R(Epf::F),
        (coda, Initial::L(Epf::V)) if devoices_v(coda) => initial = Initial::L(Epf::F),
        (coda, Initial::R(Epf::Dh)) if devoices_dh(coda) => initial = Initial::R(Epf::Th),
        (coda, Initial::L(Epf::Dh)) if devoices_dh(coda) => initial = Initial::L(Epf::Th),
        // (for v· and ð·)
        (coda, Initial::R(Epf::VDot)) if devoices_v(coda) => initial = Initial::R(Epf::FDot),
        (coda, Initial::L(Epf::VDot)) if devoices_v(coda) => initial = Initial::L(Epf::FDot),
        // the final consonant of the coda will always be a valid Epf
        (coda, Initial::R(Epf::DhDot)) if devoices_dh(coda) => {
            initial = coda
                .final_consonant()
                .unwrap()
                .try_into()
                .map(Initial::R)
                .unwrap_or(Initial::Single(Consonant::R))
        }
        (coda, Initial::L(Epf::DhDot)) if devoices_dh(coda) => {
            initial = coda
                .final_consonant()
                .unwrap()
                .try_into()
                .map(Initial::L)
                .unwrap_or(Initial::Single(Consonant::L))
        }
        _ => (),
    }
    debug_assert_canon!(coda, initial);
    // 6a. Assimilation of ⟦s⟧ after ⟦þ⟧
    'r6a: {
        if let ResolvedCoda::Coda(
            c @ (SimpleCoda::Th | SimpleCoda::RTh | SimpleCoda::CTh | SimpleCoda::S),
        ) = coda
        {
            match initial {
                Initial::Single(Consonant::S) if !step6_devoiced_dhdot => {
                    initial = Initial::Single(Consonant::Th);
                }
                Initial::R(Epf::S) => {
                    initial = Initial::R(Epf::Th);
                }
                Initial::L(Epf::S) => {
                    initial = Initial::L(Epf::Th);
                }
                _ => break 'r6a,
            }
            if c == SimpleCoda::S {
                coda = ResolvedCoda::Coda(SimpleCoda::Empty);
            }
        }
    }
    // 6a. Things with ⟦-ł⟧ codas
    if coda == ResolvedCoda::Coda(SimpleCoda::Lh) {
        match initial {
            // Stop–frciative onsets
            Initial::Cf
            | Initial::Cs
            | Initial::Csh
            | Initial::Cth
            | Initial::Tf
            | Initial::Gv
            | Initial::Gdh
            | Initial::Dv => coda = ResolvedCoda::Coda(SimpleCoda::L),
            Initial::Single(c) if !preserves_lh(c) => coda = ResolvedCoda::Coda(SimpleCoda::L),
            Initial::R(c) if !preserves_lh(c.into()) => coda = ResolvedCoda::Coda(SimpleCoda::L),
            Initial::L(c) if !preserves_lh(c.into()) => coda = ResolvedCoda::Coda(SimpleCoda::L),
            _ => (),
        }
    }
    debug_assert_canon!(coda, initial);
    // 7. Degemination before another consonant
    match (coda, &initial.as_consonants()[..]) {
        (_, [_]) => (),
        (ResolvedCoda::Coda(SimpleCoda::Th), [Consonant::Th, _]) => {
            coda = ResolvedCoda::Coda(SimpleCoda::Empty)
        }
        (ResolvedCoda::Coda(SimpleCoda::RTh), [Consonant::Th, _]) => {
            coda = ResolvedCoda::Coda(SimpleCoda::R)
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), [Consonant::Th, _]) => {
            coda = ResolvedCoda::Coda(SimpleCoda::C)
        }
        (
            ResolvedCoda::Coda(SimpleCoda::T),
            [Consonant::T | Consonant::TDot | Consonant::D | Consonant::DDot, _],
        ) => coda = ResolvedCoda::Coda(SimpleCoda::Empty),
        (
            ResolvedCoda::Coda(SimpleCoda::C),
            [Consonant::C | Consonant::CDot | Consonant::G | Consonant::GDot, _],
        ) => coda = ResolvedCoda::Coda(SimpleCoda::Empty),
        (ResolvedCoda::Coda(SimpleCoda::F), [Consonant::F | Consonant::FDot, _]) => {
            coda = ResolvedCoda::Coda(SimpleCoda::Empty)
        }
        _ => (),
    }
    debug_assert_canon!(coda, initial);
    // 8. Simplification of bridges with two-letter codas
    match (coda, &mut initial) {
        (ResolvedCoda::Coda(SimpleCoda::RTh), Initial::R(c) | Initial::L(c)) => {
            if Mgc::from(*c).get_mgc_properties().effective_class == EffectiveClass::Fricative {
                coda = ResolvedCoda::Coda(SimpleCoda::R);
            } else {
                coda = ResolvedCoda::Coda(SimpleCoda::Th);
            }
        }
        (
            ResolvedCoda::Coda(SimpleCoda::RTh),
            Initial::Cf | Initial::Cs | Initial::Csh | Initial::Cth | Initial::Tf | Initial::Dv,
        ) => {
            coda = ResolvedCoda::Coda(SimpleCoda::R);
        }
        (ResolvedCoda::Coda(SimpleCoda::RTh), Initial::Gv | Initial::Gdh) => {
            coda = ResolvedCoda::Coda(SimpleCoda::Th);
        }
        (
            ResolvedCoda::Coda(SimpleCoda::CTh),
            Initial::Single(
                Consonant::Th
                | Consonant::Sh
                | Consonant::M
                | Consonant::T
                | Consonant::Hst
                | Consonant::MDot
                | Consonant::TDot,
            ),
        ) => (),
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::Cf | Initial::Tf) => {
            initial = Initial::Single(Consonant::F);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::Cs) => {
            initial = Initial::Single(Consonant::Th);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::Csh) => {
            initial = Initial::Single(Consonant::Sh);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::Cth) => {
            initial = Initial::Single(Consonant::Th);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::R(Epf::Th | Epf::Sh | Epf::Hst)) => {
            coda = ResolvedCoda::Coda(SimpleCoda::C);
            initial = Initial::R(Epf::Th);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), Initial::L(Epf::Th | Epf::Sh | Epf::Hst)) => {
            coda = ResolvedCoda::Coda(SimpleCoda::C);
            initial = Initial::L(Epf::Th);
        }
        (ResolvedCoda::Coda(SimpleCoda::CTh), _) => coda = ResolvedCoda::Coda(SimpleCoda::Th),
        _ => (),
    }
    debug_assert_canon!(coda, initial);
    (coda, initial)
}

/// Resolve the given bridge along with the preceding glide and vowel.
pub fn resolve_bridge_with_prev_vowel(
    glide: Glide,
    vowel: Vowel,
    coda: SimpleCoda,
    initial: Initial,
) -> (Glide, Vowel, SimpleCoda, Initial) {
    let (coda, initial) = resolve_bridge(coda, initial);
    match coda {
        ResolvedCoda::Coda(coda) => (glide, vowel, coda, initial),
        ResolvedCoda::Ng => (
            glide | Glide::for_vowel(vowel),
            vowel.to_o(),
            SimpleCoda::R,
            initial,
        ),
    }
}

const fn fortites_h(coda: ResolvedCoda) -> bool {
    matches!(
        coda,
        ResolvedCoda::Coda(
            SimpleCoda::S
                | SimpleCoda::Th
                | SimpleCoda::RTh
                | SimpleCoda::T
                | SimpleCoda::C
                | SimpleCoda::F
                | SimpleCoda::Lh
                | SimpleCoda::CTh
        )
    )
}

const fn devoices_dh(coda: ResolvedCoda) -> bool {
    matches!(
        coda,
        ResolvedCoda::Coda(
            SimpleCoda::S
                | SimpleCoda::Th
                | SimpleCoda::RTh
                | SimpleCoda::T
                | SimpleCoda::C
                | SimpleCoda::F
                | SimpleCoda::Lh
                | SimpleCoda::CTh
        )
    )
}

const fn fortites_hst(coda: ResolvedCoda) -> bool {
    matches!(
        coda,
        ResolvedCoda::Coda(SimpleCoda::T | SimpleCoda::C | SimpleCoda::F | SimpleCoda::Lh)
    )
}

const fn denasalizes_ng(coda: ResolvedCoda) -> bool {
    matches!(
        coda,
        ResolvedCoda::Coda(
            SimpleCoda::S
                | SimpleCoda::Th
                | SimpleCoda::RTh
                | SimpleCoda::F
                | SimpleCoda::CTh
                | SimpleCoda::Lh
        )
    )
}

const fn devoices_v(coda: ResolvedCoda) -> bool {
    matches!(
        coda,
        ResolvedCoda::Coda(
            SimpleCoda::Th
                | SimpleCoda::RTh
                | SimpleCoda::T
                | SimpleCoda::C
                | SimpleCoda::F
                | SimpleCoda::Lh
                | SimpleCoda::CTh
        )
    )
}

const fn preserves_lh(c: Consonant) -> bool {
    matches!(
        c,
        Consonant::T
            | Consonant::D
            | Consonant::R
            | Consonant::L
            | Consonant::N
            | Consonant::C
            | Consonant::TDot
            | Consonant::DDot
            | Consonant::CDot
    )
}
