use std::convert::TryFrom;

use custom_error::custom_error;

use crate::mgc::dw::*;
use crate::mgc::simple::*;
use crate::mgc::syllable::*;

custom_error! {
    /// Indicates an error that has occurred during syllabification.
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub SyllabifyError {position: usize} = "invalid syllable at position {position}"
}

/// Represents a type that can be parsed from the end of a string of [Mgc]s.
pub trait TryExtract: Sized {
    /// Parse the item from the end of a string of [Mgc]s.
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError>;
}

impl TryExtract for () {
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        Ok((letters, ()))
    }
}

impl<A> TryExtract for (A,)
where
    A: TryExtract,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, a) = A::try_extract(letters)?;
        Ok((letters, (a,)))
    }
}

impl<A, B> TryExtract for (A, B)
where
    A: TryExtract,
    B: TryExtract,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, b) = B::try_extract(letters)?;
        let (letters, a) = A::try_extract(letters)?;
        Ok((letters, (a, b)))
    }
}

impl<A, B, C> TryExtract for (A, B, C)
where
    A: TryExtract,
    B: TryExtract,
    C: TryExtract,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, c) = C::try_extract(letters)?;
        let (letters, b) = B::try_extract(letters)?;
        let (letters, a) = A::try_extract(letters)?;
        Ok((letters, (a, b, c)))
    }
}

impl<A, B, C, D> TryExtract for (A, B, C, D)
where
    A: TryExtract,
    B: TryExtract,
    C: TryExtract,
    D: TryExtract,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, d) = D::try_extract(letters)?;
        let (letters, c) = C::try_extract(letters)?;
        let (letters, b) = B::try_extract(letters)?;
        let (letters, a) = A::try_extract(letters)?;
        Ok((letters, (a, b, c, d)))
    }
}

impl TryExtract for Vowel {
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Vowel), SyllabifyError> {
        if letters.is_empty() {
            return Err(SyllabifyError { position: 0 });
        }
        let end = letters.len();
        match Vowel::try_from(letters[end - 1]) {
            Ok(v) => Ok((&letters[..end - 1], v)),
            Err(_) => Err(SyllabifyError { position: end - 1 }),
        }
    }
}

/// Represents a type that can be parsed from the end of a string of [Mgc]s.
/// This trait is implemented for types that can always be parsed successfully
/// from the end of such a string.
pub trait Extract: Sized {
    fn extract(letters: &[Mgc]) -> (&[Mgc], Self);
}

impl<T> TryExtract for T
where
    T: Extract,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        Ok(T::extract(letters))
    }
}

impl Extract for Initial {
    fn extract(letters: &[Mgc]) -> (&[Mgc], Self) {
        match letters {
            [r @ .., Mgc::C, Mgc::F] => (r, Initial::Cf),
            [r @ .., Mgc::C, Mgc::Th] => (r, Initial::Cth),
            [r @ .., Mgc::C, Mgc::S] => (r, Initial::Cs),
            [r @ .., Mgc::C, Mgc::Sh] => (r, Initial::Csh),
            [r @ .., Mgc::G, Mgc::V] => (r, Initial::Gv),
            [r @ .., Mgc::G, Mgc::Dh] => (r, Initial::Gdh),
            [r @ .., Mgc::T, Mgc::F] => (r, Initial::Tf),
            [r @ .., Mgc::D, Mgc::V] => (r, Initial::Dv),
            [r @ .., c, d] => match Consonant::try_from(*c).and_then(Epf::try_from) {
                Ok(c) if *d == Mgc::R => (r, Initial::R(c)),
                Ok(c) if *d == Mgc::L => (r, Initial::L(c)),
                _ => {
                    if let Ok(d) = Consonant::try_from(*d) {
                        (&letters[..letters.len() - 1], Initial::Single(d))
                    } else {
                        (letters, Initial::Empty)
                    }
                }
            },
            [r @ .., d] => {
                if let Ok(d) = Consonant::try_from(*d) {
                    (r, Initial::Single(d))
                } else {
                    (letters, Initial::Empty)
                }
            }
            _ => (letters, Initial::Empty),
        }
    }
}

impl Extract for Glide {
    fn extract(letters: &[Mgc]) -> (&[Mgc], Self) {
        match letters {
            [a @ .., Mgc::J] => (a, Glide::J),
            _ => (letters, Glide::None),
        }
    }
}

impl<C> Extract for C
where
    C: TCoda,
{
    fn extract(letters: &[Mgc]) -> (&[Mgc], Self) {
        let (c, letters) = <C as TCoda>::extract(letters);
        (letters, c)
    }
}

impl TryExtract for Nucleus {
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, vowel) = Vowel::try_extract(letters)?;
        let (letters, glide) = Glide::extract(letters);
        Ok((letters, Nucleus::new(glide, vowel)))
    }
}

impl<C> TryExtract for TSyllable<C>
where
    C: TCoda,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        // Try to find a coda
        let (letters, coda) = <C as Extract>::extract(letters);
        // Try to find a nucleus
        let (letters, nucleus) = Nucleus::try_extract(letters)?;
        // Try to find an onset
        let (letters, onset) = Initial::extract(letters);
        Ok((
            letters,
            TSyllable {
                initial: onset,
                nucleus,
                coda,
            },
        ))
    }
}

impl Extract for Onset {
    fn extract(letters: &[Mgc]) -> (&[Mgc], Self) {
        let (letters, glide) = Glide::extract(letters);
        let (letters, initial) = Initial::extract(letters);
        (letters, Onset { initial, glide })
    }
}

impl<C> TryExtract for RimeWithGlide<C>
where
    C: TCoda,
{
    fn try_extract(letters: &[Mgc]) -> Result<(&[Mgc], Self), SyllabifyError> {
        let (letters, coda) = <C as Extract>::extract(letters);
        let (letters, vowel) = Vowel::try_extract(letters)?;
        let (letters, glide) = Glide::extract(letters);
        Ok((letters, RimeWithGlide { glide, vowel, coda }))
    }
}

#[deprecated(note = "will be replaced by Sylls::parse or ParsedWord::parse")]
struct SyllablesFromEnd<'a>(&'a [Mgc]);

impl<'a> Iterator for SyllablesFromEnd<'a> {
    type Item = Result<Syllable, SyllabifyError>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            None
        } else {
            match Syllable::try_extract(self.0) {
                Ok((remnant, syllable)) => {
                    self.0 = remnant;
                    Some(Ok(syllable))
                }
                Err(e) => Some(Err(e)),
            }
        }
    }
}

/// Returns an iterator over the syllables of a word, starting from the end.
///
/// That is, the last syllable is returned first, followed by the preceding ones.
///
/// If an invalid syllable is encountered, then the iterator will return the
/// same value repeatedly.
///
/// ### Examples
///
/// ```
/// # use f9i_core::analysis::*;
/// # use f9i_core::mgc::*;
/// let bogus_word = &[Mgc::S, Mgc::C, Mgc::A, Mgc::R, Mgc::E];
/// let mut iter = syllables_from_end(bogus_word);
/// assert_eq!(
///     iter.next(),
///     Some(Ok(Syllable {
///         initial: Initial::Single(Consonant::R),
///         nucleus: Nucleus::E,
///         coda: Coda::Empty
///     }))
/// );
/// assert_eq!(
///     iter.next(),
///     Some(Ok(Syllable {
///         initial: Initial::Single(Consonant::C),
///         nucleus: Nucleus::A,
///         coda: Coda::Empty
///     }))
/// );
/// // error only happens when we can’t form a syllable
/// assert_eq!(iter.next(), Some(Err(SyllabifyError { position: 0 })));
/// assert_eq!(iter.next(), Some(Err(SyllabifyError { position: 0 })));
/// ```
#[deprecated(note = "will be replaced by Sylls::parse or ParsedWord::parse")]
pub fn syllables_from_end(
    letters: &[Mgc],
) -> impl Iterator<Item = Result<Syllable, SyllabifyError>> + '_ {
    SyllablesFromEnd(letters)
}

/// Given a [DecoratedWord], gets the syllables contained inside it.
#[deprecated(note = "will be replaced by Sylls::parse or ParsedWord::parse")]
pub fn syllabify(word: &DecoratedWord) -> Result<Vec<Syllable>, SyllabifyError> {
    let mut syllables: Vec<Syllable> =
        syllables_from_end(word.letters()).collect::<Result<_, _>>()?;
    syllables.reverse();
    Ok(syllables)
}
