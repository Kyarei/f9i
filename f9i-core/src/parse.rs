/*!

Parsing of types in the [crate::mgc] module.

Most of these functions are used for macros in the `f9i-ivm-macro` crate.

*/

use crate::{
    assemblage::{Decorated, Parse},
    mgc::{
        DecoratedWord, Decoration, DecorationFlags, Fragment, Glide, Mgc, NameMarker, Nucleus,
        Phrase, Vowel,
    },
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, one_of},
    combinator::{complete, eof, map, map_res, opt, success, verify},
    error::Error as NomError,
    multi::many0,
    sequence::{delimited, pair, preceded, terminated, tuple},
    Finish, IResult,
};
use std::iter;

// Using nom for parsing for now; might replace it with a handwritten one if I need to

/// Optionally parses a *nef* and returns whether one was present.
pub fn nef(input: &str) -> IResult<&str, bool> {
    map(opt(char('*')), |o| o.is_some())(input)
}
/// Optionally parses a *sen* and returns whether one was present.
pub fn sen(input: &str) -> IResult<&str, bool> {
    map(opt(char('&')), |o| o.is_some())(input)
}
/// Optionally parses a *ŋos* and returns whether one was present.
pub fn ngos(input: &str) -> IResult<&str, bool> {
    map(opt(char('’')), |o| o.is_some())(input)
}
/// Optionally parses a *ŋos* and returns whether one was present; otherwise requiring a space.
pub fn ngos_space(input: &str) -> IResult<&str, bool> {
    map(one_of(" ’"), |o| o == '’')(input)
}
/// Parses a [name marker](NameMarker).
pub fn name_marker(input: &str) -> IResult<&str, NameMarker> {
    alt((
        map(char('#'), |_| NameMarker::Carth),
        map(tag("+*"), |_| NameMarker::Njor),
        map(char('+'), |_| NameMarker::Tor),
        map(char('@'), |_| NameMarker::Es),
        success(NameMarker::None),
    ))(input)
}
/// Parses a [Decoration] object.
pub fn decoration(input: &str) -> IResult<&str, Decoration> {
    map(tuple((nef, name_marker, sen)), |(n, m, s)| Decoration {
        flags: (if n {
            DecorationFlags::NEF
        } else {
            DecorationFlags::empty()
        }) | (if s {
            DecorationFlags::SEN
        } else {
            DecorationFlags::empty()
        }),
        name_marker: m,
    })(input)
}
/// Parses a [Decoration] object that is guaranteed not to be empty.
pub fn nonempty_decoration(input: &str) -> IResult<&str, Decoration> {
    verify(decoration, |d| {
        d.has_nef() || d.name_marker != NameMarker::None || d.has_sen()
    })(input)
}

/// Parses a non-eclipsed letter.
pub fn non_eclipsed_letter(input: &str) -> IResult<&str, Mgc> {
    alt((
        alt((
            map(tag("p·"), |_| Mgc::PDot),
            map(tag("t·"), |_| Mgc::TDot),
            map(tag("d·"), |_| Mgc::DDot),
            map(tag("č·"), |_| Mgc::ChDot),
            map(tag("c·"), |_| Mgc::CDot),
            map(tag("g·"), |_| Mgc::GDot),
            map(tag("m·"), |_| Mgc::MDot),
            map(tag("f·"), |_| Mgc::FDot),
            map(tag("v·"), |_| Mgc::VDot),
            map(tag("ð·"), |_| Mgc::DhDot),
            map(char('-'), |_| Mgc::Hyphen),
            map(char('e'), |_| Mgc::E),
            map(char('o'), |_| Mgc::O),
            map(char('a'), |_| Mgc::A),
            map(char('i'), |_| Mgc::I),
            map(char('u'), |_| Mgc::U),
            map(char('ê'), |_| Mgc::EHat),
            map(char('ô'), |_| Mgc::OHat),
            map(char('â'), |_| Mgc::AHat),
            map(char('î'), |_| Mgc::IHat),
            map(char('j'), |_| Mgc::J),
        )),
        alt((
            map(char('c'), |_| Mgc::C),
            map(char('n'), |_| Mgc::N),
            map(char('ŋ'), |_| Mgc::Ng),
            map(char('v'), |_| Mgc::V),
            map(char('s'), |_| Mgc::S),
            map(char('þ'), |_| Mgc::Th),
            map(char('š'), |_| Mgc::Sh),
            map(char('r'), |_| Mgc::R),
            map(char('l'), |_| Mgc::L),
            map(char('ł'), |_| Mgc::Lh),
            map(char('m'), |_| Mgc::M),
            map(char('f'), |_| Mgc::F),
            map(char('g'), |_| Mgc::G),
            map(char('p'), |_| Mgc::P),
            map(char('t'), |_| Mgc::T),
            map(char('č'), |_| Mgc::Ch),
            map(char('d'), |_| Mgc::D),
            map(char('ð'), |_| Mgc::Dh),
            map(char('h'), |_| Mgc::H),
            map(char('ħ'), |_| Mgc::Hst),
            map(char('G'), |_| Mgc::GEclipse),
        )),
    ))(input)
}

/// Parses a vowel.
pub fn vowel(input: &str) -> IResult<&str, Vowel> {
    alt((
        map(char('e'), |_| Vowel::E),
        map(char('o'), |_| Vowel::O),
        map(char('a'), |_| Vowel::A),
        map(char('i'), |_| Vowel::I),
        map(char('u'), |_| Vowel::U),
        map(char('ê'), |_| Vowel::EHat),
        map(char('ô'), |_| Vowel::OHat),
        map(char('â'), |_| Vowel::AHat),
        map(char('î'), |_| Vowel::IHat),
    ))(input)
}

/// Parses a [Nucleus] object.
pub fn nucleus(input: &str) -> IResult<&str, Nucleus> {
    map(tuple((opt(char('j')), vowel)), |(j, v)| Nucleus {
        glide: match j {
            Some(_) => Glide::J,
            None => Glide::None,
        },
        vowel: v,
    })(input)
}

/// Parses the first letter of a word plus any [decoration](Decoration) present.
pub fn decoration_plus_first_letter(
    want_space: bool,
    input: &str,
) -> IResult<&str, (Decoration, Mgc)> {
    map(
        pair(
            if want_space { ngos_space } else { ngos },
            alt((
                map(delimited(char('m'), decoration, char('p')), |d| {
                    (d, Mgc::Mp)
                }),
                map(delimited(char('v'), decoration, char('p')), |d| {
                    (d, Mgc::Vp)
                }),
                map(delimited(char('d'), decoration, char('t')), |d| {
                    (d, Mgc::Dt)
                }),
                map(delimited(char('n'), decoration, char('d')), |d| {
                    (d, Mgc::Nd)
                }),
                map(delimited(char('g'), decoration, char('c')), |d| {
                    (d, Mgc::Gc)
                }),
                map(delimited(char('ŋ'), decoration, char('g')), |d| {
                    (d, Mgc::Ngg)
                }),
                map(delimited(char('v'), decoration, char('f')), |d| {
                    (d, Mgc::Vf)
                }),
                map(delimited(char('ð'), decoration, char('þ')), |d| {
                    (d, Mgc::Dhth)
                }),
                map(delimited(char('l'), decoration, char('ł')), |d| {
                    (d, Mgc::Llh)
                }),
                map(preceded(one_of("g"), nonempty_decoration), |d| {
                    (d, Mgc::GEclipse)
                }),
                pair(decoration, non_eclipsed_letter),
            )),
        ),
        |(has_ngos, (decoration, letter))| {
            (
                if has_ngos {
                    Decoration {
                        flags: decoration.flags | DecorationFlags::CLITIC,
                        ..decoration
                    }
                } else {
                    decoration
                },
                letter,
            )
        },
    )(input)
}

/// Parses a [decorated word](DecoratedWord), rejecting the parse if any input is left.
pub fn decorated_word_complete(input: &str) -> IResult<&str, DecoratedWord> {
    terminated(complete(decorated_word_first), eof)(input)
}

/// Parses a [decorated word](DecoratedWord).
///
/// `want_space`: true if a space is required in the absence of an initial *ŋos*.
pub fn decorated_word(want_space: bool, input: &str) -> IResult<&str, DecoratedWord> {
    (map(
        pair(
            |input| decoration_plus_first_letter(want_space, input),
            many0(non_eclipsed_letter),
        ),
        |((decoration, first), rest)| {
            DecoratedWord::new(iter::once(first).chain(rest).collect(), decoration)
        },
    ))(input)
}

/// Equivalent to `decorated_word(false, input)`.
///
/// This is used to parse the first word of a phrase.
pub fn decorated_word_first(input: &str) -> IResult<&str, DecoratedWord> {
    decorated_word(false, input)
}

/// Equivalent to `decorated_word(true, input)`.
///
/// This is used to parse the words of a phrase after the first.
pub fn decorated_word_nonfirst(input: &str) -> IResult<&str, DecoratedWord> {
    decorated_word(true, input)
}

/// Parses a [fragment](Fragment) of a word.
pub fn fragment(input: &str) -> IResult<&str, Fragment> {
    terminated(
        complete(map(many0(non_eclipsed_letter), |v| v.into_iter().collect())),
        eof,
    )(input)
}

/// Parses the first component of a phrase.
pub fn phrase_head(input: &str) -> IResult<&str, Phrase> {
    map(decorated_word_first, Phrase::Word)(input)
}

/// Parses a component of a phrase that is not the first.
pub fn phrase_body(input: &str) -> IResult<&str, Phrase> {
    map(decorated_word_nonfirst, Phrase::Word)(input)
}

/// Parses a [phrase](Phrase).
/// Phrases are parsed into components then appended. A different method is used to parse the first component of a phrase than the others.
pub fn phrase(input: &str) -> IResult<&str, Phrase> {
    map(pair(phrase_head, many0(phrase_body)), |(c0, cs)| {
        let mut res = c0;
        for c in cs {
            res = res.appended(c);
        }
        res
    })(input)
}

impl DecoratedWord {
    /// Parses some text as a [DecoratedWord], obeying Ŋarâþ Crîþ v9 orthography.
    pub fn parse(text: &str) -> Result<DecoratedWord, NomError<&str>> {
        decorated_word_complete(text).finish().map(|(_, w)| w)
    }
    /// Parses some text as a [Fragment], obeying Ŋarâþ Crîþ v9 orthography.
    pub fn parse_fragment(text: &str) -> Result<Fragment, NomError<&str>> {
        fragment(text).finish().map(|(_, w)| w)
    }
    /// Parses some text as an object implementing [Parse], obeying Ŋarâþ Crîþ v9 orthography.
    pub fn parse_assemblage<T: Parse>(text: &str) -> Result<T, NomError<&str>> {
        assemblage(text).finish().map(|(_, w)| w)
    }
    /// Parses some text as an object implementing [Parse], obeying Ŋarâþ Crîþ v9 orthography.
    pub fn parse_assemblaged<T: Parse>(text: &str) -> Result<Decorated<T>, NomError<&str>> {
        assemblaged(text).finish().map(|(_, w)| w)
    }
}

impl Phrase {
    /// Parses some text as a [Phrase], obeying Ŋarâþ Crîþ v9 orthography.
    pub fn parse(text: &str) -> Result<Phrase, NomError<&str>> {
        terminated(complete(phrase), eof)(text)
            .finish()
            .map(|(_, w)| w)
    }
}

/// Parses an object implementing [Parse].
pub fn assemblage<T: Parse>(text: &str) -> IResult<&str, T> {
    map_res(fragment, |f| T::parse(&f))(text)
}

/// Parses an object implementing [Parse], along with decoration.
pub fn assemblaged<T: Parse>(text: &str) -> IResult<&str, Decorated<T>> {
    map_res(decorated_word_first, |f| Decorated::<T>::parse(&f))(text)
}
