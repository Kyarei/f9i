/*!

Types and functions dealing with the phonology of Ŋarâþ Crîþ v9.

See the [section in the ŊCv9 grammar] for more information.

[section in the ŊCv9 grammar]: https://ncv9.flirora.xyz/grammar/phonology/layer0.html

*/

pub(crate) mod dw;
pub(crate) mod dw_storage;
pub(crate) mod mutate;
pub(crate) mod phrase;
pub(crate) mod simple;
pub(crate) mod stem;
pub(crate) mod syllable;

pub use dw::*;
pub use mutate::*;
pub use phrase::*;
pub use simple::*;
pub use stem::*;
pub use syllable::*;
