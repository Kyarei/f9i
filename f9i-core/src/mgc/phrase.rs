use std::fmt::Write;
use std::fmt::{Debug, Display, Formatter};
use std::ops::Try;

use crate::mgc::dw::*;

/// A phrase in Ŋarâþ Crîþ.
#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum Phrase {
    /// A phrase consisting of only one word.
    Word(DecoratedWord),
    /// A phrase consisting of zero or more sub-phrases in succession.
    /// The sub-phrases are separated by spaces, unless they specifically request not to be.
    Append(Vec<Phrase>),
}

impl Phrase {
    /// Alias for [Phrase::Word].
    #[inline]
    pub fn of(word: DecoratedWord) -> Phrase {
        Phrase::Word(word)
    }

    fn wants_space_before(&self) -> bool {
        match self {
            Phrase::Word(w) => !w.decoration().is_postclitic(),
            _ => true,
        }
    }

    fn display_nc(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Phrase::Word(w) => Display::fmt(w, f),
            Phrase::Append(phs) => {
                let mut first = true;
                for subphrase in phs {
                    if !first && subphrase.wants_space_before() {
                        f.write_char(' ')?;
                    }
                    first = false;
                    Display::fmt(&subphrase, f)?;
                }
                Ok(())
            }
        }
    }

    /// Returns the result of appending one phrase to another.
    pub fn appended(self, other: Phrase) -> Phrase {
        match (self, other) {
            (Phrase::Append(mut a), Phrase::Append(mut b)) => {
                a.append(&mut b);
                Phrase::Append(a)
            }
            (Phrase::Append(mut a), b) => {
                a.push(b);
                Phrase::Append(a)
            }
            (a, Phrase::Append(mut b)) => {
                b.insert(0, a);
                Phrase::Append(b)
            }
            (a, b) => Phrase::Append(vec![a, b]),
        }
    }

    pub fn visit_words<F, O>(&self, f: &mut F) -> O
    where
        F: FnMut(&DecoratedWord) -> O,
        O: Try<Output = ()>,
    {
        match self {
            Phrase::Word(w) => f(w),
            Phrase::Append(ps) => ps.iter().try_for_each(|p| p.visit_words(f)),
        }
    }
}

impl Default for Phrase {
    fn default() -> Self {
        Phrase::Append(vec![])
    }
}

impl Display for Phrase {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.display_nc(f)
    }
}
