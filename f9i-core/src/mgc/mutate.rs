use crate::mgc::simple::*;
use crate::mgc::syllable::*;

/// Indicates whether a lenition should be *total* or *partial*.
///
/// Total lenition lenites ⟦f⟧, ⟦v⟧, and ⟦ð⟧, causing
/// them to become silent in layer 3s. Partial lenition
/// does not.
#[derive(Copy, Clone, PartialEq, Eq, Default)]
pub enum LenitionType {
    #[default]
    Total,
    Partial,
}

impl LenitionType {
    /// Returns true if the lenition type is [LenitionType::Total].
    pub fn is_total(self) -> bool {
        self == LenitionType::Total
    }
}

/// Something that can be lenited.
pub trait Lenite {
    /// Lenite the thing in place.
    fn lenite(&mut self, lt: LenitionType);
    /// Return a lenited version of oneself.
    fn lenited(mut self) -> Self
    where
        Self: Sized,
    {
        self.lenite(LenitionType::Total);
        self
    }
    /// Return a partially lenited version of oneself.
    fn lenited_partial(mut self) -> Self
    where
        Self: Sized,
    {
        self.lenite(LenitionType::Partial);
        self
    }
}

/// Something that can be eclipsed, provided that we know the [Mgc] following it.
pub trait Eclipse {
    /// Eclipse the thing in place, given that the consonant
    /// immediately following it is `next`.
    ///
    /// If there are no consonants after the thing, then
    /// `next` will be [None].
    fn eclipse(&mut self, next: Option<Mgc>);
    /// Equivalent to `self.eclipse(None)`.
    fn eclipse_terminal(&mut self) {
        self.eclipse(None)
    }
    /// Return an eclipsed version of oneself, assuming that
    /// there is nothing following it.
    fn eclipsed(mut self) -> Self
    where
        Self: Sized,
    {
        self.eclipse_terminal();
        self
    }
}

impl Lenite for Epf {
    // DD-CANDIDATE
    fn lenite(&mut self, lt: LenitionType) {
        *self = match *self {
            Epf::P => Epf::PDot,
            Epf::T => Epf::TDot,
            Epf::D => Epf::DDot,
            Epf::C => Epf::CDot,
            Epf::G => Epf::GDot,
            Epf::F if lt.is_total() => Epf::FDot,
            Epf::V if lt.is_total() => Epf::VDot,
            Epf::Dh if lt.is_total() => Epf::DhDot,
            e => e,
        }
    }
}

impl Lenite for Consonant {
    // DD-CANDIDATE
    fn lenite(&mut self, lt: LenitionType) {
        *self = match *self {
            Consonant::P => Consonant::PDot,
            Consonant::T => Consonant::TDot,
            Consonant::D => Consonant::DDot,
            Consonant::Ch => Consonant::ChDot,
            Consonant::C => Consonant::CDot,
            Consonant::G => Consonant::GDot,
            Consonant::M => Consonant::MDot,
            Consonant::F if lt.is_total() => Consonant::FDot,
            Consonant::V if lt.is_total() => Consonant::VDot,
            Consonant::Dh if lt.is_total() => Consonant::DhDot,
            e => e,
        }
    }
}

impl Lenite for Initial {
    fn lenite(&mut self, lt: LenitionType) {
        match self {
            Initial::Single(c) => c.lenite(lt),
            Initial::R(c) => c.lenite(lt),
            Initial::L(c) => c.lenite(lt),
            _ => (),
        }
    }
}

impl Eclipse for Consonant {
    // DD-CANDIDATE: questionable because of the special rules for ⟦p⟧
    fn eclipse(&mut self, next: Option<Mgc>) {
        *self = match *self {
            Consonant::P => match next {
                Some(Mgc::E | Mgc::I | Mgc::EHat | Mgc::IHat | Mgc::U) => Consonant::Vp,
                _ => Consonant::Mp,
            },
            Consonant::T => Consonant::Dt,
            Consonant::D => Consonant::Nd,
            Consonant::C => Consonant::Gc,
            Consonant::G => Consonant::Ngg,
            Consonant::F => Consonant::Vf,
            Consonant::Th => Consonant::Dhth,
            Consonant::Lh => Consonant::Llh,
            c => c,
        }
    }
}

impl Eclipse for Epf {
    // DD-CANDIDATE: questionable because of the special rules for ⟦p⟧
    fn eclipse(&mut self, next: Option<Mgc>) {
        *self = match *self {
            Epf::P => match next {
                Some(Mgc::E | Mgc::I | Mgc::EHat | Mgc::IHat | Mgc::U) => Epf::Vp,
                _ => Epf::Mp,
            },
            Epf::T => Epf::Dt,
            Epf::D => Epf::Nd,
            Epf::C => Epf::Gc,
            Epf::G => Epf::Ngg,
            Epf::F => Epf::Vf,
            Epf::Th => Epf::Dhth,
            c => c,
        }
    }
}

impl Eclipse for Initial {
    fn eclipse(&mut self, next: Option<Mgc>) {
        match self {
            Initial::Single(c) => c.eclipse(next),
            Initial::R(c) => c.eclipse(Some(Mgc::R)),
            Initial::L(c) => c.eclipse(Some(Mgc::L)),
            Initial::Empty => *self = Initial::Single(Consonant::GEclipse),
            _ => (),
        }
    }
}

impl Epf {
    // DD-CANDIDATE
    fn toggle_lenition(self) -> Self {
        match self {
            Epf::P => Epf::PDot,
            Epf::T => Epf::TDot,
            Epf::D => Epf::DDot,
            Epf::C => Epf::CDot,
            Epf::G => Epf::GDot,
            Epf::F => Epf::FDot,
            Epf::V => Epf::VDot,
            Epf::Dh => Epf::DhDot,
            Epf::PDot => Epf::P,
            Epf::TDot => Epf::T,
            Epf::DDot => Epf::D,
            Epf::CDot => Epf::C,
            Epf::GDot => Epf::G,
            Epf::FDot => Epf::F,
            Epf::VDot => Epf::V,
            Epf::DhDot => Epf::Dh,
            e => e,
        }
    }
}

impl Consonant {
    // DD-CANDIDATE
    fn toggle_lenition(self) -> Self {
        match self {
            Consonant::P => Consonant::PDot,
            Consonant::T => Consonant::TDot,
            Consonant::D => Consonant::DDot,
            Consonant::Ch => Consonant::ChDot,
            Consonant::C => Consonant::CDot,
            Consonant::G => Consonant::GDot,
            Consonant::M => Consonant::MDot,
            Consonant::F => Consonant::FDot,
            Consonant::V => Consonant::VDot,
            Consonant::Dh => Consonant::DhDot,
            Consonant::PDot => Consonant::P,
            Consonant::TDot => Consonant::T,
            Consonant::DDot => Consonant::D,
            Consonant::ChDot => Consonant::Ch,
            Consonant::CDot => Consonant::C,
            Consonant::GDot => Consonant::G,
            Consonant::MDot => Consonant::M,
            Consonant::FDot => Consonant::F,
            Consonant::VDot => Consonant::V,
            Consonant::DhDot => Consonant::Dh,
            e => e,
        }
    }
}

impl Initial {
    /// Toggles lenition of an initial, leniting it if not already lenited
    /// and removing the lenition if already present.
    pub fn toggle_lenition(self) -> Self {
        match self {
            Initial::Single(c) => Initial::Single(c.toggle_lenition()),
            Initial::R(c) => Initial::R(c.toggle_lenition()),
            Initial::L(c) => Initial::L(c.toggle_lenition()),
            _ => self,
        }
    }
}
