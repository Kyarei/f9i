use bitflags::bitflags;
use gridlock::Gridlock;
use proc_macro2::TokenStream;
use quote::quote;
use self_rust_tokenize::SelfRustTokenize;
use smallvec::SmallVec;
use static_assert_macro::static_assert;
use std::{
    fmt::{Display, Error as FmtError, Formatter, Write},
    mem,
};
use typesets::Subtype;

/**
Represents a *manifested grapheme phrase* in Ŋarâþ Crîþ v9.

The codebase assumes particular values for the discriminators. In particular:

* All of the vowels are contiguous; this is also true when looking only at either the hatless or the hatted vowels individually.
* The discriminator of a hatted vowel is 5 more than that of its corresponding hatless vowel.

The discriminants of this enum are contiguous.

 */
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Gridlock, SelfRustTokenize)]
// DD-CANDIDATE
pub enum Mgc {
    /// The morpheme boundary, ⟦-⟧. This is also used as a sentinel value in some places.
    Hyphen,
    // Vowels
    E,
    O,
    A,
    I,
    U,
    EHat,
    OHat,
    AHat,
    /// These four variants represent the hatted vowels, ⟦ê⟧, ⟦ô⟧, ⟦â⟧, and ⟦î⟧.
    IHat,
    // it's j
    J,
    // Plain consonants
    C,
    N,
    /// ⟦ŋ⟧
    Ng,
    V,
    S,
    /// ⟦þ⟧
    Th,
    /// ⟦š⟧
    Sh,
    R,
    L,
    /// ⟦ł⟧
    Lh,
    M,
    F,
    G,
    P,
    T,
    /// ⟦č⟧
    Ch,
    D,
    /// ⟦ð⟧
    Dh,
    H,
    /// ⟦ħ⟧
    Hst,
    // Lenited consonants
    PDot,
    TDot,
    DDot,
    ChDot,
    CDot,
    GDot,
    MDot,
    FDot,
    VDot,
    /// The variants above represent lenited consonants; that is, a consonant followed by a lenition mark, ⟦·⟧.
    DhDot,
    // Eclipsed consonants
    Mp,
    Vp,
    Dt,
    Nd,
    Gc,
    Ngg, // the eclipsed letter ⟦ŋg⟧
    Vf,
    Dhth, // ⟦ðþ⟧
    Llh,  // ⟦lł⟧
    /// The variants above indicate eclipsed consonants. They occur only word-initially.
    GEclipse, // ⟦g⟧ from eclipsis
}

static_assert!(Mgc::COUNT <= 128);

/// The *effective class* of a [manifested grapheme phrase](Mgc).
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum EffectiveClass {
    Plosive,
    Fricative,
    Other,
}

/// Represents the *place of articulation* of a [manifested grapheme phrase](Mgc).
///
/// This is used in consonant cluster simplification.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Poa {
    Labial,
    Coronal,
    Dorsal,
    Other,
}

/// Represents the *manner of articulation* of a [manifested grapheme phrase](Mgc).
///
/// This is used in consonant cluster simplification.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Moa {
    Obstruent,
    Nasal,
    Other,
}

/// Information about a [manifested grapheme phrase](Mgc).
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct MgcProperties {
    pub effective_class: EffectiveClass,
    pub mp_poa: Poa,
    pub mp_moa: Moa,
    /// True if this manifested grapheme phrase is voiced.
    pub voiced: bool,
    /// The sum of the letter values of the letters in this [manifested grapheme phrase](Mgc).
    pub letter_value: u16,
}

impl MgcProperties {
    /// Constructs a new [MgcProperties] object.
    #[inline]
    const fn new(
        effective_class: EffectiveClass,
        mp_poa: Poa,
        mp_moa: Moa,
        voiced: bool,
        letter_value: u16,
    ) -> Self {
        Self {
            effective_class,
            mp_poa,
            mp_moa,
            voiced,
            letter_value,
        }
    }
}

use EffectiveClass::{Other as EC, *};
use Moa::{Other as M, *};
use Poa::{Other as P, *};

// DD-CANDIDATE
#[allow(clippy::identity_op)] // the `+ 0`s are used to indicate the presence of ⟦c⟧s in the mgc
const MGC_PROPERTIES: [MgcProperties; Mgc::COUNT] = [
    MgcProperties::new(EC, P, M, true, 0), // -
    MgcProperties::new(EC, P, M, true, 1), // e
    MgcProperties::new(EC, P, M, true, 4),
    MgcProperties::new(EC, P, M, true, 9),
    MgcProperties::new(EC, P, M, true, 15),
    MgcProperties::new(EC, P, M, true, 19),  // u
    MgcProperties::new(EC, P, M, true, 257), // ê
    MgcProperties::new(EC, P, M, true, 260),
    MgcProperties::new(EC, P, M, true, 265),
    MgcProperties::new(EC, P, M, true, 14),  // î
    MgcProperties::new(EC, P, M, true, 110), // j
    MgcProperties::new(Plosive, Dorsal, Obstruent, false, 0), // c
    MgcProperties::new(EC, Coronal, Nasal, true, 2),
    MgcProperties::new(EC, Dorsal, Nasal, true, 43),
    MgcProperties::new(Fricative, Labial, Obstruent, true, 3),
    MgcProperties::new(Fricative, Coronal, Obstruent, false, 5),
    MgcProperties::new(Fricative, Coronal, Obstruent, false, 85), // þ
    MgcProperties::new(Fricative, Coronal, Obstruent, false, 94),
    MgcProperties::new(EC, Coronal, M, true, 6),
    MgcProperties::new(EC, Coronal, M, true, 7),
    MgcProperties::new(EC, Coronal, Obstruent, false, 119),
    MgcProperties::new(EC, Labial, Nasal, true, 32), // m
    MgcProperties::new(Fricative, Labial, Obstruent, false, 10),
    MgcProperties::new(Plosive, Dorsal, Obstruent, true, 11),
    MgcProperties::new(Plosive, Labial, Obstruent, false, 12),
    MgcProperties::new(Plosive, Coronal, Obstruent, false, 13),
    MgcProperties::new(EC, Coronal, Obstruent, false, 222),
    MgcProperties::new(Plosive, Coronal, Obstruent, true, 16), // d
    MgcProperties::new(Fricative, Coronal, Obstruent, true, 341),
    MgcProperties::new(Fricative, Dorsal, Obstruent, false, 17), // h
    MgcProperties::new(Fricative, P, Obstruent, true, 18),       // ħ
    MgcProperties::new(Plosive, Labial, Obstruent, false, 12),   // p·
    MgcProperties::new(Plosive, Coronal, Obstruent, false, 13),
    MgcProperties::new(Plosive, Coronal, Obstruent, true, 16),
    MgcProperties::new(EC, Coronal, Obstruent, false, 222),
    MgcProperties::new(Plosive, Coronal, Obstruent, false, 0),
    MgcProperties::new(Plosive, P, Obstruent, true, 11),
    MgcProperties::new(EC, Labial, Obstruent, true, 32),
    MgcProperties::new(Fricative, P, M, false, 10),
    MgcProperties::new(Fricative, P, M, false, 3),
    MgcProperties::new(Fricative, P, M, false, 341), // ð·
    MgcProperties::new(Plosive, Labial, Nasal, true, 32 + 12), // mp
    MgcProperties::new(Plosive, Labial, Obstruent, true, 3 + 12),
    MgcProperties::new(Plosive, Coronal, Obstruent, true, 16 + 13),
    MgcProperties::new(Plosive, Coronal, Nasal, true, 2 + 16),
    MgcProperties::new(Plosive, Dorsal, Obstruent, true, 11 + 0),
    MgcProperties::new(Plosive, Dorsal, Nasal, true, 43 + 11),
    MgcProperties::new(Fricative, Labial, Obstruent, true, 3 + 10),
    MgcProperties::new(Fricative, Coronal, Obstruent, true, 341 + 85),
    MgcProperties::new(EC, Coronal, M, true, 7 + 119),
    MgcProperties::new(EC, Dorsal, Obstruent, true, 11), // g-
];

// DD-CANDIDATE
pub const MGC_DISPLAY: [&str; Mgc::COUNT] = [
    "-", "e", "o", "a", "i", "u", "ê", "ô", "â", "î", "j", "c", "n", "ŋ", "v", "s", "þ", "š", "r",
    "l", "ł", "m", "f", "g", "p", "t", "č", "d", "ð", "h", "ħ", "p·", "t·", "d·", "č·", "c·", "g·",
    "m·", "f·", "v·", "ð·", "mp", "vp", "dt", "nd", "gc", "ŋg", "vf", "ðþ", "lł", "g",
];

impl Display for Mgc {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        f.write_str(MGC_DISPLAY[*self as usize])?;
        Ok(())
    }
}

impl Mgc {
    const fn is_between_inclusive(self, lower: Mgc, upper: Mgc) -> bool {
        (self as u8) >= (lower as u8) && (self as u8) <= (upper as u8)
    }
    /// Returns true if this manifested grapheme phrase is a vowel.
    pub const fn is_vowel(self) -> bool {
        self.is_between_inclusive(Mgc::E, Mgc::IHat)
    }
    /// Returns true if this manifested grapheme phrase is a hatless vowel: one of ⟦e⟧, ⟦o⟧, ⟦a⟧, ⟦i⟧, or ⟦u⟧.
    pub const fn is_hatless_vowel(self) -> bool {
        self.is_between_inclusive(Mgc::E, Mgc::U)
    }
    /// Returns true if this manifested grapheme phrase is a hatted vowel: one of ⟦ê⟧, ⟦ô⟧, ⟦â⟧, or ⟦î⟧.
    pub const fn is_hatted_vowel(self) -> bool {
        self.is_between_inclusive(Mgc::EHat, Mgc::IHat)
    }
    /// If `self` is a vowel, then return `Some(v)` where `v` is a vowel with the opposite tone of `self`.
    ///
    /// If `self` is not a vowel, then return `None`. `Mgc::U.invert_tone_opt()` returns `Some(Mgc::U)`.
    pub const fn invert_tone_opt(self) -> Option<Mgc> {
        match self {
            Mgc::E => Some(Mgc::EHat),
            Mgc::O => Some(Mgc::OHat),
            Mgc::A => Some(Mgc::AHat),
            Mgc::I => Some(Mgc::IHat),
            Mgc::U => Some(Mgc::U),
            Mgc::EHat => Some(Mgc::E),
            Mgc::OHat => Some(Mgc::O),
            Mgc::AHat => Some(Mgc::A),
            Mgc::IHat => Some(Mgc::I),
            _ => None,
        }
    }
    /// For vowels, returns an [Mgc] such that two vowels that differ only in tone return the same result.
    ///
    /// This is currently done by making all tones high.
    pub const fn canonize_vowel(self) -> Self {
        match self {
            Mgc::EHat => Mgc::E,
            Mgc::OHat => Mgc::O,
            Mgc::AHat => Mgc::A,
            Mgc::IHat => Mgc::I,
            _ => self,
        }
    }
    /// Returns true if this manifested grapheme phrase is a consonant.
    pub const fn is_consonant(self) -> bool {
        self.is_between_inclusive(Mgc::C, Mgc::GEclipse)
    }
    /// Returns true if this manifested grapheme phrase is a regular consonant.
    pub const fn is_regular_consonant(self) -> bool {
        self.is_between_inclusive(Mgc::C, Mgc::Hst)
    }
    /// Returns true if this manifested grapheme phrase is a lenited consonant.
    pub const fn is_lenited_consonant(self) -> bool {
        self.is_between_inclusive(Mgc::PDot, Mgc::DhDot)
    }
    /// Returns true if this manifested grapheme phrase is an eclipsed consonant.
    pub const fn is_eclipsed_consonant(self) -> bool {
        self.is_between_inclusive(Mgc::Mp, Mgc::GEclipse)
    }
    /// Returns the lenited version of this manifested grapheme phrase, or itself if it is not lenitable.
    // DD-CANDIDATE
    pub const fn lenite_consonant(self) -> Self {
        match self {
            Mgc::P => Mgc::PDot,
            Mgc::T => Mgc::TDot,
            Mgc::D => Mgc::DDot,
            Mgc::Ch => Mgc::ChDot,
            Mgc::C => Mgc::CDot,
            Mgc::G => Mgc::GDot,
            Mgc::M => Mgc::MDot,
            Mgc::F => Mgc::FDot,
            Mgc::V => Mgc::VDot,
            Mgc::Dh => Mgc::DhDot,
            _ => self,
        }
    }
    /// Returns the eclipsed version of this manifested grapheme phrase, or itself if it is not eclipsable.
    // DD-CANDIDATE
    pub const fn eclipse_consonant(self, next: Option<Mgc>) -> Self {
        match self {
            Mgc::P => match next {
                Some(Mgc::E | Mgc::I | Mgc::EHat | Mgc::IHat | Mgc::U) => Mgc::Vp,
                _ => Mgc::Mp,
            },
            Mgc::T => Mgc::Dt,
            Mgc::D => Mgc::Nd,
            Mgc::C => Mgc::Gc,
            Mgc::G => Mgc::Ngg,
            Mgc::F => Mgc::Vf,
            Mgc::Th => Mgc::Dhth,
            Mgc::Lh => Mgc::Llh,
            _ => self,
        }
    }
    /// Given a vowel, returns a vowel with the opposite tone.
    ///
    /// Panicks when given a non-vowel.
    /// `Mgc::U` maps to itself.
    #[inline]
    pub fn invert_tone(self) -> Mgc {
        self.invert_tone_opt().expect("Not a vowel")
    }
    /// Gets the [properties](MgcProperties) of this manifested grapheme phrase.
    pub const fn get_mgc_properties(self) -> MgcProperties {
        MGC_PROPERTIES[self as usize]
    }
    /// Constructs an [Mgc] from its ordinal value.
    ///
    /// # Safety
    /// `code` must represent a valid ordinal for [Mgc].
    #[inline]
    pub unsafe fn from_u8_unchecked(code: u8) -> Mgc {
        mem::transmute(code)
    }
    /// Constructs an [Mgc] from its ordinal value.
    ///
    /// Panicks when given an invalid ordinal.
    #[inline]
    pub fn from_u8(code: u8) -> Mgc {
        assert!(usize::from(code) < Mgc::COUNT);
        unsafe { Mgc::from_u8_unchecked(code) }
    }
}

// Subsets of Mgc

/// A subset of [Mgc] that includes only vowels.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Subtype, SelfRustTokenize)]
#[subtype_of(Mgc)]
// DD-CANDIDATE
pub enum Vowel {
    E,
    O,
    A,
    I,
    U,
    EHat,
    OHat,
    AHat,
    IHat,
}

impl Vowel {
    pub const fn hat(self) -> Self {
        match self {
            Vowel::E => Vowel::EHat,
            Vowel::O => Vowel::OHat,
            Vowel::A => Vowel::AHat,
            Vowel::I => Vowel::IHat,
            v => v,
        }
    }
    pub const fn unhat(self) -> Self {
        match self {
            Vowel::EHat => Vowel::E,
            Vowel::OHat => Vowel::O,
            Vowel::AHat => Vowel::A,
            Vowel::IHat => Vowel::I,
            v => v,
        }
    }
    pub const fn invert(self) -> Self {
        match self {
            Vowel::E => Vowel::EHat,
            Vowel::O => Vowel::OHat,
            Vowel::A => Vowel::AHat,
            Vowel::I => Vowel::IHat,
            Vowel::EHat => Vowel::E,
            Vowel::OHat => Vowel::O,
            Vowel::AHat => Vowel::A,
            Vowel::IHat => Vowel::I,
            Vowel::U => Vowel::U,
        }
    }
    pub const fn pi(self) -> Self {
        match self {
            Vowel::A => Vowel::O,
            Vowel::O => Vowel::E,
            Vowel::E => Vowel::I,
            Vowel::AHat => Vowel::OHat,
            Vowel::OHat => Vowel::EHat,
            Vowel::EHat => Vowel::IHat,
            v => v,
        }
    }
    pub const fn gamma(self) -> Self {
        match self {
            Vowel::A => Vowel::E,
            Vowel::O => Vowel::E,
            Vowel::I => Vowel::E,
            Vowel::E => Vowel::I,
            Vowel::AHat => Vowel::EHat,
            Vowel::OHat => Vowel::EHat,
            Vowel::IHat => Vowel::EHat,
            Vowel::EHat => Vowel::IHat,
            v => v,
        }
    }
    pub const fn lambda(self) -> Self {
        match self {
            Vowel::O => Vowel::A,
            Vowel::I => Vowel::E,
            Vowel::OHat => Vowel::AHat,
            Vowel::IHat => Vowel::EHat,
            v => v,
        }
    }
    pub const fn front(self) -> Self {
        match self {
            Vowel::O => Vowel::A,
            Vowel::OHat => Vowel::AHat,
            v => v,
        }
    }
    pub const fn kappa(self) -> Self {
        match self {
            Vowel::O => Vowel::E,
            Vowel::OHat => Vowel::EHat,
            v => v,
        }
    }
    pub const fn tau(self) -> Self {
        match self {
            Vowel::O | Vowel::I => Vowel::E,
            Vowel::OHat | Vowel::IHat => Vowel::EHat,
            v => v,
        }
    }
    pub const fn phi(self) -> Self {
        match self {
            Vowel::A => Vowel::E,
            Vowel::I => Vowel::A,
            v => v,
        }
    }
    pub const fn psi(self) -> Self {
        match self {
            Vowel::A => Vowel::I,
            Vowel::I => Vowel::O,
            v => v,
        }
    }
    pub const fn to_o(self) -> Self {
        match self {
            Vowel::EHat | Vowel::OHat | Vowel::AHat | Vowel::IHat => Vowel::OHat,
            _ => Vowel::O,
        }
    }
    pub const fn ex_e_i(self) -> Self {
        match self {
            Vowel::E => Vowel::I,
            Vowel::I => Vowel::E,
            Vowel::EHat => Vowel::IHat,
            Vowel::IHat => Vowel::EHat,
            v => v,
        }
    }
    pub const fn has_hat(self) -> bool {
        matches!(self, Vowel::EHat | Vowel::OHat | Vowel::AHat | Vowel::IHat)
    }
}

impl Display for Vowel {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Mgc::from(*self).fmt(f)
    }
}

/// A subset of [Mgc] that includes only consonants.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Subtype, Gridlock, SelfRustTokenize)]
#[subtype_of(Mgc)]
// DD-CANDIDATE
pub enum Consonant {
    C,
    N,
    Ng,
    V,
    S,
    Th,
    Sh,
    R,
    L,
    Lh,
    M,
    F,
    G,
    P,
    T,
    Ch,
    D,
    Dh,
    H,
    Hst,
    PDot,
    TDot,
    DDot,
    ChDot,
    CDot,
    GDot,
    MDot,
    FDot,
    VDot,
    DhDot,
    Mp,
    Vp,
    Dt,
    Nd,
    Gc,
    Ngg,
    Vf,
    Dhth,
    Llh,
    GEclipse,
}

impl Display for Consonant {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Mgc::from(*self).fmt(f)
    }
}

/// A subset of [Mgc] that includes only consonants that can occur in an onset with ⟦r⟧ or ⟦l⟧.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Subtype, Gridlock, SelfRustTokenize)]
#[subtype_of(Consonant)]
// DD-CANDIDATE
pub enum Epf {
    C,
    V,
    S,
    Th,
    Sh,
    F,
    G,
    P,
    T,
    D,
    Dh,
    H,
    Hst,
    PDot,
    TDot,
    DDot,
    CDot,
    GDot,
    FDot,
    VDot,
    DhDot,
    Mp,
    Vp,
    Dt,
    Nd,
    Gc,
    Ngg,
    Vf,
    Dhth,
}

impl Display for Epf {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Consonant::from(*self).fmt(f)
    }
}

impl From<Epf> for Mgc {
    #[inline]
    fn from(e: Epf) -> Self {
        Mgc::from(Consonant::from(e))
    }
}

/// Represents an optional name marker.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, SelfRustTokenize)]
pub enum NameMarker {
    /// Indicates a lack of any name marker.
    None,
    /// The carþ, ⟦#⟧.
    Carth,
    /// The tor, ⟦+⟧.
    Tor,
    /// The njor, ⟦+*⟧.
    Njor,
    /// The es, ⟦@⟧.
    Es,
}

impl Default for NameMarker {
    #[inline]
    fn default() -> Self {
        NameMarker::None
    }
}

impl Display for NameMarker {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        f.write_str(match *self {
            NameMarker::None => "",
            NameMarker::Carth => "#",
            NameMarker::Tor => "+",
            NameMarker::Njor => "+*",
            NameMarker::Es => "@",
        })?;
        Ok(())
    }
}

bitflags! {
    /// Flags for miscellaneous properties of a [Decoration].
    #[derive(Default, Copy, Clone, Eq, PartialEq, Debug, Hash)]
    pub struct DecorationFlags: u8 {
        /// Set if a nef, ⟦*⟧, is present.
        const NEF = 1;
        /// Set if a sen, ⟦&⟧, is present.
        const SEN = 2;
        /// Set if the word is a postclitic.
        const CLITIC = 4;
    }
}

impl SelfRustTokenize for DecorationFlags {
    fn to_tokens(&self) -> TokenStream {
        let underlying = self.bits();
        quote! {
            // safety: underlying is a valid bit pattern for
            // DecorationFlags
            unsafe {
                DecorationFlags::from_bits_unchecked(#underlying)
            }
        }
    }
}

/// Represents a series of markers at the start of a word.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Default, SelfRustTokenize)]
pub struct Decoration {
    pub flags: DecorationFlags,
    /// The name marker present.
    pub name_marker: NameMarker,
}

impl Decoration {
    /// A [Decoration] object that represents the lack of any marker.
    pub const EMPTY: Decoration = Decoration {
        flags: DecorationFlags::empty(),
        name_marker: NameMarker::None,
    };
    pub const fn has_nef(self) -> bool {
        self.flags.contains(DecorationFlags::NEF)
    }
    pub const fn has_sen(self) -> bool {
        self.flags.contains(DecorationFlags::SEN)
    }
    pub const fn is_postclitic(self) -> bool {
        self.flags.contains(DecorationFlags::CLITIC)
    }
}

impl Display for Decoration {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        if self.has_nef() {
            f.write_char('*')?;
        }
        self.name_marker.fmt(f)?;
        if self.has_sen() {
            f.write_char('&')?;
        }
        Ok(())
    }
}

/// A sequence of manifested grapheme phrases.
///
/// This is `SmallVec<[Mgc; 8]>` for now, but might change. Mwahahaha.
pub type Fragment = SmallVec<[Mgc; 8]>;

static_assert!(std::mem::size_of::<Fragment>() == 24);

/// A trait for anything that can be seen as a sequence of manifested grapheme phrases.
pub trait Tokenize {
    /// Appends the contents of this object to the list of MGCs.
    fn append_to_tokens(&self, s: &mut Fragment);
    /// Convenience method for returning the contents of this object as a list of MGCs.
    fn to_tokens(&self) -> Fragment {
        let mut f = Fragment::new();
        self.append_to_tokens(&mut f);
        f
    }
    /// Peeks at the next token.
    fn peek(&self) -> Option<Mgc> {
        let mut frag = Fragment::new();
        self.append_to_tokens(&mut frag);
        frag.first().copied()
    }
    fn tokens_to_string(&self) -> String {
        let mut s = String::new();
        for t in self.to_tokens() {
            s += MGC_DISPLAY[t as usize];
        }
        s
    }
}

impl Tokenize for () {
    fn append_to_tokens(&self, _: &mut Fragment) { /* no tokens to append */
    }
    fn peek(&self) -> Option<Mgc> {
        None
    }
}

impl Tokenize for Mgc {
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.push(*self);
    }
    fn peek(&self) -> Option<Mgc> {
        Some(*self)
    }
}

impl Tokenize for Vowel {
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.push(Mgc::from(*self));
    }
    fn peek(&self) -> Option<Mgc> {
        Some(Mgc::from(*self))
    }
}

impl Tokenize for Consonant {
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.push(Mgc::from(*self));
    }
    fn peek(&self) -> Option<Mgc> {
        Some(Mgc::from(*self))
    }
}

impl Tokenize for Epf {
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.push(Mgc::from(*self));
    }
    fn peek(&self) -> Option<Mgc> {
        Some(Mgc::from(*self))
    }
}

// used for generic code
impl<A> Tokenize for (A,)
where
    A: Tokenize,
{
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.0.append_to_tokens(s);
    }
    fn peek(&self) -> Option<Mgc> {
        self.0.peek()
    }
}

impl<A, B> Tokenize for (A, B)
where
    A: Tokenize,
    B: Tokenize,
{
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.0.append_to_tokens(s);
        self.1.append_to_tokens(s);
    }
    fn peek(&self) -> Option<Mgc> {
        self.0.peek().or_else(|| self.1.peek())
    }
}

impl<A, B, C> Tokenize for (A, B, C)
where
    A: Tokenize,
    B: Tokenize,
    C: Tokenize,
{
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.0.append_to_tokens(s);
        self.1.append_to_tokens(s);
        self.2.append_to_tokens(s);
    }
    fn peek(&self) -> Option<Mgc> {
        self.0
            .peek()
            .or_else(|| self.1.peek())
            .or_else(|| self.2.peek())
    }
}

impl<A, B, C, D> Tokenize for (A, B, C, D)
where
    A: Tokenize,
    B: Tokenize,
    C: Tokenize,
    D: Tokenize,
{
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.0.append_to_tokens(s);
        self.1.append_to_tokens(s);
        self.2.append_to_tokens(s);
        self.3.append_to_tokens(s);
    }
    fn peek(&self) -> Option<Mgc> {
        self.0
            .peek()
            .or_else(|| self.1.peek())
            .or_else(|| self.2.peek())
            .or_else(|| self.3.peek())
    }
}
