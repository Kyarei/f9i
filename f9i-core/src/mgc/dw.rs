use std::default::Default;
use std::fmt::{Debug, Display, Error as FmtError, Formatter, Write};

use crate::mgc::dw_storage::*;
use crate::mgc::simple::*;

/// A word, possibly with markers.
#[derive(Clone, Eq, PartialEq, Hash, Default)]
#[repr(transparent)]
pub struct DecoratedWord {
    pub(crate) storage: DecoratedWordStorage,
}

impl DecoratedWord {
    /// Creates a new decorated word.
    #[inline]
    pub fn new(main: Fragment, decoration: Decoration) -> Self {
        DecoratedWord {
            storage: DecoratedWordStorage::new(main, decoration),
        }
    }

    /// Creates a new decorated word with no letters.
    #[inline]
    pub fn empty(decoration: Decoration) -> Self {
        DecoratedWord {
            storage: DecoratedWordStorage::empty(decoration),
        }
    }

    /// Gets a slice to the markers on this word.
    #[inline]
    pub fn decoration(&self) -> Decoration {
        self.storage.decoration()
    }

    /// Gets a slice to the letters of this word, excluding any of its markers.
    #[inline]
    pub fn letters(&self) -> &[Mgc] {
        self.storage.letters()
    }

    /// Like [DecoratedWord::letters], but gets a mutable slice.
    #[inline]
    pub fn letters_mut(&mut self) -> &mut [Mgc] {
        self.storage.letters_mut()
    }

    #[inline]
    pub fn letters_cloned(&self) -> Fragment {
        Fragment::from_slice(self.letters())
    }

    #[inline]
    pub fn into_parts(self) -> (Fragment, Decoration) {
        let d = self.decoration();
        (self.storage.into_fragment(), d)
    }

    /// Returns the letter sum of this word.
    #[inline]
    pub fn letter_sum(&self) -> u32 {
        Self::letter_sum_u(self.letters(), self.decoration())
    }
    pub fn letter_sum_u(main: &[Mgc], decoration: Decoration) -> u32 {
        let main_sum: u32 = main
            .iter()
            .map(|l| l.get_mgc_properties().letter_value as u32)
            .sum();
        let nef_value: u32 = if decoration.has_nef() { 25 } else { 0 };
        let sen_value: u32 = if decoration.has_sen() { 26 } else { 0 };
        let name_marker_value: u32 = match decoration.name_marker {
            NameMarker::Carth => 20,
            NameMarker::Tor => 21,
            NameMarker::Njor => 22,
            NameMarker::Es => 23,
            NameMarker::None => 0,
        };
        nef_value + name_marker_value + sen_value + main_sum
    }
    /// Lenites a word.
    pub fn lenite_in_place(&mut self) {
        let letters = self.letters_mut();
        let lenite = match letters {
            [Mgc::C, Mgc::F | Mgc::Th | Mgc::S | Mgc::Sh, ..] => false,
            [Mgc::G, Mgc::V | Mgc::Dh, ..] => false,
            [Mgc::T, Mgc::F, ..] => false,
            [Mgc::D, Mgc::V, ..] => false,
            [_, ..] => true,
            _ => false,
        };
        if lenite {
            letters[0] = letters[0].lenite_consonant();
        }
    }
    /// Returns the lenited version of a word.
    #[inline]
    pub fn lenite(mut self) -> Self {
        self.lenite_in_place();
        self
    }
    /// Eclipses a word.
    pub fn eclipse_in_place(&mut self) {
        let letters = self.letters_mut();
        let eclipse = match letters {
            [Mgc::C, Mgc::F | Mgc::Th | Mgc::S | Mgc::Sh, ..] => false,
            [Mgc::G, Mgc::V | Mgc::Dh, ..] => false,
            [Mgc::T, Mgc::F, ..] => false,
            [Mgc::D, Mgc::V, ..] => false,
            [_, ..] => true,
            _ => false,
        };
        if letters[0].is_vowel() {
            self.storage.prepend(Mgc::GEclipse);
        } else if eclipse {
            letters[0] = letters[0].eclipse_consonant(letters.get(1).copied());
        }
    }
    /// Returns an eclipsed version of a word.
    #[inline]
    pub fn eclipse(mut self) -> Self {
        self.eclipse_in_place();
        self
    }
    #[inline]
    pub fn last_letter(&self) -> Option<Mgc> {
        self.letters().last().copied()
    }
    /// Appends a manifested grapheme phrase to a word.
    #[inline]
    pub fn push(&mut self, mgc: Mgc) -> &mut Self {
        self.storage.push(mgc);
        self
    }
    /// Appends a manifested grapheme phrase to a word and returns the new word.
    #[inline]
    pub fn plus(mut self, mgc: Mgc) -> Self {
        self.push(mgc);
        self
    }
    /// If the condition is true, then appends a manifested grapheme phrase to a word and returns the new word.
    /// Otherwise, returns the word unchanged.
    #[inline]
    pub fn plus_if(mut self, cond: bool, mgc: Mgc) -> Self {
        if cond {
            self.push(mgc);
        }
        self
    }
    /// If the given manifested grapheme phrase is not a hyphen, then appends it to a word and returns the new word.
    #[inline]
    pub fn plus_nh(mut self, mgc: Mgc) -> Self {
        if mgc != Mgc::Hyphen {
            self.push(mgc);
        }
        self
    }
    /// Appends a sequence of manifested grapheme phrases to a word.
    #[inline]
    pub fn extend(&mut self, mgcs: &[Mgc]) -> &mut Self {
        self.storage.extend(mgcs);
        self
    }
    /// Appends a sequence of manifested grapheme phrases to a word and returns the new word.
    #[inline]
    pub fn plusx(mut self, mgcs: &[Mgc]) -> Self {
        self.extend(mgcs);
        self
    }
    /// Prepends a manifested grapheme phrase to a word.
    #[inline]
    pub fn prepend(&mut self, mgc: Mgc) -> &mut Self {
        self.storage.prepend(mgc);
        self
    }
    /// Prepends a sequence of manifested grapheme phrases to a word.
    #[inline]
    pub fn prepend_frag(&mut self, mgc: &[Mgc]) -> &mut Self {
        self.storage.prepend_frag(mgc);
        self
    }

    /// Removes all hyphens from a word.
    pub fn remove_hyphens(&mut self) {
        self.storage.remove_hyphens();
    }

    pub fn clone_with_prefix(&self, prefix: &[Mgc]) -> Self {
        Self::new(
            prefix
                .iter()
                .copied()
                .chain(self.letters().iter().copied())
                .collect(),
            self.decoration(),
        )
    }
}

impl Display for DecoratedWord {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        if self.decoration().is_postclitic() {
            f.write_char('’')?;
        }
        let mut mgcs = self.letters().iter();
        if let Some(first_letter) = mgcs.next() {
            let repr = MGC_DISPLAY[*first_letter as usize];
            if first_letter.is_eclipsed_consonant() {
                let mut chars = repr.chars();
                // Doesn't panic because all MGCs are displayed with at least one character
                f.write_char(chars.next().unwrap())?;
                Display::fmt(&self.decoration(), f)?;
                f.write_str(chars.as_str())?;
            } else {
                Display::fmt(&self.decoration(), f)?;
                f.write_str(repr)?;
            }
            for mgc in mgcs {
                Display::fmt(&mgc, f)?;
            }
        } else {
            // Don't know how we get into this, but :shrug: OK
            Display::fmt(&self.decoration(), f)?;
        }
        Ok(())
    }
}

impl Debug for DecoratedWord {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.storage, f)
    }
}
