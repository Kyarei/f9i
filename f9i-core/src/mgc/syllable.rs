use arrayvec::ArrayVec;
use self_rust_tokenize::SelfRustTokenize;
use typesets::Subtype;

use crate::mgc::simple::*;
use gridlock::Gridlock;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::fmt::{Display, Error as FmtError, Formatter, Write};
use std::ops::BitOr;

/// A valid initial of a Ŋarâþ Crîþ v9 syllable.
// DD-CANDIDATE: at least for the stop–fricative clusters
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, SelfRustTokenize)]
pub enum Initial {
    Empty,
    /// A single consonant.
    Single(Consonant),
    /// A consonant plus ⟦r⟧.
    R(Epf),
    /// A consonant plus ⟦l⟧.
    L(Epf),
    Cf,
    Cth,
    Cs,
    Csh,
    Gv,
    Gdh,
    Tf,
    Dv,
}

impl Initial {
    /// Return the [Mgc]s that make up this initial, filling in any empty slots with [Mgc::Hyphen].
    #[deprecated(note = "replaced with as_consonants")]
    pub fn as_mgcs(self) -> [Mgc; 2] {
        match self {
            Initial::Empty => [Mgc::Hyphen; 2],
            Initial::Single(c) => [Mgc::from(c), Mgc::Hyphen],
            Initial::R(c) => [Mgc::from(c), Mgc::R],
            Initial::L(c) => [Mgc::from(c), Mgc::L],
            Initial::Cf => [Mgc::C, Mgc::F],
            Initial::Cth => [Mgc::C, Mgc::Th],
            Initial::Cs => [Mgc::C, Mgc::S],
            Initial::Csh => [Mgc::C, Mgc::Sh],
            Initial::Gv => [Mgc::G, Mgc::V],
            Initial::Gdh => [Mgc::G, Mgc::Dh],
            Initial::Tf => [Mgc::T, Mgc::F],
            Initial::Dv => [Mgc::D, Mgc::V],
        }
    }
    /// Return the [Consonant]s that make up this initial.
    ///
    /// This function returns an [ArrayVec] instead of a slice
    /// reference because it might sometimes need to create a
    /// new slice to hold the data. As a result, this function
    /// should be const but currently is not.
    // DD-CANDIDATE
    #[inline]
    pub fn as_consonants(self) -> ArrayVec<Consonant, 2> {
        match self {
            Initial::Empty => ArrayVec::new_const(),
            Initial::Single(c) => {
                let mut v = ArrayVec::new_const();
                v.push(c);
                v
            }
            Initial::R(c) => ArrayVec::from([Consonant::from(c), Consonant::R]),
            Initial::L(c) => ArrayVec::from([Consonant::from(c), Consonant::L]),
            Initial::Cf => ArrayVec::from([Consonant::C, Consonant::F]),
            Initial::Cth => ArrayVec::from([Consonant::C, Consonant::Th]),
            Initial::Cs => ArrayVec::from([Consonant::C, Consonant::S]),
            Initial::Csh => ArrayVec::from([Consonant::C, Consonant::Sh]),
            Initial::Gv => ArrayVec::from([Consonant::G, Consonant::V]),
            Initial::Gdh => ArrayVec::from([Consonant::G, Consonant::Dh]),
            Initial::Tf => ArrayVec::from([Consonant::T, Consonant::F]),
            Initial::Dv => ArrayVec::from([Consonant::D, Consonant::V]),
        }
    }
    // DD-CANDIDATE
    pub fn append_to_tokens(self, s: &mut Fragment) {
        match self {
            Initial::Empty => (),
            Initial::Single(c) => s.push(Mgc::from(c)),
            Initial::R(c) => {
                s.push(Mgc::from(c));
                s.push(Mgc::R);
            }
            Initial::L(c) => {
                s.push(Mgc::from(c));
                s.push(Mgc::L);
            }
            Initial::Cf => s.extend_from_slice(&[Mgc::C, Mgc::F]),
            Initial::Cth => s.extend_from_slice(&[Mgc::C, Mgc::Th]),
            Initial::Cs => s.extend_from_slice(&[Mgc::C, Mgc::S]),
            Initial::Csh => s.extend_from_slice(&[Mgc::C, Mgc::Sh]),
            Initial::Gv => s.extend_from_slice(&[Mgc::G, Mgc::V]),
            Initial::Gdh => s.extend_from_slice(&[Mgc::G, Mgc::Dh]),
            Initial::Tf => s.extend_from_slice(&[Mgc::T, Mgc::F]),
            Initial::Dv => s.extend_from_slice(&[Mgc::D, Mgc::V]),
        }
    }
    // DD-CANDIDATE
    pub fn enumerate(include_empty: bool) -> impl Iterator<Item = Self> {
        let init: &[Initial] = if include_empty {
            &[Initial::Empty]
        } else {
            &[]
        };
        init.iter()
            .copied()
            .chain(Consonant::VALUES.iter().map(|c| Initial::Single(*c)))
            .chain(Epf::VALUES.iter().map(|c| Initial::R(*c)))
            .chain(Epf::VALUES.iter().map(|c| Initial::L(*c)))
            .chain(
                [
                    Initial::Cf,
                    Initial::Cth,
                    Initial::Cs,
                    Initial::Csh,
                    Initial::Gv,
                    Initial::Gdh,
                    Initial::Tf,
                    Initial::Dv,
                ]
                .iter()
                .copied(),
            )
    }
    // DD-CANDIDATE
    pub fn enumerate_without_eclipse() -> impl Iterator<Item = Self> {
        [Initial::Empty]
            .iter()
            .copied()
            .chain(Consonant::VALUES[..30].iter().map(|c| Initial::Single(*c)))
            .chain(Epf::VALUES[..21].iter().map(|c| Initial::R(*c)))
            .chain(Epf::VALUES[..21].iter().map(|c| Initial::L(*c)))
            .chain(
                [
                    Initial::Cf,
                    Initial::Cth,
                    Initial::Cs,
                    Initial::Csh,
                    Initial::Gv,
                    Initial::Gdh,
                    Initial::Tf,
                    Initial::Dv,
                ]
                .iter()
                .copied(),
            )
    }
    // DD-CANDIDATE
    pub fn enumerate_base() -> impl Iterator<Item = Self> {
        std::iter::once(Initial::Empty)
            .chain(Consonant::VALUES[..20].iter().map(|c| Initial::Single(*c)))
            .chain(Epf::VALUES[..13].iter().map(|c| Initial::R(*c)))
            .chain(Epf::VALUES[..13].iter().map(|c| Initial::L(*c)))
            .chain(
                [
                    Initial::Cf,
                    Initial::Cth,
                    Initial::Cs,
                    Initial::Csh,
                    Initial::Gv,
                    Initial::Gdh,
                    Initial::Tf,
                    Initial::Dv,
                ]
                .iter()
                .copied(),
            )
    }
    /// Return true if this initial is lenited.
    // DD-CANDIDATE
    #[inline]
    pub fn is_lenited(self) -> bool {
        match self {
            Initial::Single(c) => Mgc::from(c).is_lenited_consonant(),
            Initial::R(c) => Mgc::from(c).is_lenited_consonant(),
            Initial::L(c) => Mgc::from(c).is_lenited_consonant(),
            _ => false,
        }
    }
}

impl From<Consonant> for Initial {
    #[inline]
    fn from(c: Consonant) -> Self {
        Initial::Single(c)
    }
}

impl From<Epf> for Initial {
    #[inline]
    fn from(c: Epf) -> Self {
        Initial::Single(c.into())
    }
}

impl Tokenize for Initial {
    #[inline]
    fn append_to_tokens(&self, s: &mut Fragment) {
        (*self).append_to_tokens(s);
    }
}

impl Display for Initial {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mgcs = self.as_mgcs();
        for m in mgcs {
            if m != Mgc::Hyphen {
                Display::fmt(&m, f)?;
            }
        }
        Ok(())
    }
}

/// An optional *glide*: either empty or ⟦j⟧.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, SelfRustTokenize)]
pub enum Glide {
    None,
    J,
}

impl Glide {
    pub const fn for_vowel(v: Vowel) -> Self {
        match v {
            Vowel::EHat | Vowel::IHat | Vowel::E | Vowel::I => Glide::J,
            _ => Glide::None,
        }
    }
}

impl Tokenize for Glide {
    #[inline]
    fn append_to_tokens(&self, s: &mut Fragment) {
        match self {
            Glide::None => (),
            Glide::J => s.push(Mgc::J),
        }
    }
}

impl Display for Glide {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Glide::None => Ok(()),
            Glide::J => f.write_char('j'),
        }
    }
}

impl BitOr for Glide {
    type Output = Glide;

    /// Returns [Glide::J] if either of the operands is [Glide::J]; that is,
    /// coalesces ⟦j⟧s together.
    #[inline]
    fn bitor(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Glide::None, Glide::None) => Glide::None,
            _ => Glide::J,
        }
    }
}

/// An *onset*: this includes both the initial and the optional ⟦j⟧.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct Onset {
    pub initial: Initial,
    pub glide: Glide,
}

impl Onset {
    pub const fn of(initial: Initial) -> Self {
        Onset {
            initial,
            glide: Glide::None,
        }
    }
    pub const fn with_j(initial: Initial) -> Self {
        Onset {
            initial,
            glide: Glide::J,
        }
    }
    pub const fn new(initial: Initial, glide: Glide) -> Self {
        Onset { initial, glide }
    }
}

impl Tokenize for Onset {
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.initial.append_to_tokens(s);
        self.glide.append_to_tokens(s);
    }
}

impl Display for Onset {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.initial, f)?;
        Display::fmt(&self.glide, f)
    }
}

impl From<Initial> for Onset {
    #[inline]
    fn from(i: Initial) -> Self {
        Onset::of(i)
    }
}

impl From<Consonant> for Onset {
    #[inline]
    fn from(i: Consonant) -> Self {
        Onset::of(Initial::Single(i))
    }
}

impl From<Epf> for Onset {
    #[inline]
    fn from(i: Epf) -> Self {
        Onset::of(Initial::Single(Consonant::from(i)))
    }
}

/// A valid coda of a Ŋarâþ Crîþ v9 syllable.
///
/// This does not include ⟦m⟧, since this coda is used only in a handful of function words.
// DD-CANDIDATE
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash, Gridlock, SelfRustTokenize)]
pub enum Coda {
    Empty,
    S,
    R,
    N,
    Th,
    RTh,
    L,
    T,
    C,
    F,
    Lh,
    St,
    Lt,
    Lht,
    Ns,
    Ls,
    NTh,
    CTh,
}

impl Coda {
    /// Gets the [Mgc]s corresponding to the coda as a slice.
    #[deprecated(note = "Replaced by as_consonant_slice")]
    pub const fn as_mgc_slice(self) -> &'static [Mgc] {
        match self {
            Coda::Empty => &[],
            Coda::S => &[Mgc::S],
            Coda::R => &[Mgc::R],
            Coda::N => &[Mgc::N],
            Coda::Th => &[Mgc::Th],
            Coda::RTh => &[Mgc::R, Mgc::Th],
            Coda::L => &[Mgc::L],
            Coda::T => &[Mgc::T],
            Coda::C => &[Mgc::C],
            Coda::F => &[Mgc::F],
            Coda::St => &[Mgc::S, Mgc::T],
            Coda::Lt => &[Mgc::L, Mgc::T],
            Coda::Ns => &[Mgc::N, Mgc::S],
            Coda::Ls => &[Mgc::L, Mgc::S],
            Coda::NTh => &[Mgc::N, Mgc::Th],
            Coda::CTh => &[Mgc::C, Mgc::Th],
            Coda::Lh => &[Mgc::Lh],
            Coda::Lht => &[Mgc::Lh, Mgc::T],
        }
    }
    /// Gets the [Consonant]s corresponding to the coda as a slice.
    // DD-CANDIDATE
    pub const fn as_consonant_slice(self) -> &'static [Consonant] {
        match self {
            Coda::Empty => &[],
            Coda::S => &[Consonant::S],
            Coda::R => &[Consonant::R],
            Coda::N => &[Consonant::N],
            Coda::Th => &[Consonant::Th],
            Coda::RTh => &[Consonant::R, Consonant::Th],
            Coda::L => &[Consonant::L],
            Coda::T => &[Consonant::T],
            Coda::C => &[Consonant::C],
            Coda::F => &[Consonant::F],
            Coda::St => &[Consonant::S, Consonant::T],
            Coda::Lt => &[Consonant::L, Consonant::T],
            Coda::Ns => &[Consonant::N, Consonant::S],
            Coda::Ls => &[Consonant::L, Consonant::S],
            Coda::NTh => &[Consonant::N, Consonant::Th],
            Coda::CTh => &[Consonant::C, Consonant::Th],
            Coda::Lh => &[Consonant::Lh],
            Coda::Lht => &[Consonant::Lh, Consonant::T],
        }
    }
    /// Gets the maximal simple coda at the beginning of this coda.
    // DD-CANDIDATE
    pub const fn truncated(self) -> SimpleCoda {
        match self {
            Coda::Empty => SimpleCoda::Empty,
            Coda::S => SimpleCoda::S,
            Coda::R => SimpleCoda::R,
            Coda::N => SimpleCoda::N,
            Coda::Th => SimpleCoda::Th,
            Coda::RTh => SimpleCoda::RTh,
            Coda::L => SimpleCoda::L,
            Coda::T => SimpleCoda::T,
            Coda::C => SimpleCoda::C,
            Coda::F => SimpleCoda::F,
            Coda::St => SimpleCoda::S,
            Coda::Lt => SimpleCoda::L,
            Coda::Ns => SimpleCoda::N,
            Coda::Ls => SimpleCoda::L,
            Coda::NTh => SimpleCoda::N,
            Coda::CTh => SimpleCoda::CTh,
            Coda::Lh => SimpleCoda::Lh,
            Coda::Lht => SimpleCoda::Lh,
        }
    }

    // DD-CANDIDATE
    const fn as_str(&self) -> &'static str {
        match self {
            Coda::Empty => "",
            Coda::S => "s",
            Coda::R => "r",
            Coda::N => "n",
            Coda::Th => "þ",
            Coda::RTh => "rþ",
            Coda::L => "l",
            Coda::T => "t",
            Coda::C => "c",
            Coda::F => "f",
            Coda::St => "st",
            Coda::Lt => "lt",
            Coda::Ns => "ns",
            Coda::Ls => "ls",
            Coda::NTh => "nþ",
            Coda::CTh => "cþ",
            Coda::Lh => "ł",
            Coda::Lht => "łt",
        }
    }

    pub const fn final_consonant(self) -> Option<Consonant> {
        self.as_consonant_slice().last().copied()
    }
}

impl Display for Coda {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

/// A subtype of [Coda] that contains only the simple codas.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Subtype, Gridlock, SelfRustTokenize)]
#[subtype_of(Coda)]
#[repr(u8)]
// DD-CANDIDATE
pub enum SimpleCoda {
    Empty,
    S,
    R,
    N,
    Th,
    RTh,
    L,
    T,
    C,
    F,
    Lh,
    CTh,
}

impl SimpleCoda {
    #[inline]
    pub fn final_consonant(self) -> Option<Consonant> {
        Coda::from(self).final_consonant()
    }

    /// Gets the [Consonant]s corresponding to the coda as a slice.
    #[inline]
    pub fn as_consonant_slice(self) -> &'static [Consonant] {
        Coda::from(self).as_consonant_slice()
    }
}

impl Display for SimpleCoda {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&Coda::from(*self), f)
    }
}

/// A common type for codas.
pub trait TCoda: Sized + Tokenize {
    /// Extracts a maximal coda from the end of a word, returning the coda and the unparsed prefix.
    fn extract(letters: &[Mgc]) -> (Self, &[Mgc]);
}

impl Tokenize for Coda {
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.extend_from_slice(self.as_mgc_slice());
    }
}

impl TCoda for Coda {
    // DD-CANDIDATE
    fn extract(letters: &[Mgc]) -> (Self, &[Mgc]) {
        match letters {
            [r @ .., Mgc::R, Mgc::Th] => (Coda::RTh, r),
            [r @ .., Mgc::N, Mgc::Th] => (Coda::NTh, r),
            [r @ .., Mgc::C, Mgc::Th] => (Coda::CTh, r),
            [r @ .., Mgc::S, Mgc::T] => (Coda::St, r),
            [r @ .., Mgc::L, Mgc::T] => (Coda::Lt, r),
            [r @ .., Mgc::Lh, Mgc::T] => (Coda::Lht, r),
            [r @ .., Mgc::N, Mgc::S] => (Coda::Ns, r),
            [r @ .., Mgc::L, Mgc::S] => (Coda::Ls, r),
            [r @ .., Mgc::S] => (Coda::S, r),
            [r @ .., Mgc::R] => (Coda::R, r),
            [r @ .., Mgc::N] => (Coda::N, r),
            [r @ .., Mgc::Th] => (Coda::Th, r),
            [r @ .., Mgc::L] => (Coda::L, r),
            [r @ .., Mgc::T] => (Coda::T, r),
            [r @ .., Mgc::C] => (Coda::C, r),
            [r @ .., Mgc::F] => (Coda::F, r),
            [r @ .., Mgc::Lh] => (Coda::Lh, r),
            _ => (Coda::Empty, letters),
        }
    }
}

impl Tokenize for SimpleCoda {
    fn append_to_tokens(&self, s: &mut Fragment) {
        Coda::from(*self).append_to_tokens(s);
    }
}

impl TCoda for SimpleCoda {
    // DD-CANDIDATE
    fn extract(letters: &[Mgc]) -> (Self, &[Mgc]) {
        match letters {
            [r @ .., Mgc::R, Mgc::Th] => (SimpleCoda::RTh, r),
            [r @ .., Mgc::C, Mgc::Th] => (SimpleCoda::CTh, r),
            [r @ .., Mgc::S] => (SimpleCoda::S, r),
            [r @ .., Mgc::R] => (SimpleCoda::R, r),
            [r @ .., Mgc::N] => (SimpleCoda::N, r),
            [r @ .., Mgc::Th] => (SimpleCoda::Th, r),
            [r @ .., Mgc::L] => (SimpleCoda::L, r),
            [r @ .., Mgc::T] => (SimpleCoda::T, r),
            [r @ .., Mgc::C] => (SimpleCoda::C, r),
            [r @ .., Mgc::F] => (SimpleCoda::F, r),
            [r @ .., Mgc::Lh] => (SimpleCoda::Lh, r),
            _ => (SimpleCoda::Empty, letters),
        }
    }
}

/// A medial-nucleus pair in a syllable.
///
/// Despite its name, this structure contains both the medial and the nucleus.
/// If you do not want the medial, then use [Vowel] instead.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Nucleus {
    /// True if this nucleus contains a ⟦j⟧.
    pub glide: Glide,
    /// The vowel of this nucleus.
    pub vowel: Vowel,
}

impl Nucleus {
    pub const A: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::A,
    };
    pub const E: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::E,
    };
    pub const I: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::I,
    };
    pub const O: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::O,
    };
    pub const U: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::U,
    };
    pub const AHAT: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::AHat,
    };
    pub const EHAT: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::EHat,
    };
    pub const IHAT: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::IHat,
    };
    pub const OHAT: Nucleus = Nucleus {
        glide: Glide::None,
        vowel: Vowel::OHat,
    };
    pub const JA: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::A,
    };
    pub const JE: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::E,
    };
    pub const JO: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::O,
    };
    pub const JAHAT: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::AHat,
    };
    pub const JEHAT: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::EHat,
    };
    pub const JOHAT: Nucleus = Nucleus {
        glide: Glide::J,
        vowel: Vowel::OHat,
    };

    /// Returns a nucleus with a glide and a vowel.
    ///
    /// This is preferred over calling the raw constructor of [Nucleus] because
    /// it prevents forbidden combinations such as ⟦ji⟧, ⟦jî⟧, and ⟦ju⟧.
    pub const fn new(glide: Glide, vowel: Vowel) -> Self {
        match vowel {
            Vowel::I | Vowel::IHat | Vowel::U => Nucleus {
                glide: Glide::None,
                vowel,
            },
            _ => Nucleus { glide, vowel },
        }
    }
    /// Returns an array of manifested grapheme phrases corresponding to this nucleus.
    ///
    /// Any excess space is padded with [Mgc::Hyphen].
    #[inline]
    // This should be able to be const, but it currently isn't because it uses
    // a trait method and const trait impls are not yet stable.
    pub fn as_mgcs(self) -> [Mgc; 2] {
        match self.glide {
            Glide::None => [self.vowel.into(), Mgc::Hyphen],
            Glide::J => [Mgc::J, self.vowel.into()],
        }
    }
    /// Returns a nucleus consisting only of a vowel.
    pub const fn of(vowel: Vowel) -> Self {
        Self {
            glide: Glide::None,
            vowel,
        }
    }
    /// Returns the result of applying the *ξ*-transformation on the nucleus.
    pub const fn xi(self) -> Self {
        match self {
            Nucleus {
                glide: Glide::J,
                vowel: Vowel::A | Vowel::E | Vowel::I | Vowel::O | Vowel::U,
            } => Nucleus::JO,
            Nucleus {
                glide: Glide::J,
                vowel: Vowel::AHat | Vowel::EHat | Vowel::IHat | Vowel::OHat,
            } => Nucleus::JOHAT,
            Nucleus {
                vowel: Vowel::E | Vowel::I,
                ..
            } => Nucleus::JO,
            Nucleus {
                vowel: Vowel::EHat | Vowel::IHat,
                ..
            } => Nucleus::JOHAT,
            Nucleus {
                vowel: Vowel::A | Vowel::O | Vowel::U,
                ..
            } => Nucleus::O,
            Nucleus {
                vowel: Vowel::AHat | Vowel::OHat,
                ..
            } => Nucleus::OHAT,
        }
    }
    /// Returns the result of applying the *η*-transformation on the nucleus.
    pub const fn eta(self) -> Self {
        Nucleus {
            glide: self.glide,
            vowel: self.vowel.hat(),
        }
    }
}

impl Tokenize for Nucleus {
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.glide.append_to_tokens(s);
        s.push(self.vowel.into());
    }
}

impl Display for Nucleus {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        Display::fmt(&self.glide, f)?;
        Into::<Mgc>::into(self.vowel).fmt(f)
    }
}

/// A valid syllable in Ŋarâþ Crîþ.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct TSyllable<C: TCoda> {
    pub initial: Initial,
    pub nucleus: Nucleus,
    pub coda: C,
}

impl<C: TCoda> Tokenize for TSyllable<C> {
    /// Appends the [Mgc]s making up this syllable to a [Fragment].
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.initial.append_to_tokens(s);
        self.nucleus.append_to_tokens(s);
        self.coda.append_to_tokens(s);
    }
}

/// A syllable with a simple or complex coda.
pub type Syllable = TSyllable<Coda>;
/// A syllable with a simple coda.
pub type SimpleSyllable = TSyllable<SimpleCoda>;

impl From<SimpleSyllable> for Syllable {
    fn from(s: SimpleSyllable) -> Self {
        Syllable {
            initial: s.initial,
            nucleus: s.nucleus,
            coda: s.coda.into(),
        }
    }
}

impl TryFrom<Syllable> for SimpleSyllable {
    type Error = <SimpleCoda as TryFrom<Coda>>::Error;

    fn try_from(s: Syllable) -> Result<Self, Self::Error> {
        Ok(SimpleSyllable {
            initial: s.initial,
            nucleus: s.nucleus,
            coda: s.coda.try_into()?,
        })
    }
}

/// A rime, along with its glide.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct RimeWithGlide<C: TCoda> {
    pub glide: Glide,
    pub vowel: Vowel,
    pub coda: C,
}

impl<C: TCoda> Tokenize for RimeWithGlide<C> {
    fn append_to_tokens(&self, s: &mut Fragment) {
        self.glide.append_to_tokens(s);
        s.push(self.vowel.into());
        self.coda.append_to_tokens(s);
    }
}
