//! The fourth declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::{fuse_d, fuse_with_eps_d};
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::vc2x;
use f9i_core::assemblage::{AStem, AStemRef, Decorated, ParsedWord};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{Coda, FusionConsonant, Glide, Initial, Phrase, Vowel};
use f9i_ivm_macro::{f, x};

use super::base::*;

/// Data to describe a fourth-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionIV {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The L stem.
    pub l: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The main thematic vowel.
    pub theta: Vowel,
    /// The locative vowel.
    pub lambda: Vowel,
}

impl Declension for DeclensionIV {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
        let n_t = fuse_d(self.n.clone(), FusionConsonant::T);
        let c_d = get_c_d(&self.l);
        let phi_n = GenCons::for_stem(&self.n);
        let phi_l = GenCons::for_stem(&self.l);

        let nom_dir = fuse_with_eps_d(self.n.clone());
        let letter_sum = nom_dir.to_dw().letter_sum();
        let (ace, deuce) = if self.theta == Vowel::O {
            (Vowel::O, Vowel::E)
        } else {
            let i = letter_sum.rem_euclid(6) as usize;
            [
                (Vowel::E, Vowel::I),
                (Vowel::A, Vowel::I),
                (Vowel::I, Vowel::A),
                (Vowel::A, Vowel::I),
                (Vowel::I, Vowel::E),
                (Vowel::E, Vowel::I),
            ][i]
        };

        move |case, number| match (case, number) {
            (Case::Nominative, NNumber::Direct) => nom_dir.clone(),
            (Case::Nominative, NNumber::Dual) => self.n.clone() + x!("ec"),
            (Case::Nominative, NNumber::Plural) => self.n.clone() + vc2x(self.theta, Coda::R),
            (Case::Nominative, NNumber::Singulative) => self.n.clone() + vc2x(deuce, Coda::N),
            (Case::Nominative, NNumber::Generic) => self.n.clone() + vc2x(Vowel::A, phi_n.into()),
            (Case::Accusative, NNumber::Direct) => self.n.clone() + vc2x(ace, Coda::N),
            (Case::Accusative, NNumber::Dual) => n_t.clone() + vc2x(ace, Coda::N),
            (Case::Accusative, NNumber::Plural) => self.n.clone() + x!("as"),
            (Case::Accusative, NNumber::Singulative) => self.n.clone() + vc2x(ace, Coda::NTh),
            (Case::Accusative, NNumber::Generic) => {
                self.n.clone() + f!("a") + Initial::from(phi_n) + x!("en")
            }
            (Case::Dative, NNumber::Direct) => {
                self.n.clone()
                    + match self.n.frag.view() {
                        AStemRef {
                            remainder: (_, Glide::J),
                            ..
                        } => x!("es"),
                        AStemRef {
                            tiles: [.., (_, _, Vowel::I | Vowel::IHat, _)],
                            ..
                        } => x!("e"),
                        _ => x!("i"),
                    }
            }
            (Case::Dative, NNumber::Dual) => self.n.clone() + x!("ic"),
            (Case::Dative, NNumber::Plural) => self.n.clone() + x!("ir"),
            (Case::Dative, NNumber::Singulative) => self.n.clone() + x!("ên"),
            (Case::Dative, NNumber::Generic) => {
                self.n.clone() + f!("a") + Initial::from(phi_n) + x!("es")
            }
            (Case::Genitive, NNumber::Direct) => self.n.clone() + x!("a"),
            (Case::Genitive, NNumber::Dual) => self.n.clone() + x!("ac"),
            (Case::Genitive, NNumber::Plural) => self.n.clone() + x!("o"),
            (Case::Genitive, NNumber::Singulative) => self.n.clone() + x!("ân"),
            (Case::Genitive, NNumber::Generic) => n_n.clone() + vc2x(Vowel::E, phi_n.into()),
            (Case::Locative, _) => locative_i(&self.l, self.lambda, number, phi_l),
            (Case::Instrumental, _) => instrumental_i(&self.l, c_d, number, phi_l),
            (Case::Abessive, _) => abessive_i(&self.l, c_d, number, phi_l),
            (Case::Semblative, _) => {
                if self.theta == Vowel::O {
                    semblative_t(&self.s, number)
                } else {
                    semblative_c(&self.s, self.theta, number)
                }
            }
        }
    }

    fn get_headword_inner(
        &self,
        _clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: vec![
                decl(Case::Nominative, NNumber::Direct),
                decl(Case::Accusative, NNumber::Direct),
                decl(Case::Locative, NNumber::Direct),
                decl(Case::Semblative, NNumber::Direct),
            ],
            exhaustive: true,
        }
    }
}
