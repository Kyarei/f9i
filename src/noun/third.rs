//! The third declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::fuse_d;
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::vc2x;
use f9i_core::assemblage::{AStem, AStemRef, Decorated, ParsedWord};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{Coda, Consonant, FusionConsonant, Glide, Initial, Phrase, SimpleCoda, Vowel};
use f9i_ivm_macro::x;

use super::base::*;

/// Describes the nominative ending to use for a first-declension noun.
#[non_exhaustive]
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum DeclensionIIIKind {
    /// ⟦-os⟧ nouns.
    Os,
    /// ⟦-on⟧ nouns.
    On,
    /// ⟦-or⟧ nouns.
    Or,
    /// ⟦-el⟧ nouns.
    El {
        a: Decorated<AStem>,
        g: Decorated<AStem>,
    },
}

impl DeclensionIIIKind {
    fn coda(&self) -> SimpleCoda {
        match self {
            DeclensionIIIKind::Os => SimpleCoda::S,
            DeclensionIIIKind::On => SimpleCoda::N,
            DeclensionIIIKind::Or => SimpleCoda::R,
            DeclensionIIIKind::El { .. } => SimpleCoda::L,
        }
    }

    fn coda_ca_du(&self) -> Coda {
        match self {
            DeclensionIIIKind::Os => Coda::CTh,
            DeclensionIIIKind::On | DeclensionIIIKind::Or => Coda::C,
            DeclensionIIIKind::El { .. } => Coda::Ls,
        }
    }

    fn vowel(&self) -> Vowel {
        match self {
            DeclensionIIIKind::Os | DeclensionIIIKind::On | DeclensionIIIKind::Or => Vowel::O,
            DeclensionIIIKind::El { .. } => Vowel::E,
        }
    }

    fn is_el(&self) -> bool {
        matches!(self, DeclensionIIIKind::El { .. })
    }
}

fn ends_with_l(stem: &Decorated<AStem>) -> bool {
    stem.frag.remainder.1 == Glide::None
        && matches!(
            stem.frag.remainder.0,
            Initial::Single(Consonant::L) | Initial::L(_)
        )
}

fn ends_with_l_bridge(stem: &Decorated<AStem>) -> bool {
    stem.frag.remainder.1 == Glide::None
        && stem.frag.remainder.0 == Initial::Single(Consonant::L)
        && stem.frag.last_tile().3 == SimpleCoda::Empty
}

/// Data to describe a first-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionIII {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The L stem.
    pub l: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The ending type.
    pub kind: DeclensionIIIKind,
}

impl Declension for DeclensionIII {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let kind = &self.kind;
        let (cja_l, thja_l) = istems(&self.l);
        let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
        let n_t = fuse_d(self.n.clone(), FusionConsonant::T);
        let n_th = fuse_d(self.n.clone(), FusionConsonant::Th);
        let phi_l = GenCons::for_stem(&self.l);
        let a_or_n = match kind {
            DeclensionIIIKind::El { a, .. } => a,
            _ => &self.n,
        };

        let npr = if let AStemRef {
            tiles: [ts @ .., (init, glide, Vowel::I, SimpleCoda::Empty)],
            remainder: (Initial::Empty, Glide::None),
        } = self.n.frag.view()
        {
            Some(Decorated {
                frag: AStem {
                    tiles: ts.into(),
                    remainder: (*init, *glide),
                },
                decoration: self.n.decoration,
            })
        } else {
            None
        };

        move |case, number| {
            match (case, number) {
            (Case::Nominative, NNumber::Direct) => {
                self.n.clone() + vc2x(self.kind.vowel(), self.kind.coda().into())
            }
            (Case::Nominative, NNumber::Dual) => match &npr {
                Some(npr) => npr.clone() + x!("ice"),
                None => self.n.clone() + x!("oc"),
            },
            (Case::Nominative, NNumber::Plural) => match (&npr, kind) {
                (Some(npr), DeclensionIIIKind::Or) => npr.clone() + x!("esôr"),
                (Some(npr), _) => npr.clone() + x!("ia"),
                (None, DeclensionIIIKind::Or) => self.n.clone() + x!("osôr"),
                (None, _) => self.n.clone() + x!("or"),
            },
            (Case::Nominative, NNumber::Singulative) => match (&npr, kind) {
                (Some(npr), DeclensionIIIKind::Or | DeclensionIIIKind::El { .. }) => {
                    npr.clone() + x!("ines")
                }
                (Some(npr), _) => npr.clone() + x!("ien"),
                (None, DeclensionIIIKind::Or | DeclensionIIIKind::El { .. }) => {
                    self.n.clone() + x!("ons")
                }
                (None, _) => self.n.clone() + x!("oren"),
            },
            (Case::Nominative, NNumber::Generic) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { a, .. }) => a.clone() + x!("ul"),
                (Some(npr), _) => npr.clone() + x!("iva"),
                (None, _) => self.n.clone() + x!("u"),
            },
            (Case::Accusative, NNumber::Direct) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { a, .. }) => a.clone() + x!("en"),
                (None, DeclensionIIIKind::On) => self.n.clone() + x!("anon"),
                (Some(npr), DeclensionIIIKind::On) => npr.clone() + x!("enon"),
                _ => self.n.clone() + x!("on"),
            },
            (Case::Accusative, NNumber::Dual) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { a, .. }) => {
                    fuse_d(a.clone(), FusionConsonant::T) + x!("en")
                }
                (Some(npr), DeclensionIIIKind::On) => npr.clone() + x!("iaþ"),
                (Some(npr), _) => npr.clone() + x!("eton"),
                (None, DeclensionIIIKind::On) => self.n.clone() + x!("anor"),
                (None, _) => n_t.clone() + x!("on"),
            },
            (Case::Accusative, NNumber::Plural) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { a, .. }) => a.clone() + x!("on"),
                (Some(npr), DeclensionIIIKind::On) => npr.clone() + x!("iaþ"),
                (Some(npr), _) => npr.clone() + x!("eþon"),
                (None, DeclensionIIIKind::On) => self.n.clone() + x!("anor"),
                (None, _) => n_th.clone() + x!("on"),
            },
            (Case::Accusative, NNumber::Singulative) => self.n.clone() + x!("elt"),
            (Case::Accusative, NNumber::Generic) => a_or_n.clone() + x!("an"),
            (Case::Dative, NNumber::Direct) => match (&npr, kind) {
                (Some(npr), kind) if !kind.is_el() => npr.clone() + x!("isa"),
                _ => a_or_n.clone() + x!("oþ"),
            },
            (Case::Dative, NNumber::Dual) => match (&npr, kind) {
                (Some(npr), DeclensionIIIKind::El { .. }) => {
                    fuse_d(npr.clone(), FusionConsonant::T) + x!("eþ")
                }
                (Some(npr), _) => npr.clone() + x!("ista"),
                _ => n_t.clone() + x!("oþ"),
            },
            (Case::Dative, NNumber::Plural) => match (&npr, kind) {
                (Some(npr), kind) if !kind.is_el() => npr.clone() + x!("esor"),
                _ => a_or_n.clone() + x!("asor"),
            },
            (Case::Dative, NNumber::Singulative) => match (&npr, kind) {
                (Some(npr), DeclensionIIIKind::Or | DeclensionIIIKind::El { .. }) => {
                    npr.clone() + x!("ineþ")
                }
                (Some(npr), _) => npr.clone() + x!("ines"),
                _ => n_n.clone() + x!("es"),
            },
            (Case::Dative, NNumber::Generic) => a_or_n.clone() + x!("as"),
            (Case::Genitive, NNumber::Direct) => if !kind.is_el() && let Some(npr) = &npr {
                npr.clone() + x!("ina")
            } else {
                let mut stem = (match kind {
                    DeclensionIIIKind::El { g, .. } => g,
                    _ => &self.n,
                })
                .clone();
                if ends_with_l_bridge(&stem) {
                    stem.frag.last_tile_mut().3 = SimpleCoda::L;
                    stem + x!("u")
                } else if ends_with_l(&stem) {
                    stem + x!("ô")
                } else {
                    stem + x!("el")
                }
            }
            (Case::Genitive, NNumber::Dual) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { g, .. }) => fuse_d(g.clone(), FusionConsonant::T) + x!("el"),
                (Some(npr), _) => npr.clone() + x!("inta"),
                (None, _) => n_t.clone() + x!("el"),
            },
            (Case::Genitive, NNumber::Plural) => match (&npr, kind) {
                (_, DeclensionIIIKind::El { g, .. }) => g.clone() + x!("ol"),
                (Some(npr), _) => npr.clone() + x!("ide"),
                (None, _) => self.n.clone() + x!("jel"),
            },
            (Case::Genitive, NNumber::Singulative) => match kind {
                DeclensionIIIKind::El { g, .. } => fuse_d(g.clone(), FusionConsonant::N) + x!("el"),
                _ => n_n.clone() + x!("el"),
            },
            (Case::Genitive, NNumber::Generic) => {
                (match kind {
                    DeclensionIIIKind::El { a, .. } => fuse_d(a.clone(), FusionConsonant::N),
                    _ => n_n.clone(),
                }) + x!("e")
            }
            (Case::Locative, NNumber::Direct) => self.l.clone() + x!("os"),
            (Case::Locative, NNumber::Dual) => self.l.clone() + x!("ocþ"),
            (Case::Locative, NNumber::Plural) => self.l.clone() + x!("or"),
            (Case::Locative, NNumber::Singulative) => self.l.clone() + x!("oren"),
            (Case::Locative, NNumber::Generic) => self.l.clone() + vc2x(Vowel::E, phi_l.into()),
            (Case::Instrumental, NNumber::Direct) => {
                cja_l.clone() + vc2x(kind.vowel(), kind.coda().into())
            }
            (Case::Instrumental, NNumber::Dual) => {
                cja_l.clone() + vc2x(kind.vowel(), kind.coda_ca_du())
            }
            (Case::Instrumental, NNumber::Plural) => cja_l.clone() + x!("or"),
            (Case::Instrumental, NNumber::Singulative) => cja_l.clone() + x!("olt"),
            (Case::Instrumental, NNumber::Generic) => cja_l.clone() + vc2x(Vowel::E, phi_l.into()),
            (Case::Abessive, NNumber::Direct) => {
                thja_l.clone() + vc2x(kind.vowel(), kind.coda().into())
            }
            (Case::Abessive, NNumber::Dual) => {
                thja_l.clone() + vc2x(kind.vowel(), kind.coda_ca_du())
            }
            (Case::Abessive, NNumber::Plural) => thja_l.clone() + x!("or"),
            (Case::Abessive, NNumber::Singulative) => thja_l.clone() + x!("olt"),
            (Case::Abessive, NNumber::Generic) => thja_l.clone() + vc2x(Vowel::E, phi_l.into()),
            (Case::Semblative, _) => semblative_t(&self.s, number),
        }
        }
    }

    fn get_headword_inner(
        &self,
        _clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: match self.kind {
                DeclensionIIIKind::El { .. } => vec![
                    decl(Case::Nominative, NNumber::Direct),
                    decl(Case::Accusative, NNumber::Direct),
                    decl(Case::Genitive, NNumber::Direct),
                    decl(Case::Locative, NNumber::Direct),
                    decl(Case::Semblative, NNumber::Direct),
                ],
                _ => vec![
                    decl(Case::Nominative, NNumber::Direct),
                    decl(Case::Locative, NNumber::Direct),
                    decl(Case::Semblative, NNumber::Direct),
                ],
            },
            exhaustive: true,
        }
    }
}
