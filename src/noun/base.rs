use f9i_core::{
    assemblage::{AStem, Decorated, ParsedWord, Sylls},
    category::{Case, Clareth, NNumber},
    mgc::{Coda, Consonant, Initial, Lenite, LenitionType, Phrase, SimpleCoda, Vowel},
};
use f9i_ivm_macro::{gg, nw, w, x, y};

use crate::{
    error::InflectionError,
    headword::Headword,
    morphophonology::{gencons::GenCons, v2x, vc2x},
};

use super::{NounCategories, NounTable};

pub fn get_c_d(l: &Decorated<AStem>) -> Vowel {
    match l.frag.remainder.0 {
        Initial::Single(
            Consonant::T
            | Consonant::D
            | Consonant::S
            | Consonant::Th
            | Consonant::Dh
            | Consonant::L,
        )
        | Initial::L(_)
        | Initial::Tf
        | Initial::Dv => Vowel::E,
        _ => Vowel::I,
    }
}

pub fn locative_i(
    l: &Decorated<AStem>,
    lambda: Vowel,
    number: NNumber,
    phi: GenCons,
) -> Decorated<ParsedWord> {
    match number {
        NNumber::Direct => l.clone() + vc2x(lambda, Coda::S),
        NNumber::Dual => l.clone() + vc2x(lambda, Coda::C),
        NNumber::Plural => l.clone() + vc2x(lambda.pi(), Coda::S),
        NNumber::Singulative => l.clone() + vc2x(lambda.gamma(), Coda::Ns),
        NNumber::Generic => l.clone() + Vowel::E + Coda::from(phi),
    }
}

pub fn instrumental_i(
    l: &Decorated<AStem>,
    c_d: Vowel,
    number: NNumber,
    phi: GenCons,
) -> Decorated<ParsedWord> {
    match number {
        NNumber::Direct => l.clone() + x!("eca"),
        NNumber::Dual => l.clone() + x!("ecca"),
        NNumber::Plural => l.clone() + c_d + nw!("ca"),
        NNumber::Singulative => l.clone() + c_d + nw!("nca"),
        NNumber::Generic => l.clone() + Vowel::E + SimpleCoda::from(phi) + w!("ca"),
    }
}

pub fn abessive_i(
    l: &Decorated<AStem>,
    c_d: Vowel,
    number: NNumber,
    phi: GenCons,
) -> Decorated<ParsedWord> {
    match number {
        NNumber::Direct => l.clone() + x!("eþa"),
        NNumber::Dual => l.clone() + x!("ecþa"),
        NNumber::Plural => l.clone() + c_d + nw!("þa"),
        NNumber::Singulative => l.clone() + c_d + nw!("nþa"),
        NNumber::Generic => l.clone() + Vowel::E + SimpleCoda::from(phi) + w!("þa"),
    }
}

pub fn semblative_c(s: &Decorated<AStem>, theta: Vowel, number: NNumber) -> Decorated<ParsedWord> {
    match number {
        NNumber::Direct => s.clone() + x!("it"),
        NNumber::Dual => s.clone() + x!("et"),
        NNumber::Plural => s.clone() + x!("et"),
        NNumber::Singulative => s.clone() + gg!("ict") + v2x(theta),
        NNumber::Generic => s.clone() + x!("icþ"),
    }
}

pub fn semblative_t(s: &Decorated<AStem>, number: NNumber) -> Decorated<ParsedWord> {
    match number {
        NNumber::Direct => s.clone() + x!("ot"),
        NNumber::Dual => s.clone() + x!("octos"),
        NNumber::Plural => s.clone() + x!("et"),
        NNumber::Singulative => s.clone() + x!("ełi"),
        NNumber::Generic => s.clone() + x!("ocþ"),
    }
}

pub fn istems(stem: &Decorated<AStem>) -> (Decorated<AStem>, Decorated<AStem>) {
    let cja = Sylls::from(y!("cja")) + &stem.frag;
    let cja = Decorated {
        frag: cja,
        decoration: stem.decoration,
    };
    let lenited = {
        let mut stem = stem.clone();
        stem.lenite(LenitionType::Total);
        stem
    };
    let thja = Sylls::from(y!("þja")) + &lenited.frag;
    let thja = Decorated {
        frag: thja,
        decoration: lenited.decoration,
    };
    (cja, thja)
}

/// A common trait for any noun form in a regular declension paradigm.
pub trait Declension {
    /// The callback generated by [Declension::decline_inner].
    type Callback<'a>: (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a
    where
        Self: 'a;

    /// Return a closure that takes a case and a number and
    /// yields an assemblage for the declined form of the noun
    /// in those categories.
    ///
    /// Some declensions share computations; these should be
    /// done once when `decline_inner` is called instead of
    /// every time its result is called for those declensions.
    fn decline_inner(&self) -> Self::Callback<'_>;

    /// Given the [clareþ][Clareth] of the noun and a callback
    /// to get a declined form in any case and number appropriate
    /// for that clareþ, return the headword for this noun.
    ///
    /// `decl` must be called only for the numbers that are valid
    /// for the given clareþ.
    fn get_headword_inner(
        &self,
        clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword;

    /// Get the declensions of this noun and the appropriate
    /// headword, computing each form only once.
    fn decline_with_headword(
        &self,
        clareth: Clareth,
    ) -> Result<(NounTable<Phrase>, Headword), InflectionError> {
        let table = NounTable::construct_from_assemblages(clareth, self.decline_inner());
        let hw = self.get_headword_inner(clareth, |case, number| {
            table.get(NounCategories { case, number }).unwrap().clone()
        });
        Ok((table, hw))
    }
}
