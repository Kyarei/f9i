//! The first declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::fuse_d;
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::vc2x;
use f9i_core::assemblage::{AStem, Decorated, ParsedWord, SuffixRef};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{Coda, Consonant, FusionConsonant, Glide, Initial, Phrase, SimpleCoda, Vowel};
use f9i_ivm_macro::{f, nw, w, x};

use super::base::*;

/// Describes the nominative ending to use for a first-declension noun.
#[non_exhaustive]
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum DeclensionIKind {
    /// Vowel-final nouns.
    V,
    /// Vowel + ⟦-s⟧ nouns.
    Vs,
    /// Vowel + ⟦-þ⟧ nouns. The vowel is hatted.
    Vxth,
    /// Vowel + ⟦-n⟧ nouns.
    Vn,
}

impl DeclensionIKind {
    fn coda(self) -> SimpleCoda {
        match self {
            DeclensionIKind::V => SimpleCoda::Empty,
            DeclensionIKind::Vs => SimpleCoda::S,
            DeclensionIKind::Vxth => SimpleCoda::Th,
            DeclensionIKind::Vn => SimpleCoda::N,
        }
    }

    fn dative_plural_onset(self) -> Initial {
        match self {
            DeclensionIKind::V => Initial::Empty,
            DeclensionIKind::Vs => Initial::Single(Consonant::R),
            DeclensionIKind::Vxth => Initial::Single(Consonant::S),
            DeclensionIKind::Vn => Initial::Single(Consonant::R),
        }
    }
}

/// Data to describe a first-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionI {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The L stem.
    pub l: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The main thematic vowel.
    pub theta: Vowel,
    /// The locative vowel.
    pub lambda: Vowel,
    /// The ending type.
    pub kind: DeclensionIKind,
}

impl Declension for DeclensionI {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let kind = self.kind;
        let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
        let n_t = fuse_d(self.n.clone(), FusionConsonant::T);
        let c_d = get_c_d(&self.l);
        let phi_n = GenCons::for_stem(&self.n);
        let phi_l = GenCons::for_stem(&self.l);
        move |case, number| match (case, number) {
            (Case::Nominative, NNumber::Direct) => match kind {
                DeclensionIKind::Vxth => {
                    self.n.clone() + vc2x(self.theta.hat(), kind.coda().into())
                }
                _ => self.n.clone() + vc2x(self.theta, kind.coda().into()),
            },
            (Case::Nominative, NNumber::Dual) => self.n.clone() + vc2x(self.theta, Coda::C),
            (Case::Nominative, NNumber::Plural) => match kind {
                // not used because it causes syncretism with the dative singular
                // DeclensionIKind::Vs if self.theta != Vowel::E => {
                //     self.n.clone() + v2x(self.theta.pi())
                // }
                DeclensionIKind::Vs => {
                    self.n.clone() + vc2x(self.theta.gamma(), kind.coda().into())
                }
                DeclensionIKind::Vxth => {
                    self.n.clone() + vc2x(self.theta.pi().hat(), kind.coda().into())
                }
                _ => self.n.clone() + vc2x(self.theta.pi(), kind.coda().into()),
            },
            (Case::Nominative, NNumber::Singulative) => match kind {
                DeclensionIKind::V => self.n.clone() + vc2x(self.theta.gamma(), Coda::L),
                DeclensionIKind::Vs => n_n.clone() + vc2x(self.theta.gamma(), Coda::S),
                DeclensionIKind::Vxth => n_n.clone() + vc2x(self.theta.gamma().hat(), Coda::Th),
                DeclensionIKind::Vn => n_n.clone() + vc2x(self.theta.gamma(), Coda::L),
            },
            (Case::Nominative, NNumber::Generic) => {
                self.n.clone() + vc2x(self.theta.front(), phi_n.into())
            }
            (Case::Accusative, NNumber::Direct) => match kind {
                DeclensionIKind::V => self.n.clone() + vc2x(self.theta, Coda::N),
                DeclensionIKind::Vs | DeclensionIKind::Vxth => {
                    self.n.clone() + vc2x(self.theta.hat(), Coda::Ns)
                }
                DeclensionIKind::Vn => self.n.clone() + self.theta + nw!("nen"),
            },
            (Case::Accusative, NNumber::Dual) => self.n.clone() + x!("ôr"),
            (Case::Accusative, NNumber::Plural) => self.n.clone() + vc2x(self.theta, Coda::R),
            (Case::Accusative, NNumber::Singulative) => match kind {
                DeclensionIKind::V | DeclensionIKind::Vn => {
                    n_n.clone() + vc2x(self.theta.gamma(), Coda::S)
                }
                DeclensionIKind::Vs | DeclensionIKind::Vxth => n_n.clone() + x!("je"),
            },
            (Case::Accusative, NNumber::Generic) => {
                self.n.clone() + f!("e") + Initial::from(phi_n) + x!("en")
            }
            (Case::Dative, NNumber::Direct) => match kind {
                DeclensionIKind::V | DeclensionIKind::Vxth | DeclensionIKind::Vn => {
                    self.n.clone() + vc2x(self.theta, Coda::S)
                }
                DeclensionIKind::Vs => self.n.clone() + x!("o"),
            },
            (Case::Dative, NNumber::Dual) => n_t.clone() + vc2x(self.theta, Coda::S),
            (Case::Dative, NNumber::Plural) => {
                self.n.clone()
                    + SuffixRef {
                        tiles: &[(
                            Glide::None,
                            self.theta.front(),
                            SimpleCoda::Empty,
                            kind.dative_plural_onset(),
                        )],
                        remainder: (Glide::None, Vowel::I, Coda::Empty),
                    }
            }
            (Case::Dative, NNumber::Singulative) => {
                n_n.clone() + vc2x(self.theta.gamma(), Coda::Th)
            }
            (Case::Dative, NNumber::Generic) => {
                self.n.clone() + self.theta.front() + SimpleCoda::from(phi_n) + w!("es")
            }
            (Case::Genitive, NNumber::Direct) => self.n.clone() + vc2x(self.theta.gamma(), Coda::N),
            (Case::Genitive, NNumber::Dual) => n_t.clone() + vc2x(self.theta.gamma(), Coda::N),
            (Case::Genitive, NNumber::Plural) => self.n.clone() + x!("in"),
            (Case::Genitive, NNumber::Singulative) => {
                n_n.clone() + vc2x(self.theta.gamma(), Coda::N)
            }
            (Case::Genitive, NNumber::Generic) => n_n.clone() + Vowel::E + Coda::from(phi_n),
            (Case::Locative, _) => locative_i(&self.l, self.lambda, number, phi_l),
            (Case::Instrumental, _) => instrumental_i(&self.l, c_d, number, phi_l),
            (Case::Abessive, _) => abessive_i(&self.l, c_d, number, phi_l),
            (Case::Semblative, _) => semblative_c(&self.s, self.theta, number),
        }
    }

    fn get_headword_inner(
        &self,
        _clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: vec![
                decl(Case::Nominative, NNumber::Direct),
                decl(Case::Locative, NNumber::Direct),
                decl(Case::Semblative, NNumber::Direct),
            ],
            exhaustive: true,
        }
    }
}
