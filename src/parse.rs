/*!

Parsing of input to f9i.

*/

use crate::error::InflectionError;
use crate::lexical::{LexicalEntryEnum, Uninflectable};
use crate::nest::*;
use crate::noun::fifth::DeclensionV;
use crate::noun::first::{DeclensionI, DeclensionIKind};
use crate::noun::fourth::DeclensionIV;
use crate::noun::second::{DeclensionII, DeclensionIIKind, DeclensionIIpKind, DeclensionIIuKind};
use crate::noun::sixth::DeclensionVI;
use crate::noun::third::{DeclensionIII, DeclensionIIIKind};
use crate::noun::{Noun, NounForm, NounTable};
use crate::relational::{NominalizationType, RelationalForm, RelationalObjectType};
use crate::verb::participle::{I1Subspecies, I3Subspecies, II3Infixes, InstAbessEndings, Species};
use crate::verb::{Material, ResinousIrregular, Verb, VerbForm, VitreousIrregular};
use f9i_core::assemblage::{AStem, Decorated, ParsedWord};
use f9i_core::category::{Aspect, Attachment, Case, Clareth, Gender, SubjectType, Transitivity};
use f9i_core::mgc::{Phrase, SimpleCoda, Vowel};
use f9i_core::parse::*;
use nom::bytes::complete::is_a;
use nom::combinator::{map_res, success};
use nom::multi::separated_list1;
use nom::{
    branch::alt,
    bytes::complete::{is_not, tag},
    character::complete::{alpha1, char, space0, space1},
    combinator::{complete, eof, flat_map, map, opt},
    error::Error as NomError,
    sequence::{delimited, pair, preceded, separated_pair, terminated, tuple},
    Finish, IResult,
};

/// Parses a [SimpleCoda].
///
/// If no characters could be parsed, then the empty coda is returned.
pub fn simple_coda(input: &str) -> IResult<&str, SimpleCoda> {
    alt((
        map(tag("rþ"), |_| SimpleCoda::RTh),
        map(tag("cþ"), |_| SimpleCoda::CTh),
        map(tag("s"), |_| SimpleCoda::S),
        map(tag("r"), |_| SimpleCoda::R),
        map(tag("n"), |_| SimpleCoda::N),
        map(tag("þ"), |_| SimpleCoda::Th),
        map(tag("l"), |_| SimpleCoda::L),
        map(tag("f"), |_| SimpleCoda::F),
        map(tag(""), |_| SimpleCoda::Empty),
    ))(input)
}

/// Parses a [SimpleCoda].
///
/// This function parses `.` as the empty coda.
pub fn simple_coda_v(input: &str) -> IResult<&str, SimpleCoda> {
    alt((
        map(tag("rþ"), |_| SimpleCoda::RTh),
        map(tag("cþ"), |_| SimpleCoda::CTh),
        map(tag("."), |_| SimpleCoda::Empty),
        map(tag("s"), |_| SimpleCoda::S),
        map(tag("r"), |_| SimpleCoda::R),
        map(tag("n"), |_| SimpleCoda::N),
        map(tag("þ"), |_| SimpleCoda::Th),
        map(tag("l"), |_| SimpleCoda::L),
        map(tag("f"), |_| SimpleCoda::F),
    ))(input)
}

/// Parses a [Gender] from its name in lowercase.
pub fn gender(input: &str) -> IResult<&str, Gender> {
    alt((
        map(tag("celestial"), |_| Gender::Celestial),
        map(tag("terrestrial"), |_| Gender::Terrestrial),
        map(tag("human"), |_| Gender::Human),
    ))(input)
}

/// Parses a [Clareth] from its name in lowercase.
pub fn clareth(input: &str) -> IResult<&str, Clareth> {
    alt((
        map(tag("singular"), |_| Clareth::Singular),
        map(tag("collective"), |_| Clareth::Collective),
        map(tag("mass"), |_| Clareth::Mass),
        map(tag("universal"), |_| Clareth::Universal),
    ))(input)
}

/// Parses a [Transitivity] from its name in lowercase.
pub fn transitivity(input: &str) -> IResult<&str, Transitivity> {
    alt((
        map(tag("intransitive"), |_| Transitivity::Intransitive),
        map(tag("semitransitive"), |_| Transitivity::Semitransitive),
        map(tag("transitive"), |_| Transitivity::Transitive),
        map(tag("ditransitive"), |_| Transitivity::Ditransitive),
        map(tag("auxiliary"), |_| Transitivity::Auxiliary),
    ))(input)
}

/// Parses a set of irregular verb overrides for a vitreous verb.
fn vitreous_irregular_overrides(input: &str) -> IResult<&str, VitreousIrregular> {
    alt((
        map(
            delimited(
                tuple((space1, char('{'), space0, tag("apn:"), space0)),
                map_res(
                    separated_list1(space1, assemblaged),
                    shape_2d::<Decorated<ParsedWord>, { Aspect::COUNT }, { SubjectType::COUNT }>,
                ),
                tuple((space0, char('}'))),
            ),
            |a| VitreousIrregular::Apn(Box::new(a)),
        ),
        // this rule should come last
        success(VitreousIrregular::None),
    ))(input)
}

/// Parses a set of irregular verb overrides for a resinous verb.
fn resinous_irregular_overrides(input: &str) -> IResult<&str, ResinousIrregular> {
    alt((
        // this rule should come last
        success(ResinousIrregular::None),
    ))(input)
}

/// Parses a [Material] from its name in lowercase.
pub fn material(input: &str) -> IResult<&str, Material> {
    alt((
        map(
            tuple((tag("vitreous"), vitreous_irregular_overrides)),
            |(_, o)| Material::Vitreous(o),
        ),
        map(
            tuple((tag("resinous"), resinous_irregular_overrides)),
            |(_, o)| Material::Resinous(o),
        ),
    ))(input)
}

/// Parses an [Attachment] from its name in lowercase.
pub fn attachment(input: &str) -> IResult<&str, Attachment> {
    alt((
        map(tag("adnominal"), |_| Attachment::Adnominal),
        map(tag("adverbial"), |_| Attachment::Adverbial),
    ))(input)
}

/// Parses a [RelationalObjectType] from its name in lowercase.
pub fn relational_obj_type(input: &str) -> IResult<&str, RelationalObjectType> {
    alt((
        map(tag("dative_motion"), |_| RelationalObjectType::DativeMotion),
        map(tag("dative"), |_| RelationalObjectType::Dative),
        map(tag("genitive"), |_| RelationalObjectType::Genitive),
        map(tag("locative"), |_| RelationalObjectType::Locative),
        map(tag("polymorphic"), |_| RelationalObjectType::Polymorphic),
    ))(input)
}

fn nfa_aaa<'a>(
    pname: &'a str,
    thsuffix: &'a str,
) -> impl FnMut(&'a str) -> IResult<&'a str, (Decorated<AStem>, Decorated<AStem>, Decorated<AStem>)>
{
    delimited(
        tag(pname),
        tuple((
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
        )),
        preceded(space1, tag(thsuffix)),
    )
}

fn nfa_aaaaa<'a>(
    pname: &'a str,
    thsuffix: &'a str,
) -> impl FnMut(
    &'a str,
) -> IResult<
    &'a str,
    (
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
    ),
> {
    delimited(
        tag(pname),
        tuple((
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
        )),
        preceded(space1, tag(thsuffix)),
    )
}

fn nfa_aaavl<'a>(
    pname: &'a str,
    thsuffix: &'a str,
) -> impl FnMut(
    &'a str,
) -> IResult<
    &'a str,
    (
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
        Vowel,
        Vowel,
    ),
> {
    preceded(
        tag(pname),
        tuple((
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, terminated(vowel, tag(thsuffix))),
            preceded(space1, vowel),
        )),
    )
}

fn nfa_aaaavl<'a>(
    pname: &'a str,
    thsuffix: &'a str,
) -> impl FnMut(
    &'a str,
) -> IResult<
    &'a str,
    (
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
        Decorated<AStem>,
        Vowel,
        Vowel,
    ),
> {
    preceded(
        tag(pname),
        tuple((
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, terminated(vowel, tag(thsuffix))),
            preceded(space1, vowel),
        )),
    )
}

/// Converts a linear vector of values into a [NounTable], given a [Clareth].
fn noun_table_from_entries<T>(clareth: Clareth, storage: Vec<T>) -> Result<NounTable<T>, Vec<T>> {
    match clareth {
        Clareth::Singular => Ok(NounTable::Singular(shape_2d::<
            T,
            { Case::COUNT },
            { Clareth::Singular.num_count() },
        >(storage)?)),
        Clareth::Collective => Ok(NounTable::Collective(shape_2d::<
            T,
            { Case::COUNT },
            { Clareth::Collective.num_count() },
        >(storage)?)),
        Clareth::Mass => Ok(NounTable::Mass(shape_2d::<
            T,
            { Case::COUNT },
            { Clareth::Mass.num_count() },
        >(storage)?)),
        Clareth::Universal => Ok(NounTable::Universal(shape_2d::<
            T,
            { Case::COUNT },
            { Clareth::Universal.num_count() },
        >(storage)?)),
    }
}

/// Parses input that describes the inflection for a noun, excluding the `noun` tag, as well as the gender and clareþ information.
pub fn noun_form() -> impl FnMut(&str) -> IResult<&str, NounForm> {
    move |input| {
        alt((
            alt((
                map(nfa_aaavl("I", ""), |(n, l, s, theta, lambda)| {
                    NounForm::I(DeclensionI {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIKind::V,
                    })
                }),
                map(nfa_aaavl("I", "s"), |(n, l, s, theta, lambda)| {
                    NounForm::I(DeclensionI {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIKind::Vs,
                    })
                }),
                map(nfa_aaavl("I", "þ"), |(n, l, s, theta, lambda)| {
                    NounForm::I(DeclensionI {
                        n,
                        l,
                        s,
                        theta: theta.unhat(),
                        lambda,
                        kind: DeclensionIKind::Vxth,
                    })
                }),
                map(nfa_aaavl("I", "n"), |(n, l, s, theta, lambda)| {
                    NounForm::I(DeclensionI {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIKind::Vn,
                    })
                }),
                map(nfa_aaaavl("IIp", "n"), |(n, g, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::P(DeclensionIIpKind::In, g),
                    })
                }),
                map(nfa_aaaavl("IIp", "s"), |(n, g, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::P(DeclensionIIpKind::Is, g),
                    })
                }),
                map(nfa_aaaavl("IIp", "r"), |(n, g, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta: theta.unhat(),
                        lambda,
                        kind: DeclensionIIKind::P(DeclensionIIpKind::Vxr, g),
                    })
                }),
                map(nfa_aaavl("IIu", "þ"), |(n, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::U(DeclensionIIuKind::Vth),
                    })
                }),
                map(nfa_aaavl("IIu", "rþ"), |(n, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::U(DeclensionIIuKind::Vrth),
                    })
                }),
                map(nfa_aaavl("IIu", "r"), |(n, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::U(DeclensionIIuKind::Vr),
                    })
                }),
                map(nfa_aaavl("IIu", "l"), |(n, l, s, theta, lambda)| {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                        kind: DeclensionIIKind::U(DeclensionIIuKind::Vl),
                    })
                }),
                map(nfa_aaaaa("III", "el"), |(n, a, g, l, s)| {
                    NounForm::III(DeclensionIII {
                        n,
                        l,
                        s,
                        kind: DeclensionIIIKind::El { a, g },
                    })
                }),
                map(nfa_aaa("III", "os"), |(n, l, s)| {
                    NounForm::III(DeclensionIII {
                        n,
                        l,
                        s,
                        kind: DeclensionIIIKind::Os,
                    })
                }),
                map(nfa_aaa("III", "on"), |(n, l, s)| {
                    NounForm::III(DeclensionIII {
                        n,
                        l,
                        s,
                        kind: DeclensionIIIKind::On,
                    })
                }),
                map(nfa_aaa("III", "or"), |(n, l, s)| {
                    NounForm::III(DeclensionIII {
                        n,
                        l,
                        s,
                        kind: DeclensionIIIKind::Or,
                    })
                }),
                map(nfa_aaavl("IV", ""), |(n, l, s, theta, lambda)| {
                    NounForm::IV(DeclensionIV {
                        n,
                        l,
                        s,
                        theta,
                        lambda,
                    })
                }),
                map(
                    preceded(
                        tag("V"),
                        tuple((
                            preceded(space1, assemblaged),
                            preceded(space1, assemblaged),
                            preceded(space1, nucleus),
                            preceded(space1, simple_coda_v),
                        )),
                    ),
                    |(n, s, theta, sigma)| NounForm::V(DeclensionV { n, s, theta, sigma }),
                ),
                map(
                    preceded(
                        tag("VI"),
                        tuple((
                            preceded(space1, assemblaged),
                            preceded(space1, assemblaged),
                            preceded(space1, assemblaged),
                            preceded(space1, assemblaged),
                            preceded(space1, assemblaged),
                            delimited(space1, vowel, tag("n")),
                            preceded(space1, vowel),
                        )),
                    ),
                    |(n, l, i, i_prime, s, theta, lambda)| {
                        NounForm::VI(DeclensionVI {
                            n,
                            l,
                            i,
                            i_prime,
                            s,
                            theta,
                            lambda,
                        })
                    },
                ),
            )),
            map(
                delimited(
                    pair(tag("{"), space0),
                    flat_map(terminated(clareth, space1), move |cl| {
                        map_res(separated_list1(space1, decorated_word_first), move |x| {
                            noun_table_from_entries(cl, x.into_iter().map(Phrase::of).collect())
                        })
                    }),
                    pair(space0, tag("}")),
                ),
                |forms| NounForm::Irregular(Box::new(forms)),
            ),
            map(
                delimited(
                    pair(tag("{{"), space0),
                    flat_map(terminated(clareth, space1), move |cl| {
                        map_res(separated_list1(space1, phrase), move |x| {
                            noun_table_from_entries(cl, x)
                        })
                    }),
                    pair(space0, tag("}}")),
                ),
                |forms| NounForm::Irregular(Box::new(forms)),
            ),
            map(
                delimited(
                    pair(tag("("), space0),
                    separated_pair(
                        noun_form(),
                        delimited(space1, tag("|"), space1),
                        noun_form(),
                    ),
                    pair(space0, tag(")")),
                ),
                |(n1, n2)| NounForm::Compound(Box::new([n1, n2])),
            ),
            map(
                delimited(
                    pair(tag("("), space0),
                    separated_pair(phrase, delimited(space1, tag("<"), space1), noun_form()),
                    pair(space0, tag(")")),
                ),
                |(l, r)| NounForm::Left(l, Box::new(r)),
            ),
            map(
                delimited(
                    pair(tag("("), space0),
                    separated_pair(noun_form(), delimited(space1, tag(">"), space1), phrase),
                    pair(space0, tag(")")),
                ),
                |(l, r)| NounForm::Right(Box::new(l), r),
            ),
        ))(input)
    }
}

/// Parses input that describes a noun, including the `noun` tag, as well as the gender and clareþ information.
pub fn noun_line(input: &str) -> IResult<&str, Noun> {
    preceded(
        pair(tag("noun"), space1),
        flat_map(gender, |g| {
            flat_map(preceded(space1, clareth), move |cl| {
                map(preceded(space1, noun_form()), move |nf| Noun {
                    gender: g,
                    clareth: cl,
                    form: nf,
                })
            })
        }),
    )(input)
}

fn slash(input: &str) -> IResult<&str, ()> {
    map(tuple((space0, char('/'), space0)), |_| ())(input)
}

fn inst_abess_endings(input: &str) -> IResult<&str, InstAbessEndings> {
    alt((
        map(tag("a"), |_| InstAbessEndings::Alpha),
        map(tag("b"), |_| InstAbessEndings::Beta),
        map(tag("g"), |_| InstAbessEndings::Gamma),
        map(tag("d"), |_| InstAbessEndings::Delta),
    ))(input)
}

/// Parses a description of a participle species.
pub fn species(input: &str) -> IResult<&str, Species> {
    alt((
        map_res(
            tuple((
                tag("I1"),
                preceded(space1, vowel),
                opt(char('n')),
                preceded(space1, char('o')),
                is_a("snr"),
                preceded(space1, inst_abess_endings),
            )),
            |(_, lambda, sigma, _, gamma, ia)| {
                let subspecies = match (sigma, gamma) {
                    (None, "s") => I1Subspecies::VxOs,
                    (None, "r") => I1Subspecies::VxOr,
                    (Some(_), "n") => I1Subspecies::NxOn,
                    (Some(_), "r") => I1Subspecies::NxOr,
                    _ => return Err(InflectionError::InvalidSpecies),
                };
                Ok(Species::I1(subspecies, lambda, ia))
            },
        ),
        map(
            tuple((
                tag("I3"),
                preceded(space1, is_a("wx")),
                preceded(space1, inst_abess_endings),
            )),
            |(_, wx, ia)| {
                Species::I3(
                    match wx {
                        "w" => I3Subspecies::W,
                        "x" => I3Subspecies::X,
                        _ => unreachable!(),
                    },
                    ia,
                )
            },
        ),
        map(
            tuple((tag("II4"), preceded(space1, vowel), simple_coda)),
            |(_, lambda, sigma)| Species::II4(lambda, sigma),
        ),
        map(
            tuple((tag("II3"), preceded(space1, is_a("sv")))),
            |(_, sv)| {
                Species::II3(match sv {
                    "s" => II3Infixes::S,
                    "v" => II3Infixes::V,
                    _ => unreachable!(),
                })
            },
        ),
        map(tag("III2"), |_| Species::III2),
    ))(input)
}

/// Parses input that describes the conjugation of a verb, excluding the `verb` tag and the transitivity.
pub fn verb_form(input: &str) -> IResult<&str, VerbForm> {
    map(
        tuple((
            material,
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, vowel),
            preceded(slash, assemblaged),
            preceded(space1, assemblaged),
            preceded(space1, species),
            preceded(slash, assemblaged),
        )),
        |(material, i, n, p, theta, r, q, species, l)| VerbForm {
            material,
            i,
            n,
            p,
            r,
            q,
            l,
            species,
            theta,
        },
    )(input)
}

/// Parses input that describes a verb, including the `verb` tag and the transitivity.
pub fn verb_line(input: &str) -> IResult<&str, Verb> {
    preceded(
        pair(tag("verb"), space1),
        map(
            separated_pair(transitivity, slash, verb_form),
            |(transitivity, form)| Verb { transitivity, form },
        ),
    )(input)
}

/// Parses a [Case] from its name in lowercase.
pub fn case(input: &str) -> IResult<&str, Case> {
    alt((
        map(tag("nominative"), |_| Case::Nominative),
        map(tag("accusative"), |_| Case::Accusative),
        map(tag("dative"), |_| Case::Dative),
        map(tag("genitive"), |_| Case::Genitive),
        map(tag("locative"), |_| Case::Locative),
        map(tag("instrumental"), |_| Case::Instrumental),
        map(tag("abessive"), |_| Case::Abessive),
        map(tag("semblative"), |_| Case::Semblative),
    ))(input)
}

/// Parses a [NominalizationType].
pub fn nominalization_type(input: &str) -> IResult<&str, NominalizationType> {
    alt((
        map(tag("II"), |_| NominalizationType::TypeII),
        map(tag("I"), |_| NominalizationType::TypeI),
    ))(input)
}

/// Parses input that describes a relational.
pub fn relational_line(input: &str) -> IResult<&str, RelationalForm> {
    preceded(
        pair(tag("relational"), space1),
        map(
            tuple((
                nominalization_type,
                preceded(space1, relational_obj_type),
                opt(preceded(pair(space1, char('~')), case)),
                preceded(space1, assemblaged),
                preceded(space1, assemblaged),
                preceded(space1, assemblaged),
                preceded(space1, assemblaged),
            )),
            |(nominalization_type, object_type, ancilliary_object_case, a, v, n, c)| {
                RelationalForm {
                    a,
                    v,
                    n,
                    c,
                    object_type,
                    nominalization_type,
                    ancilliary_object_case,
                }
            },
        ),
    )(input)
}

/// Parses input that describes an uninflected word.
pub fn uninflected_line(input: &str) -> IResult<&str, Uninflectable> {
    map(
        preceded(tag("!"), separated_pair(alpha1, space1, phrase)),
        |(pos, form)| Uninflectable {
            form,
            pos: pos.into(),
        },
    )(input)
}

/// Parses an input line that describes a lexical entry, without any label.
pub fn bare_line(input: &str) -> IResult<&str, LexicalEntryEnum> {
    alt((
        map(noun_line, LexicalEntryEnum::from),
        map(verb_line, LexicalEntryEnum::from),
        map(relational_line, LexicalEntryEnum::from),
        map(uninflected_line, LexicalEntryEnum::from),
    ))(input)
}

/// Parses an input line that describes a lexical entry, with the label.
pub fn input_line(input: &str) -> IResult<&str, (&str, LexicalEntryEnum)> {
    separated_pair(is_not(":"), pair(tag(":"), space0), bare_line)(input)
}

/// Parses an input line that describes a lexical entry, without any label.
///
/// This requires a complete line.
pub fn parse_bare_line(input: &str) -> Result<LexicalEntryEnum, NomError<&str>> {
    terminated(
        complete(alt((bare_line, map(input_line, |a| a.1)))),
        pair(space0, eof),
    )(input)
    .finish()
    .map(|(_, w)| w)
}

/// Parses an input line that describes a lexical entry, with the label.
///
/// This requires a complete line.
pub fn parse_input_line(input: &str) -> Result<(&str, LexicalEntryEnum), NomError<&str>> {
    terminated(complete(input_line), pair(space0, eof))(input)
        .finish()
        .map(|(_, w)| w)
}
