//! Errors for f9i.

use custom_error::custom_error;
use f9i_core::{
    category::Clareth,
    mgc::{Mgc, Nucleus},
};

custom_error! {#[derive(Clone, Eq, PartialEq)]
    /// Indicates that an error occurred during inflection.
    pub InflectionError
    InvalidThematicVowel{ v0: Nucleus } = "invalid thematic vowel ⟦{v0}⟧",
    InvalidThematicConsonant{ c0: Mgc } = "invalid thematic consonant ⟦{c0}⟧",
    InvalidThematicConsonants{ c1: Mgc, c2 : Mgc } = "invalid thematic consonant cluster ⟦{c1}⟧, ⟦{c2}⟧",
    InvalidSpecies = "invalid species",
    InvalidGender = "a relational may not have human gender",
    ClarethMismatch{ cl1: Clareth, cl2: Clareth } = "the parts of a compound noun are mismatching in clareþ: {cl1} and {cl2}"
}
