use std::ops::Add;

use f9i_core::{
    assemblage::{AStem, Decorated, SimpleSuffixRef, SyllabicToNuclear, Sylls},
    mgc::{DecoratedWord, Initial, Mgc, Onset, SimpleCoda, Tokenize},
};

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum StemOrSylls {
    Stem(Decorated<AStem>),
    Sylls(Decorated<Sylls>),
}

impl StemOrSylls {
    pub fn starts_with_e(&self) -> bool {
        match self {
            StemOrSylls::Stem(s) => matches!(s.frag.peek(), Some(Mgc::E | Mgc::EHat)),
            StemOrSylls::Sylls(s) => matches!(s.frag.peek(), Some(Mgc::E | Mgc::EHat)),
        }
    }

    pub fn ends_with_empty_bridge(&self) -> bool {
        match self {
            StemOrSylls::Stem(stem) => stem.ends_with_empty_bridge(),
            StemOrSylls::Sylls(sylls) => sylls
                .frag
                .tiles
                .last()
                .is_some_and(|tile| tile.3 == SimpleCoda::Empty),
        }
    }

    pub fn strip_final_empty_bridge(self) -> Result<Decorated<SyllabicToNuclear>, Self> {
        if !self.ends_with_empty_bridge() {
            return Err(self);
        }
        Ok(match self {
            StemOrSylls::Stem(stem) => stem.map(|stem| stem.peel().peel().peelx()),
            StemOrSylls::Sylls(sylls) => sylls.map(|sylls| sylls.peelx()),
        })
    }

    pub fn to_dw(&self) -> DecoratedWord {
        match self {
            StemOrSylls::Stem(s) => s.to_dw(),
            StemOrSylls::Sylls(s) => s.to_dw(),
        }
    }

    pub fn into_stem(self) -> Decorated<AStem> {
        match self {
            StemOrSylls::Stem(stem) => stem,
            StemOrSylls::Sylls(sylls) => sylls + Onset::from(Initial::Empty),
        }
    }
}

impl<'a> Add<SimpleSuffixRef<'a>> for StemOrSylls {
    type Output = Self;

    fn add(self, rhs: SimpleSuffixRef<'a>) -> Self::Output {
        StemOrSylls::Sylls(match self {
            StemOrSylls::Stem(stem) => stem + rhs,
            StemOrSylls::Sylls(sylls) => sylls + Initial::Empty + rhs,
        })
    }
}
