//! Conjugation of relationals in Ŋarâþ Crîþ v9.

use crate::{
    morphophonology::fuse::{fuse_d, fuse_with_eps_d},
    noun::{
        first::{DeclensionI, DeclensionIKind},
        second::{DeclensionII, DeclensionIIKind, DeclensionIIuKind},
        third::{DeclensionIII, DeclensionIIIKind},
    },
    verb::finite::VitreousResult,
};
use default_boxed::DefaultBoxed;
use f9i_core::{
    assemblage::{
        AStem, Decorated, GlideToOnsetRef, ParsedWord, ParsedWordRef, SimpleSuffixRef, SuffixRef,
        Sylls, SyllsRef,
    },
    category::{
        Aspect, Attachment, Case, Clareth, MotionType, ObjectType, Polarity, SubjectType, Tense,
    },
    mgc::{
        Coda, Consonant, DecoratedWord, Eclipse, FusionConsonant, Glide, Initial, Onset, Phrase,
        SimpleCoda, Vowel,
    },
};
use f9i_ivm_macro::{a, b, f, go, w, x, y};
use std::ops::DerefMut;

use crate::{
    error::InflectionError,
    headword::Headword,
    nd_array_type,
    nest::*,
    noun::{NounCategories, NounForm, NounTable},
    verb::finite::{Fin, FiniteVerbCategories},
};

use self::base::StemOrSylls;

mod base;

/// The case taken by the object of a relational.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum RelationalObjectType {
    Dative,
    /// Dative, and has motion forms.
    DativeMotion,
    Locative,
    Genitive,
    /// Any case, usually agreeing with another noun phrase.
    Polymorphic,
}

impl RelationalObjectType {
    /// Returns the abbreviation for the [RelationalObjectType].
    pub fn abbreviation(self) -> &'static str {
        match self {
            RelationalObjectType::Dative => "",
            RelationalObjectType::DativeMotion => "*",
            RelationalObjectType::Locative => "LOC",
            RelationalObjectType::Genitive => "GEN",
            RelationalObjectType::Polymorphic => "POLY",
        }
    }
}

/// The type of the relational (I or II), governing how nominalized forms are derived.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum NominalizationType {
    TypeI,
    TypeII,
}

impl NominalizationType {
    pub fn abbreviation(self) -> &'static str {
        match self {
            NominalizationType::TypeI => "I",
            NominalizationType::TypeII => "II",
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct RelationalCategories {
    pub attachment: Attachment,
    pub motion: MotionType,
    pub object_type: ObjectType,
    pub ancillary_object_type: ObjectType,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct RelationalCategoriesOpt {
    pub attachment: Attachment,
    pub motion: Option<MotionType>,
    pub object_type: ObjectType,
    pub ancillary_object_type: Option<ObjectType>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct FiniteRelationalCategories {
    pub vc: FiniteVerbCategories,
    pub motion: MotionType,
    pub polarity: Polarity,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct FiniteRelationalCategoriesOpt {
    pub vc: FiniteVerbCategories,
    pub motion: Option<MotionType>,
    pub polarity: Polarity,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct NominalRelationalCategories {
    pub nc: NounCategories,
    pub motion: MotionType,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct NominalRelationalCategoriesOpt {
    pub nc: NounCategories,
    pub motion: Option<MotionType>,
}

/// A container that can hold the set of attributive forms of a relational for a given motion type.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum RelA<T> {
    /// Used for a divalent relational.
    Divalent([[T; ObjectType::COUNT]; Attachment::COUNT]),
    /// Used for a trivalent relational; that is, one that admits an ancilliary object.
    Trivalent(Box<[[[T; ObjectType::COUNT]; Attachment::COUNT]; ObjectType::COUNT]>),
}

impl<T> Default for RelA<T>
where
    T: Default,
{
    fn default() -> Self {
        RelA::Divalent(Default::default())
    }
}

/// A container that can hold a set of relational forms for a given motion type.
#[derive(Clone, Eq, PartialEq, Debug, DefaultBoxed)]
pub struct Rel<T: Default> {
    pub attributive: RelA<T>,
    pub finite: [Fin<T>; 2],
    pub nominal: NounTable<T>,
}

impl<T: Default> Rel<T> {
    pub fn get_attr(&self, cat: RelationalCategories) -> &T {
        match &self.attributive {
            RelA::Divalent(array) => &array[cat.attachment as usize][cat.object_type as usize],
            RelA::Trivalent(array) => {
                &array[cat.ancillary_object_type as usize][cat.attachment as usize]
                    [cat.object_type as usize]
            }
        }
    }
    pub fn get_attr_mut(&mut self, cat: RelationalCategories) -> &mut T {
        match &mut self.attributive {
            RelA::Divalent(array) => &mut array[cat.attachment as usize][cat.object_type as usize],
            RelA::Trivalent(array) => {
                &mut array[cat.ancillary_object_type as usize][cat.attachment as usize]
                    [cat.object_type as usize]
            }
        }
    }
    pub fn get_fin(&self, cat: FiniteRelationalCategories) -> &T {
        let decl = &cat.vc;
        &self.finite[cat.polarity as usize][decl.aspect as usize][decl.tense as usize]
            [decl.obj_marking as usize][decl.subj_marking as usize]
    }
    pub fn get_fin_mut(&mut self, cat: FiniteRelationalCategories) -> &mut T {
        let decl = &cat.vc;
        &mut self.finite[cat.polarity as usize][decl.aspect as usize][decl.tense as usize]
            [decl.obj_marking as usize][decl.subj_marking as usize]
    }
    pub fn get_nom(&self, cat: NounCategories) -> Option<&T> {
        self.nominal.get(cat)
    }
    pub fn get_nom_mut(&mut self, cat: NounCategories) -> Option<&mut T> {
        self.nominal.get_mut(cat)
    }

    /*
        map, map_ref, and try_map are intentionally unimplemented, as they have a high chance of overflowing the stack unless LTO is enabled. The inflection code does not rely on these methods.
    */
}

/// A container that can hold all inflected forms of a relational.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum RelFull<T: Default> {
    Basic(Box<Rel<T>>),
    Motion(Box<[Rel<T>; MotionType::COUNT]>),
}

impl<T: Default> RelFull<T> {
    pub fn get_attr(&self, cat: RelationalCategories) -> &T {
        match self {
            RelFull::Basic(r) => r.get_attr(cat),
            RelFull::Motion(r) => r[cat.motion as usize].get_attr(cat),
        }
    }
    pub fn get_attr_mut(&mut self, cat: RelationalCategories) -> &mut T {
        match self {
            RelFull::Basic(r) => r.get_attr_mut(cat),
            RelFull::Motion(r) => r[cat.motion as usize].get_attr_mut(cat),
        }
    }
    pub fn get_fin(&self, cat: FiniteRelationalCategories) -> &T {
        match self {
            RelFull::Basic(r) => r.get_fin(cat),
            RelFull::Motion(r) => r[cat.motion as usize].get_fin(cat),
        }
    }
    pub fn get_fin_mut(&mut self, cat: FiniteRelationalCategories) -> &mut T {
        match self {
            RelFull::Basic(r) => r.get_fin_mut(cat),
            RelFull::Motion(r) => r[cat.motion as usize].get_fin_mut(cat),
        }
    }
    pub fn get_nom(&self, cat: NominalRelationalCategories) -> Option<&T> {
        match self {
            RelFull::Basic(r) => r.get_nom(cat.nc),
            RelFull::Motion(r) => r[cat.motion as usize].get_nom(cat.nc),
        }
    }
    pub fn get_nom_mut(&mut self, cat: NominalRelationalCategories) -> Option<&mut T> {
        match self {
            RelFull::Basic(r) => r.get_nom_mut(cat.nc),
            RelFull::Motion(r) => r[cat.motion as usize].get_nom_mut(cat.nc),
        }
    }

    /*
        map, map_ref, and try_map are intentionally unimplemented, as they have a high chance of overflowing the stack unless LTO is enabled. The inflection code does not rely on these methods.
    */
}

/// A description of how a relational is conjugated.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct RelationalForm {
    pub a: Decorated<AStem>,
    pub v: Decorated<AStem>,
    pub n: Decorated<AStem>,
    pub c: Decorated<Sylls>,
    pub object_type: RelationalObjectType,
    pub nominalization_type: NominalizationType,
    pub ancilliary_object_case: Option<Case>,
}

impl RelationalForm {
    pub fn inflect(&self) -> Result<RelFull<DecoratedWord>, InflectionError> {
        match self.object_type {
            RelationalObjectType::DativeMotion => {
                let mut res: Box<[Rel<DecoratedWord>; MotionType::COUNT]> = default_boxed_array();
                for motion in MotionType::VALUES {
                    self.inflect_in_place(motion, &mut res[motion as usize])?;
                }
                Ok(RelFull::Motion(res))
            }
            _ => {
                let mut res: Box<Rel<DecoratedWord>> = DefaultBoxed::default_boxed();
                self.inflect_in_place(MotionType::Static, res.deref_mut())?;
                Ok(RelFull::Basic(res))
            }
        }
    }
    fn inflect_in_place(
        &self,
        motion: MotionType,
        out: &mut Rel<DecoratedWord>,
    ) -> Result<(), InflectionError> {
        // Attributive forms
        if self.ancilliary_object_case.is_none() {
            // Divalent relational; call `write_attributive_forms` once
            if let RelA::Divalent(forms_out) = &mut out.attributive {
                self.write_attributive_forms(motion, ObjectType::G3, forms_out)?
            } else {
                unreachable!("should have started as RelA::Divalent");
            }
        } else {
            // Trivalent relational; call `write_attributive_forms` for each ancillary object type
            let mut forms: Box<
                [[[DecoratedWord; ObjectType::COUNT]; Attachment::COUNT]; ObjectType::COUNT],
            > = default_boxed_array();
            for ancilliary_obj_type in ObjectType::VALUES {
                self.write_attributive_forms(
                    motion,
                    ancilliary_obj_type,
                    &mut forms[ancilliary_obj_type as usize],
                )?;
            }
            out.attributive = RelA::Trivalent(forms);
        }

        // Finite forms
        for polarity in Polarity::VALUES {
            for aspect in Aspect::VALUES {
                for tense in Tense::VALUES {
                    for obj_marking in ObjectType::VALUES {
                        for subj_marking in SubjectType::VALUES {
                            let scaffold = SCAFFOLDING_FORMS[polarity as usize]
                                [(aspect as usize) ^ (tense as usize)]
                                [subj_marking as usize];

                            let motion_prefix = match motion {
                                MotionType::Static => y!(""),
                                MotionType::Allative => y!("ar"),
                                MotionType::Ablative => y!("as"),
                            };

                            let w = self
                                .c
                                .clone()
                                .map(|conj| Sylls::from(motion_prefix) + &conj);

                            // Scaffolding
                            let w = w + scaffold;

                            let mut w = VitreousResult::inflect(w, tense, obj_marking);

                            if aspect == Aspect::Perfective {
                                w.eclipse_terminal();
                            }

                            let out = out.get_fin_mut(FiniteRelationalCategories {
                                vc: FiniteVerbCategories {
                                    subj_marking,
                                    obj_marking,
                                    tense,
                                    aspect,
                                },
                                motion,
                                polarity,
                            });
                            *out = w.to_dw();
                        }
                    }
                }
            }
        }

        // Nominalized forms

        let noun_form = match self.nominalization_type {
            NominalizationType::TypeI => {
                let b = match motion {
                    MotionType::Static => self.c.clone(),
                    MotionType::Allative => {
                        Self::attach_affix_with_syncope(self.a.clone(), f!("ar"))
                    }
                    MotionType::Ablative => Self::attach_affix_with_syncope(
                        self.a.clone(),
                        self.adverbial_abessive_suffix(),
                    ),
                };
                NounForm::I(DeclensionI {
                    n: b.clone() + a!("tar"),
                    l: b.clone() + a!("tor"),
                    s: b + a!("tel"),
                    theta: Vowel::E,
                    lambda: Vowel::E,
                    kind: DeclensionIKind::V,
                })
            }
            NominalizationType::TypeII => {
                let b = match motion {
                    MotionType::Static => {
                        if self.n.frag.tiles.len() > self.a.frag.tiles.len() {
                            self.a.clone()
                        } else {
                            self.n.clone()
                        }
                    }
                    MotionType::Allative => {
                        Self::attach_affix_with_syncope_2(self.a.clone(), go!("ara"))
                    }
                    MotionType::Ablative => {
                        Self::attach_affix_with_syncope_2(self.a.clone(), go!("eis"))
                    }
                };

                let (n, l, s) = if b.frag.tiles.len() == 1 && b.ends_with_1c_bridge() {
                    let l_infix = match b.frag.last_tile().2 {
                        Vowel::E | Vowel::EHat => go!("il"),
                        _ => go!("el"),
                    };
                    (b.clone() + go!("al"), b.clone() + l_infix, b + go!("al"))
                } else if b.ends_with_empty_bridge() {
                    let b = b.map(|x| x.peel().peel().peelx());

                    let mut l = b.clone();
                    let l = match &mut l.frag.remainder.2 {
                        v @ (Vowel::A | Vowel::E) => {
                            *v = Vowel::O;
                            l + b!("s")
                        }
                        v @ Vowel::O => {
                            *v = Vowel::A;
                            l + b!("s")
                        }
                        v @ (Vowel::AHat | Vowel::EHat | Vowel::IHat | Vowel::OHat) => {
                            *v = v.unhat();
                            l + b!("s")
                        }
                        _ => l + b!("st"),
                    };
                    (b.clone() + b!("s"), l, b + b!("d"))
                } else {
                    let l = match motion {
                        MotionType::Static => self.c.clone() + Onset::from(Consonant::L),
                        MotionType::Allative => {
                            fuse_d(self.a.clone(), FusionConsonant::Th) + go!("er")
                        }
                        MotionType::Ablative => {
                            fuse_d(self.a.clone(), FusionConsonant::T) + go!("as")
                        }
                    };

                    (b.clone(), l, b)
                };

                if self.n.to_dw().letter_sum() % 2 != 0 {
                    NounForm::II(DeclensionII {
                        n,
                        l,
                        s,
                        theta: Vowel::E,
                        lambda: Vowel::E,
                        kind: DeclensionIIKind::U(DeclensionIIuKind::Vr),
                    })
                } else {
                    NounForm::III(DeclensionIII {
                        n,
                        l,
                        s,
                        kind: DeclensionIIIKind::Os,
                    })
                }
            }
        };

        out.nominal = noun_form.decline(Clareth::Mass)?.map(|a| match a {
            Phrase::Word(w) => w,
            _ => panic!(
                "nominal form declension should have produced only one word; got {}",
                a
            ),
        });

        Ok(())
    }

    fn write_attributive_forms(
        &self,
        motion: MotionType,
        ancillary_object_type: ObjectType,
        out: &mut nd_array_type!(DecoratedWord, Attachment::COUNT, ObjectType::COUNT),
    ) -> Result<(), InflectionError> {
        for attachment in Attachment::VALUES {
            let stem = match (motion, attachment) {
                (MotionType::Static, Attachment::Adverbial) => StemOrSylls::Stem(self.v.clone()),
                (MotionType::Static, Attachment::Adnominal) => StemOrSylls::Stem(self.n.clone()),
                (MotionType::Allative, Attachment::Adverbial) => {
                    StemOrSylls::Sylls(Self::attach_affix_with_syncope(self.a.clone(), f!("ar")))
                }
                (MotionType::Allative, Attachment::Adnominal) => {
                    StemOrSylls::Sylls(Self::attach_affix_with_syncope(self.a.clone(), f!("ara")))
                }
                (MotionType::Ablative, Attachment::Adverbial) => {
                    StemOrSylls::Sylls(Self::attach_affix_with_syncope(
                        self.a.clone(),
                        self.adverbial_abessive_suffix(),
                    ))
                }
                (MotionType::Ablative, Attachment::Adnominal) => {
                    StemOrSylls::Sylls(Self::attach_affix_with_syncope(self.a.clone(), f!("eis")))
                }
            };
            let starts_with_e = stem.starts_with_e();
            for object_type in ObjectType::VALUES {
                let w = match &stem {
                    StemOrSylls::Stem(s) => StemOrSylls::Stem(s.clone().map(|s| {
                        Sylls::from(
                            RELATIONAL_OBJECT_PREFIXES[starts_with_e as usize]
                                [object_type as usize],
                        ) + &s
                    })),
                    StemOrSylls::Sylls(s) => StemOrSylls::Sylls(s.clone().map(|s| {
                        Sylls::from(
                            RELATIONAL_OBJECT_PREFIXES[starts_with_e as usize]
                                [object_type as usize],
                        ) + &s
                    })),
                };

                // Ancilliary object suffix
                let w = Self::apply_ancillary_object_suffix(w, ancillary_object_type);

                out[attachment as usize][object_type as usize] = w.to_dw();
            }
        }
        Ok(())
    }

    fn adverbial_abessive_suffix(&self) -> SimpleSuffixRef<'_> {
        if self.a.ends_with_empty_bridge() {
            f!("jas")
        } else {
            let last_vowel = self.a.frag.last_tile().2;
            match last_vowel {
                Vowel::E | Vowel::EHat => f!("as"),
                Vowel::A | Vowel::AHat => f!("es"),
                _ => {
                    let letter_sum = DecoratedWord::letter_sum(&self.a.to_dw());
                    [f!("as"), f!("es")][((letter_sum >> 1) & 1) as usize]
                }
            }
        }
    }

    fn attach_affix_with_syncope(
        stem: Decorated<AStem>,
        affix: SimpleSuffixRef,
    ) -> Decorated<Sylls> {
        if stem.ends_with_empty_bridge()
            && affix.get_glide(0).is_some_and(|glide| glide == Glide::None)
            && Some(stem.frag.last_tile().2) == affix.get_vowel(0)
        {
            let idx = stem.frag.tiles.len() - 1;
            let mut stem = stem.map(|stem| stem.peel().peel().peelx().peel() + affix);
            let v = &mut stem.frag.tiles[idx].2;
            *v = v.invert();
            return stem;
        }
        stem + affix
    }

    fn attach_affix_with_syncope_2(
        stem: Decorated<AStem>,
        affix: GlideToOnsetRef,
    ) -> Decorated<AStem> {
        if stem.ends_with_empty_bridge()
            && affix.get_glide(0).is_some_and(|glide| glide == Glide::None)
            && Some(stem.frag.last_tile().2) == affix.get_vowel(0)
        {
            let idx = stem.frag.tiles.len() - 1;
            let mut stem = stem.map(|stem| stem.peel().peel().peelx().peel() + affix);
            let v = &mut stem.frag.tiles[idx].2;
            *v = v.invert();
            return stem;
        }
        stem + affix
    }

    fn apply_ancillary_object_suffix(
        cumul: StemOrSylls,
        ancillary_object_type: ObjectType,
    ) -> Decorated<ParsedWord> {
        match cumul {
            StemOrSylls::Stem(stem) => {
                if ancillary_object_type == ObjectType::None
                    && stem.frag.last_tile().3 == SimpleCoda::Empty
                {
                    match stem.frag.remainder.0 {
                        Initial::Empty => return stem.map(|s| s.peel().peel().peelx() + Coda::S),
                        Initial::Single(Consonant::L) => {
                            return stem.map(|s| s.peel().peel().peelx() + Coda::Ls)
                        }
                        _ => (),
                    }
                }
                match ANCILLARY_OBJECT_SUFFIXES_ONSET_END[ancillary_object_type as usize] {
                    Some(suffix) => stem + suffix,
                    None => fuse_with_eps_d(stem),
                }
            }
            StemOrSylls::Sylls(sylls) => {
                if ancillary_object_type == ObjectType::None {
                    match sylls.frag.last_tile().3 {
                        SimpleCoda::Empty => return sylls.map(|s| s.peelx() + Coda::S),
                        SimpleCoda::L => return sylls.map(|s| s.peelx() + Coda::Ls),
                        _ => (),
                    }
                }
                (sylls + ANCILLARY_OBJECT_SUFFIXES_SYLLABIC_END[ancillary_object_type as usize])
                    .map(ParsedWord::from_sylls)
            }
        }
    }

    pub fn get_headword_v(&self) -> Headword {
        Headword::Enumerated {
            items: vec![Phrase::of(fuse_with_eps_d(self.a.clone()).to_dw())],
            exhaustive: false,
        }
    }
}

static RELATIONAL_OBJECT_PREFIXES: [[SyllsRef; ObjectType::COUNT]; 2] = [
    [
        y!(""),
        y!("e"),
        y!("ec"),
        y!("en"),
        y!("ef"),
        y!("êc"),
        y!("ên"),
        y!("o"),
        y!("oc"),
        y!("on"),
        y!("of"),
        y!("er"),
        y!("os"),
        y!("an"),
        y!("or"),
        y!("ran"),
        y!("ac"),
        y!("ren"),
        y!("fê"),
        y!("ce"),
        y!("re"),
    ],
    [
        y!(""),
        y!("el"),
        y!("ec"),
        y!("en"),
        y!("ef"),
        y!("êc"),
        y!("ên"),
        y!("o"),
        y!("oc"),
        y!("on"),
        y!("of"),
        y!("er"),
        y!("os"),
        y!("an"),
        y!("or"),
        y!("ran"),
        y!("ac"),
        y!("ren"),
        y!("fês"),
        y!("cen"),
        y!("reþ"),
    ],
];

static ANCILLARY_OBJECT_SUFFIXES_ONSET_END: [Option<SuffixRef>; ObjectType::COUNT] = [
    Some(x!("es")),
    Some(x!("ef")),
    Some(x!("ecþ")),
    Some(x!("if")),
    Some(x!("af")),
    Some(x!("êcþ")),
    Some(x!("îf")),
    Some(x!("or")),
    Some(x!("ocþ")),
    Some(x!("orþ")),
    Some(x!("of")),
    Some(x!("ir")),
    Some(x!("jos")),
    Some(x!("aren")),
    Some(x!("oł")),
    Some(x!("ens")),
    Some(x!("ac")),
    Some(x!("erþ")),
    None,
    Some(x!("ef")),
    Some(x!("iren")),
];

static ANCILLARY_OBJECT_SUFFIXES_SYLLABIC_END: [SyllsRef; ObjectType::COUNT] = [
    y!("es"),
    y!("ef"),
    y!("ecþ"),
    y!("if"),
    y!("af"),
    y!("êcþ"),
    y!("îf"),
    y!("or"),
    y!("ocþ"),
    y!("orþ"),
    y!("of"),
    y!("ir"),
    y!("jos"),
    y!("ran"),
    y!("lor"),
    y!("ren"),
    y!("ac"),
    y!("erþ"),
    y!(""),
    y!("lef"),
    y!("rin"),
];

static SCAFFOLDING_FORMS: nd_array_type!(
    ParsedWordRef,
    Polarity::COUNT,
    Aspect::COUNT,
    SubjectType::COUNT
) = [
    [
        // affirmative
        [
            // imperfective
            w!("ve"),
            w!("ven"),
            w!("vjaþ"),
            w!("vef"),
            w!("veac"),
            w!("vea"),
            w!("ves"),
            w!("vesen"),
            w!("vełar"),
            w!("vełaf"),
            w!("veła"),
            w!("vełan"),
            w!("von"),
            w!("ver"),
        ],
        [
            // perfective
            w!("vel"),
            w!("vins"),
            w!("vjaþis"),
            w!("vesif"),
            w!("veacel"),
            w!("varel"),
            w!("verþ"),
            w!("vinse"),
            w!("veris"),
            w!("vełesaf"),
            w!("vełal"),
            w!("vełans"),
            w!("veron"),
            w!("vros"),
        ],
    ],
    [
        // negative
        [
            // imperfective
            w!("ce"),
            w!("cen"),
            w!("čaþ"),
            w!("cef"),
            w!("cjor"),
            w!("cea"),
            w!("ces"),
            w!("cesen"),
            w!("cełar"),
            w!("cełaf"),
            w!("ceła"),
            w!("cełan"),
            w!("cþon"),
            w!("cir"),
        ],
        [
            // perfective
            w!("cel"),
            w!("cins"),
            w!("čaþis"),
            w!("cesif"),
            w!("tacel"),
            w!("cjarel"),
            w!("cerþ"),
            w!("cinse"),
            w!("ceris"),
            w!("cełesaf"),
            w!("cełal"),
            w!("cełans"),
            w!("ceþen"),
            w!("cþaros"),
        ],
    ],
];
