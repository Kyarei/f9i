/*!
Utilities for working with nested arrays.

For the given values of `N`, this module provides the following:

* `map_Nd`: applies a function to each element of an array, consuming it in the process
* `map_ref_Nd`: applies a function to each element of an array without consuming it
* `try_map_Nd`: applies a function returning a [Result] to each element of an array, exiting early when an [Err] is returned
* `try_map_ref_Nd`: like `try_map_Nd` but doesn't consume the array
* `zip_Nd`: zips together two multidimensional arrays
* `zip_with_Nd`: zips together two multidimensional arrays and applies a function on each of them
*/

use default_boxed::DefaultBoxed;
use std::alloc::{alloc as alloc_raw, handle_alloc_error, Layout};
use std::mem::{transmute, transmute_copy, MaybeUninit};
use std::ptr;

pub fn zip_arrays_with<T, U, V, const N: usize>(
    mut f: impl FnMut(T, U) -> V,
    t: [T; N],
    u: [U; N],
) -> [V; N] {
    let mut result: [MaybeUninit<V>; N] = unsafe {
        // SAFETY: the elements of result do not require initialization
        MaybeUninit::uninit().assume_init()
    };
    for (i, (t, u)) in IntoIterator::into_iter(t)
        .zip(IntoIterator::into_iter(u))
        .enumerate()
    {
        result[i].write(f(t, u));
    }
    unsafe {
        // SAFETY: both types are of the same size, and the entire array is filled.
        // We have to use transmute_copy instead of transmute here because the compiler can't deduce that the two types we're converting between have the same size.
        transmute_copy::<_, [V; N]>(&result)
    }
}

/// A more macro-friendly way to write an n-dimensional array type.
///
/// For example, `nd_array_type!(i32, 6, 7)` translates to `[[i32; 7]; 6]`.
#[macro_export]
macro_rules! nd_array_type {
    ($elem: ty) => {
        $elem
    };
    ($elem: ty, $dim0: expr) => {
        [$elem; $dim0]
    };
    ($elem: ty, $dim0: expr, $($dims: expr),*) => {
        [nd_array_type!($elem, $($dims),*); $dim0]
    };
}

macro_rules! product {
    ($($dims: expr),*) => {
        1 $(* $dims)*
    };
}

macro_rules! recursive_map {
    ($f: expr, $expr: expr) => {
        $f($expr)
    };
    ($f: expr, $expr: expr, $slice0: ident) => {
        $expr.map(|axis| recursive_map!($f, axis))
    };
    ($f: expr, $expr: expr, $slice0: ident, $($slices: ident),*) => {
        $expr.map(|axis| recursive_map!($f, axis, $($slices),*))
    };
}

macro_rules! recursive_map_ref {
    ($f: expr, $expr: expr) => {
        $f(&$expr)
    };
    ($f: expr, $expr: expr, $slice0: ident) => {
        $expr.each_ref().map(|axis| recursive_map_ref!($f, axis))
    };
    ($f: expr, $expr: expr, $slice0: ident, $($slices: ident),*) => {
        $expr.each_ref().map(|axis| recursive_map_ref!($f, axis, $($slices),*))
    };
}

macro_rules! recursive_map_box {
    ($f: expr, $dest: ident, $src: ident) => {
        let x = unsafe { ptr::read($src) };
        let fx = $f(x);
        unsafe { ptr::write($dest, fx); }
    };
    ($f: expr, $dest: ident, $src: ident, $slice0: ident) => {
        for i in 0..$slice0 {
            let $dest = unsafe { ($dest as *mut [_]).get_unchecked_mut(i) };
            let $src = unsafe { ($src as *const [_]).get_unchecked(i) };
            recursive_map_box!($f, $dest, $src);
        }
    };
    ($f: expr, $dest: ident, $src: ident, $slice0: ident, $($slices: ident),*) => {
        for i in 0..$slice0 {
            let $dest = unsafe { ($dest as *mut [_]).get_unchecked_mut(i) };
            let $src = unsafe { ($src as *const [_]).get_unchecked(i) };
            recursive_map_box!($f, $dest, $src, $($slices),*);
        }
    };
}

macro_rules! recursive_try_map {
    ($f: expr, $expr: expr) => {
        $f($expr)
    };
    ($f: expr, $expr: expr, $slice0: ident) => {
        $expr.try_map(|axis| recursive_try_map!($f, axis))
    };
    ($f: expr, $expr: expr, $slice0: ident, $($slices: ident),*) => {
        $expr.try_map(|axis| recursive_try_map!($f, axis, $($slices),*))
    };
}

macro_rules! recursive_try_map_ref {
    ($f: expr, $expr: expr) => {
        $f(&$expr)
    };
    ($f: expr, $expr: expr, $slice0: ident) => {
        $expr.each_ref().try_map(|axis| recursive_try_map_ref!($f, axis))
    };
    ($f: expr, $expr: expr, $slice0: ident, $($slices: ident),*) => {
        $expr.each_ref().try_map(|axis| recursive_try_map_ref!($f, axis, $($slices),*))
    };
}

macro_rules! recursive_zip_with {
    ($f: expr, $expr1: expr, $expr2: expr) => {
        $f($expr1, $expr2)
    };
    ($f: expr, $expr1: expr, $expr2: expr, $slice0: ident) => {
        zip_arrays_with(|u, v| recursive_zip_with!($f, u, v), $expr1, $expr2)
    };
    ($f: expr, $expr1: expr, $expr2: expr, $slice0: ident, $($slices: ident),*) => {
        zip_arrays_with(|u, v| recursive_zip_with!($f, u, v, $($slices),*), $expr1, $expr2)
    };
}

macro_rules! recursive_shape_assign_unchecked {
    ($iter: expr, $dest: expr) => {
        *$dest = MaybeUninit::new($iter.next().unwrap_unchecked());
    };
    ($iter: expr, $dest: expr, $slice0: ident) => {
        for elem in &mut $dest[..] {
            *elem = MaybeUninit::new($iter.next().unwrap_unchecked());
        }
    };
    ($iter: expr, $dest: expr, $slice0: ident, $($slices: ident),*) => {
        for elem in &mut $dest[..] {
            recursive_shape_assign_unchecked!($iter, elem, $($slices),*);
        }
    };
}

macro_rules! recursive_flatten {
    ($expr: expr) => {
        iter::once($expr)
    };
    ($expr: expr, $slice0: ident) => {
        IntoIterator::into_iter($expr)
    };
    ($expr: expr, $slice0: ident, $($slices: ident),*) => {
        IntoIterator::into_iter($expr).map(|elem| recursive_flatten!(elem, $($slices),*)).flatten()
    };
}

macro_rules! generate_nested_defs {
    (($map: ident, $map_ref: ident, $map_box: ident, $try_map: ident, $try_map_ref: ident, $zip_with: ident, $shape: ident, $flatten: ident), [$($dims: ident),*], [$($idxs: ident),*]) => {
        pub fn $map<T, U, F, $(const $dims: usize),*>(mut f: F, array: nd_array_type!(T, $($dims),*)) -> nd_array_type!(U, $($dims),*)
        where
            F: FnMut(T) -> U, {
            recursive_map!(f, array, $($dims),*)
        }
        pub fn $map_ref<T, U, F, $(const $dims: usize),*>(mut f: F, array: &nd_array_type!(T, $($dims),*)) -> nd_array_type!(U, $($dims),*)
        where
            F: FnMut(&T) -> U, {
            recursive_map_ref!(f, array, $($dims),*)
        }
        pub fn $map_box<T, U, F, $(const $dims: usize),*>(mut f: F, array: Box<nd_array_type!(T, $($dims),*)>) -> Box<nd_array_type!(U, $($dims),*)>
        where
            F: FnMut(T) -> U, {
            let array: Box<MaybeUninit<nd_array_type!(T, $($dims),*)>> = unsafe { transmute(array) };
            let mut result: Box<MaybeUninit<nd_array_type!(U, $($dims),*)>> = Box::new_uninit();
            {
                let dest = result.as_mut_ptr();
                let src =  array.as_ptr();
                recursive_map_box!(f, dest, src, $($dims),*)
            }
            unsafe { transmute(result) }
        }
        pub fn $try_map<T, U, F, E, $(const $dims: usize),*>(mut f: F, array: nd_array_type!(T, $($dims),*)) -> Result<nd_array_type!(U, $($dims),*), E>
        where
            F: FnMut(T) -> Result<U, E>, {
            recursive_try_map!(f, array, $($dims),*)
        }
        pub fn $try_map_ref<T, U, F, E, $(const $dims: usize),*>(mut f: F, array: &nd_array_type!(T, $($dims),*)) -> Result<nd_array_type!(U, $($dims),*), E>
        where
            F: FnMut(&T) -> Result<U, E>, {
            recursive_try_map_ref!(f, array, $($dims),*)
        }
        pub fn $zip_with<T, U, V, F, $(const $dims: usize),*>(mut f: F, array1: nd_array_type!(T, $($dims),*), array2: nd_array_type!(U, $($dims),*)) -> nd_array_type!(V, $($dims),*)
        where
            F: FnMut(T, U) -> V, {
            recursive_zip_with!(f, array1, array2, $($dims),*)
        }
        pub fn $shape<T, $(const $dims: usize),*>(input: Vec<T>) -> Result<nd_array_type!(T, $($dims),*), Vec<T>> {
            let expected_elems = product!($($dims),*);
            if input.len() != expected_elems {
                return Err(input);
            }
            let mut input_iter = input.into_iter();
            let mut array: nd_array_type!(MaybeUninit<T>, $($dims),*) = unsafe {
                // SAFETY: we are assigning to an array of MaybeUninits
                MaybeUninit::uninit().assume_init()
            };
            unsafe {
                // SAFETY: checked that there were enough elements in input
                recursive_shape_assign_unchecked!(input_iter, array, $($dims),*);
                // SAFETY: both types are of the same size, and the entire array is filled.
                // We have to use transmute_copy instead of transmute here because the compiler can't deduce that the two types we're converting between have the same size.
                Ok(transmute_copy::<_, nd_array_type!(T, $($dims),*)>(&array))
            }
        }
        pub fn $flatten<T, $(const $dims: usize),*>(input: nd_array_type!(T, $($dims),*)) -> impl Iterator<Item=T> {
            recursive_flatten!(input, $($dims),*)
        }
    };
}

generate_nested_defs! { (map_2d, map_ref_2d, map_box_2d, try_map_2d, try_map_ref_2d, zip_with_2d, shape_2d, flatten_2d), [A, B], [a, b] }
generate_nested_defs! { (map_3d, map_ref_3d, map_box_3d, try_map_3d, try_map_ref_3d, zip_with_3d, shape_3d, flatten_3d), [A, B, C], [a, b, c] }
generate_nested_defs! { (map_4d, map_ref_4d, map_box_4d, try_map_4d, try_map_ref_4d, zip_with_4d, shape_4d, flatten_4d), [A, B, C, D], [a, b, c, d] }
generate_nested_defs! { (map_5d, map_ref_5d, map_box_5d, try_map_5d, try_map_ref_5d, zip_with_5d, shape_5d, flatten_5d), [A, B, C, D, Z], [a, b, c, d, z] }
generate_nested_defs! { (map_6d, map_ref_6d, map_box_6d, try_map_6d, try_map_ref_6d, zip_with_6d, shape_6d, flatten_6d), [A, B, C, D, Z, Y], [a, b, c, d, z, y] }

pub fn default_boxed_array<T: DefaultBoxed + Sized, const N: usize>() -> Box<[T; N]> {
    assert!(N <= (isize::MAX as usize));
    let layout = Layout::new::<[T; N]>();
    unsafe {
        if layout.size() == 0 {
            return Box::from_raw(ptr::NonNull::<[T; N]>::dangling().as_ptr());
        }
        let raw = alloc_raw(layout) as *mut [T; N];
        let raw_i = raw.cast::<T>();
        if raw.is_null() {
            handle_alloc_error(layout)
        } else {
            for i in 0..N {
                T::default_in_place(raw_i.add(i));
            }
            Box::from_raw(raw)
        }
    }
}
