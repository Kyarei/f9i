use std::{
    collections::HashMap,
    io::{Error as IOError, Result as IOResult, Write},
};

use f9i_core::{category::AxisType, mgc::Phrase};
use regex::Captures;

use crate::{
    lexical::EntryRecord,
    output::{
        helper::word_label_to_element_id,
        regex::{ARK_PATTERN, PERCENT_PATTERN},
        tablewalker::TableWalker,
    },
    table::Table,
};

use super::regex::create_reverse_index_lookup;

fn str_literal(str: &str) -> String {
    let mut s: String = String::with_capacity(str.len() + 2);
    s.push('"');
    for c in str.chars() {
        match c {
            '"' => s.push_str(r#"\""#),
            '\\' => s.push_str(r#"\\"#),
            _ => s.push(c),
        }
    }
    s.push('"');
    s
}

fn write_str_literal<W: Write>(fh: &mut W, str: &str) -> IOResult<()> {
    let s = str_literal(str);
    fh.write_all(s.as_bytes())
}

fn interpolate_extra(
    record: &EntryRecord,
    all_records: &[EntryRecord],
    reverse_lookup: &HashMap<&str, usize>,
) -> String {
    let pp_replaced =
        PERCENT_PATTERN.replace_all(&record.extra, |caps: &Captures| match &caps[1] {
            "POS" => str_literal(&record.pos),
            "PP" => "%%".into(),
            x => format!("%%{x}%%"),
        });
    ARK_PATTERN
        .replace_all(&pp_replaced, |caps: &Captures| {
            let label = &caps[1];
            str_literal(
                &reverse_lookup
                    .get(label)
                    .map(|i| all_records[*i].headword.to_string())
                    .unwrap_or_else(|| format!("LABEL {label}")),
            )
        })
        .to_string()
}

struct Walker<'a, W: Write> {
    fh: &'a mut W,
    hw_s: &'a str,
}

impl<'a, W: Write> Walker<'a, W> {
    fn w(&mut self, s: &str) -> IOResult<()> {
        self.fh.write_all(s.as_bytes())
    }
}

impl<'a, W: Write> TableWalker<Option<Phrase>, IOError> for Walker<'a, W> {
    fn begin_table(&mut self, _prior_axes: &[AxisType], _valuess: &[u8]) -> Result<(), IOError> {
        self.w(r##"(figure(table(thead(tr(th"##)
    }

    fn visit_first_cell(&mut self, row_type: AxisType, col_type: AxisType) -> Result<(), IOError> {
        write_str_literal(
            self.fh,
            &format!(
                "{} \\ {}",
                row_type.get_display_name(),
                col_type.get_display_name()
            ),
        )
    }

    fn visit_col_header(&mut self, label: &str) -> Result<(), IOError> {
        self.w(")(th")?;
        write_str_literal(self.fh, label)
    }

    fn begin_body(&mut self, header: bool) -> Result<(), IOError> {
        self.w(if header { ")))(tbody" } else { "(tbody" })
    }

    fn begin_row(&mut self, label: &str) -> Result<(), IOError> {
        self.w(r##"(tr(th((class"rowhead"))"##)?;
        write_str_literal(self.fh, label)?;
        self.w(")")
    }

    fn visit_body_cell(&mut self, value: &Option<Phrase>) -> Result<(), IOError> {
        match value {
            Some(value) => {
                self.w(r##"(td(span((lang"art-x-ncs-v9"))"##)?;
                write_str_literal(self.fh, &value.to_string())?;
                self.w(r##"))"##)
            }
            None => self.w(r##"(td)"##),
        }
    }

    fn end_row(&mut self) -> Result<(), IOError> {
        self.w(")")
    }

    fn end_table(&mut self, prior_axes: &[AxisType], values: &[u8]) -> Result<(), IOError> {
        self.w(r##"))(figcaption"Inflections for ""##)?;
        self.w(r##"(span((lang"art-x-ncs-v9"))"##)?;
        write_str_literal(self.fh, self.hw_s)?;
        self.w(r##")"##)?;
        let mut caption = String::new();
        if !values.is_empty() {
            caption.push_str(" – ");
            let mut first = true;
            for (prior_axis, prefix_val) in prior_axes.iter().zip(values) {
                if !first {
                    caption.push_str(", ");
                }
                first = false;
                if prior_axis.wants_label() {
                    caption.push_str(prior_axis.get_display_name());
                    caption.push_str(" = ");
                }
                caption.push_str(prior_axis.get_axis_labels()[*prefix_val as usize]);
            }
        }
        write_str_literal(self.fh, &caption)?;
        self.w("))")
    }
}

pub fn write_table<W: Write>(
    fh: &mut W,
    hw_s: &str,
    table: &Table<Option<Phrase>>,
) -> IOResult<()> {
    let mut walker = Walker { fh, hw_s };
    walker.walk_table(table)
}

pub fn write_entry_records<W: Write>(fh: &mut W, records: &[EntryRecord]) -> IOResult<()> {
    let reverse_lookup = create_reverse_index_lookup(records);
    for record in records {
        let hw_s = record.headword.to_string();
        fh.write_all(br##"(h2((id""##)?;
        fh.write_all(word_label_to_element_id(&record.label).as_bytes())?;
        fh.write_all(br##""))(span((lang"art-x-ncs-v9"))"##)?;
        write_str_literal(fh, &hw_s)?;
        fh.write_all(br##"))(div((class"f9t-input"))"##)?;
        write_str_literal(fh, &record.input)?;
        fh.write_all(br##")"##)?;
        fh.write_all(interpolate_extra(record, records, &reverse_lookup).as_bytes())?;
        if !record.tables.is_empty() {
            fh.write_all(
                concat!(
                    r##"(div((class"declension-tables"))(details"##,
                    r##"(summary"Show inflections")"##
                )
                .as_bytes(),
            )?;
            for table in &record.tables {
                write_table(fh, &hw_s, table)?;
            }
            fh.write_all(br##"))"##)?;
        }
    }
    Ok(())
}
