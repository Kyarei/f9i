use std::{
    io::{Error as IOError, Result as IOResult, Write},
    mem::take,
};

use f9i_core::{category::AxisType, mgc::Phrase};

use crate::{output::tablewalker::TableWalker, table::Table};

use comfy_table::{presets, Attribute, Cell as CCell, Color, Row as CRow, Table as CTable};

struct Walker<'a, W: Write> {
    fh: &'a mut W,
    hw_s: &'a str,
    table: CTable,
    pending_row: CRow,
}

impl<'a, W: Write> Walker<'a, W> {
    fn w(&mut self, s: &str) -> IOResult<()> {
        self.fh.write_all(s.as_bytes())
    }
}

impl<'a, W: Write> TableWalker<Option<Phrase>, IOError> for Walker<'a, W> {
    fn begin_table(&mut self, prior_axes: &[AxisType], values: &[u8]) -> Result<(), IOError> {
        self.w("Inflections for ")?;
        self.w(self.hw_s)?;
        if !values.is_empty() {
            self.w(" – ")?;
            let mut first = true;
            for (prior_axis, prefix_val) in prior_axes.iter().zip(values) {
                if !first {
                    self.w(", ")?;
                }
                first = false;
                if prior_axis.wants_label() {
                    self.w(prior_axis.get_display_name())?;
                    self.w(" = ")?;
                }
                self.w(prior_axis.get_axis_labels()[*prefix_val as usize])?;
            }
        }
        self.w("\n")
    }

    fn visit_first_cell(&mut self, row_type: AxisType, col_type: AxisType) -> Result<(), IOError> {
        self.pending_row.add_cell(
            CCell::new(format!(
                "{} \\ {}",
                row_type.get_display_name(),
                col_type.get_display_name()
            ))
            .add_attribute(Attribute::Bold),
        );
        Ok(())
    }

    fn visit_col_header(&mut self, label: &str) -> Result<(), IOError> {
        self.pending_row
            .add_cell(CCell::new(label).add_attribute(Attribute::Bold));
        Ok(())
    }

    fn begin_body(&mut self, header: bool) -> Result<(), IOError> {
        let row = take(&mut self.pending_row);
        if header {
            self.table.set_header(row);
        }
        Ok(())
    }

    fn begin_row(&mut self, label: &str) -> Result<(), IOError> {
        self.pending_row
            .add_cell(CCell::new(label).add_attribute(Attribute::Bold));
        Ok(())
    }

    fn visit_body_cell(&mut self, value: &Option<Phrase>) -> Result<(), IOError> {
        self.pending_row.add_cell(match value {
            Some(value) => CCell::new(value),
            None => CCell::new("---").fg(Color::Grey),
        });
        Ok(())
    }

    fn end_row(&mut self) -> Result<(), IOError> {
        let row = take(&mut self.pending_row);
        self.table.add_row(row);
        Ok(())
    }

    fn end_table(&mut self, _prior_axes: &[AxisType], _values: &[u8]) -> Result<(), IOError> {
        self.table.load_preset(presets::UTF8_BORDERS_ONLY);
        writeln!(&mut self.fh, "{}\n", &self.table)?;
        take(&mut self.table);
        Ok(())
    }
}

pub fn write_table<W: Write>(
    fh: &mut W,
    hw_s: &str,
    table: &Table<Option<Phrase>>,
) -> IOResult<()> {
    let mut walker = Walker {
        fh,
        hw_s,
        table: CTable::new(),
        pending_row: CRow::new(),
    };
    walker.walk_table(table)
}
