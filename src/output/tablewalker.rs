//! Module for visiting a table.

use f9i_core::category::AxisType;
use smallvec::{smallvec, SmallVec};

use crate::table::Table;

/// A trait for visiting a [Table] for display.
pub trait TableWalker<T, E> {
    /// Starts visiting a table.
    fn begin_table(&mut self, prior_axes: &[AxisType], values: &[u8]) -> Result<(), E>;
    /// Visit the first (top-left) cell of a table.
    fn visit_first_cell(&mut self, row_type: AxisType, col_type: AxisType) -> Result<(), E>;
    /// Visit a column header cell of a table with the given label.
    fn visit_col_header(&mut self, label: &str) -> Result<(), E>;
    /// Starts visiting the body of a table.
    fn begin_body(&mut self, header: bool) -> Result<(), E>;
    /// Starts visiting a row of a table with the given label.
    fn begin_row(&mut self, label: &str) -> Result<(), E>;
    /// Visit a body cell of a table with the given value.
    fn visit_body_cell(&mut self, value: &T) -> Result<(), E>;
    /// Finishes visiting a row of a table.
    fn end_row(&mut self) -> Result<(), E>;
    /// Finishes visiting a table.
    fn end_table(&mut self, prior_axes: &[AxisType], values: &[u8]) -> Result<(), E>;

    fn walk_table_h(
        &mut self,
        all_axes: &[AxisType],
        start: usize,
        entries: &[T],
        prefix: &mut SmallVec<[u8; 8]>,
    ) -> Result<(), E> {
        let (prior_axes, axes) = all_axes.split_at(start);
        let rank = axes.len();
        match rank {
            0 => todo!("rank-0 table rendering is not implemented"),
            1 => {
                let cat = axes[0];
                self.begin_table(prior_axes, prefix)?;
                self.begin_body(false)?;
                for (label, entry) in Iterator::zip(cat.get_axis_labels().iter(), entries.iter()) {
                    self.begin_row(label)?;
                    self.visit_body_cell(entry)?;
                    self.end_row()?;
                }
                self.end_table(prior_axes, prefix)
            }
            2 => {
                let row_cat = axes[0];
                let col_cat = axes[1];
                self.begin_table(prior_axes, prefix)?;
                self.visit_first_cell(row_cat, col_cat)?;
                for col_label in col_cat.get_axis_labels() {
                    self.visit_col_header(col_label)?;
                }
                self.begin_body(true)?;
                let mut idx: usize = 0;
                for row_label in row_cat.get_axis_labels() {
                    self.begin_row(row_label)?;
                    for _ in 0..col_cat.get_count() {
                        self.visit_body_cell(&entries[idx])?;
                        idx += 1;
                    }
                    self.end_row()?;
                }
                self.end_table(prior_axes, prefix)
            }
            _ => {
                // Split it into sub-tables; again, this won't panic
                let cat = axes[0];
                let cat_count = cat.get_count();
                debug_assert!(entries.len() % cat_count == 0);
                let stride = entries.len() / cat_count;
                for i in 0..cat_count {
                    prefix.push(i as u8);
                    self.walk_table_h(
                        all_axes,
                        start + 1,
                        &entries[i * stride..(i + 1) * stride],
                        prefix,
                    )?;
                    prefix.pop();
                }
                Ok(())
            }
        }
    }

    /// Visits the given table.
    ///
    /// If the input table has 3 or more dimensions, then
    /// 2-dimensional slices are taken from the table and
    /// each slice is visited individually.
    fn walk_table(&mut self, table: &Table<T>) -> Result<(), E> {
        let axes: Vec<_> = table
            .axes
            .iter()
            .take_while(|e| e.is_some())
            .map(|e| e.unwrap())
            .collect();
        let mut stack = smallvec![];
        self.walk_table_h(&axes, 0, &table.entries, &mut stack)
    }
}
