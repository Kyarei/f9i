//! The phi-consonant.

use f9i_core::{
    assemblage::{AStem, Decorated},
    mgc::{Coda, Consonant, Initial, SimpleCoda},
};

/// Describes a possible value for a phi-consonant.
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum GenCons {
    /// ⟦f⟧
    F,
    /// ⟦ł⟧
    Lh,
}

impl From<GenCons> for SimpleCoda {
    #[inline]
    fn from(c: GenCons) -> Self {
        match c {
            GenCons::F => SimpleCoda::F,
            GenCons::Lh => SimpleCoda::Lh,
        }
    }
}

impl From<GenCons> for Coda {
    #[inline]
    fn from(c: GenCons) -> Self {
        SimpleCoda::from(c).into()
    }
}

impl From<GenCons> for Consonant {
    #[inline]
    fn from(c: GenCons) -> Self {
        match c {
            GenCons::F => Consonant::F,
            GenCons::Lh => Consonant::Lh,
        }
    }
}

impl From<GenCons> for Initial {
    #[inline]
    fn from(c: GenCons) -> Self {
        Initial::Single(c.into())
    }
}

fn is_labial(c: Consonant) -> bool {
    matches!(
        c,
        Consonant::P
            | Consonant::PDot
            | Consonant::F
            | Consonant::FDot
            | Consonant::V
            | Consonant::VDot
            | Consonant::M
            | Consonant::MDot
            | Consonant::Mp
            | Consonant::Vp
            | Consonant::Vf
    )
}

fn is_labial_onset(c: Initial) -> bool {
    match c {
        Initial::Cf | Initial::Gv | Initial::Tf | Initial::Dv => true,
        Initial::Single(c) if is_labial(c) => true,
        Initial::R(c) if is_labial(c.into()) => true,
        Initial::L(c) if is_labial(c.into()) => true,
        _ => false,
    }
}

fn is_labial_coda(c: SimpleCoda) -> bool {
    matches!(c, SimpleCoda::F)
}

fn is_voiced(c: Consonant) -> bool {
    matches!(
        c,
        Consonant::V
            | Consonant::G
            | Consonant::D
            | Consonant::Dh
            | Consonant::Hst
            | Consonant::DDot
            | Consonant::GDot
            | Consonant::VDot
            | Consonant::DhDot
    )
}

fn is_voiced_onset(c: Initial) -> bool {
    match c {
        Initial::Gdh | Initial::Gv | Initial::Dv => true,
        Initial::Single(c) if is_voiced(c) => true,
        Initial::R(c) if is_voiced(c.into()) => true,
        Initial::L(c) if is_voiced(c.into()) => true,
        _ => false,
    }
}

impl GenCons {
    /// Determines the phi-consonant to use for a given stem.
    pub fn for_stem(stem: &Decorated<AStem>) -> Self {
        let stem = &stem.frag;
        if (is_labial_onset(stem.remainder.0) || is_voiced_onset(stem.remainder.0))
            && !matches!(
                stem.tiles.last(),
                Some((_, _, _, SimpleCoda::L | SimpleCoda::Lh))
            )
        {
            return GenCons::Lh;
        }
        for tile in &stem.tiles {
            if is_labial_onset(tile.0) || is_labial_coda(tile.3) {
                return GenCons::Lh;
            }
        }
        GenCons::F
    }
}
