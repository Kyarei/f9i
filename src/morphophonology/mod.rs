//! Morphophonological processes in Ŋarâþ Crîþ, as specified in the [grammar].
//!
//! [grammar]: https://ncv9.flirora.xyz/grammar/phonology/layer0.html

use f9i_core::{
    assemblage::{SimpleSuffixRef, SuffixRef},
    mgc::{Coda, Glide, SimpleCoda, Vowel},
};

pub mod fuse;
pub mod gencons;

/// Creates a terminal suffix containing the given vowel.
pub fn v2x(v: Vowel) -> SuffixRef<'static> {
    SuffixRef {
        remainder: (Glide::None, v, Coda::Empty),
        tiles: &[],
    }
}

/// Creates a terminal suffix containing the given vowel and coda.
pub fn vc2x(v: Vowel, c: Coda) -> SuffixRef<'static> {
    SuffixRef {
        remainder: (Glide::None, v, c),
        tiles: &[],
    }
}

/// Creates a nonterminal suffix containing the given vowel.
pub fn v2f(v: Vowel) -> SimpleSuffixRef<'static> {
    SimpleSuffixRef {
        remainder: (Glide::None, v, SimpleCoda::Empty),
        tiles: &[],
    }
}

/// Creates a nonterminal suffix containing the given vowel and simple coda.
pub fn vc2f(v: Vowel, c: SimpleCoda) -> SimpleSuffixRef<'static> {
    SimpleSuffixRef {
        remainder: (Glide::None, v, c),
        tiles: &[],
    }
}
