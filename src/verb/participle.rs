//! Conjugation of verb participles.
use std::fmt::{Display, Write};

use f9i_core::{
    assemblage::{AStem, Decorated, GlideToOnsetRef, ParsedWord, SuffixRef},
    category::{unpack, AxisType, Case, Gender, RCase, VNumber},
    mgc::{Coda, Consonant, Epf, FusionConsonant, Glide, Initial, Phrase, SimpleCoda, Vowel},
};
use f9i_ivm_macro::{b, go, nw, w, x};

use crate::{
    error::InflectionError,
    morphophonology::{fuse::fuse_d, gencons::GenCons, v2f, v2x, vc2f, vc2x},
    nd_array_type,
    table::Table,
};

use super::{base::StemExt, VerbForm, VerbTables};

/// The *genus* of a verb, without any further information.
#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub enum Genus {
    I,
    II,
    III,
}

impl Genus {
    pub fn axis_type(self) -> AxisType {
        match self {
            Genus::I => AxisType::HGender,
            Genus::II => AxisType::HNumber,
            Genus::III => AxisType::H3,
        }
    }

    pub fn resolve_hgender_and_number(self, hgender: Gender, hnumber: VNumber) -> usize {
        match self {
            Genus::I => hgender as usize,
            Genus::II => hnumber as usize,
            Genus::III => match hnumber {
                VNumber::Singular => hgender as usize,
                _ => 3,
            },
        }
    }

    pub fn hgender_hnumber_pairs(self) -> &'static [(Option<Gender>, Option<VNumber>)] {
        match self {
            Genus::I => &[
                (Some(Gender::Celestial), None),
                (Some(Gender::Terrestrial), None),
                (Some(Gender::Human), None),
            ],
            Genus::II => &[
                (None, Some(VNumber::Singular)),
                (None, Some(VNumber::Dual)),
                (None, Some(VNumber::Plural)),
                (None, Some(VNumber::Generic)),
            ],
            Genus::III => &[
                (Some(Gender::Celestial), Some(VNumber::Singular)),
                (Some(Gender::Terrestrial), Some(VNumber::Singular)),
                (Some(Gender::Human), Some(VNumber::Singular)),
                (None, Some(VNumber::Plural)),
            ],
        }
    }

    fn linear_index(self, decl: VerbParticipleCategories) -> usize {
        let axis_type = self.axis_type();
        let num_cols = axis_type.get_count();
        let num_rows = Case::COUNT;

        let plane_idx = decl.rcase as usize;
        let row_idx = decl.hcase as usize;
        let col_idx = self.resolve_hgender_and_number(decl.hgender, decl.hnumber);

        col_idx + num_cols * (row_idx + num_rows * plane_idx)
    }
}

/// The grammatical categories relevant to verb participle conjugation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct VerbParticipleCategories {
    // Omitting this for now, lest we end up with thousands of forms.
    // obj_marking: ObjectType,
    pub rcase: RCase,
    pub hcase: Case,
    pub hgender: Gender,
    pub hnumber: VNumber,
}

/// The grammatical categories relevant to verb participle conjugation. This is returned when iterating through forms; categories that don't matter have None values.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct VerbParticipleCategoriesOpt {
    // Omitting this for now, lest we end up with thousands of forms.
    // obj_marking: ObjectType,
    pub rcase: RCase,
    pub hcase: Case,
    pub hgender: Option<Gender>,
    pub hnumber: Option<VNumber>,
}

/// A container for holding participle forms.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Par<T> {
    pub genus: Genus,
    pub data: Box<[T]>,
}

impl<T> Par<T> {
    pub fn get(&self, decl: VerbParticipleCategories) -> &T {
        &self.data[self.genus.linear_index(decl)]
    }
    pub fn get_mut(&mut self, decl: VerbParticipleCategories) -> &mut T {
        &mut self.data[self.genus.linear_index(decl)]
    }

    pub fn for_each<F>(&self, mut f: F)
    where
        F: FnMut((VerbParticipleCategoriesOpt, &T)),
    {
        let mut i = 0;
        for rcase in RCase::VALUES {
            for hcase in Case::VALUES {
                for (hgender, hnumber) in self.genus.hgender_hnumber_pairs().iter().copied() {
                    f((
                        VerbParticipleCategoriesOpt {
                            rcase,
                            hcase,
                            hgender,
                            hnumber,
                        },
                        &self.data[i],
                    ));
                    i += 1
                }
            }
        }
    }

    pub fn to_table(self) -> Table<Option<T>> {
        Table {
            axes: unpack(&[AxisType::RCase, AxisType::HCase, self.genus.axis_type()]),
            entries: Vec::from(self.data).into_iter().map(Some).collect(),
        }
    }

    pub fn i(mut f: impl FnMut(RCase, Case, Gender) -> T) -> Self {
        let mut buf = Vec::with_capacity(RCase::COUNT * Case::COUNT * Gender::COUNT);
        for rcase in RCase::VALUES {
            for hcase in Case::VALUES {
                for gender in Gender::VALUES {
                    buf.push(f(rcase, hcase, gender));
                }
            }
        }
        Self {
            genus: Genus::I,
            data: buf.into_boxed_slice(),
        }
    }

    pub fn ii(mut f: impl FnMut(RCase, Case, VNumber) -> T) -> Self {
        let mut buf = Vec::with_capacity(RCase::COUNT * Case::COUNT * Gender::COUNT);
        for rcase in RCase::VALUES {
            for hcase in Case::VALUES {
                for hnumber in VNumber::VALUES {
                    buf.push(f(rcase, hcase, hnumber));
                }
            }
        }
        Self {
            genus: Genus::II,
            data: buf.into_boxed_slice(),
        }
    }

    pub fn iii(mut f: impl FnMut(RCase, Case, Option<Gender>) -> T) -> Self {
        let mut buf = Vec::with_capacity(RCase::COUNT * Case::COUNT * Gender::COUNT);
        for rcase in RCase::VALUES {
            for hcase in Case::VALUES {
                for hgender in Gender::VALUES {
                    buf.push(f(rcase, hcase, Some(hgender)));
                }
                buf.push(f(rcase, hcase, None));
            }
        }
        Self {
            genus: Genus::III,
            data: buf.into_boxed_slice(),
        }
    }
}

/// Represents one of the four instrumental and abessive ending sets for genus I participles.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum InstAbessEndings {
    Alpha,
    Beta,
    Gamma,
    Delta,
}

impl InstAbessEndings {
    fn beta_or_gamma_inst_after_l(hgender: Gender) -> SuffixRef<'static> {
        match hgender {
            Gender::Celestial => x!("a"),
            Gender::Terrestrial => x!("on"),
            Gender::Human => x!("ac"),
        }
    }
    fn beta_or_gamma_abess_after_l(hgender: Gender) -> SuffixRef<'static> {
        match hgender {
            Gender::Celestial => x!("a"),
            Gender::Terrestrial => x!("or"),
            Gender::Human => x!("ac"),
        }
    }

    fn instrumental(
        self,
        stem: Decorated<AStem>,
        rcase: RCase,
        hgender: Gender,
    ) -> Decorated<ParsedWord> {
        match self {
            InstAbessEndings::Alpha => stem.append_fin_suffix(match hgender {
                Gender::Celestial => x!("epa"),
                Gender::Terrestrial => x!("epos"),
                Gender::Human => x!("epac"),
            }),
            InstAbessEndings::Beta | InstAbessEndings::Gamma => match rcase {
                RCase::Nominative => {
                    stem.append_fin_vowel(Vowel::E)
                        + b!("l")
                        + Self::beta_or_gamma_inst_after_l(hgender)
                }
                _ => {
                    let mut stem = stem;
                    let onset = stem.frag.remainder.0;
                    match onset {
                        Initial::Single(Consonant::Th) => {
                            stem.frag.remainder.0 = Initial::L(Epf::Th)
                        }
                        Initial::Single(Consonant::T) => {
                            stem.frag.remainder.0 = Initial::L(Epf::T)
                        }
                        Initial::Single(Consonant::N) => {
                            let last_tile = stem.frag.last_tile_mut();
                            assert_eq!(last_tile.3, SimpleCoda::Empty);
                            last_tile.3 = SimpleCoda::N;
                            stem.frag.remainder.0 = Initial::Single(Consonant::L);
                        }
                        _ => unreachable!("non-nominative rcase infix must have a final onset of either ⟦þ-⟧ or ⟦n-⟧"),
                    }
                    stem + Self::beta_or_gamma_inst_after_l(hgender)
                }
            },
            InstAbessEndings::Delta => stem.append_fin_suffix(x!("êl")),
        }
    }

    fn abessive(
        self,
        stem: Decorated<AStem>,
        rcase: RCase,
        hgender: Gender,
    ) -> Decorated<ParsedWord> {
        match self {
            InstAbessEndings::Alpha => stem.append_fin_suffix(match hgender {
                Gender::Celestial | Gender::Terrestrial => x!("eši"),
                Gender::Human => x!("ešic"),
            }),
            InstAbessEndings::Beta => match rcase {
                RCase::Nominative => {
                    stem.append_fin_vowel(Vowel::E)
                        + b!("n")
                        + Self::beta_or_gamma_abess_after_l(hgender)
                }
                _ => fuse_d(stem, FusionConsonant::N) + Self::beta_or_gamma_abess_after_l(hgender),
            },
            InstAbessEndings::Gamma => match rcase {
                RCase::Nominative => {
                    stem.append_fin_vowel(Vowel::E)
                        + b!("t")
                        + Self::beta_or_gamma_abess_after_l(hgender)
                }
                _ => fuse_d(stem, FusionConsonant::T) + Self::beta_or_gamma_abess_after_l(hgender),
            },
            InstAbessEndings::Delta => stem.append_fin_suffix(match hgender {
                Gender::Celestial => x!("eva"),
                Gender::Terrestrial => x!("evor"),
                Gender::Human => x!("evac"),
            }),
        }
    }
}

impl Display for InstAbessEndings {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(match self {
            InstAbessEndings::Alpha => 'α',
            InstAbessEndings::Beta => 'β',
            InstAbessEndings::Gamma => 'γ',
            InstAbessEndings::Delta => 'δ',
        })
    }
}

/// One of the four subspecies of species I₁.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum I1Subspecies {
    VxOs,
    VxOr,
    NxOn,
    NxOr,
}

/// One of the two subspecies of species I₃.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u8)]
pub enum I3Subspecies {
    W,
    X,
}

impl Display for I3Subspecies {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(match self {
            I3Subspecies::W => 'w',
            I3Subspecies::X => 'x',
        })
    }
}

/// One of the two rcase infix types of species II₃.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u8)]
pub enum II3Infixes {
    S,
    V,
}

impl Display for II3Infixes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(match self {
            II3Infixes::S => 's',
            II3Infixes::V => 'v',
        })
    }
}

/// Represents a participle species in Ŋarâþ Crîþ.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Species {
    /// Species I₁.
    I1(I1Subspecies, Vowel, InstAbessEndings),
    /// Species I₃.
    I3(I3Subspecies, InstAbessEndings),
    /// Species II₄.
    II4(Vowel, SimpleCoda),
    /// Species II₃.
    II3(II3Infixes),
    /// Species III₂.
    III2,
}

impl Species {
    pub fn add_participle_hw_entries(
        &self,
        conjugated: &VerbTables<Phrase>,
        out: &mut Vec<Phrase>,
    ) {
        let mut f = |rcase, hcase, hgender, hnumber| {
            out.push(
                conjugated
                    .participle_forms
                    .get(VerbParticipleCategories {
                        rcase,
                        hcase,
                        hgender,
                        hnumber,
                    })
                    .clone(),
            )
        };
        match self {
            Species::I1(_, _, _) | Species::I3(_, _) => {
                f(
                    RCase::Nominative,
                    Case::Nominative,
                    Gender::Celestial,
                    VNumber::Singular,
                );
                f(
                    RCase::Nominative,
                    Case::Abessive,
                    Gender::Celestial,
                    VNumber::Singular,
                );
                f(
                    RCase::Accusative,
                    Case::Nominative,
                    Gender::Terrestrial,
                    VNumber::Singular,
                );
            }
            Species::II4(_, _) | Species::II3(_) => {
                f(
                    RCase::Nominative,
                    Case::Nominative,
                    Gender::Celestial,
                    VNumber::Singular,
                );
                f(
                    RCase::Accusative,
                    Case::Nominative,
                    Gender::Terrestrial,
                    VNumber::Singular,
                );
            }
            Species::III2 => {
                f(
                    RCase::Accusative,
                    Case::Nominative,
                    Gender::Celestial,
                    VNumber::Singular,
                );
                f(
                    RCase::Dative,
                    Case::Nominative,
                    Gender::Terrestrial,
                    VNumber::Singular,
                );
            }
        }
    }
}

impl Display for Species {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Species::I1(sub, lambda, ia) => write!(
                f,
                "I₁·{}{}·{}/{}",
                lambda,
                match sub {
                    I1Subspecies::VxOs | I1Subspecies::VxOr => "",
                    I1Subspecies::NxOn | I1Subspecies::NxOr => "n",
                },
                match sub {
                    I1Subspecies::VxOs => "os",
                    I1Subspecies::VxOr | I1Subspecies::NxOr => "or",
                    I1Subspecies::NxOn => "on",
                },
                ia
            ),
            Species::I3(wx, ia) => write!(f, "I₃{wx}/{ia}"),
            Species::II4(lambda, sigma) => write!(f, "II₄·{lambda}{sigma}"),
            Species::II3(sv) => write!(f, "II₃{sv}"),
            Species::III2 => write!(f, "III₂"),
        }
    }
}

impl VerbForm {
    pub fn conjugate_participle(&self) -> Result<Par<Phrase>, InflectionError> {
        match self.species {
            Species::I1(sub, lambda, ia_endings) => {
                self.conjugate_participle_i1(sub, lambda, ia_endings)
            }
            Species::I3(wx, ia_endings) => self.conjugate_participle_i3(wx, ia_endings),
            Species::II4(lambda, sigma) => match (lambda, sigma) {
                (Vowel::O, sigma) if sigma != SimpleCoda::Empty => {
                    self.conjugate_participle_ii4_t(sigma)
                }
                _ => self.conjugate_participle_ii4_c(lambda, sigma),
            },
            Species::II3(sv) => self.conjugate_participle_ii3(sv),
            Species::III2 => self.conjugate_participle_iii2(),
        }
    }

    pub fn conjugate_participle_i1(
        &self,
        sub: I1Subspecies,
        lambda: Vowel,
        ia_endings: InstAbessEndings,
    ) -> Result<Par<Phrase>, InflectionError> {
        let lambda_gamma = lambda.gamma();
        let th = match self.q.frag.remainder.0.as_consonants().last() {
            Some(Consonant::Th | Consonant::Dh) => Consonant::T,
            _ => Consonant::Th,
        };
        let infixes = [
            go!(""),
            GlideToOnsetRef {
                remainder: (Glide::None,),
                tiles: &[(
                    Glide::None,
                    Vowel::A,
                    SimpleCoda::Empty,
                    Initial::Single(th),
                )],
            },
            GlideToOnsetRef {
                remainder: (Glide::None,),
                tiles: &[(
                    Glide::None,
                    lambda_gamma,
                    SimpleCoda::Empty,
                    Initial::Single(th),
                )],
            },
            GlideToOnsetRef {
                remainder: (Glide::None,),
                tiles: &[(
                    Glide::None,
                    lambda_gamma,
                    SimpleCoda::Empty,
                    Initial::Single(Consonant::N),
                )],
            },
            GlideToOnsetRef {
                remainder: (Glide::None,),
                tiles: &[(Glide::None, Vowel::A, SimpleCoda::N, Initial::Single(th))],
            },
            GlideToOnsetRef {
                remainder: (Glide::None,),
                tiles: &[(
                    Glide::None,
                    lambda_gamma,
                    SimpleCoda::N,
                    Initial::Single(th),
                )],
            },
        ];
        Ok(Par::i(|rcase, hcase, gender| {
            let infix = infixes[rcase as usize];
            let stem = match rcase {
                RCase::Nominative => &self.r,
                _ => &self.q,
            }
            .clone();
            let stem = stem.append_fin_go(infix);

            let n = matches!(sub, I1Subspecies::NxOn | I1Subspecies::NxOr);

            let word = match (hcase, gender) {
                (Case::Nominative, Gender::Celestial) => stem.append_fin_suffix(if n {
                    vc2x(lambda, Coda::N)
                } else {
                    v2x(lambda)
                }),
                (Case::Nominative, Gender::Terrestrial) => stem.append_fin_suffix(match sub {
                    I1Subspecies::VxOs => x!("os"),
                    I1Subspecies::VxOr | I1Subspecies::NxOr => x!("or"),
                    I1Subspecies::NxOn => x!("on"),
                }),
                (Case::Nominative, Gender::Human) => stem.append_fin_suffix(if n {
                    vc2x(lambda, Coda::N)
                } else {
                    vc2x(lambda, Coda::C)
                }),
                (Case::Accusative, Gender::Celestial) => {
                    if n {
                        stem.append_fin_vowel(lambda) + nw!("na")
                    } else {
                        stem.append_fin_suffix(vc2x(lambda, Coda::N))
                    }
                }
                (Case::Accusative, Gender::Terrestrial) => stem.append_fin_suffix(match sub {
                    I1Subspecies::NxOn => x!("anon"),
                    _ => x!("on"),
                }),
                (Case::Accusative, Gender::Human) => {
                    if n {
                        stem.append_fin_vowel(match lambda {
                            Vowel::A => Vowel::E,
                            _ => lambda,
                        }) + nw!("an")
                    } else {
                        stem.append_fin_suffix(x!("ôr"))
                    }
                }
                (Case::Dative, Gender::Celestial | Gender::Human) => stem.append_fin_suffix(if n {
                    vc2x(lambda, Coda::Ns)
                } else {
                    vc2x(lambda, Coda::S)
                }),
                (Case::Dative, Gender::Terrestrial) => {
                    stem.append_fin_suffix(if n { x!("os") } else { x!("oþ") })
                }
                (Case::Genitive, Gender::Celestial) => stem.append_fin_suffix(if n {
                    x!("il")
                } else {
                    vc2x(lambda_gamma, Coda::N)
                }),
                (Case::Genitive, Gender::Terrestrial) => stem.append_fin_suffix(x!("el")),
                (Case::Genitive, Gender::Human) => {
                    stem.append_fin_suffix(if n { x!("il") } else { x!("jôr") })
                }
                (Case::Locative, Gender::Celestial | Gender::Human) => {
                    stem.append_fin_suffix(vc2x(lambda_gamma, Coda::S))
                }
                (Case::Locative, Gender::Terrestrial) => {
                    stem.append_fin_suffix(if n { x!("ori") } else { x!("orþ") })
                }
                (Case::Instrumental, hgender) => ia_endings.instrumental(stem, rcase, hgender),
                (Case::Abessive, hgender) => ia_endings.abessive(stem, rcase, hgender),
                (Case::Semblative, _) => stem.append_fin_suffix(x!("eł")),
            };
            Phrase::of(word.to_dw())
        }))
    }

    pub fn conjugate_participle_i3(
        &self,
        wx: I3Subspecies,
        ia_endings: InstAbessEndings,
    ) -> Result<Par<Phrase>, InflectionError> {
        #[derive(Copy, Clone)]
        #[repr(u8)]
        enum RRCase {
            Nominative,
            Accusative,
            Dative,
        }

        let r_n = fuse_d(self.r.clone(), FusionConsonant::N);
        let q_n = fuse_d(self.q.clone(), FusionConsonant::N);

        Ok(Par::i(|rcase, hcase, hgender| {
            let stem = match rcase {
                RCase::Nominative => &self.r,
                _ => &self.q,
            };
            let stem_n = match rcase {
                RCase::Nominative => &r_n,
                _ => &q_n,
            };
            let (stem, reduced_rcase) = match rcase {
                RCase::Nominative => (stem, RRCase::Nominative),
                RCase::Accusative => (stem, RRCase::Accusative),
                RCase::Dative => (stem, RRCase::Dative),
                RCase::GenitiveNom => (stem_n, RRCase::Nominative),
                RCase::GenitiveAcc => (stem_n, RRCase::Accusative),
                RCase::GenitiveDat => (stem_n, RRCase::Dative),
            };
            let stem = stem.clone();

            let word = match hcase {
                Case::Instrumental => {
                    let stem = match reduced_rcase {
                        RRCase::Nominative => stem,
                        RRCase::Accusative => fuse_d(stem, FusionConsonant::T),
                        RRCase::Dative => fuse_d(stem, FusionConsonant::Th),
                    };
                    ia_endings.instrumental(stem, RCase::Nominative, hgender)
                }
                Case::Abessive => {
                    let stem = match reduced_rcase {
                        RRCase::Nominative => stem,
                        RRCase::Accusative => fuse_d(stem, FusionConsonant::T),
                        RRCase::Dative => fuse_d(stem, FusionConsonant::Th),
                    };
                    ia_endings.abessive(stem, RCase::Nominative, hgender)
                }
                _ => stem.append_fin_suffix(
                    I3_ENDINGS[wx as usize][reduced_rcase as usize][(hcase as usize).min(5)]
                        [hgender as usize],
                ),
            };
            Phrase::of(word.to_dw())
        }))
    }

    fn participle_ii4_stem(&self, rcase: RCase, lambda: Vowel) -> Decorated<AStem> {
        match rcase {
            RCase::Nominative => self.r.clone(),
            RCase::Accusative => self.q.clone().append_fin_go(go!("el")),
            RCase::Dative => self.q.clone().append_fin_go(go!("eł")),
            RCase::GenitiveNom => self.q.clone().append_fin_vowel(lambda) + b!("n"),
            RCase::GenitiveAcc => self.q.clone().append_fin_vowel(lambda) + b!("nl"),
            RCase::GenitiveDat => self.q.clone().append_fin_vowel(lambda) + b!("nł"),
        }
    }

    pub fn conjugate_participle_ii4_c(
        &self,
        lambda: Vowel,
        sigma: SimpleCoda,
    ) -> Result<Par<Phrase>, InflectionError> {
        let phi_r = GenCons::for_stem(&self.r);
        let phi_q = GenCons::for_stem(&self.q);

        Ok(Par::ii(|rcase, hcase, hnumber| {
            let stem = self.participle_ii4_stem(rcase, lambda);
            let phi = match rcase {
                RCase::Nominative => phi_r,
                _ => phi_q,
            };

            let word = match (hcase, hnumber) {
                (Case::Nominative, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(lambda, Coda::from(sigma)))
                }
                (Case::Nominative, VNumber::Dual) => stem.append_fin_suffix(vc2x(lambda, Coda::C)),
                (Case::Nominative, VNumber::Plural) => stem.append_fin_suffix(v2x(lambda.pi())),
                (Case::Nominative, VNumber::Generic) => {
                    stem.append_fin_suffix(vc2x(lambda.front(), phi.into()))
                }
                (Case::Accusative, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(lambda, Coda::N))
                }
                (Case::Accusative, VNumber::Dual) => stem.append_fin_suffix(x!("ôr")),
                (Case::Accusative, VNumber::Plural) => stem.append_fin_suffix(x!("on")),
                (Case::Accusative, VNumber::Generic) => {
                    stem.append_fin_ssuffix(vc2f(lambda, phi.into())) + w!("en")
                }
                (Case::Dative, VNumber::Singular) => stem.append_fin_suffix(match sigma {
                    SimpleCoda::S => x!("o"),
                    _ => vc2x(lambda, Coda::S),
                }),
                (Case::Dative, VNumber::Dual) => {
                    fuse_d(stem, FusionConsonant::T).append_fin_suffix(vc2x(lambda, Coda::S))
                }
                (Case::Dative, VNumber::Plural) => stem.append_fin_suffix(x!("os")),
                (Case::Dative, VNumber::Generic) => {
                    stem.append_fin_ssuffix(vc2f(lambda, phi.into())) + w!("es")
                }
                (Case::Genitive, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(lambda.gamma(), Coda::N))
                }
                (Case::Genitive, VNumber::Dual) => fuse_d(stem, FusionConsonant::T)
                    .append_fin_suffix(vc2x(lambda.gamma(), Coda::N)),
                (Case::Genitive, VNumber::Plural) => stem.append_fin_suffix(x!("in")),
                (Case::Genitive, VNumber::Generic) => {
                    fuse_d(stem, FusionConsonant::N).append_fin_suffix(vc2x(Vowel::E, phi.into()))
                }
                (Case::Locative, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(lambda.gamma(), Coda::S))
                }
                (Case::Locative, VNumber::Dual) => {
                    stem.append_fin_ssuffix(vc2f(lambda.gamma(), SimpleCoda::S)) + w!("ta")
                }
                (Case::Locative, VNumber::Plural) => stem.append_fin_suffix(x!("is")),
                (Case::Locative, VNumber::Generic) => {
                    stem.append_fin_ssuffix(v2f(Vowel::E)) + Initial::from(Consonant::F) + x!("o")
                }
                (Case::Instrumental, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(lambda.gamma(), Coda::Ls))
                }
                (Case::Instrumental, VNumber::Dual) => {
                    stem.append_fin_ssuffix(vc2f(lambda.gamma(), SimpleCoda::L)) + w!("þa")
                }
                (Case::Instrumental, VNumber::Plural) => stem.append_fin_suffix(x!("ils")),
                (Case::Instrumental, VNumber::Generic) => {
                    stem.append_fin_ssuffix(v2f(Vowel::E)) + Initial::from(Consonant::F) + x!("os")
                }
                (Case::Abessive, VNumber::Singular) => {
                    stem.append_fin_vowel(lambda.gamma()) + nw!("ma")
                }
                (Case::Abessive, VNumber::Dual) => {
                    stem.append_fin_vowel(lambda.gamma()) + nw!("nva")
                }
                (Case::Abessive, VNumber::Plural) => stem.append_fin_suffix(x!("ima")),
                (Case::Abessive, VNumber::Generic) => {
                    stem.append_fin_go(go!("elc")) + Vowel::E + Coda::from(phi)
                }
                (Case::Semblative, VNumber::Singular) => stem.append_fin_suffix(x!("it")),
                (Case::Semblative, VNumber::Dual) => stem.append_fin_suffix(x!("icta")),
                (Case::Semblative, VNumber::Plural) => stem.append_fin_suffix(x!("et")),
                (Case::Semblative, VNumber::Generic) => stem.append_fin_suffix(x!("icþ")),
            };
            Phrase::of(word.to_dw())
        }))
    }

    pub fn conjugate_participle_ii4_t(
        &self,
        sigma: f9i_core::mgc::SimpleCoda,
    ) -> Result<Par<Phrase>, InflectionError> {
        let phi_r = GenCons::for_stem(&self.r);
        let phi_q = GenCons::for_stem(&self.q);

        Ok(Par::ii(|rcase, hcase, hnumber| {
            let stem = self.participle_ii4_stem(rcase, Vowel::O);
            let phi = match rcase {
                RCase::Nominative => phi_r,
                _ => phi_q,
            };

            let word = match (hcase, hnumber) {
                (Case::Nominative, VNumber::Singular) => {
                    stem.append_fin_suffix(vc2x(Vowel::O, Coda::from(sigma)))
                }
                (Case::Nominative, VNumber::Dual) => {
                    stem.append_fin_suffix(vc2x(Vowel::O, Coda::C))
                }
                (Case::Nominative, VNumber::Plural) => stem.append_fin_suffix(match sigma {
                    SimpleCoda::R => x!("es"),
                    _ => x!("or"),
                }),
                (Case::Nominative, VNumber::Generic) => stem.append_fin_suffix(x!("u")),
                (Case::Accusative, VNumber::Singular) => stem.append_fin_suffix(match sigma {
                    SimpleCoda::N => x!("an"),
                    _ => x!("on"),
                }),
                (Case::Accusative, VNumber::Dual) => stem.append_fin_suffix(x!("ar")),
                (Case::Accusative, VNumber::Plural) => stem.append_fin_suffix(x!("en")),
                (Case::Accusative, VNumber::Generic) => {
                    stem.append_fin_suffix(vc2x(Vowel::A, phi.into()))
                }
                (Case::Dative, VNumber::Singular) => stem.append_fin_suffix(match sigma {
                    SimpleCoda::S => x!("oþ"),
                    _ => x!("os"),
                }),
                (Case::Dative, VNumber::Dual) => stem.append_fin_suffix(x!("ocþ")),
                (Case::Dative, VNumber::Plural) => stem.append_fin_suffix(x!("erþ")),
                (Case::Dative, VNumber::Generic) => stem.append_fin_suffix(x!("as")),
                (Case::Genitive, VNumber::Singular) => stem.append_fin_suffix(x!("el")),
                (Case::Genitive, VNumber::Dual) => stem.append_fin_suffix(x!("acel")),
                (Case::Genitive, VNumber::Plural) => stem.append_fin_suffix(x!("il")),
                (Case::Genitive, VNumber::Generic) => {
                    stem.append_fin_suffix(vc2x(Vowel::E, phi.into()))
                }
                (Case::Locative, VNumber::Singular) => stem.append_fin_suffix(x!("ecþ")),
                (Case::Locative, VNumber::Dual) => stem.append_fin_suffix(x!("ecþ")),
                (Case::Locative, VNumber::Plural) => stem.append_fin_suffix(x!("acþ")),
                (Case::Locative, VNumber::Generic) => {
                    stem.append_fin_go(go!("ec")) + Vowel::E + Coda::from(phi)
                }
                (Case::Instrumental, VNumber::Singular) => stem.append_fin_suffix(x!("els")),
                (Case::Instrumental, VNumber::Dual) => stem.append_fin_suffix(x!("elþa")),
                (Case::Instrumental, VNumber::Plural) => stem.append_fin_suffix(x!("ils")),
                (Case::Instrumental, VNumber::Generic) => {
                    stem.append_fin_ssuffix(v2f(Vowel::E)) + Initial::from(phi) + x!("os")
                }
                (Case::Abessive, VNumber::Singular) => stem.append_fin_suffix(x!("ema")),
                (Case::Abessive, VNumber::Dual) => stem.append_fin_suffix(x!("enva")),
                (Case::Abessive, VNumber::Plural) => stem.append_fin_suffix(x!("ima")),
                (Case::Abessive, VNumber::Generic) => {
                    stem.append_fin_go(go!("elc")) + Vowel::E + Coda::from(phi)
                }
                (Case::Semblative, VNumber::Singular) => stem.append_fin_suffix(x!("ot")),
                (Case::Semblative, VNumber::Dual) => stem.append_fin_suffix(x!("octos")),
                (Case::Semblative, VNumber::Plural) => stem.append_fin_suffix(x!("ot")),
                (Case::Semblative, VNumber::Generic) => stem.append_fin_suffix(x!("ocþ")),
            };
            Phrase::of(word.to_dw())
        }))
    }

    pub fn conjugate_participle_ii3(&self, sv: II3Infixes) -> Result<Par<Phrase>, InflectionError> {
        Ok(Par::ii(|rcase, hcase, hnumber| {
            let mut word = match rcase {
                RCase::Nominative => &self.r,
                _ => &self.q,
            }
            .clone()
            .append_fin_go(II3_INFIXES[sv as usize][rcase as usize])
                + II3_SUFFIXES[hcase as usize][(hnumber as usize) % 3];

            if sv == II3Infixes::V && hcase == Case::Abessive {
                word.frag.remainder.3 = Coda::Lh;
            }

            Phrase::of(word.to_dw())
        }))
    }

    pub fn conjugate_participle_iii2(&self) -> Result<Par<Phrase>, InflectionError> {
        Ok(Par::iii(|rcase, hcase, hgender| {
            let i = match rcase {
                RCase::Nominative | RCase::Accusative | RCase::GenitiveNom | RCase::GenitiveAcc => {
                    0
                }
                RCase::Dative | RCase::GenitiveDat => 1,
            };
            let suffix = III2_SUFFIXES[i][hcase as usize][hgender.map(|g| g as usize).unwrap_or(3)];

            let stem = match rcase {
                RCase::Nominative => self.r.clone().append_fin_go(match suffix.get_vowel(0) {
                    Some(Vowel::E) => go!("an"),
                    _ => go!("en"),
                }),
                RCase::Accusative => self.r.clone(),
                RCase::Dative => self.q.clone(),
                RCase::GenitiveNom => self.q.clone().append_fin_go(go!("il")),
                RCase::GenitiveAcc => self.q.clone().append_fin_go(match hcase {
                    Case::Locative => go!("aþ"),
                    _ => go!("ag"),
                }),
                RCase::GenitiveDat => self.q.clone().append_fin_go(go!("eg")),
            };

            let word = stem.append_fin_suffix(suffix);

            Phrase::of(word.to_dw())
        }))
    }
}

static I3_ENDINGS: nd_array_type!(SuffixRef, 2, 3, 6, 3) = [
    // w
    [
        // nominative
        [
            [x!("a"), x!("os"), x!("ac")],
            [x!("an"), x!("on"), x!("ôr")],
            [x!("as"), x!("oþ"), x!("as")],
            [x!("er"), x!("or"), x!("er")],
            [x!("ei"), x!("ei"), x!("ei")],
            [x!("et"), x!("ot"), x!("est")],
        ],
        // accusative
        [
            [x!("e"), x!("ios"), x!("ec")],
            [x!("en"), x!("osi"), x!("en")],
            [x!("es"), x!("ioþ"), x!("es")],
            [x!("eri"), x!("ori"), x!("eri")],
            [x!("ami"), x!("ami"), x!("ami")],
            [x!("aþet"), x!("aþot"), x!("aþest")],
        ],
        // dative
        [
            [x!("ae"), x!("ajos"), x!("ace")],
            [x!("aen"), x!("ajon"), x!("acen")],
            [x!("aes"), x!("ajoþ"), x!("aces")],
            [x!("ari"), x!("aôr"), x!("ari")],
            [x!("emi"), x!("emi"), x!("emi")],
            [x!("eþet"), x!("eþot"), x!("eþest")],
        ],
    ],
    // x
    [
        // nominative
        [
            [x!("iþ"), x!("os"), x!("icþ")],
            [x!("inþ"), x!("on"), x!("orþ")],
            [x!("iþo"), x!("oþ"), x!("icþo")],
            [x!("ir"), x!("or"), x!("ir")],
            [x!("ei"), x!("ei"), x!("ei")],
            [x!("it"), x!("ot"), x!("ist")],
        ],
        // accusative
        [
            [x!("aþ"), x!("avas"), x!("acþ")],
            [x!("anþ"), x!("avan"), x!("arþ")],
            [x!("aþo"), x!("avaþ"), x!("acþo")],
            [x!("ar"), x!("avar"), x!("ar")],
            [x!("ami"), x!("ami"), x!("ami")],
            [x!("aþit"), x!("aþot"), x!("aþist")],
        ],
        // dative
        [
            [x!("eþ"), x!("evas"), x!("ecþ")],
            [x!("enþ"), x!("evan"), x!("erþ")],
            [x!("eþo"), x!("evaþ"), x!("ecþo")],
            [x!("er"), x!("evar"), x!("er")],
            [x!("emi"), x!("emi"), x!("emi")],
            [x!("eþit"), x!("eþot"), x!("eþist")],
        ],
    ],
];

static II3_INFIXES: nd_array_type!(GlideToOnsetRef, 2, RCase::COUNT) = [
    [
        go!("es"),
        go!("est"),
        go!("ist"),
        go!("ens"),
        go!("ent"),
        go!("int"),
    ],
    [
        go!("ev"),
        go!("eft"),
        go!("ift"),
        go!("env"),
        go!("enf"),
        go!("inf"),
    ],
];

static II3_SUFFIXES: nd_array_type!(SuffixRef, Case::COUNT, 3) = [
    [x!("a"), x!("ac"), x!("o")],
    [x!("an"), x!("or"), x!("on")],
    [x!("as"), x!("acþ"), x!("os")],
    [x!("en"), x!("er"), x!("in")],
    [x!("al"), x!("al"), x!("al")],
    [x!("aþ"), x!("aþ"), x!("oþ")],
    [x!("af"), x!("af"), x!("ef")],
    [x!("et"), x!("ecþ"), x!("it")],
];

static III2_SUFFIXES: nd_array_type!(SuffixRef, 2, Case::COUNT, 4) = [
    [
        [x!("ar"), x!("on"), x!("ar"), x!("als")],
        [x!("an"), x!("anon"), x!("an"), x!("alna")],
        [x!("arþ"), x!("os"), x!("as"), x!("arþ")],
        [x!("en"), x!("el"), x!("en"), x!("il")],
        [x!("eca"), x!("ecos"), x!("eac"), x!("ego")],
        [x!("eli"), x!("els"), x!("eli"), x!("ili")],
        [x!("eno"), x!("enos"), x!("eno"), x!("ene")],
        [x!("et"), x!("ot"), x!("est"), x!("et")],
    ],
    [
        [x!("or"), x!("eħon"), x!("or"), x!("els")],
        [x!("aħon"), x!("onon"), x!("aħon"), x!("ana")],
        [x!("orþ"), x!("eħos"), x!("orþ"), x!("orþ")],
        [x!("in"), x!("il"), x!("in"), x!("in")],
        [x!("oca"), x!("ecos"), x!("avac"), x!("avo")],
        [x!("ore"), x!("ore"), x!("ore"), x!("ori")],
        [x!("one"), x!("one"), x!("one"), x!("oneþ")],
        [x!("aħet"), x!("aħot"), x!("aħest"), x!("aħet")],
    ],
];
