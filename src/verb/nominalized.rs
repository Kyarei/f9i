//! Conjugation of nominalized forms of verbs.
use f9i_core::{
    category::{unpack, AxisType, Case, Mood, Nominalized},
    mgc::{Coda, Consonant, Eclipse, Lenite, Onset, Phrase, SimpleCoda, Vowel},
};
use f9i_ivm_macro::{phr, x};
use itertools::Itertools;

use crate::{
    error::InflectionError,
    morphophonology::gencons::GenCons,
    table::{Table, ToTable},
};

use super::{base::StemExt, VerbForm};

/// A container for holding nominalized forms.
pub type Nomz<T> = [T; Nominalized::COUNT];

impl ToTable for Nomz<Phrase> {
    fn to_table(&self) -> Table<Option<Phrase>> {
        let storage = Case::VALUES
            .iter()
            .cartesian_product(Mood::VALUES.iter())
            .map(|(case, mood)| {
                Nominalized::from_case_and_mood(*case, *mood).map(|n| self[n as usize].clone())
            })
            .collect();

        Table {
            axes: unpack(&[AxisType::Case, AxisType::Mood]),
            entries: storage,
        }
    }
}

impl VerbForm {
    pub fn conjugate_nominalized(&self) -> Result<Box<Nomz<Phrase>>, InflectionError> {
        let infinitive = self.get_infinitive();
        let infinitive_e = Phrase::of((self.i.clone() + self.theta + Coda::T).eclipsed().to_dw());
        let phi_i = GenCons::for_stem(&self.i);
        Ok(Box::new([
            // nominative
            Phrase::Append(vec![phr!("o"), infinitive.clone()]),
            // accusative
            Phrase::Append(vec![phr!("on"), infinitive.clone()]),
            // dative indicative
            Phrase::of(
                self.i
                    .clone()
                    .append_fin_suffix(x!("ilt"))
                    .lenited()
                    .to_dw(),
            ),
            // dative subjunctive
            Phrase::of(
                (self.i.clone().append_fin_vowel(Vowel::I)
                    + SimpleCoda::Empty
                    + Onset::from(Consonant::from(phi_i))
                    + x!("os"))
                .lenited()
                .to_dw(),
            ),
            // genitive
            Phrase::Append(vec![phr!("en"), infinitive.clone()]),
            // locative indicative
            Phrase::of(
                self.l
                    .clone()
                    .append_fin_suffix(x!("eve"))
                    .eclipsed()
                    .to_dw(),
            ),
            // locative subjunctive
            Phrase::Append(vec![phr!("a"), infinitive_e.clone()]),
            // instrumental indicative
            Phrase::of(
                self.l
                    .clone()
                    .append_fin_suffix(x!("eveca"))
                    .eclipsed()
                    .to_dw(),
            ),
            // instrumental subjunctive
            Phrase::Append(vec![phr!("ac"), infinitive_e.clone()]),
            // abessive indicative
            Phrase::of(
                self.l
                    .clone()
                    .append_fin_suffix(x!("eveþa"))
                    .eclipsed()
                    .to_dw(),
            ),
            // abessive subjunctive
            Phrase::Append(vec![phr!("aþ"), infinitive_e]),
            // semblative indicative
            Phrase::Append(vec![phr!("it"), infinitive.clone()]),
            // semblative subjunctive
            Phrase::Append(vec![phr!("eti"), infinitive]),
        ]))
    }
}
