//! Dictionary headwords.

use f9i_core::mgc::Phrase;
use std::fmt::{Display, Error as FmtError, Formatter, Write};

/// A headword in a dictionary.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Headword {
    /// A headword consisting of a list of words.
    Enumerated {
        /// The words to list in this headword.
        items: Vec<Phrase>,
        /// Indicates whether the words enumerated (along with the part-of-speech tag) are sufficient to reconstruct the full inflection paradigm of this word.
        ///
        /// If this is false, then the headword will be displayed with ellipses after the last element.
        exhaustive: bool,
    },
    /// A headword consisting of two smaller headwords.
    ///
    /// This is used for compound nouns such as full names.
    Compound(Box<[Headword; 2]>),
}

impl Headword {
    /// Writes the lemma form of a headword to a formatter.
    pub fn write_lemma(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        match self {
            Headword::Enumerated {
                items,
                exhaustive: _,
            } => {
                items[0].fmt(f)?;
            }
            Headword::Compound(components) => {
                components[0].write_lemma(f)?;
                f.write_char(' ')?;
                components[1].write_lemma(f)?;
            }
        }
        Ok(())
    }
    pub fn prepend(self, phr: &Phrase) -> Self {
        match self {
            Headword::Enumerated { items, exhaustive } => Headword::Enumerated {
                items: items.into_iter().map(|p| phr.clone().appended(p)).collect(),
                exhaustive,
            },
            Headword::Compound(components) => {
                let [left, right] = *components;
                Headword::Compound(Box::new([left.prepend(phr), right]))
            }
        }
    }
    pub fn append(self, phr: &Phrase) -> Self {
        match self {
            Headword::Enumerated { items, exhaustive } => Headword::Enumerated {
                items: items.into_iter().map(|p| p.appended(phr.clone())).collect(),
                exhaustive,
            },
            Headword::Compound(components) => {
                let [left, right] = *components;
                Headword::Compound(Box::new([left, right.append(phr)]))
            }
        }
    }
}

impl Display for Headword {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), FmtError> {
        match self {
            Headword::Enumerated { items, exhaustive } => {
                let mut first = true;
                for word in items {
                    if !first {
                        f.write_str(", ")?;
                    }
                    first = false;
                    word.fmt(f)?;
                }
                if !*exhaustive {
                    f.write_str(", …")?;
                }
            }
            Headword::Compound(components) => {
                self.write_lemma(f)?;
                f.write_str(" (")?;
                components[0].fmt(f)?;
                f.write_str("; ")?;
                components[1].fmt(f)?;
                f.write_char(')')?;
            }
        }
        Ok(())
    }
}
