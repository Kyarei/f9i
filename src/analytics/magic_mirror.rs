//! Functions for outputting tables to visualize morphophonological processes in Ŋarâþ Crîþ.

use std::{
    error::Error,
    panic::{catch_unwind, resume_unwind, set_hook, take_hook, UnwindSafe},
};

use comfy_table::{presets, Attribute, Cell, Color, Row, Table};
use custom_error::custom_error;
use f9i_core::{
    analysis::{canonicalize_bridge, resolve_bridge, resolve_bridge_with_prev_vowel, ResolvedCoda},
    assemblage::AStem,
    mgc::{FusionConsonant, Initial, SimpleCoda, Tokenize},
};

use crate::morphophonology::fuse::{fuse, fuse_with_eps, FusionTodo};

custom_error! {
    #[derive(Clone, Eq, PartialEq)]
    pub TooShort{ required_len: usize, actual_len: usize } = "too short (actual len = {actual_len} < required = {required_len}"
}

custom_error! {
    #[derive(Clone, Eq, PartialEq)]
    pub FusionFailed{} = "one or more errors during fusion"
}

fn empty_to_null(s: String) -> String {
    if s.is_empty() {
        "∅".to_owned()
    } else {
        s
    }
}

/// Output a table of bridge resolution results to stdout.
pub fn subcmd_magic_mirror(full: bool) -> Result<(), Box<dyn Error>> {
    let mut n_cells_violating_r1 = 0;
    let table = magic_mirror_base(full, |coda, initial| {
        let canonicalized = canonicalize_bridge(coda, initial);
        let canonical = canonicalized.0 == coda;
        let resolved = resolve_bridge(coda, initial);
        let valid = resolved == (ResolvedCoda::Coda(coda), initial);
        let follows_r1 = {
            let lenited_initial = initial.toggle_lenition();
            let canonicalized_lenited = canonicalize_bridge(coda, lenited_initial);
            let canonical_lenited = canonicalized_lenited.0 == coda;
            let resolved_lenited = resolve_bridge(coda, lenited_initial);
            let valid_lenited = resolved_lenited == (ResolvedCoda::Coda(coda), lenited_initial);
            (valid || !canonical) == (valid_lenited || !canonical_lenited)
        };
        let follows_r4 = !valid || {
            let init_cons = initial.as_consonants();
            init_cons.len() <= 2 || {
                let init2 = Initial::Single(init_cons[0]);
                let resolved2 = resolve_bridge(coda, init2);
                resolved2 == (ResolvedCoda::Coda(coda), init2)
            }
        };
        let contents = match resolved {
            (ResolvedCoda::Coda(c), i) => c.tokens_to_string() + &i.tokens_to_string(),
            (ResolvedCoda::Ng, i) => "ŋ".to_owned() + &i.tokens_to_string(),
        };
        let overlong = contents.chars().filter(|c| *c != '·').count() >= 4;
        let cell = Cell::new(empty_to_null(contents))
            .add_attribute(if canonical {
                if overlong {
                    Attribute::Bold
                } else {
                    Attribute::NormalIntensity
                }
            } else {
                Attribute::Italic
            })
            .add_attribute(if valid || !canonical {
                Attribute::NoUnderline
            } else {
                Attribute::Underlined
            })
            .fg(if !canonical {
                Color::DarkGrey
            } else if !follows_r1 {
                Color::Red
            } else if !follows_r4 {
                Color::Yellow
            } else if valid {
                Color::White
            } else {
                Color::Cyan
            });
        if canonical && !follows_r1 {
            n_cells_violating_r1 += 1;
        }
        cell
    });
    println!("{table}");
    println!("Rule 1 violations: {n_cells_violating_r1}");
    Ok(())
}

/// Output tables showing stem fusion to stdout.
///
/// The final bridge of `stem` is ignored, as this function cycles through all possible bridges for the final bridge.
pub fn subcmd_magic_mirror_fusion(_full: bool, stem: &AStem) -> Result<(), Box<dyn Error>> {
    fn call_fuse_method_to_cell<T: Tokenize>(
        callback: impl FnOnce() -> T + UnwindSafe,
        ambiguity_check: impl FnOnce(&T) -> bool,
        n_failures: &mut i32,
        canonical: bool,
    ) -> Cell {
        let fused = catch_unwind(callback);
        match fused {
            Ok(fused) => Cell::new(fused.tokens_to_string())
                .fg(if canonical {
                    if ambiguity_check(&fused) {
                        Color::Yellow
                    } else {
                        Color::White
                    }
                } else {
                    Color::DarkGrey
                })
                .add_attribute(if canonical {
                    Attribute::NoItalic
                } else {
                    Attribute::Italic
                }),
            Err(e) => {
                if let Some(message) = e.downcast_ref::<FusionTodo>() {
                    *n_failures += 1;
                    let content = match message.fusion_consonant {
                        Some(c) => format!(
                            "! {}{} × {}",
                            message.stem.last_tile().3.tokens_to_string(),
                            message.stem.remainder.0.tokens_to_string(),
                            c
                        ),
                        None => format!(
                            "! {}{} × ε",
                            message.stem.last_tile().3.tokens_to_string(),
                            message.stem.remainder.0.tokens_to_string(),
                        ),
                    };
                    Cell::new(content)
                        .fg(Color::Red)
                        .add_attribute(Attribute::Bold)
                } else {
                    let _ = take_hook();
                    if let Some(message) = e.downcast_ref::<String>() {
                        eprintln!("panic while evaluating: {message}");
                    } else if let Some(message) = e.downcast_ref::<&str>() {
                        eprintln!("panic while evaluating: {message}");
                    }
                    resume_unwind(e);
                }
            }
        }
    }

    set_hook(Box::new(|_| { /* nothing */ }));
    if stem.tiles.is_empty() {
        Err(TooShort {
            required_len: 1,
            actual_len: stem.tiles.len(),
        })?;
    }
    let mut ok = true;

    {
        let mut n_failures = 0;
        let table = magic_mirror_base(false, |coda, initial| {
            let mut stem = stem.clone();
            let last_tile = stem.tiles.last_mut().unwrap();
            (last_tile.1, last_tile.2, last_tile.3, stem.remainder.0) =
                resolve_bridge_with_prev_vowel(last_tile.1, last_tile.2, coda, initial);
            let canonical = (coda, initial) == (last_tile.3, stem.remainder.0);
            call_fuse_method_to_cell(
                || fuse_with_eps(stem),
                |_| false,
                &mut n_failures,
                canonical,
            )
        });
        println!("=== ε ({n_failures} failures) ===");
        println!("{table}");
        if n_failures > 0 {
            ok = false;
        }
    }
    for c in FusionConsonant::VALUES {
        let mut n_failures = 0;
        let table = magic_mirror_base(false, |coda, initial| {
            let mut stem = stem.clone();
            let last_tile = stem.tiles.last_mut().unwrap();
            (last_tile.1, last_tile.2, last_tile.3, stem.remainder.0) =
                resolve_bridge_with_prev_vowel(last_tile.1, last_tile.2, coda, initial);
            let canonical = (coda, initial) == (last_tile.3, stem.remainder.0);
            let stem2 = stem.clone();
            call_fuse_method_to_cell(
                || fuse(stem, c),
                |new_stem| *new_stem == stem2,
                &mut n_failures,
                canonical,
            )
        });
        println!("=== {c} ({n_failures} failures) ===");
        println!("{table}");
        if n_failures > 0 {
            ok = false;
        }
    }
    let _ = take_hook();
    if ok {
        Ok(())
    } else {
        Err(Box::new(FusionFailed {}))
    }
}

fn magic_mirror_base(full: bool, mut callback: impl FnMut(SimpleCoda, Initial) -> Cell) -> Table {
    let mut table = Table::new();
    {
        let mut row = Row::new();
        row.add_cell(Cell::new(""));
        for coda in SimpleCoda::VALUES {
            row.add_cell(
                Cell::new(empty_to_null(coda.tokens_to_string())).add_attribute(Attribute::Bold),
            );
        }
        table.add_row(row);
    }
    let initials: Vec<_> = if full {
        Initial::enumerate_without_eclipse().collect()
    } else {
        Initial::enumerate_base().collect()
    };
    for initial in initials {
        let mut row = Row::new();
        row.add_cell(
            Cell::new(empty_to_null(initial.tokens_to_string())).add_attribute(Attribute::Bold),
        );
        for coda in SimpleCoda::VALUES {
            let cell = callback(coda, initial);
            row.add_cell(cell);
        }
        table.add_row(row);
    }
    table.load_preset(presets::UTF8_BORDERS_ONLY);
    table
}
