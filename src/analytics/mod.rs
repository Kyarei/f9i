//! Code to analyze Ŋarâþ Crîþ v9 inflection.

pub mod magic_mirror;

use std::hash::Hash;
use std::io;
use std::{collections::HashMap, io::Write};

use f9i_core::mgc::{Coda, Consonant, Epf, Glide, Initial, Onset, Syllable, Vowel};
use itertools::Itertools;

fn increment<K: Hash + Eq>(map: &mut HashMap<K, usize>, key: K) {
    *map.entry(key).or_insert(0) += 1;
}

/// Holds statistics for the frequency of phonological features.
///
/// Here, ⟦j⟧ is considered part of the onset rather than the nucleus.
#[derive(Clone, Debug, Default)]
pub struct CorpusPhonologicalStatistics {
    pub onsets: HashMap<Onset, usize>,
    pub nuclei: HashMap<Vowel, usize>,
    pub codas: HashMap<Coda, usize>,
    pub bridges: HashMap<(Coda, Onset), usize>,
    pub bridges_reduced: HashMap<(Coda, Initial), usize>,
}

impl CorpusPhonologicalStatistics {
    pub fn new() -> Self {
        Default::default()
    }

    fn update_from_single_syllable(&mut self, syllable: &Syllable) {
        increment(
            &mut self.onsets,
            Onset::new(syllable.initial, syllable.nucleus.glide),
        );
        increment(&mut self.nuclei, syllable.nucleus.vowel);
        increment(&mut self.codas, syllable.coda);
    }

    fn update_from_syllable_pair(&mut self, s1: &Syllable, s2: &Syllable) {
        increment(
            &mut self.bridges,
            (s1.coda, Onset::new(s2.initial, s2.nucleus.glide)),
        );
        increment(
            &mut self.bridges_reduced,
            (s1.coda, reduce_initial(s2.initial)),
        );
    }

    /// Updates the object with data from a word.
    pub fn update(&mut self, word: &[Syllable]) {
        for syllable in word {
            self.update_from_single_syllable(syllable);
        }
        for (s1, s2) in word.iter().tuple_windows() {
            self.update_from_syllable_pair(s1, s2);
        }
    }

    /// Outputs the data in this object as HTML.
    pub fn output_html<W: Write>(&self, fh: &mut W) -> io::Result<()> {
        // Write header
        fh.write_all(br##"<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"/><title>Corpus analytics</title><style>.fig{text-align:right;min-width:5ch;}</style></head><body><header><h1>Corpus analytics</h1></header><main>"##)?;
        // Onsets...
        fh.write_all(br##"<h2>Onsets</h2><table><thead><tr><th>Onset</th><th>Frequency</th></tr></thead><tbody>"##)?;
        for (onset, freq) in top(&self.onsets) {
            write!(
                fh,
                r##"<tr><td>{onset}</td><td class="fig">{freq}</td></tr>"##
            )?;
        }
        fh.write_all(br##"</tbody></table>"##)?;
        // Nuclei...
        fh.write_all(br##"<h2>Nuclei</h2><table><thead><tr><th>Nucleus</th><th>Frequency</th></tr></thead><tbody>"##)?;
        for (nucleus, freq) in top(&self.nuclei) {
            write!(
                fh,
                r##"<tr><td>{nucleus}</td><td class="fig">{freq}</td></tr>"##
            )?;
        }
        fh.write_all(br##"</tbody></table>"##)?;
        // Codas...
        fh.write_all(br##"<h2>Codas</h2><table><thead><tr><th>Coda</th><th>Frequency</th></tr></thead><tbody>"##)?;
        for (coda, freq) in top(&self.codas) {
            write!(
                fh,
                r##"<tr><td>{coda}</td><td class="fig">{freq}</td></tr>"##
            )?;
        }
        fh.write_all(br##"</tbody></table>"##)?;
        // Bridges...
        let highest_frequency = *self.bridges.values().max().unwrap_or(&1);
        fh.write_all(r##"<h2>Bridges</h2><table><thead><tr><th>Onset 2 \ Coda 1</th><th>∅</th><th>s</th><th>r</th><th>n</th><th>þ</th><th>rþ</th><th>l</th><th>t</th><th>c</th><th>f</th><th>st</th><th>lt</th><th>ns</th><th>ls</th><th>nþ</th><th>cþ</th></tr></thead><tbody>"##.as_bytes())?;
        for glide in [Glide::None, Glide::J] {
            for onset in Initial::enumerate(glide == Glide::None) {
                write!(fh, r##"<tr><th scope="row">{onset}{glide}</th>"##,)?;
                for coda in Coda::VALUES {
                    let frequency = *self
                        .bridges
                        .get(&(coda, Onset::new(onset, glide)))
                        .unwrap_or(&0);
                    let lightness = 95 - 20 * frequency / highest_frequency;
                    if frequency == 0 {
                        write!(fh, r##"<td class="fig"">—</td>"##)?;
                    } else {
                        write!(
                            fh,
                            r##"<td class="fig" style="background-color:hsl(0,100%,{lightness}%)">{frequency}</td>"##
                        )?;
                    }
                }
                fh.write_all(br##"</tr>"##)?;
            }
        }
        fh.write_all(br##"</tbody></table>"##)?;
        // Bridges (reduced)...
        let highest_frequency = *self.bridges_reduced.values().max().unwrap_or(&1);
        fh.write_all(r##"<h2>Bridges (reduced)</h2><table><thead><tr><th>Onset 2 \ Coda 1</th><th>∅</th><th>s</th><th>r</th><th>n</th><th>þ</th><th>rþ</th><th>l</th><th>t</th><th>c</th><th>f</th><th>st</th><th>lt</th><th>ns</th><th>ls</th><th>nþ</th><th>cþ</th></tr></thead><tbody>"##.as_bytes())?;
        for onset in Initial::enumerate_base() {
            write!(fh, r##"<tr><th scope="row">{onset}</th>"##,)?;
            for coda in Coda::VALUES {
                let frequency = *self.bridges_reduced.get(&(coda, onset)).unwrap_or(&0);
                let lightness = 95 - 20 * frequency / highest_frequency;
                if frequency == 0 {
                    write!(fh, r##"<td class="fig"">—</td>"##)?;
                } else {
                    write!(
                        fh,
                        r##"<td class="fig" style="background-color:hsl(0,100%,{lightness}%)">{frequency}</td>"##
                    )?;
                }
            }
            fh.write_all(br##"</tr>"##)?;
        }
        fh.write_all(br##"</tbody></table>"##)?;
        // Write footer
        fh.write_all(br##"</main></body></html>"##)?;
        Ok(())
    }
}

fn base_consonant(c: Consonant) -> Consonant {
    match c {
        Consonant::PDot => Consonant::P,
        Consonant::TDot => Consonant::T,
        Consonant::DDot => Consonant::D,
        Consonant::ChDot => Consonant::Ch,
        Consonant::CDot => Consonant::C,
        Consonant::GDot => Consonant::G,
        Consonant::MDot => Consonant::M,
        Consonant::FDot => Consonant::F,
        Consonant::VDot => Consonant::V,
        Consonant::DhDot => Consonant::Dh,
        _ => c,
    }
}

fn base_epf(c: Epf) -> Epf {
    match c {
        Epf::PDot => Epf::P,
        Epf::TDot => Epf::T,
        Epf::DDot => Epf::D,
        Epf::CDot => Epf::C,
        Epf::GDot => Epf::G,
        Epf::FDot => Epf::F,
        Epf::VDot => Epf::V,
        Epf::DhDot => Epf::Dh,
        _ => c,
    }
}

fn reduce_initial(initial: Initial) -> Initial {
    match initial {
        Initial::Single(c) => Initial::Single(base_consonant(c)),
        Initial::R(c) => Initial::R(base_epf(c)),
        Initial::L(c) => Initial::L(base_epf(c)),
        _ => initial,
    }
}

fn top<K: Hash + Eq>(map: &HashMap<K, usize>) -> impl Iterator<Item = (&K, &usize)> {
    map.iter().sorted_by_key(|(_, f)| usize::MAX - **f)
}
