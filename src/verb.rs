//! Conjugation of verbs in Ŋarâþ Crîþ v9.
//!
//! See the [sections on verbs] in the Ŋarâþ Crîþ v9 grammar for more information.
//!
//! This API is currently subject to change. Really. It's very cursed.
//!
//! [sections on verbs]: https://ncv9.flirora.xyz/grammar/morphology/verbs.html

mod base;
pub mod finite;
pub mod nominalized;
pub mod participle;

use crate::error::InflectionError;
use crate::headword::Headword;
use crate::nest::*;
use crate::table::Table;
use f9i_core::assemblage::AStem;
use f9i_core::assemblage::Decorated;
use f9i_core::assemblage::ParsedWord;
use f9i_core::category::AxisType;
use f9i_core::category::Nominalized;
use f9i_core::category::SubjectType;
use f9i_core::category::{
    unpack, Aspect, Case, Gender, ObjectType, RCase, Tense, Transitivity, VNumber,
    MAX_CATEGORY_TAGS,
};
use f9i_core::mgc::Coda;
use f9i_core::mgc::Vowel;
use f9i_core::mgc::{DecoratedWord, Phrase};

use self::finite::*;
use self::nominalized::Nomz;
use self::participle::Par;
use self::participle::Species;
use self::participle::VerbParticipleCategories;
use self::participle::VerbParticipleCategoriesOpt;

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum VitreousIrregular {
    None,
    Apn(Box<[[Decorated<ParsedWord>; SubjectType::COUNT]; Aspect::COUNT]>),
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum ResinousIrregular {
    None,
}

/// A *material* in Ŋarâþ Crîþ v9: either *vitreous* or *resinous*.
// TODO: this should support irregular verbs
#[repr(u8)]
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Material {
    Vitreous(VitreousIrregular),
    Resinous(ResinousIrregular),
}

impl Material {
    #[inline]
    pub const fn name(&self) -> &'static str {
        match self {
            Material::Vitreous(_) => "vitreous",
            Material::Resinous(_) => "resinous",
        }
    }
    pub const fn abbr(&self) -> &'static str {
        match self {
            Material::Vitreous(VitreousIrregular::None) => "v",
            Material::Resinous(ResinousIrregular::None) => "r",
            Material::Vitreous(_) => "v*",
            Material::Resinous(_) => "r*",
        }
    }
    pub const fn is_irregular(&self) -> bool {
        match self {
            Material::Vitreous(VitreousIrregular::None) => false,
            Material::Resinous(ResinousIrregular::None) => false,
            Material::Vitreous(_) => true,
            Material::Resinous(_) => true,
        }
    }
}

/// A container for all of the conjugated verb forms supported by f9i.
///
/// # Limitations
///
/// * Object suffixes on participle and nominalized forms
///   are not yet supported.
/// * Forms with voice prefixes (such as ⟦do-⟧ or ⟦es-⟧) are
///   not derived.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct VerbTables<T> {
    pub finite_forms: Box<Fin<T>>,
    pub transfinite_forms: Option<Box<Trf<T>>>,
    pub participle_forms: Par<T>,
    pub nominalized_forms: Box<Nomz<T>>,
}

/// Represents a verb in the lexicon.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Verb {
    pub transitivity: Transitivity,
    pub form: VerbForm,
}

/// Represents the information relevant to the conjugation of a verb, such as the stem or the thematic vowel.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct VerbForm {
    pub i: Decorated<AStem>,
    pub n: Decorated<AStem>,
    pub p: Decorated<AStem>,
    pub r: Decorated<AStem>,
    pub q: Decorated<AStem>,
    pub l: Decorated<AStem>,
    pub theta: Vowel,
    pub material: Material,
    pub species: Species,
}

impl VerbForm {
    pub fn conjugate(&self) -> Result<VerbTables<Phrase>, InflectionError> {
        Ok(VerbTables {
            finite_forms: self.conjugate_finite()?,
            transfinite_forms: self.conjugate_transfinite()?,
            participle_forms: self.conjugate_participle()?,
            nominalized_forms: self.conjugate_nominalized()?,
        })
    }

    pub fn headword(&self, conjugated: &VerbTables<Phrase>) -> Headword {
        let mut items = vec![
            self.get_infinitive(),
            conjugated
                .finite_forms
                .form(FiniteVerbCategories {
                    subj_marking: SubjectType::S1,
                    obj_marking: ObjectType::None,
                    tense: Tense::Present,
                    aspect: Aspect::Imperfective,
                })
                .clone(),
            conjugated
                .finite_forms
                .form(FiniteVerbCategories {
                    subj_marking: SubjectType::S1,
                    obj_marking: ObjectType::None,
                    tense: Tense::Past,
                    aspect: Aspect::Imperfective,
                })
                .clone(),
        ];
        self.species
            .add_participle_hw_entries(conjugated, &mut items);
        items.push(conjugated.nominalized_forms[Nominalized::LocativeInd as usize].clone());
        Headword::Enumerated {
            items,
            exhaustive: !self.material.is_irregular(),
        }
    }

    pub fn get_infinitive_w(&self) -> DecoratedWord {
        (self.i.clone() + self.theta + Coda::T).to_dw()
    }

    pub fn get_infinitive(&self) -> Phrase {
        Phrase::of(self.get_infinitive_w())
    }
}

// === Legacy verb inflection ===

pub type Par1<T> = [[[T; Gender::COUNT]; Case::COUNT]; RCase::COUNT];
pub type Par2<T> = [[[T; VNumber::COUNT]; Case::COUNT]; RCase::COUNT];

// Wouldn't it be just great if we could just #[derive(Functor)]?
/// Holds all of the participle forms of a verb.
///
/// These forms come in two types:
/// * **Type 1**: conjugated by rcase, hcase, and *hgender*.
/// * **Type 2**: conjugated by rcase, hcase, and *hnumber*.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum LPar<T> {
    Type1(Par1<T>),
    Type2(Par2<T>),
}

impl<T> LPar<T> {
    pub fn get(&self, decl: VerbParticipleCategories) -> &T {
        match self {
            LPar::Type1(par1) => {
                &par1[decl.rcase as usize][decl.hcase as usize][decl.hgender as usize]
            }
            LPar::Type2(par2) => {
                &par2[decl.rcase as usize][decl.hcase as usize][decl.hnumber as usize]
            }
        }
    }
    pub fn get_mut(&mut self, decl: VerbParticipleCategories) -> &mut T {
        match self {
            LPar::Type1(par1) => {
                &mut par1[decl.rcase as usize][decl.hcase as usize][decl.hgender as usize]
            }
            LPar::Type2(par2) => {
                &mut par2[decl.rcase as usize][decl.hcase as usize][decl.hnumber as usize]
            }
        }
    }
    pub fn map<U, F>(self, f: F) -> LPar<U>
    where
        F: FnMut(T) -> U + Copy,
    {
        match self {
            LPar::Type1(par1) => LPar::Type1(map_3d(f, par1)),
            LPar::Type2(par2) => LPar::Type2(map_3d(f, par2)),
        }
    }
    pub fn map_ref<U, F>(&self, f: F) -> LPar<U>
    where
        F: FnMut(&T) -> U + Copy,
    {
        match self {
            LPar::Type1(par1) => LPar::Type1(map_ref_3d(f, par1)),
            LPar::Type2(par2) => LPar::Type2(map_ref_3d(f, par2)),
        }
    }
    pub fn try_map<U, F, E>(self, f: F) -> Result<LPar<U>, E>
    where
        F: FnMut(T) -> Result<U, E> + Copy,
    {
        match self {
            LPar::Type1(par1) => Ok(LPar::Type1(try_map_3d(f, par1)?)),
            LPar::Type2(par2) => Ok(LPar::Type2(try_map_3d(f, par2)?)),
        }
    }
    pub fn for_each<F>(&self, mut f: F)
    where
        F: FnMut((VerbParticipleCategoriesOpt, &T)),
    {
        match self {
            LPar::Type1(par1) => {
                for (i_rcase, row) in par1.iter().enumerate() {
                    for (i_hcase, row) in row.iter().enumerate() {
                        for (i_hgender, e) in row.iter().enumerate() {
                            let infl = VerbParticipleCategoriesOpt {
                                rcase: RCase::from_ordinal(i_rcase as u8),
                                hcase: Case::from_ordinal(i_hcase as u8),
                                hgender: Some(Gender::from_ordinal(i_hgender as u8)),
                                hnumber: None,
                            };
                            f((infl, e));
                        }
                    }
                }
            }
            LPar::Type2(par2) => {
                for (i_rcase, row) in par2.iter().enumerate() {
                    for (i_hcase, row) in row.iter().enumerate() {
                        for (i_hnumber, e) in row.iter().enumerate() {
                            let infl = VerbParticipleCategoriesOpt {
                                rcase: RCase::from_ordinal(i_rcase as u8),
                                hcase: Case::from_ordinal(i_hcase as u8),
                                hgender: None,
                                hnumber: Some(VNumber::from_ordinal(i_hnumber as u8)),
                            };
                            f((infl, e));
                        }
                    }
                }
            }
        }
    }
    pub fn to_table(self) -> Table<Option<T>> {
        match self {
            LPar::Type1(par1) => Table {
                axes: unpack::<AxisType, MAX_CATEGORY_TAGS>(&[
                    AxisType::RCase,
                    AxisType::HCase,
                    AxisType::HGender,
                ]),
                entries: IntoIterator::into_iter(par1)
                    .flatten()
                    .flatten()
                    .map(Some)
                    .collect::<Vec<Option<T>>>(),
            },
            LPar::Type2(par2) => Table {
                axes: unpack::<AxisType, MAX_CATEGORY_TAGS>(&[
                    AxisType::RCase,
                    AxisType::HCase,
                    AxisType::HNumber,
                ]),
                entries: IntoIterator::into_iter(par2)
                    .flatten()
                    .flatten()
                    .map(Some)
                    .collect::<Vec<Option<T>>>(),
            },
        }
    }
}
