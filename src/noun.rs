//! Declension of nouns in Ŋarâþ Crîþ v9.
//!
//! See the [sections on nouns] in the Ŋarâþ Crîþ v9 grammar for more information.
//!
//! [sections on nouns]: https://ncv9.flirora.xyz/grammar/morphology/nouns.html

mod base;
pub mod fifth;
pub mod first;
pub mod fourth;
pub mod second;
pub mod sixth;
pub mod third;

use crate::error::InflectionError;
use crate::headword::Headword;
use crate::nest::*;
use crate::noun::base::Declension;
use crate::table::{Table, ToTable};
use f9i_core::assemblage::{Decorated, ParsedWord};
use f9i_core::category::{unpack, AxisType, Case, Clareth, Gender, NNumber};
use f9i_core::mgc::{Eclipse, Phrase};
use std::ops::Deref;
use std::slice;

use self::fifth::DeclensionV;
use self::first::DeclensionI;
use self::fourth::DeclensionIV;
use self::second::DeclensionII;
use self::sixth::DeclensionVI;
use self::third::DeclensionIII;

/// The grammatical categories relevant to noun declension.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct NounCategories {
    pub case: Case,
    pub number: NNumber,
}

/// A table of objects indexed by a [NounCategories] object.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum NounTable<T> {
    Singular([[T; 4]; Case::COUNT]),
    Collective([[T; 3]; Case::COUNT]),
    Mass([[T; 2]; Case::COUNT]),
    Universal([[T; 5]; Case::COUNT]),
}

impl<T> NounTable<T> {
    fn construct_aux<F, const N: usize>(f: F, numbers: [NNumber; N]) -> [[T; N]; Case::COUNT]
    where
        F: Fn(Case, NNumber) -> T,
    {
        Case::VALUES.map(|case| numbers.map(|number| f(case, number)))
    }
    /// Construct a [NounTable] by specifying how its elements are inflected.
    ///
    /// The case–number combinations that are actually evaluated depends on the [clareþ](Clareth) specified.
    pub fn construct<F>(clareth: Clareth, f: F) -> NounTable<T>
    where
        F: Fn(Case, NNumber) -> T,
    {
        match clareth {
            Clareth::Singular => NounTable::Singular(Self::construct_aux(
                f,
                [
                    NNumber::Direct,
                    NNumber::Dual,
                    NNumber::Plural,
                    NNumber::Generic,
                ],
            )),
            Clareth::Collective => NounTable::Collective(Self::construct_aux(
                f,
                [NNumber::Direct, NNumber::Singulative, NNumber::Generic],
            )),
            Clareth::Mass => {
                NounTable::Mass(Self::construct_aux(f, [NNumber::Direct, NNumber::Generic]))
            }
            Clareth::Universal => NounTable::Universal(Self::construct_aux(f, NNumber::VALUES)),
        }
    }
    pub const fn clareth(&self) -> Clareth {
        match *self {
            NounTable::Singular(_) => Clareth::Singular,
            NounTable::Collective(_) => Clareth::Collective,
            NounTable::Mass(_) => Clareth::Mass,
            NounTable::Universal(_) => Clareth::Universal,
        }
    }
    pub fn get(&self, decl: NounCategories) -> Option<&T> {
        let row = decl.case as usize;
        match self {
            NounTable::Singular(t) => decl
                .number
                .get_column_index(Clareth::Singular)
                .map(|col| &t[row][col]),
            NounTable::Collective(t) => decl
                .number
                .get_column_index(Clareth::Collective)
                .map(|col| &t[row][col]),
            NounTable::Mass(t) => decl
                .number
                .get_column_index(Clareth::Mass)
                .map(|col| &t[row][col]),
            NounTable::Universal(t) => decl
                .number
                .get_column_index(Clareth::Universal)
                .map(|col| &t[row][col]),
        }
    }
    pub fn get_mut(&mut self, decl: NounCategories) -> Option<&mut T> {
        let row = decl.case as usize;
        match self {
            NounTable::Singular(t) => decl
                .number
                .get_column_index(Clareth::Singular)
                .map(move |col| &mut t[row][col]),
            NounTable::Collective(t) => decl
                .number
                .get_column_index(Clareth::Collective)
                .map(move |col| &mut t[row][col]),
            NounTable::Mass(t) => decl
                .number
                .get_column_index(Clareth::Mass)
                .map(move |col| &mut t[row][col]),
            NounTable::Universal(t) => decl
                .number
                .get_column_index(Clareth::Universal)
                .map(move |col| &mut t[row][col]),
        }
    }
    pub fn map<U, F>(self, f: F) -> NounTable<U>
    where
        F: FnMut(T) -> U,
    {
        match self {
            NounTable::Singular(t) => NounTable::Singular(map_2d(f, t)),
            NounTable::Collective(t) => NounTable::Collective(map_2d(f, t)),
            NounTable::Mass(t) => NounTable::Mass(map_2d(f, t)),
            NounTable::Universal(t) => NounTable::Universal(map_2d(f, t)),
        }
    }
    pub fn map_ref<U, F>(&self, f: F) -> NounTable<U>
    where
        F: FnMut(&T) -> U,
    {
        match self {
            NounTable::Singular(t) => NounTable::Singular(map_ref_2d(f, t)),
            NounTable::Collective(t) => NounTable::Collective(map_ref_2d(f, t)),
            NounTable::Mass(t) => NounTable::Mass(map_ref_2d(f, t)),
            NounTable::Universal(t) => NounTable::Universal(map_ref_2d(f, t)),
        }
    }
    pub fn try_map<U, F, E>(self, f: F) -> Result<NounTable<U>, E>
    where
        F: FnMut(T) -> Result<U, E>,
    {
        Ok(match self {
            NounTable::Singular(t) => NounTable::Singular(try_map_2d(f, t)?),
            NounTable::Collective(t) => NounTable::Collective(try_map_2d(f, t)?),
            NounTable::Mass(t) => NounTable::Mass(try_map_2d(f, t)?),
            NounTable::Universal(t) => NounTable::Universal(try_map_2d(f, t)?),
        })
    }
    pub fn zip_with<U, V, F>(
        self,
        other: NounTable<U>,
        f: F,
    ) -> Result<NounTable<V>, InflectionError>
    where
        F: FnMut(T, U) -> V,
    {
        let cl1 = self.clareth();
        let cl2 = other.clareth();
        match (self, other) {
            (NounTable::Singular(t), NounTable::Singular(u)) => {
                Ok(NounTable::Singular(zip_with_2d(f, t, u)))
            }
            (NounTable::Collective(t), NounTable::Collective(u)) => {
                Ok(NounTable::Collective(zip_with_2d(f, t, u)))
            }
            (NounTable::Mass(t), NounTable::Mass(u)) => Ok(NounTable::Mass(zip_with_2d(f, t, u))),
            _ => Err(InflectionError::ClarethMismatch { cl1, cl2 }),
        }
    }
}

impl NounTable<Phrase> {
    pub fn construct_from_assemblages<F>(clareth: Clareth, decl: F) -> NounTable<Phrase>
    where
        F: Fn(Case, NNumber) -> Decorated<ParsedWord>,
    {
        NounTable::construct(clareth, |case, number| {
            let mut res = decl(case, number);
            if case == Case::Genitive
                && matches!(
                    number,
                    NNumber::Dual | NNumber::Plural | NNumber::Singulative
                )
            {
                res.eclipse_terminal();
            }
            Phrase::of(res.to_dw())
        })
    }
}

impl<T: Default> Default for NounTable<T> {
    fn default() -> Self {
        NounTable::Mass(Default::default())
    }
}

pub struct NounTableIntoIter<T> {
    clareth: Clareth,
    i: usize,
    storage: <Vec<T> as IntoIterator>::IntoIter,
}

impl<T> Iterator for NounTableIntoIter<T> {
    type Item = (NounCategories, T);

    fn next(&mut self) -> Option<Self::Item> {
        match self.storage.next() {
            None => None,
            Some(e) => {
                let stride = self.clareth.num_count();
                let case = Case::from_ordinal((self.i / stride) as u8);
                let number = NNumber::from_index_and_clareth(self.i % stride, self.clareth);
                let res = Some((NounCategories { case, number }, e));
                self.i += 1;
                res
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.storage.size_hint()
    }
}

impl<T> IntoIterator for NounTable<T> {
    type Item = (NounCategories, T);

    type IntoIter = NounTableIntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            NounTable::Singular(t) => NounTableIntoIter {
                clareth: Clareth::Singular,
                i: 0,
                storage: IntoIterator::into_iter(t)
                    .flatten()
                    .collect::<Vec<_>>()
                    .into_iter(),
            },
            NounTable::Collective(t) => NounTableIntoIter {
                clareth: Clareth::Collective,
                i: 0,
                storage: IntoIterator::into_iter(t)
                    .flatten()
                    .collect::<Vec<_>>()
                    .into_iter(),
            },
            NounTable::Mass(t) => NounTableIntoIter {
                clareth: Clareth::Mass,
                i: 0,
                storage: IntoIterator::into_iter(t)
                    .flatten()
                    .collect::<Vec<_>>()
                    .into_iter(),
            },
            NounTable::Universal(t) => NounTableIntoIter {
                clareth: Clareth::Universal,
                i: 0,
                storage: IntoIterator::into_iter(t)
                    .flatten()
                    .collect::<Vec<_>>()
                    .into_iter(),
            },
        }
    }
}

pub struct NounTableIter<'a, T> {
    clareth: Clareth,
    i: usize,
    storage: &'a [T],
}

impl<'a, T> Iterator for NounTableIter<'a, T> {
    type Item = (NounCategories, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        if self.i >= self.storage.len() {
            None
        } else {
            let stride = self.clareth.num_count();
            let case = Case::from_ordinal((self.i / stride) as u8);
            let number = NNumber::from_index_and_clareth(self.i % stride, self.clareth);
            let res = Some((NounCategories { case, number }, &self.storage[self.i]));
            self.i += 1;
            res
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let remaining = self.storage.len() - self.i;
        (remaining, Some(remaining))
    }
}

fn as_flattened<T, const N: usize>(folded: &[[T; N]]) -> &[T] {
    unsafe {
        // SAFETY: [[T; N]] has the same alignment as [T], and the first N * folded.len() elements from folded.as_ptr() are valid.
        slice::from_raw_parts(folded.as_ptr().cast(), folded.len() * N)
    }
}

impl<'a, T> IntoIterator for &'a NounTable<T> {
    type Item = (NounCategories, &'a T);

    type IntoIter = NounTableIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            NounTable::Singular(t) => NounTableIter {
                clareth: Clareth::Singular,
                i: 0,
                storage: as_flattened(t.as_slice()),
            },
            NounTable::Collective(t) => NounTableIter {
                clareth: Clareth::Collective,
                i: 0,
                storage: as_flattened(t.as_slice()),
            },
            NounTable::Mass(t) => NounTableIter {
                clareth: Clareth::Mass,
                i: 0,
                storage: as_flattened(t.as_slice()),
            },
            NounTable::Universal(t) => NounTableIter {
                clareth: Clareth::Universal,
                i: 0,
                storage: as_flattened(t.as_slice()),
            },
        }
    }
}

impl ToTable for NounTable<Phrase> {
    fn to_table(&self) -> Table<Option<Phrase>> {
        let mut table_storage = Vec::new();
        for (_, form) in self {
            table_storage.push(Some(form.clone()));
        }
        Table {
            axes: unpack(&[AxisType::Case, self.clareth().associated_axis_type()]),
            entries: table_storage,
        }
    }
}

/// Represents a noun in the lexicon.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Noun {
    pub gender: Gender,
    pub clareth: Clareth,
    pub form: NounForm,
}

/// Represents the information relevant to the declension of a noun, such as its stems or its thematic vowel.
#[non_exhaustive]
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum NounForm {
    I(DeclensionI),
    II(DeclensionII),
    III(DeclensionIII),
    IV(DeclensionIV),
    V(DeclensionV),
    VI(DeclensionVI),
    Irregular(Box<NounTable<Phrase>>),
    /// A compound noun, both of whose components are declined.
    Compound(Box<[NounForm; 2]>),
    /// A compound noun in which the first word is not declined.
    Left(Phrase, Box<NounForm>),
    /// A compound noun in which the second word is not declined.
    Right(Box<NounForm>, Phrase),
}

impl NounForm {
    pub fn decline_with_headword(
        &self,
        clareth: Clareth,
    ) -> Result<(NounTable<Phrase>, Headword), InflectionError> {
        use NounForm::*;
        Ok(match self {
            I(decl) => decl.decline_with_headword(clareth)?,
            II(decl) => decl.decline_with_headword(clareth)?,
            III(decl) => decl.decline_with_headword(clareth)?,
            IV(decl) => decl.decline_with_headword(clareth)?,
            V(decl) => decl.decline_with_headword(clareth)?,
            VI(decl) => decl.decline_with_headword(clareth)?,
            Irregular(forms) => (
                forms.deref().clone(),
                Headword::Enumerated {
                    items: vec![forms
                        .get(NounCategories {
                            case: Case::Nominative,
                            number: NNumber::Direct,
                        })
                        .expect("nominative direct form should always be available")
                        .clone()],
                    exhaustive: false,
                },
            ),
            Compound(components) => {
                let (declined1, hw1) = components[0].decline_with_headword(clareth)?;
                let (declined2, hw2) = components[1].decline_with_headword(clareth)?;
                // Should never fail because the components are being declined with the same clareþ.
                (
                    declined1.zip_with(declined2, |a, b| a.appended(b))?,
                    Headword::Compound(Box::new([hw1, hw2])),
                )
            }
            Left(phrase, form) => {
                let (declined, hw) = form.decline_with_headword(clareth)?;
                (
                    declined.map(|infl| phrase.clone().appended(infl)),
                    hw.prepend(phrase),
                )
            }
            Right(form, phrase) => {
                let (declined, hw) = form.decline_with_headword(clareth)?;
                (
                    declined.map(|infl| infl.appended(phrase.clone())),
                    hw.append(phrase),
                )
            }
        })
    }
    pub fn decline(&self, clareth: Clareth) -> Result<NounTable<Phrase>, InflectionError> {
        Ok(self.decline_with_headword(clareth)?.0)
    }

    /// Gets the part-of-speech tag for the noun paradigm.
    pub fn get_pos(&self, noun: &Noun) -> String {
        match self {
            NounForm::Compound(components) => {
                format!(
                    "n{}{} ({}, {})",
                    noun.gender.abbreviation(),
                    noun.clareth.suffix(),
                    components[0].get_pos_word(),
                    components[1].get_pos_word()
                )
            }
            NounForm::Left(_, pos) => pos.get_pos(noun),
            NounForm::Right(pos, _) => pos.get_pos(noun),
            _ => format!(
                "{}{}{}",
                self.get_pos_word(),
                noun.gender.abbreviation(),
                noun.clareth.suffix()
            ),
        }
    }

    /// Gets the part-of-speech tag for the noun paradigm, excluding any gender marker.
    fn get_pos_word(&self) -> &'static str {
        match self {
            NounForm::I(_) => "nI",
            NounForm::II(_) => "nII",
            NounForm::III(_) => "nIII",
            NounForm::IV(_) => "nIV",
            NounForm::V(_) => "nV",
            NounForm::VI(_) => "nVI",
            &NounForm::Irregular(_) => "n!",
            NounForm::Compound(_) => {
                unimplemented!("get_pos_word on NounForm::Compound not supported")
            }
            NounForm::Left(_, f) => f.get_pos_word(),
            NounForm::Right(f, _) => f.get_pos_word(),
        }
    }
    /// Returns true if this noun form is using a deprecated pre-Caladrius inflection paradigm.
    pub fn is_legacy(&self) -> bool {
        match self {
            NounForm::Compound(b) => b[0].is_legacy() && b[1].is_legacy(),
            NounForm::Left(_, n) => n.is_legacy(),
            NounForm::Right(n, _) => n.is_legacy(),
            _ => false,
        }
    }
}
