use clap::{Args, Parser, Subcommand};
use custom_error::custom_error;
use f9i::analytics::magic_mirror::{subcmd_magic_mirror, subcmd_magic_mirror_fusion};
use f9i::analytics::CorpusPhonologicalStatistics;
use f9i::collate::CollationKey;
use f9i::lexical::{EntryRecord, Inflection, LexicalEntry};
use f9i::morphophonology::fuse::{fuse, fuse_with_eps};
use f9i::output::{bin, html, sqlite, text, xexpr};
use f9i::parse::{parse_bare_line, parse_input_line};
use f9i::table::Table;
use f9i_core::analysis::syllabify;
use f9i_core::assemblage::{AStem, Sylls};
use f9i_core::mgc::{DecoratedWord, FusionConsonant, Phrase, Tokenize};
use perm_vec::{Perm, Permute};
use rusqlite::Connection;
use rustyline::error::ReadlineError;
use rustyline::highlight::Highlighter;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::history::DefaultHistory;
use rustyline::{Config, Context, Editor};
use rustyline_derive::{Completer, Helper, Validator};
use smallvec::smallvec;
use smallvec::SmallVec;
use std::borrow::Cow;
use std::collections::HashMap;
use std::error::Error;
use std::fs::{self, File};
use std::io::{self, stdout, BufRead, BufReader, BufWriter, Read, Result as IOResult, Write};
use std::process::exit;
use std::time::Instant;

custom_error! {InputError
    MissingExLine = "extra line is missing",
    ParseFailed { message: String } = "parse failed: {message}"
}

#[derive(Args)]
struct GlobalOptions {
    /// Run expensive checks
    ///
    /// For developers of f9i. Currently consists of:
    ///
    /// * checking ParticipleSpecies::participle_principal_parts against ParticipleSpecies::conjugate_for
    #[clap(long = "paranoid")]
    pub paranoid: bool,
}

#[derive(Args)]
struct FileArgs {
    /// The input .f9t file
    input_f9t: String,
    /// Binary entry file output
    ///
    /// If specified, a binary entry (.f9i) file is written here.
    #[clap(long = "output-f9i")]
    output_f9i: Option<String>,
    /// HTML output
    ///
    /// If specified, a file containing a rendered HTML fragment is written here.
    ///
    /// The extra text should be in HTML, and the output will follow the conventions of the Ŋarâþ Crîþ v9 website.
    #[clap(long = "output-html")]
    output_html: Option<String>,
    /// X-expr output (DEPRECATED)
    ///
    /// Similar to the HTML output, but works with X-expressions.
    ///
    /// The extra text should be an X-expression (without the quote; e.g. `(b \"hello\")`.
    #[clap(long = "output-xexpr")]
    output_xexpr: Option<String>,
    /// SQLite output
    ///
    /// If specified, an SQLite file for searching will be written here.
    ///
    /// The extra text should be in HTML, and the output will follow the conventions of the Ŋarâþ Crîþ v9 website.
    #[clap(long = "output-sqlite")]
    output_sqlite: Option<String>,
}

#[derive(Args)]
struct AnalyzeCorpusArgs {
    /// Where to output the result file.
    output_path: String,
}

#[derive(Args)]
struct MagicMirrorArgs {
    /// Output the full mirror?
    #[clap(long = "full")]
    full: bool,
    stem: Option<String>,
}

#[derive(Subcommand)]
enum F9iSubCmd {
    /// Process a file.
    ///
    /// To generate an output file, use one of the --output-* options. Otherwise, the program will only validate the input file.
    File(FileArgs),
    /// Inflect words interactively
    Repl,
    /// Analyze a corpus of text
    AnalyzeCorpus(AnalyzeCorpusArgs),
    /// Show a table of all bridges
    #[clap(alias("magjormirror"))]
    MagicMirror(MagicMirrorArgs),
}

#[derive(Parser)]
#[clap(author, version)]
/// The fast Ŋarâþ Crîþ v9 inflector
struct F9iCmd {
    /// Global options.
    #[clap(flatten)]
    options: GlobalOptions,
    #[clap(subcommand)]
    sub_cmd: F9iSubCmd,
}

fn main() {
    match _main() {
        Ok(()) => (),
        Err(e) => {
            println!("error: {e}");
            exit(1);
        }
    }
}

fn _main() -> Result<(), Box<dyn Error>> {
    let cmd = F9iCmd::parse();

    if cmd.options.paranoid {
        eprintln!("Paranoid checks enabled");
    }

    match cmd.sub_cmd {
        F9iSubCmd::File(args) => subcmd_file(&args, &cmd.options),
        F9iSubCmd::Repl => subcmd_repl(&cmd.options),
        F9iSubCmd::AnalyzeCorpus(args) => subcmd_analyze_corpus(&args, &cmd.options),
        F9iSubCmd::MagicMirror(args) => match args.stem {
            None => subcmd_magic_mirror(args.full),
            Some(s) => {
                let parsed = DecoratedWord::parse_assemblage::<AStem>(&s).map_err(|e| {
                    InputError::ParseFailed {
                        message: e.to_string(),
                    }
                })?;
                subcmd_magic_mirror_fusion(args.full, &parsed)
            }
        },
    }
}

fn open_input_file(path: &str) -> IOResult<BufReader<Box<dyn Read>>> {
    if path == "-" {
        Ok(BufReader::new(Box::new(io::stdin())))
    } else {
        Ok(BufReader::new(Box::new(File::open(path)?)))
    }
}

fn open_output_file(path: &str) -> IOResult<BufWriter<Box<dyn Write>>> {
    if path == "-" {
        Ok(BufWriter::new(Box::new(io::stdout())))
    } else {
        Ok(BufWriter::new(Box::new(File::create(path)?)))
    }
}

fn subcmd_file(subcmd: &FileArgs, options: &GlobalOptions) -> Result<(), Box<dyn Error>> {
    let input_path = subcmd.input_f9t.as_str();
    let input = open_input_file(input_path)?;
    let mut input_lines = input.lines();

    let mut entry_records: Vec<EntryRecord> = vec![];
    let mut form_redirects: HashMap<Phrase, SmallVec<[(Inflection, usize); 1]>> = HashMap::new();

    let instant_before_read = Instant::now();

    loop {
        let le_line = match input_lines.next() {
            None => break,
            Some(r) => r?,
        };
        if le_line.is_empty() {
            continue;
        }
        let extra = match input_lines.next() {
            None => return Err(Box::new(InputError::MissingExLine)),
            Some(r) => r?,
        };
        let (le_key, le) = parse_input_line(&le_line).map_err(|e| InputError::ParseFailed {
            message: e.to_string(),
        })?;
        if options.paranoid {
            le.run_paranoid_checks();
        }
        if le.is_legacy() {
            eprintln!("Warning: found legacy entry: {le_line}");
        }
        let pos = le.get_pos();
        let mut tables: Vec<Table<Option<Phrase>>> = vec![];
        let headword = le.get_headword_and_inflections(
            |table| {
                tables.push(table);
            },
            |(inflection, phrase)| {
                form_redirects
                    .entry(phrase)
                    .or_insert_with(|| smallvec![])
                    .push((inflection, entry_records.len()));
            },
        )?;
        entry_records.push(EntryRecord {
            label: le_key.to_string(),
            headword,
            pos,
            tables,
            extra,
            input: le_line,
        });
    }

    let collation_keys = entry_records
        .iter()
        .map(|e| CollationKey::of_headword(&e.headword))
        .collect::<Vec<_>>();

    let perm = Perm::argsort(&collation_keys);

    entry_records = entry_records.permuted_by(&perm);

    for (_, v) in form_redirects.iter_mut() {
        for idx in v {
            idx.1 = perm.permute_index(idx.1);
        }
    }

    let instant_before_write = Instant::now();
    eprintln!(
        "Processed {} records in {}s",
        entry_records.len(),
        instant_before_write
            .duration_since(instant_before_read)
            .as_secs_f64()
    );

    let mut num_output_fmts = 0;

    if let Some(output_path) = &subcmd.output_f9i {
        // Stdout not supported yet (this needs a seekable file handle)
        let mut output = BufWriter::new(File::create(output_path)?);
        bin::write_entry_records(&mut output, &entry_records)?;
        num_output_fmts += 1;
    }

    if let Some(output_path) = &subcmd.output_html {
        let mut output = open_output_file(output_path)?;
        html::write_entry_records(&mut output, &entry_records)?;
        num_output_fmts += 1;
    }

    if let Some(output_path) = &subcmd.output_xexpr {
        eprintln!("WARN: xexpr output is deprecated and might be removed in the future");
        let mut output = open_output_file(output_path)?;
        xexpr::write_entry_records(&mut output, &entry_records)?;
        num_output_fmts += 1;
    }

    if let Some(output_path) = &subcmd.output_sqlite {
        match fs::remove_file(output_path) {
            Ok(_) => (),
            Err(e) => {
                if e.kind() != io::ErrorKind::AlreadyExists {
                    return Err(Box::new(e));
                }
            }
        }
        let mut output = Connection::open(output_path)?;
        sqlite::write_entry_records(&mut output, &entry_records, &form_redirects)?;
        num_output_fmts += 1;
    }

    let instant_after_write = Instant::now();
    eprintln!(
        "Converted to {} output format{} in {}s",
        num_output_fmts,
        if num_output_fmts == 1 { "" } else { "s" },
        instant_after_write
            .duration_since(instant_before_write)
            .as_secs_f64()
    );

    Ok(())
}

fn subcmd_repl(options: &GlobalOptions) -> Result<(), Box<dyn Error>> {
    let mut rl = Editor::<Hl, DefaultHistory>::with_config(
        Config::builder().auto_add_history(true).build(),
    )?;
    rl.set_helper(Some(Hl(HistoryHinter {})));
    loop {
        let readline = rl.readline("f9i> ");
        print!("\x1B[0m");
        eprint!("\x1B[0m");
        match readline {
            Ok(line) => {
                let line = line.trim(); // compatibility with piped input
                if line.is_empty() {
                    continue;
                }
                if let Some(l) = line.strip_prefix('$') {
                    let mut l = l.split_ascii_whitespace();
                    let (l1, l2) = match (l.next(), l.next()) {
                        (Some(l1), Some(l2)) => (l1, l2),
                        _ => {
                            eprintln!("$ requires 2 entries");
                            continue;
                        }
                    };
                    let stem = match DecoratedWord::parse_assemblage::<AStem>(l1) {
                        Ok(s) => s,
                        Err(e) => {
                            eprintln!("Parse error in stem: {e}");
                            continue;
                        }
                    };
                    let cs: &[_] = match l2 {
                        "." => &[None],
                        "t" => &[Some(FusionConsonant::T)],
                        "n" => &[Some(FusionConsonant::N)],
                        "þ" => &[Some(FusionConsonant::Th)],
                        "*" => &[
                            None,
                            Some(FusionConsonant::T),
                            Some(FusionConsonant::N),
                            Some(FusionConsonant::Th),
                        ],
                        _ => {
                            eprintln!("Invalid fusion consonant: {l2}");
                            continue;
                        }
                    };
                    for c in cs {
                        match c {
                            None => {
                                println!(
                                    "{} × ε = {}",
                                    stem.tokens_to_string(),
                                    fuse_with_eps(stem.clone()).tokens_to_string()
                                );
                            }
                            Some(c) => {
                                println!(
                                    "{} × {} = {}",
                                    stem.tokens_to_string(),
                                    c,
                                    fuse(stem.clone(), *c).tokens_to_string()
                                );
                            }
                        }
                    }
                    continue;
                }
                if let Some(l) = line.strip_prefix('~') {
                    let l = l.split_ascii_whitespace();
                    let mut res = Sylls::default();
                    for word in l {
                        let parsed = match DecoratedWord::parse_assemblage::<Sylls>(word) {
                            Ok(o) => o,
                            Err(e) => {
                                eprintln!("Could not parse `{word}`: {e}");
                                continue;
                            }
                        };
                        res = res + &parsed;
                    }
                    eprintln!("{}", res.tokens_to_string());
                    continue;
                }
                if let Some(l) = line.strip_prefix('+') {
                    let l = l.trim();
                    let l = match DecoratedWord::parse(l) {
                        Ok(l) => l,
                        Err(e) => {
                            eprintln!("Parse error: {e}");
                            continue;
                        }
                    };
                    eprintln!("{}", l.letter_sum());
                    continue;
                }
                let instant_before = Instant::now();
                let le = match parse_bare_line(line) {
                    Ok(le) => le,
                    Err(e) => {
                        eprintln!("Parse error: {e}");
                        continue;
                    }
                };
                if options.paranoid {
                    le.run_paranoid_checks();
                }
                if le.is_legacy() {
                    eprintln!("Warning: this is a legacy entry");
                }
                let pos = le.get_pos();
                let mut tables: Vec<Table<Option<Phrase>>> = vec![];
                let headword = match le.get_headword_and_inflections(
                    |table| {
                        tables.push(table);
                    },
                    |(_inflection, _phrase)| {},
                ) {
                    Ok(headword) => headword,
                    Err(e) => {
                        eprintln!("Inflection error: {e}");
                        continue;
                    }
                };
                let hw_s = headword.to_string();
                println!("{}: {}\n", &hw_s, pos);
                for table in tables {
                    text::write_table(&mut stdout(), &hw_s, &table)?;
                }
                let instant_after = Instant::now();
                eprintln!(
                    "Elapsed time: {}s",
                    instant_after.duration_since(instant_before).as_secs_f64()
                );
            }
            Err(ReadlineError::Eof) => break,
            Err(e) => eprintln!("{e}"),
        }
    }
    Ok(())
}

#[derive(Completer, Helper, Validator)]
struct Hl(HistoryHinter);
impl Highlighter for Hl {
    fn highlight<'l>(&self, line: &'l str, _pos: usize) -> Cow<'l, str> {
        Cow::Owned(format!("\x1B[92m{line}"))
    }

    fn highlight_prompt<'b, 's: 'b, 'p: 'b>(
        &'s self,
        prompt: &'p str,
        default: bool,
    ) -> Cow<'b, str> {
        if default {
            Cow::Owned(format!("\x1B[96;1m{prompt}\x1B[m"))
        } else {
            Cow::Borrowed(prompt)
        }
    }

    fn highlight_hint<'h>(&self, hint: &'h str) -> Cow<'h, str> {
        Cow::Owned(format!("\x1B[2m{hint}\x1B[m"))
    }
}

impl Hinter for Hl {
    type Hint = String;

    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.0.hint(line, pos, ctx)
    }
}

fn subcmd_analyze_corpus(
    args: &AnalyzeCorpusArgs,
    _options: &GlobalOptions,
) -> Result<(), Box<dyn Error>> {
    let mut stdin = BufReader::new(io::stdin());
    let mut input_buf = String::new();
    let mut stats = CorpusPhonologicalStatistics::new();
    loop {
        match stdin.read_line(&mut input_buf) {
            Ok(0) => break,
            Ok(_) => (),
            Err(e) => return Err(Box::new(e)),
        };
        let input = input_buf.trim();
        if input.is_empty() {
            continue;
        }
        let phrase = Phrase::parse(input).map_err(|e| e.to_string())?;
        phrase.visit_words(&mut |word| {
            let syllabified = match syllabify(word) {
                Ok(s) => s,
                Err(e) => {
                    eprintln!(
                        "Warning: could not syllabify {word}: {e} (perhaps it’s a function word with marginal phonotactics?)"
                    );
                    return Ok(());
                }
            };
            stats.update(&syllabified);
            Ok::<(), Box<dyn Error>>(())
        })?;
        input_buf.clear();
    }
    let mut fh = BufWriter::new(File::create(&args.output_path)?);
    stats.output_html(&mut fh)?;
    Ok(())
}
