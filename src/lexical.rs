//! Lexical entries under a common banner.

use std::fmt::Display;

use enum_dispatch::enum_dispatch;
use f9i_core::category::{
    Attachment, AxisType, MotionType, Nominalized, PersonObj, Polarity, SubjectType, Transfinite,
    VNumber,
};
use f9i_core::mgc::DecoratedWord;
use f9i_core::{
    category::{unpack, Aspect, Case, NNumber, ObjectType, Tense},
    mgc::Phrase,
};

use crate::nd_array_type;
use crate::relational::{
    FiniteRelationalCategoriesOpt, NominalRelationalCategoriesOpt, Rel, RelA, RelFull,
    RelationalForm,
};
use crate::table::ToTable;
use crate::verb::finite::table_finite_forms;
use crate::{
    error::InflectionError,
    headword::Headword,
    noun::{Noun, NounCategories},
    relational::RelationalCategoriesOpt,
    table::Table,
    verb::{
        finite::{FiniteVerbCategories, TransfiniteVerbCategories},
        participle::VerbParticipleCategoriesOpt,
        Verb,
    },
};

/// A generic inflectional category, used for naming the inflectional categories of a word.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Inflection {
    Uninflectable,
    Noun(NounCategories),
    VerbFinite(FiniteVerbCategories),
    VerbTransfinite(TransfiniteVerbCategories),
    VerbParticiple(VerbParticipleCategoriesOpt),
    VerbNominalized(Nominalized),
    VerbInfinitive,
    RelationalAttributive(RelationalCategoriesOpt),
    RelationalFinite(FiniteRelationalCategoriesOpt),
    RelationalNominal(NominalRelationalCategoriesOpt),
}

impl Inflection {
    /// Returns true if this category corresponds to the lemma form.
    pub fn is_lemma(&self) -> bool {
        match *self {
            Inflection::Uninflectable => true,
            Inflection::Noun(NounCategories {
                case: Case::Nominative,
                number: NNumber::Direct,
            }) => true,
            Inflection::RelationalAttributive(RelationalCategoriesOpt {
                attachment: _,
                motion: None | Some(MotionType::Static),
                object_type: ObjectType::None,
                ancillary_object_type: None | Some(ObjectType::G3),
            }) => true,
            Inflection::VerbInfinitive => true,
            _ => false,
        }
    }

    /// Serializes this object into bytes.
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut f = Vec::new();
        self.push_to_bytes(&mut f);
        f
    }

    /// Serializes this object into bytes.
    pub fn push_to_bytes(&self, f: &mut Vec<u8>) {
        match self {
            Inflection::Uninflectable => f.push(0),
            Inflection::Noun(NounCategories { case, number }) => {
                f.push(1);
                f.push(*case as u8);
                f.push(*number as u8);
            }
            Inflection::VerbFinite(vc) => {
                f.push(2);
                to_bytes_vc(f, vc);
            }
            Inflection::VerbParticiple(VerbParticipleCategoriesOpt {
                rcase,
                hcase,
                hgender,
                hnumber,
            }) => {
                f.push(3);
                f.push(*rcase as u8);
                f.push(*hcase as u8);
                f.push(optord(hgender.map(|a| a as u8)));
                f.push(optord(hnumber.map(|a| a as u8)));
            }
            Inflection::VerbInfinitive => f.push(4),
            Inflection::RelationalAttributive(RelationalCategoriesOpt {
                attachment,
                motion,
                object_type,
                ancillary_object_type: ancilliary_object_type,
            }) => {
                f.push(5);
                f.push(*attachment as u8);
                f.push(optord(motion.map(|a| a as u8)));
                f.push(*object_type as u8);
                f.push(optord(ancilliary_object_type.map(|a| a as u8)));
            }
            Inflection::RelationalFinite(FiniteRelationalCategoriesOpt {
                vc,
                motion,
                polarity,
            }) => {
                f.push(6);
                to_bytes_vc(f, vc);
                f.push(optord(motion.map(|a| a as u8)));
                f.push(*polarity as u8);
            }
            Inflection::RelationalNominal(NominalRelationalCategoriesOpt {
                nc: NounCategories { case, number },
                motion,
            }) => {
                f.push(7);
                f.push(*case as u8);
                f.push(*number as u8);
                f.push(optord(motion.map(|a| a as u8)));
            }
            Inflection::VerbTransfinite(TransfiniteVerbCategories {
                transfinite,
                obj_marking,
            }) => {
                f.push(8);
                f.push(*transfinite as u8);
                f.push(*obj_marking as u8);
            }
            Inflection::VerbNominalized(case_mood) => {
                f.push(9);
                f.push(*case_mood as u8);
            }
        }
    }
}

fn to_bytes_vc(f: &mut Vec<u8>, vc: &FiniteVerbCategories) {
    let FiniteVerbCategories {
        subj_marking,
        obj_marking,
        tense,
        aspect,
    } = vc;
    f.push(*subj_marking as u8);
    f.push(*obj_marking as u8);
    f.push(*tense as u8);
    f.push(*aspect as u8);
}

fn optord(t: Option<u8>) -> u8 {
    t.map(|a| a + 1).unwrap_or(0)
}

impl Display for Inflection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Inflection::Uninflectable => f.write_str("lemma"),
            Inflection::Noun(NounCategories { case, number }) => {
                write!(f, "{} {}", case.name(), number.name())
            }
            Inflection::VerbFinite(FiniteVerbCategories {
                subj_marking,
                obj_marking,
                tense,
                aspect,
            }) => {
                write!(f, "{}", subj_marking.name())?;
                if *obj_marking != ObjectType::None {
                    write!(f, " to {}", obj_marking.name())?;
                }
                write!(f, " {} {}", aspect.name(), tense.name())?;
                Ok(())
            }
            Inflection::VerbParticiple(VerbParticipleCategoriesOpt {
                rcase,
                hcase,
                hgender,
                hnumber,
            }) => {
                write!(f, "{} / {}", rcase.name(), hcase.name())?;
                if let Some(hgender) = hgender {
                    write!(f, " {}", hgender.name())?;
                }
                if let Some(hnumber) = hnumber {
                    write!(f, " {}", hnumber.name())?;
                }
                f.write_str(" participle")?;
                Ok(())
            }
            Inflection::VerbInfinitive => f.write_str("infinitive"),
            Inflection::RelationalAttributive(RelationalCategoriesOpt {
                attachment,
                motion,
                object_type,
                ancillary_object_type: ancilliary_object_type,
            }) => {
                write!(f, "{}", attachment.name())?;
                if let Some(motion) = motion {
                    write!(f, " {}", motion.name())?;
                }
                if *object_type != ObjectType::None {
                    write!(f, " {}", object_type.name())?;
                }
                if let Some(ancilliary_object_type) = ancilliary_object_type {
                    write!(f, " with ancilliary {}", ancilliary_object_type.name())?;
                }
                Ok(())
            }
            Inflection::RelationalFinite(FiniteRelationalCategoriesOpt {
                vc,
                motion,
                polarity,
            }) => {
                if let Some(motion) = motion {
                    write!(f, "{} ", motion.name())?;
                }
                Inflection::VerbFinite(*vc).fmt(f)?;
                write!(f, " {}", polarity.name())?;
                f.write_str(" finite")?;
                Ok(())
            }
            Inflection::RelationalNominal(NominalRelationalCategoriesOpt {
                nc: NounCategories { case, number },
                motion,
            }) => {
                write!(f, "{} {}", case.name(), number.name())?;
                if let Some(motion) = motion {
                    write!(f, " {}", motion.name())?;
                }
                f.write_str(" nominal")
            }
            Inflection::VerbTransfinite(TransfiniteVerbCategories {
                transfinite,
                obj_marking,
            }) => {
                write!(f, "{}", transfinite.name())?;
                if *obj_marking != ObjectType::None {
                    write!(f, " to {}", obj_marking.name())?;
                }
                Ok(())
            }
            Inflection::VerbNominalized(case_mood) => {
                write!(f, "{} infinitive", case_mood.name())
            }
        }
    }
}

/// A trait for a *lexical entry*.
#[enum_dispatch(LexicalEntryEnum)]
pub trait LexicalEntry {
    /// Gets the headword for this lexical entry along with
    /// the tables and forms associated with it.
    ///
    /// In particular, this method:
    /// * Calls `table_callback` on each table that should be processed.
    /// * Calls `form_callback` on each pair `(inflection, word)` that should be processed, where `word` is a form of the lexical entry with categories described by `inflection`.
    ///
    /// It is legal to pass some forms into `form_callback` while omitting them from any table passed into `table_callback`. This could be done, for instance, if including such forms would make the tables too large to display. This is the case with listing finite forms of verbs, where forms with object suffixes are not shown in the tables.
    ///
    /// This method gets both the inflections and the headword
    /// jointly because the headword often takes forms from the
    /// inflection tables.
    fn get_headword_and_inflections<TC, FC>(
        &self,
        table_callback: TC,
        form_callback: FC,
    ) -> Result<Headword, InflectionError>
    where
        TC: FnMut(Table<Option<Phrase>>),
        FC: FnMut((Inflection, Phrase));

    /// Describes the part of speech of this entry as a string.
    fn get_pos(&self) -> String;

    /// Runs possibly expensive checks for consistency.
    ///
    /// These checks can optionally be turned on when f9i is run.
    fn run_paranoid_checks(&self) {}

    /// Returns true if this lexical entry is using a deprecated inflection paradigm.
    fn is_legacy(&self) -> bool {
        false
    }
}

/// An enum type that can hold any type defined by f9i that implements [LexicalEntry].
#[enum_dispatch]
pub enum LexicalEntryEnum {
    Noun,
    Verb,
    Uninflectable,
    RelationalForm,
}

impl LexicalEntry for Noun {
    fn get_headword_and_inflections<TC, FC>(
        &self,
        mut table_callback: TC,
        mut form_callback: FC,
    ) -> Result<Headword, InflectionError>
    where
        TC: FnMut(Table<Option<Phrase>>),
        FC: FnMut((Inflection, Phrase)),
    {
        let (declensions, headword) = self.form.decline_with_headword(self.clareth)?;

        table_callback(declensions.to_table());
        for (nc, form) in declensions {
            form_callback((Inflection::Noun(nc), form));
        }

        Ok(headword)
    }

    fn get_pos(&self) -> String {
        self.form.get_pos(self)
    }

    fn is_legacy(&self) -> bool {
        self.form.is_legacy()
    }
}

impl LexicalEntry for Verb {
    fn get_headword_and_inflections<TC, FC>(
        &self,
        mut table_callback: TC,
        mut form_callback: FC,
    ) -> Result<Headword, InflectionError>
    where
        TC: FnMut(Table<Option<Phrase>>),
        FC: FnMut((Inflection, Phrase)),
    {
        form_callback((Inflection::VerbInfinitive, self.form.get_infinitive()));

        let table = self.form.conjugate()?;
        let headword = self.form.headword(&table);

        table_callback(table.finite_forms.to_table());

        // List finite forms – note that this is currently pretty ugly.
        // Written this way to avoid an extra clone() of the elements.
        for (a, tos) in IntoIterator::into_iter(*table.finite_forms).enumerate() {
            for (t, os) in IntoIterator::into_iter(tos).enumerate() {
                for (o, s_) in IntoIterator::into_iter(os).enumerate() {
                    for (s, e) in IntoIterator::into_iter(s_).enumerate() {
                        let vc = FiniteVerbCategories {
                            subj_marking: SubjectType::from_ordinal(s as u8),
                            obj_marking: ObjectType::from_ordinal(o as u8),
                            tense: Tense::from_ordinal(t as u8),
                            aspect: Aspect::from_ordinal(a as u8),
                        };
                        form_callback((Inflection::VerbFinite(vc), e));
                    }
                }
            }
        }

        // List transfinite forms
        if let Some(trf) = table.transfinite_forms {
            table_callback(trf.to_table());

            for (f, row) in IntoIterator::into_iter(*trf).enumerate() {
                for (o, elem) in IntoIterator::into_iter(row).enumerate() {
                    let vc = TransfiniteVerbCategories {
                        transfinite: Transfinite::from_ordinal(f as u8),
                        obj_marking: ObjectType::from_ordinal(o as u8),
                    };

                    form_callback((Inflection::VerbTransfinite(vc), elem));
                }
            }
        }

        // List participles
        table
            .participle_forms
            .for_each(|(infl, elem): (_, &Phrase)| {
                form_callback((Inflection::VerbParticiple(infl), elem.clone()));
            });
        table_callback(table.participle_forms.to_table());

        // List nominalized forms
        table_callback(table.nominalized_forms.to_table());
        for (i, e) in IntoIterator::into_iter(*table.nominalized_forms).enumerate() {
            form_callback((
                Inflection::VerbNominalized(Nominalized::from_ordinal(i as u8)),
                e,
            ));
        }

        Ok(headword)
    }
    fn get_pos(&self) -> String {
        format!(
            "v{}{}:{}",
            self.transitivity.to_letter(),
            self.form.material.abbr(),
            self.form.species,
        )
    }
}

impl RelationalForm {
    fn list_forms<FC>(
        r: Rel<DecoratedWord>,
        motion: Option<MotionType>,
        form_callback: &mut FC,
    ) -> Result<(), InflectionError>
    where
        FC: FnMut((Inflection, Phrase)),
    {
        let Rel {
            attributive,
            finite,
            nominal,
        } = r;
        match attributive {
            RelA::Divalent(forms) => list_attributive_forms(form_callback, motion, None, forms),
            RelA::Trivalent(forms) => {
                for (ao, row) in IntoIterator::into_iter(*forms).enumerate() {
                    list_attributive_forms(
                        form_callback,
                        motion,
                        Some(ObjectType::from_ordinal(ao as u8)),
                        row,
                    );
                }
            }
        }
        for (p, atos) in IntoIterator::into_iter(finite).enumerate() {
            for (a, tos) in IntoIterator::into_iter(atos).enumerate() {
                for (t, os) in IntoIterator::into_iter(tos).enumerate() {
                    for (o, s_) in IntoIterator::into_iter(os).enumerate() {
                        for (s, e) in IntoIterator::into_iter(s_).enumerate() {
                            let vc = FiniteVerbCategories {
                                subj_marking: SubjectType::from_ordinal(s as u8),
                                obj_marking: ObjectType::from_ordinal(o as u8),
                                tense: Tense::from_ordinal(t as u8),
                                aspect: Aspect::from_ordinal(a as u8),
                            };
                            form_callback((
                                Inflection::RelationalFinite(FiniteRelationalCategoriesOpt {
                                    vc,
                                    motion,
                                    polarity: Polarity::from_ordinal(p as u8),
                                }),
                                Phrase::of(e),
                            ))
                        }
                    }
                }
            }
        }
        for (nc, form) in nominal {
            form_callback((
                Inflection::RelationalNominal(NominalRelationalCategoriesOpt { nc, motion }),
                Phrase::of(form),
            ));
        }
        Ok(())
    }

    fn add_table_data(
        r: &Rel<DecoratedWord>,
        td_attr: &mut Vec<Option<Phrase>>,
        td_fin: &mut Vec<Option<Phrase>>,
        td_nom: &mut Vec<Option<Phrase>>,
    ) -> Result<(), InflectionError>
where {
        let Rel {
            attributive,
            finite,
            nominal,
        } = r;
        match attributive {
            RelA::Divalent(forms) => table_attributive_forms(forms, td_attr),
            // For brevity, we only list forms where AOT = 3GC
            RelA::Trivalent(forms) => {
                table_attributive_forms(&forms[ObjectType::G3 as usize], td_attr)
            }
        }
        // finite: polarity aspect tense person number
        //         2        2      2     4      4
        for atos in finite {
            table_finite_forms(atos, td_fin, |x| Phrase::of(x.clone()));
        }
        for (_, form) in nominal {
            td_nom.push(Some(Phrase::of(form.clone())));
        }
        Ok(())
    }
}

fn list_attributive_forms<FC>(
    form_callback: &mut FC,
    motion: Option<MotionType>,
    ancilliary_object_type: Option<ObjectType>,
    attr: nd_array_type!(DecoratedWord, Attachment::COUNT, ObjectType::COUNT),
) where
    FC: FnMut((Inflection, Phrase)),
{
    for (a, row) in IntoIterator::into_iter(attr).enumerate() {
        for (o, e) in IntoIterator::into_iter(row).enumerate() {
            form_callback((
                Inflection::RelationalAttributive(RelationalCategoriesOpt {
                    attachment: Attachment::from_ordinal(a as u8),
                    motion,
                    object_type: ObjectType::from_ordinal(o as u8),
                    ancillary_object_type: ancilliary_object_type,
                }),
                Phrase::of(e),
            ))
        }
    }
}

fn table_attributive_forms(
    attr: &nd_array_type!(DecoratedWord, Attachment::COUNT, ObjectType::COUNT),
    td_attr: &mut Vec<Option<Phrase>>,
) {
    // attr: attachment personobj number
    //       2          9         4
    for row in IntoIterator::into_iter(attr) {
        let mut arr = vec![None; PersonObj::COUNT * VNumber::COUNT];
        for (obj_type_ordinal, e) in IntoIterator::into_iter(row).enumerate() {
            let idx = ObjectType::from_ordinal(obj_type_ordinal as u8).to_table_index();
            arr[idx] = Some(Phrase::of(e.clone()));
        }
        td_attr.append(&mut arr);
    }
}

impl LexicalEntry for RelationalForm {
    fn get_headword_and_inflections<TC, FC>(
        &self,
        mut table_callback: TC,
        mut form_callback: FC,
    ) -> Result<Headword, InflectionError>
    where
        TC: FnMut(Table<Option<Phrase>>),
        FC: FnMut((Inflection, Phrase)),
    {
        let inflections = self.inflect()?;
        // tables – for finite forms, include only object suffix-less forms
        match &inflections {
            RelFull::Basic(r) => {
                let mut td_attr: Vec<Option<Phrase>> = Vec::new();
                let mut td_fin: Vec<Option<Phrase>> = Vec::new();
                let mut td_nom: Vec<Option<Phrase>> = Vec::new();
                RelationalForm::add_table_data(r, &mut td_attr, &mut td_fin, &mut td_nom)?;
                table_callback(Table {
                    axes: unpack(&[AxisType::Attachment, AxisType::PersonObj, AxisType::VNumber]),
                    entries: td_attr,
                });
                table_callback(Table {
                    axes: unpack(&[
                        AxisType::Polarity,
                        AxisType::Aspect,
                        AxisType::Tense,
                        AxisType::SubjPerson,
                        AxisType::SubjNumber,
                    ]),
                    entries: td_fin,
                });
                table_callback(Table {
                    axes: unpack(&[AxisType::Case, AxisType::NumberM]),
                    entries: td_nom,
                });
            }
            RelFull::Motion(rr) => {
                let mut td_attr: Vec<Option<Phrase>> = Vec::new();
                let mut td_fin: Vec<Option<Phrase>> = Vec::new();
                let mut td_nom: Vec<Option<Phrase>> = Vec::new();
                for r in rr.iter() {
                    RelationalForm::add_table_data(r, &mut td_attr, &mut td_fin, &mut td_nom)?;
                }
                table_callback(Table {
                    axes: unpack(&[
                        AxisType::MotionType,
                        AxisType::Attachment,
                        AxisType::PersonObj,
                        AxisType::VNumber,
                    ]),
                    entries: td_attr,
                });
                table_callback(Table {
                    axes: unpack(&[
                        AxisType::MotionType,
                        AxisType::Polarity,
                        AxisType::Aspect,
                        AxisType::Tense,
                        AxisType::SubjPerson,
                        AxisType::SubjNumber,
                    ]),
                    entries: td_fin,
                });
                table_callback(Table {
                    axes: unpack(&[AxisType::MotionType, AxisType::Case, AxisType::NumberM]),
                    entries: td_nom,
                });
            }
        }
        // forms
        match inflections {
            RelFull::Basic(r) => RelationalForm::list_forms(*r, None, &mut form_callback)?,
            RelFull::Motion(r) => {
                for (m, e) in IntoIterator::into_iter(*r).enumerate() {
                    RelationalForm::list_forms(
                        e,
                        Some(MotionType::from_ordinal(m as u8)),
                        &mut form_callback,
                    )?
                }
            }
        }

        Ok(self.get_headword_v())
    }

    fn get_pos(&self) -> String {
        match self.ancilliary_object_case {
            Some(c) => {
                format!(
                    "r{}{}+{}",
                    self.nominalization_type.abbreviation(),
                    self.object_type.abbreviation(),
                    c.abbreviation(),
                )
            }
            None => {
                format!(
                    "r{}{}",
                    self.nominalization_type.abbreviation(),
                    self.object_type.abbreviation()
                )
            }
        }
    }
}

/// Represents a lexical entry with only one form.
pub struct Uninflectable {
    /// The form of the entry.
    pub form: Phrase,
    /// The abbreviation that should be shown for the part of speech.
    pub pos: String,
}

impl LexicalEntry for Uninflectable {
    fn get_headword_and_inflections<TC, FC>(
        &self,
        // Do not generate tables for an uninflectable word
        _table_callback: TC,
        mut form_callback: FC,
    ) -> Result<Headword, InflectionError>
    where
        TC: FnMut(Table<Option<Phrase>>),
        FC: FnMut((Inflection, Phrase)),
    {
        form_callback((Inflection::Uninflectable, self.form.clone()));
        Ok(Headword::Enumerated {
            items: vec![self.form.clone()],
            exhaustive: true,
        })
    }

    fn get_pos(&self) -> String {
        self.pos.clone()
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct EntryRecord {
    pub label: String,
    pub headword: Headword,
    pub pos: String,
    pub tables: Vec<Table<Option<Phrase>>>,
    pub extra: String,
    pub input: String,
}
