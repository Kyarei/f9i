use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DeriveInput, Fields, Index, Meta, NestedMeta};

#[proc_macro_derive(Gridlock)]
pub fn derive_gridlock(enum_def: TokenStream) -> TokenStream {
    let enum_def = syn::parse_macro_input!(enum_def as DeriveInput);
    let enum_name = enum_def.ident;
    let mut repr_type = None;
    for attr in enum_def.attrs {
        if attr.path.is_ident("repr") {
            let meta = attr
                .parse_meta()
                .expect("Unexpected content in repr attribute!");
            match meta {
                Meta::List(ls) => {
                    if ls.nested.len() != 1 {
                        panic!("Repr attribute must contain exactly one element");
                    }
                    // won’t panic because we checked that length > 0
                    match ls.nested.first().unwrap() {
                        NestedMeta::Meta(Meta::Path(p)) => {
                            repr_type = Some(p.clone());
                            break;
                        }
                        _ => panic!("Repr attribute must contain a type name"),
                    }
                }
                _ => panic!("Unexpected content in repr attribute!"),
            }
        }
    }
    match enum_def.data {
        Data::Enum(data) => {
            for var in &data.variants {
                if !matches!(var.fields, Fields::Unit) {
                    panic!(
                        "All variants must be fieldless, but {} has fields",
                        var.ident
                    );
                }
                if var.discriminant.is_some() {
                    panic!(
                        "Explicit discriminants currently not supported but found for {}",
                        var.ident
                    );
                }
            }
            let elem_count = data.variants.len();
            let ids: Vec<_> = data.variants.iter().map(|v| &v.ident).collect();
            let ty = match repr_type {
                Some(s) => quote! { #s },
                None => quote! { usize },
            };
            let iota = (0..(data.variants.len())).map(Index::from);
            let err_msg = enum_name.to_string() + "::from_ordinal: {} is not a valid ordinal";
            (quote! {
                impl #enum_name {
                    /// The number of variants in this enum.
                    pub const COUNT: usize = #elem_count;
                    /// An array containing the variants of this enum.
                    pub const VALUES: [Self; Self::COUNT] = [
                        #(Self::#ids,)*
                    ];
                    /// Get the variant given an ordinal.
                    ///
                    /// # Safety
                    ///
                    /// The ordinal must correspond to a valid variant.
                    #[inline]
                    pub unsafe fn from_ordinal_unchecked(i: #ty) -> Self {
                        ::std::mem::transmute(i)
                    }
                    /// Get the variant given an ordinal.
                    ///
                    /// Returns `None` if no such variant is found.
                    pub const fn try_from_ordinal(i: #ty) -> Option<Self> {
                        match i {
                            #(#iota => Some(Self::#ids),)*
                            _ => None,
                        }
                    }
                    /// Get the variant given an ordinal.
                    ///
                    /// # Panics
                    ///
                    /// Panics if the ordinal does not correspond to a valid variant.
                    #[inline]
                    pub fn from_ordinal(i: #ty) -> Self {
                        match Self::try_from_ordinal(i) {
                            Some(s) => s,
                            None => panic!(#err_msg, i),
                        }
                    }
                    /// Gets the ordinal of a variant.
                    pub const fn to_ordinal(self) -> #ty {
                        self as #ty
                    }
                }
            })
            .into()
        }
        _ => (quote! { compile_error!("Gridlock may be applied only on an enum") }).into(),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
